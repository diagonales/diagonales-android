package eco.darmas.base.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;

import eco.darmas.App;
import eco.darmas.R;
import eco.darmas.data.DataManager;

public abstract class BaseFragment<Presenter extends BaseFragmentPresenter, View extends BaseFragmentView, Model extends BaseFragmentModel> extends DialogFragment {

    private Presenter mPresenter;
    private Model mModel;
    private View mView;

    protected abstract Model createModel(DataManager dataManager, Bundle savedInstanceState);

    protected abstract Presenter createPresenter(final Model model, final View view);

    protected abstract View createView(final Context context);

    @CallSuper
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = createModel(App.getInstance().getDataManager(), savedInstanceState);
        mView = createView(getContext());
        mPresenter = createPresenter(mModel, mView);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlide;
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public android.view.View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return mView.inflate(inflater, container);
    }

    @CallSuper
    @Override
    public void onViewCreated(@NonNull android.view.View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.onViewCreated();
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        mModel.saveInstanceState(outState);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestroyView();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}