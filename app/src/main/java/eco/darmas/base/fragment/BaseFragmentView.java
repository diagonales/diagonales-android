package eco.darmas.base.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Unbinder;

public abstract class BaseFragmentView extends View {

    protected BaseFragmentView(Context context) {
        super(context);
    }

    protected abstract View inflate(final LayoutInflater inflater, ViewGroup container);

    protected abstract void initialize(BaseFragmentPresenter presenter);

    protected abstract Unbinder getUnBinder();
}