package eco.darmas.base.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import eco.darmas.pojos.User;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragmentPresenter<Model extends BaseFragmentModel, View extends BaseFragmentView> {

    private final Model mModel;
    private final View mView;
    private final CompositeDisposable disposables;

    protected BaseFragmentPresenter(final Model mModel, final View mView) {
        this.mModel = mModel;
        this.mView = mView;
        this.disposables = new CompositeDisposable();
    }

    protected void onViewCreated() {
        mView.initialize(this);
    }

    protected void onResume() {
    }

    protected void onPause() {
    }

    @CallSuper
    protected void onDestroyView() {
        if (mView.getUnBinder() != null) {
            mView.getUnBinder().unbind();
        }
        disposables.clear();
    }

    protected void onDestroy() {
        disposables.dispose();
    }

    @NonNull
    public User getUser(int id) {
        return mModel.getUser(id);
    }

    public CompositeDisposable getDisposables() {
        return disposables;
    }

    public boolean isInEventMode() {
        return mModel.getEventModeId() >= 0;
    }

    public int getEventModeId() {
        return mModel.getEventModeId();
    }

    public boolean isInscribedInEventMode() {
        return mModel.getUserEventInscriptionMap().indexOfKey(mModel.getEventModeId()) >= 0;
    }

    public boolean isInscribedInEvent(int communityEventId) {
        return mModel.getUserEventInscriptionMap().indexOfKey(communityEventId) >= 0;
    }
}
