package eco.darmas.base.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.LruCache;
import android.util.SparseArray;

import com.thirtydegreesray.dataautoaccess.DataAutoAccess;

import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.User;
import eco.darmas.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseFragmentModel {

    private final DataManager dataManager;
    private final LruCache<Class<?>, Observable<?>> apiObservables = new LruCache<>(10);

    protected BaseFragmentModel(DataManager dataManager, Bundle savedInstanceState) {
        this.dataManager = dataManager;
        DataAutoAccess.getData(this, savedInstanceState);
    }

    private Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache){
        Observable<?> preparedObservable = null;

        if(useCache) //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if(preparedObservable != null)
            return preparedObservable;

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if(cacheObservable){
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }

        return preparedObservable;
    }

    @NonNull
    public List<CommonUtils.StringWithTag> getNameList() {
        return dataManager.getNameList();
    }

    @NonNull
    public User getUser(int id) {
        return dataManager.getUser(id);
    }

    @NonNull
    public User getLoggedUser() {
        return AppData.INSTANCE.getLoggedUser();
    }

    public void saveInstanceState(Bundle outState) {
        DataAutoAccess.saveData(this, outState);
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public SparseArray<Inscription> getUserEventInscriptionMap() {
        return getDataManager().getUserEventInscriptionMap();
    }

    public int getEventModeId() {
        return App.getInstance().getPreferencesHelper().getSelectedEventId();
    }
}