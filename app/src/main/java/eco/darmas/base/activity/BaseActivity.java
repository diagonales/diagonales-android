package eco.darmas.base.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import eco.darmas.App;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.activities.login.LoginActivity;
import eco.darmas.auxiliary.Action;
import eco.darmas.auxiliary.PermissionPresenter;
import eco.darmas.data.DataManager;

public abstract class BaseActivity<Presenter extends BaseActivityPresenter, Model extends BaseActivityModel, View extends BaseActivityView> extends AppCompatActivity {

    private Presenter mPresenter;
    private Model mModel;
    private View mView;

    protected abstract Model createModel(final DataManager dataManager, Bundle savedInstanceState);

    protected abstract Presenter createPresenter(final Model model, final View view);

    protected abstract View createView(final Context context);

    @CallSuper
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Reached when the user taps a notification when app is closed, we need to start app from login activity
        if (!(this instanceof LoginActivity) && AppData.INSTANCE.getLoggedUser() == null) {
            AppNav.navigateToActivity(AppConstants.LOGIN_ACTIVITY, this);
            return;
        }

        mModel = createModel(App.getInstance().getDataManager(), savedInstanceState);
        mView = createView(this);
        setContentView(mView);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            setSupportActionBar(new Toolbar(this));
            actionBar = getSupportActionBar();
        }
        mView.setupActionBar(actionBar);

        mPresenter = createPresenter(mModel, mView);
    }

    @CallSuper
    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mPresenter.onPostCreate();
    }

    @CallSuper
    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @CallSuper
    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        mModel.saveInstanceState(outState);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mView.onNewIntent(intent);
    }

    @CallSuper
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPresenter != null) {
            mPresenter.onActivityResult(requestCode, resultCode, data);
        }
    }

    @CallSuper
    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onAcceptRationaleClick(android.view.View view) {
        int action = mView.dismissPermissionRationale();
        switch (action) {
            case Action.ACTION_CODE_READ_CONTACTS:
                mView.getPermissionPresenter().requestReadContactsPermissionAfterRationale();
                break;
            case Action.ACTION_CODE_SAVE_IMAGE:
                mView.getPermissionPresenter().requestWriteExternalStoragePermissionAfterRationale();
                break;
            case Action.ACTION_CODE_SEND_SMS:
                mView.getPermissionPresenter().requestSendSMSAfterRationale();
                break;
        }
    }

    public void onDismissRationaleClick(android.view.View view) {
        mView.dismissPermissionRationale();
    }

    public void showProgress(boolean show) {
    }

    public PermissionPresenter getPermissionPresenter() {
        return mView.getPermissionPresenter();
    }
}