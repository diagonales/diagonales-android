package eco.darmas.base.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.TextView;

import eco.darmas.BuildConfig;
import eco.darmas.R;
import eco.darmas.auxiliary.Action;
import eco.darmas.auxiliary.PermissionAction;
import eco.darmas.auxiliary.PermissionActionFactory;
import eco.darmas.auxiliary.PermissionPresenter;

import static eco.darmas.auxiliary.PermissionActionFactory.SUPPORT_IMPL;

public abstract class BaseActivityView extends FrameLayout implements PermissionPresenter.PermissionCallbacks {

    private AppCompatActivity mContext;
    private PermissionPresenter permissionPresenter;

    protected BaseActivityView(Context context) {
        super(context);
        this.mContext = (AppCompatActivity) context;
        PermissionActionFactory permissionActionFactory = new PermissionActionFactory(mContext);
        PermissionAction permissionAction = permissionActionFactory.getPermissionAction(SUPPORT_IMPL);
        permissionPresenter = new PermissionPresenter(permissionAction, this);
    }

    protected abstract void initialize(BaseActivityPresenter presenter);

    protected void setupActionBar(ActionBar supportActionBar) {
    }

    public void onNewIntent(Intent intent) {
    }

    protected View rationaleView;

    protected void createAndShowPermissionRationale(int action, int titleResId, int subtitleResId) {
        if (rationaleView == null) {
            rationaleView = ((ViewStub) mContext.findViewById(R.id.permission_rationale_stub)).inflate();
        } else {
            rationaleView.setVisibility(android.view.View.VISIBLE);
        }
        ((TextView) rationaleView.findViewById(R.id.rationale_title)).setText(titleResId);
        ((TextView) rationaleView.findViewById(R.id.rationale_subtitle)).setText(subtitleResId);
        rationaleView.setTag(action);
    }

    protected void showSnackBarPermissionMessage(int message) {
        final CoordinatorLayout coordinatorLayout = findViewById(R.id.main_coordinator_layout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, mContext.getString(message), Snackbar.LENGTH_LONG).setAction(mContext.getString(R.string.snackbar_settings), v -> {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + BuildConfig.APPLICATION_ID));
            mContext.startActivity(intent);
        });
        snackbar.show();
    }

    /**
     * Dismiss and returns the action associated to the rationale
     * @return int Action
     */
    public int dismissPermissionRationale() {
        if (rationaleView != null && rationaleView.getVisibility() == android.view.View.VISIBLE) {
            rationaleView.setVisibility(android.view.View.GONE);
            return (int) rationaleView.getTag();
        }
        return 0;
    }

    //----- Permission management ----//
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Action.ACTION_CODE_READ_CONTACTS:
            case Action.ACTION_CODE_SAVE_IMAGE:
            case Action.ACTION_CODE_SEND_SMS:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
        }
    }

    public PermissionPresenter getPermissionPresenter() {
        return permissionPresenter;
    }

    //----- Must override these to process permissions----//
    @Override
    public void permissionAccepted(int actionCode) {
    }

    @Override
    public void permissionDenied(int actionCode) {
    }

    @Override
    public void showRationale(int actionCode) {
    }
}