package eco.darmas.base.activity;

import android.content.Intent;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.App;
import eco.darmas.BuildConfig;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.pojos.User;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseActivityPresenter<Model extends BaseActivityModel, View extends BaseActivityView> {

    private final Model mModel;
    private final View mView;
    private final Unbinder unBinder;
    private static boolean activityVisible;
    private final CompositeDisposable disposables;

    protected BaseActivityPresenter(final Model mModel, final View mView) {
        this.mModel = mModel;
        this.mView = mView;
        this.unBinder = ButterKnife.bind(mView, (AppCompatActivity) mView.getContext());
        this.disposables = new CompositeDisposable();
    }

    @CallSuper
    protected void onPostCreate() {
        this.mView.initialize(this);
    }

    @CallSuper
    protected void onResume() {
        activityResumed();
    }

    @CallSuper
    protected void onStart() {
    }

    @CallSuper
    protected void onPause() {
        activityPaused();
    }

    @CallSuper
    protected void onStop() {
        disposables.clear();
    }

    @CallSuper
    protected void onDestroy() {
        if (unBinder != null) {
            unBinder.unbind();
        }
        disposables.dispose();
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    private static void activityResumed() {
        activityVisible = true;
    }

    private static void activityPaused() {
        activityVisible = false;
    }

    @NonNull
    public User getUser(int id) {
        return mModel.getUser(id);
    }

    @CallSuper
    void onActivityResult(int requestCode, int resultCode, Intent data) {
        EventBus.getDefault().post(new ActivityResultEvent(requestCode, resultCode, data));
    }

    public CompositeDisposable getDisposables() {
        return disposables;
    }

    public boolean isInEventMode() {
        return mModel.getEventModeId() >= 0;
    }

    public int getEventModeId() {
        return mModel.getEventModeId();
    }

    public boolean isInscribedInEventMode() {
        return mModel.getUserEventInscriptionMap().indexOfKey(mModel.getEventModeId()) >= 0;
    }

    public boolean isTestMode() {
        return BuildConfig.DEBUG || App.getInstance().getPreferencesHelper().isTestMode();
    }
}