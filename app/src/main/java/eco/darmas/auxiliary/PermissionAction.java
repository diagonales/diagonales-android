package eco.darmas.auxiliary;

public interface PermissionAction {

    boolean hasSelfPermission(String permission);

    void requestPermission(String permission, int requestCode);

    boolean shouldShowRequestPermissionRationale(String permission);
}
