package eco.darmas.auxiliary;

import android.app.AlertDialog;
import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.Window;

import eco.darmas.R;

public class CustomDialog extends AlertDialog {

    //private View view;

    public CustomDialog(Context context) {
        super(new ContextThemeWrapper(context, R.style.DialogTheme)); // R.style.AppTheme
        //super(context, R.style.DialogTheme); // R.style.AppTheme
        /*view = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null);
        setView(view);*/
        getWindow().getAttributes().windowAnimations = R.style.DialogSlide;
    }

    /*@Override
    public void setTitle(CharSequence title) {
        TextView tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText(title);
        tv_title.setVisibility(View.VISIBLE);
    }

    @Override
    public void setMessage(CharSequence message) {
        TextView tv_message = view.findViewById(R.id.tv_message);
        ScrollView sv_message = view.findViewById(R.id.sv_message);
        tv_message.setText(message);
        sv_message.setVisibility(View.VISIBLE);
    }*/

    public void removeTitleBar() {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }
}