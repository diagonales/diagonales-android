package eco.darmas.auxiliary;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;

import eco.darmas.utils.CommonUtils;

public class ActivityPermissionActionImpl implements PermissionAction {

    private AppCompatActivity activity;

    public ActivityPermissionActionImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean hasSelfPermission(String permission) {
        if (!CommonUtils.isMarshmallowOrHigher()) {
            return true;
        }
        return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void requestPermission(String permission, int requestCode) {
        activity.requestPermissions(new String[] { permission }, requestCode);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean shouldShowRequestPermissionRationale(String permission) {
        return activity.shouldShowRequestPermissionRationale(permission);
    }
}
