package eco.darmas.auxiliary;

import android.support.v7.app.AppCompatActivity;

public class PermissionActionFactory {

    public static final int ACTIVITY_IMPL = 1;
    public static final int SUPPORT_IMPL = 2;

    private AppCompatActivity activity;

    public PermissionActionFactory(AppCompatActivity activity) {
        this.activity = activity;
    }

    public PermissionAction getPermissionAction(int type) {
        if (type == SUPPORT_IMPL) {
            return new SupportPermissionActionImpl(activity);
        } else {
            return new ActivityPermissionActionImpl(activity);
        }
    }
}
