package eco.darmas.auxiliary;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.json.JSONObject;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.utils.AppLogger;
import me.pushy.sdk.Pushy;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {

    protected Exception doInBackground(Void... params) {
        try {
            // Assign a unique token to this device
            String deviceToken = Pushy.register(App.getInstance());

            // Log it for debugging purposes
            AppLogger.i("Pushy device token: " + deviceToken);

            // Send the token to your backend server via an HTTP GET request
            App.getInstance().getNetworkService().getCallableAPI().registerDevice(AppData.INSTANCE.getLoggedUser().getIdUser(), deviceToken).enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    try {
                        JSONObject JSONResponse = new JSONObject(new String(response.body().bytes()));
                        if (JSONResponse.get("result").equals("true")) {
                            AppLogger.i("Pushy registered client token: " + deviceToken);
                        } else {
                            AppLogger.i("Pushy error registering client token: " + deviceToken);
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    AppLogger.e(t, null);
                }
            });
        } catch (Exception exc) {
            // Return exc to onPostExecute
            return exc;
        }
        // Success
        return null;
    }

    @Override
    protected void onPostExecute(Exception exc) {
        // Failed?
        if (exc != null) {
            AppLogger.e(exc, null);
            // Show error as toast conversation
            //mView.showErrorToast(exc.toString());
        }
        // Succeeded, do something to alert the user
    }
}
