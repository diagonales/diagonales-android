package eco.darmas.auxiliary;

import android.content.pm.PackageManager;

public class PermissionPresenter {

    private PermissionAction permissionAction;
    private PermissionCallbacks permissionCallbacks;

    public PermissionPresenter(PermissionAction permissionAction, PermissionCallbacks permissionCallbacks) {
        this.permissionAction = permissionAction;
        this.permissionCallbacks = permissionCallbacks;
    }

    public void requestReadContactsPermission() {
        checkAndRequestPermission(Action.READ_CONTACTS);
    }

    public void requestReadContactsPermissionAfterRationale() {
        requestPermission(Action.READ_CONTACTS);
    }

    public void requestWriteExternalStoragePermission() {
        checkAndRequestPermission(Action.SAVE_IMAGE);
    }

    public boolean checkGrantedWriteExternalStoragePermission() {
        return checkPermission(Action.SAVE_IMAGE);
    }

    public void requestWriteExternalStoragePermissionAfterRationale() {
        requestPermission(Action.SAVE_IMAGE);
    }

    public void requestSendSMS() {
        checkAndRequestPermission(Action.SEND_SMS);
    }

    public void requestSendSMSAfterRationale() {
        requestPermission(Action.SEND_SMS);
    }

    private void checkAndRequestPermission(Action action) {
        if (permissionAction.hasSelfPermission(action.getPermission())) {
            permissionCallbacks.permissionAccepted(action.getCode());
        } else {
            if (permissionAction.shouldShowRequestPermissionRationale(action.getPermission())) {
                permissionCallbacks.showRationale(action.getCode());
            } else {
                permissionAction.requestPermission(action.getPermission(), action.getCode());
            }
        }
    }

    private boolean checkPermission(Action action) {
        return permissionAction.hasSelfPermission(action.getPermission());
    }

    private void requestPermission(Action action) {
        permissionAction.requestPermission(action.getPermission(), action.getCode());
    }

    public void checkGrantedPermission(int[] grantResults, int requestCode) {
        if (verifyGrantedPermission(grantResults)) {
            permissionCallbacks.permissionAccepted(requestCode);
        } else {
            permissionCallbacks.permissionDenied(requestCode);
        }
    }

    private boolean verifyGrantedPermission(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public interface PermissionCallbacks {

        void permissionAccepted(@Action.ActionCode int actionCode);

        void permissionDenied(@Action.ActionCode int actionCode);

        void showRationale(@Action.ActionCode int actionCode);
    }
}
