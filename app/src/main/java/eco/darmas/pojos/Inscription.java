package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashMap;
import java.util.Map;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_inscription",
        "id_event",
        "id_user",
        "id_transaction",
        "inscription",
        "date_inscription",
        "stand",
        "listings",
        "note",
        "status"
})
@Entity
public class Inscription implements Parcelable {

    @Id(assignable = true)
    @JsonProperty("id_inscription")
    private long idInscription;
    @JsonProperty("id_event")
    private int idEvent;
    @JsonProperty("id_user")
    private int idUser;
    @JsonProperty("id_transaction")
    private int idTransaction;
    @JsonProperty("inscription")
    private double inscription;
    @JsonProperty("date_inscription")
    private String dateInscription;
    @JsonProperty("stand")
    private int stand;
    @JsonProperty("listings")
    private String listings;
    @JsonProperty("note")
    private String note;
    @JsonProperty("status")
    private int status;
    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<>();

    public final static Parcelable.Creator<Inscription> CREATOR = new Creator<Inscription>() {

        @SuppressWarnings({"unchecked"})
        public Inscription createFromParcel(Parcel in) {
            return new Inscription(in);
        }

        public Inscription[] newArray(int size) {
            return (new Inscription[size]);
        }
    }
            ;

    protected Inscription(Parcel in) {
        this.idInscription = ((Long) in.readValue((Long.class.getClassLoader())));
        this.idEvent = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idUser = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idTransaction = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.inscription = ((Double) in.readValue((Double.class.getClassLoader())));
        this.dateInscription = ((String) in.readValue((String.class.getClassLoader())));
        this.stand = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.listings = ((String) in.readValue((String.class.getClassLoader())));
        this.note = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Inscription() {
    }

    @JsonProperty("id_inscription")
    public int getIdInscription() {
        return (int) idInscription;
    }

    @JsonProperty("id_inscription")
    public void setIdInscription(long idInscription) {
        this.idInscription = idInscription;
    }

    public Inscription withIdInscription(long idInscription) {
        this.idInscription = idInscription;
        return this;
    }

    @JsonProperty("id_event")
    public int getIdEvent() {
        return idEvent;
    }

    @JsonProperty("id_event")
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public Inscription withIdEvent(int idEvent) {
        this.idEvent = idEvent;
        return this;
    }

    @JsonProperty("id_user")
    public int getIdUser() {
        return idUser;
    }

    @JsonProperty("id_user")
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Inscription withIdUser(int idUser) {
        this.idUser = idUser;
        return this;
    }

    @JsonProperty("id_transaction")
    public int getIdTransaction() {
        return idTransaction;
    }

    @JsonProperty("id_transaction")
    public void setIdTransaction(int idTransaction) {
        this.idTransaction = idTransaction;
    }

    public Inscription withIdTransaction(int idTransaction) {
        this.idTransaction = idTransaction;
        return this;
    }

    @JsonProperty("inscription")
    public double getInscription() {
        return inscription;
    }

    @JsonProperty("inscription")
    public void setInscription(double inscription) {
        this.inscription = inscription;
    }

    public Inscription withInscription(double inscription) {
        this.inscription = inscription;
        return this;
    }

    @JsonProperty("date_inscription")
    public String getDateInscription() {
        return dateInscription;
    }

    @JsonProperty("date_inscription")
    public void setDateInscription(String dateInscription) {
        this.dateInscription = dateInscription;
    }

    public Inscription withDateInscription(String dateInscription) {
        this.dateInscription = dateInscription;
        return this;
    }

    @JsonProperty("stand")
    public int getStand() {
        return stand;
    }

    @JsonProperty("stand")
    public void setStand(int stand) {
        this.stand = stand;
    }

    public Inscription withStand(int stand) {
        this.stand = stand;
        return this;
    }

    @JsonProperty("listings")
    public String getListings() {
        return listings;
    }

    @JsonProperty("listings")
    public void setListings(String listings) {
        this.listings = listings;
    }

    public Inscription withListings(String listings) {
        this.listings = listings;
        return this;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("note")
    public void setNote(String note) {
        this.note = note;
    }

    public Inscription withNote(String note) {
        this.note = note;
        return this;
    }

    @JsonProperty("status")
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    public Inscription withStatus(int status) {
        this.status = status;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Inscription withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dateInscription).append(status).append(stand).append(idInscription).append(additionalProperties).append(idUser).append(inscription).append(listings).append(idTransaction).append(note).append(idEvent).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Inscription)) {
            return false;
        }
        Inscription rhs = ((Inscription) other);
        return new EqualsBuilder().append(dateInscription, rhs.dateInscription).append(status, rhs.status).append(stand, rhs.stand).append(idInscription, rhs.idInscription).append(additionalProperties, rhs.additionalProperties).append(idUser, rhs.idUser).append(inscription, rhs.inscription).append(listings, rhs.listings).append(idTransaction, rhs.idTransaction).append(note, rhs.note).append(idEvent, rhs.idEvent).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idInscription);
        dest.writeValue(idEvent);
        dest.writeValue(idUser);
        dest.writeValue(idTransaction);
        dest.writeValue(inscription);
        dest.writeValue(dateInscription);
        dest.writeValue(stand);
        dest.writeValue(listings);
        dest.writeValue(note);
        dest.writeValue(status);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }
}