package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashMap;
import java.util.Map;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_event",
        "type",
        "id_creator",
        "date_created",
        "date_edited",
        "title",
        "text",
        "image",
        "date_start",
        "date_end",
        "address",
        "price_min",
        "price_max",
        "ranged",
        "vacants",
        "inscriptions",
        "attendees",
        "strict_assistance",
        "date_inscription_open",
        "date_inscription_close",
        "income",
        "status"
})
public class CommunityEvent implements Parcelable {

    public class Type {
        public static final int FAIR = 0;
        public static final int MEETING = 1;
        public static final int STAFF = 2;
        public static final int PARTY = 3;
    }

    public class Status {
        public static final int PLANNED = 0;
        public static final int DELAYED = 1;
        public static final int POSTPONED = 2;
        public static final int CANCELED = 3;
        public static final int ENDED = 4;
    }

    @Id(assignable = true)
    @JsonProperty("id_event")
    private long idEvent;
    @JsonProperty("type")
    private int type;
    @JsonProperty("id_creator")
    private int idCreator;
    @JsonProperty("date_created")
    private String dateCreated;
    @JsonProperty("date_edited")
    private String dateEdited;
    @JsonProperty("title")
    private String title;
    @JsonProperty("text")
    private String text;
    @JsonProperty("image")
    private String image;
    @JsonProperty("date_start")
    private String dateStart;
    @JsonProperty("date_end")
    private String dateEnd;
    @JsonProperty("address")
    private String address;
    @JsonProperty("price_min")
    private double priceMin;
    @JsonProperty("price_max")
    private double priceMax;
    @JsonProperty("ranged")
    private int ranged;
    @JsonProperty("vacants")
    private int vacants;
    @JsonProperty("inscriptions")
    private int inscriptions;
    @JsonProperty("attendees")
    private int attendees;
    @JsonProperty("strict_assistance")
    private int strictAssistance;
    @JsonProperty("date_inscription_open")
    private String dateInscriptionOpen;
    @JsonProperty("date_inscription_close")
    private String dateInscriptionClose;
    @JsonProperty("income")
    private double income;
    @JsonProperty("status")
    private int status;
    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public final static Parcelable.Creator<CommunityEvent> CREATOR = new Creator<CommunityEvent>() {

        @SuppressWarnings({"unchecked"})
        public CommunityEvent createFromParcel(Parcel in) {
            return new CommunityEvent(in);
        }

        public CommunityEvent[] newArray(int size) {
            return (new CommunityEvent[size]);
        }

    };

    protected CommunityEvent(Parcel in) {
        this.idEvent = ((Long) in.readValue((Long.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idCreator = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateCreated = ((String) in.readValue((String.class.getClassLoader())));
        this.dateEdited = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.dateStart = ((String) in.readValue((String.class.getClassLoader())));
        this.dateEnd = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.priceMin = ((Double) in.readValue((Double.class.getClassLoader())));
        this.priceMax = ((Double) in.readValue((Double.class.getClassLoader())));
        this.ranged = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.vacants = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.inscriptions = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.attendees = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.strictAssistance = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateInscriptionOpen = ((String) in.readValue((String.class.getClassLoader())));
        this.dateInscriptionClose = ((String) in.readValue((String.class.getClassLoader())));
        this.income = ((Double) in.readValue((Double.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object>) in.readValue((Map.class.getClassLoader())));
    }

    public CommunityEvent() {
    }

    @JsonProperty("id_event")
    public int getIdEvent() {
        return (int) idEvent;
    }

    @JsonProperty("id_event")
    public void setIdEvent(long idEvent) {
        this.idEvent = idEvent;
    }

    public CommunityEvent withIdEvent(long idEvent) {
        this.idEvent = idEvent;
        return this;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(int type) {
        this.type = type;
    }

    public CommunityEvent withType(int type) {
        this.type = type;
        return this;
    }

    @JsonProperty("id_creator")
    public int getIdCreator() {
        return idCreator;
    }

    @JsonProperty("id_creator")
    public void setIdCreator(int idCreator) {
        this.idCreator = idCreator;
    }

    public CommunityEvent withIdCreator(int idCreator) {
        this.idCreator = idCreator;
        return this;
    }

    @JsonProperty("date_created")
    public String getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date_created")
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public CommunityEvent withDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    @JsonProperty("date_edited")
    public String getDateEdited() {
        return dateEdited;
    }

    @JsonProperty("date_edited")
    public void setDateEdited(String dateEdited) {
        this.dateEdited = dateEdited;
    }

    public CommunityEvent withDateEdited(String dateEdited) {
        this.dateEdited = dateEdited;
        return this;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public CommunityEvent withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public CommunityEvent withText(String text) {
        this.text = text;
        return this;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public CommunityEvent withImage(String image) {
        this.image = image;
        return this;
    }

    @JsonProperty("date_start")
    public String getDateStart() {
        return dateStart;
    }

    @JsonProperty("date_start")
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public CommunityEvent withDateStart(String dateStart) {
        this.dateStart = dateStart;
        return this;
    }

    @JsonProperty("date_end")
    public String getDateEnd() {
        return dateEnd;
    }

    @JsonProperty("date_end")
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public CommunityEvent withDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public CommunityEvent withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("price_min")
    public double getPriceMin() {
        return priceMin;
    }

    @JsonProperty("price_min")
    public void setPriceMin(double priceMin) {
        this.priceMin = priceMin;
    }

    public CommunityEvent withPriceMin(double priceMin) {
        this.priceMin = priceMin;
        return this;
    }

    @JsonProperty("price_max")
    public double getPriceMax() {
        return priceMax;
    }

    @JsonProperty("price_max")
    public void setPriceMax(double priceMax) {
        this.priceMax = priceMax;
    }

    public CommunityEvent withPriceMax(double priceMax) {
        this.priceMax = priceMax;
        return this;
    }

    @JsonProperty("ranged")
    public int getRanged() {
        return ranged;
    }

    @JsonProperty("ranged")
    public void setRanged(int ranged) {
        this.ranged = ranged;
    }

    public CommunityEvent withRanged(int ranged) {
        this.ranged = ranged;
        return this;
    }

    @JsonProperty("vacants")
    public int getVacants() {
        return vacants;
    }

    @JsonProperty("vacants")
    public void setVacants(int vacants) {
        this.vacants = vacants;
    }

    public CommunityEvent withVacants(int vacants) {
        this.vacants = vacants;
        return this;
    }

    @JsonProperty("inscriptions")
    public int getInscriptions() {
        return inscriptions;
    }

    @JsonProperty("inscriptions")
    public void setInscriptions(int inscriptions) {
        this.inscriptions = inscriptions;
    }

    public CommunityEvent withInscriptions(int participants) {
        this.inscriptions = participants;
        return this;
    }

    @JsonProperty("attendees")
    public int getAttendees() {
        return attendees;
    }

    @JsonProperty("attendees")
    public void setAttendees(int attendees) {
        this.attendees = attendees;
    }

    public CommunityEvent withAttendees(int attendees) {
        this.attendees = attendees;
        return this;
    }

    @JsonProperty("strict_assistance")
    public int getStrictAssistance() {
        return strictAssistance;
    }

    @JsonProperty("strict_assistance")
    public void setStrictAssistance(int strictAssistance) {
        this.strictAssistance = strictAssistance;
    }

    public CommunityEvent withStrictAssistance(int strictAssistance) {
        this.strictAssistance = strictAssistance;
        return this;
    }

    @JsonProperty("date_inscription_open")
    public String getDateInscriptionOpen() {
        return dateInscriptionOpen;
    }

    @JsonProperty("date_inscription_open")
    public void setDateInscriptionOpen(String dateInscriptionOpen) {
        this.dateInscriptionOpen = dateInscriptionOpen;
    }

    public CommunityEvent withDateInscriptionOpen(String dateInscriptionOpen) {
        this.dateInscriptionOpen = dateInscriptionOpen;
        return this;
    }

    @JsonProperty("date_inscription_close")
    public String getDateInscriptionClose() {
        return dateInscriptionClose;
    }

    @JsonProperty("date_inscription_close")
    public void setDateInscriptionClose(String dateInscriptionClose) {
        this.dateInscriptionClose = dateInscriptionClose;
    }

    public CommunityEvent withDateInscriptionClose(String dateInscriptionClose) {
        this.dateInscriptionClose = dateInscriptionClose;
        return this;
    }

    @JsonProperty("income")
    public double getIncome() {
        return income;
    }

    @JsonProperty("income")
    public void setIncome(double income) {
        this.income = income;
    }

    public CommunityEvent withIncome(double income) {
        this.income = income;
        return this;
    }

    @JsonProperty("status")
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    public CommunityEvent withStatus(int status) {
        this.status = status;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dateInscriptionOpen).append(dateInscriptionClose).append(attendees).append(text).append(vacants).append(idCreator).append(status).append(image).append(dateEnd).append(income).append(dateStart).append(type).append(inscriptions).append(idEvent).append(title).append(address).append(priceMin).append(priceMax).append(ranged).append(strictAssistance).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof CommunityEvent)) {
            return false;
        }
        CommunityEvent rhs = ((CommunityEvent) other);
        return new EqualsBuilder().append(dateInscriptionOpen, rhs.dateInscriptionOpen).append(dateInscriptionClose, rhs.dateInscriptionClose).append(attendees, rhs.attendees).append(text, rhs.text).append(vacants, rhs.vacants).append(idCreator, rhs.idCreator).append(status, rhs.status).append(image, rhs.image).append(dateEnd, rhs.dateEnd).append(dateEdited, rhs.dateEdited).append(income, rhs.income).append(dateStart, rhs.dateStart).append(type, rhs.type).append(inscriptions, rhs.inscriptions).append(idEvent, rhs.idEvent).append(title, rhs.title).append(address, rhs.address).append(priceMin, rhs.priceMin).append(priceMax, rhs.priceMax).append(ranged, rhs.ranged).append(strictAssistance, rhs.strictAssistance).append(dateCreated, rhs.dateCreated).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idEvent);
        dest.writeValue(type);
        dest.writeValue(idCreator);
        dest.writeValue(dateCreated);
        dest.writeValue(dateEdited);
        dest.writeValue(title);
        dest.writeValue(text);
        dest.writeValue(image);
        dest.writeValue(dateStart);
        dest.writeValue(dateEnd);
        dest.writeValue(address);
        dest.writeValue(priceMin);
        dest.writeValue(priceMax);
        dest.writeValue(ranged);
        dest.writeValue(vacants);
        dest.writeValue(inscriptions);
        dest.writeValue(attendees);
        dest.writeValue(strictAssistance);
        dest.writeValue(dateInscriptionOpen);
        dest.writeValue(dateInscriptionClose);
        dest.writeValue(income);
        dest.writeValue(status);
        dest.writeValue(additionalProperties);
    }

    @Override
    public String toString() {
        return "CommunityEvent{" +
                "idEvent=" + idEvent +
                ", type=" + type +
                ", idCreator=" + idCreator +
                ", dateCreated='" + dateCreated + '\'' +
                ", dateEdited='" + dateEdited + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", dateStart='" + dateStart + '\'' +
                ", dateEnd='" + dateEnd + '\'' +
                ", address='" + address + '\'' +
                ", priceMin=" + priceMin +
                ", priceMax=" + priceMax +
                ", ranged=" + ranged +
                ", vacants=" + vacants +
                ", inscriptions='" + inscriptions + '\'' +
                ", attendees='" + attendees + '\'' +
                ", strictAssistance=" + strictAssistance +
                ", dateInscriptionOpen='" + dateInscriptionOpen + '\'' +
                ", dateInscriptionClose='" + dateInscriptionClose + '\'' +
                ", income=" + income +
                ", status=" + status +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public int describeContents() {
        return 0;
    }
}