package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_rating",
        "id_sender",
        "id_target",
        "id_receiver",
        "type",
        "comment",
        "rating",
        "date"
})
public class Rating implements Parcelable {

    public class Type {
        public static final int MEMBER = 0;
        public static final int WORK = 1;
        public static final int LISTING = 2;
    }

    @Id(assignable = true)
    @JsonProperty("id_rating")
    private long idRating;
    @JsonProperty("id_sender")
    private int idSender;
    @JsonProperty("id_target")
    private int idTarget;
    @JsonProperty("id_receiver")
    private int idReceiver;
    @JsonProperty("type")
    private int type;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("rating")
    private float rating;
    @JsonProperty("date")
    private String date;

    public final static Parcelable.Creator<Rating> CREATOR = new Creator<Rating>() {

        @SuppressWarnings({"unchecked"})
        public Rating createFromParcel(Parcel in) {
            return new Rating(in);
        }

        public Rating[] newArray(int size) {
            return (new Rating[size]);
        }
    };

    protected Rating(Parcel in) {
        this.idRating = ((Long) in.readValue((Long.class.getClassLoader())));
        this.idSender = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idTarget = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idReceiver = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.comment = ((String) in.readValue((String.class.getClassLoader())));
        this.rating = ((Float) in.readValue((Float.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Rating() {
    }

    /**
     * @param idTarget
     * @param rating
     * @param idRating
     * @param date
     * @param comment
     * @param type
     * @param idReceiver
     * @param idSender
     */
    public Rating(int idRating, int idSender, int idTarget, int idReceiver, int type, String comment, float rating, String date) {
        super();
        this.idRating = idRating;
        this.idSender = idSender;
        this.idTarget = idTarget;
        this.idReceiver = idReceiver;
        this.type = type;
        this.comment = comment;
        this.rating = rating;
        this.date = date;
    }

    @JsonProperty("id_rating")
    public int getIdRating() {
        return (int) idRating;
    }

    @JsonProperty("id_rating")
    public void setIdRating(long idRating) {
        this.idRating = idRating;
    }

    public Rating withIdRating(int idRating) {
        this.idRating = idRating;
        return this;
    }

    @JsonProperty("id_sender")
    public int getIdSender() {
        return idSender;
    }

    @JsonProperty("id_sender")
    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    public Rating withIdSender(int idSender) {
        this.idSender = idSender;
        return this;
    }

    @JsonProperty("id_target")
    public int getIdTarget() {
        return idTarget;
    }

    @JsonProperty("id_target")
    public void setIdTarget(int idTarget) {
        this.idTarget = idTarget;
    }

    public Rating withIdTarget(int idTarget) {
        this.idTarget = idTarget;
        return this;
    }

    @JsonProperty("id_receiver")
    public int getIdReceiver() {
        return idReceiver;
    }

    @JsonProperty("id_receiver")
    public void setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
    }

    public Rating withIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
        return this;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(int type) {
        this.type = type;
    }

    public Rating withType(int type) {
        this.type = type;
        return this;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    public Rating withComment(String comment) {
        this.comment = comment;
        return this;
    }

    @JsonProperty("rating")
    public float getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(float rating) {
        this.rating = rating;
    }

    public Rating withRating(float rating) {
        this.rating = rating;
        return this;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    public Rating withDate(String date) {
        this.date = date;
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(idTarget).append(rating).append(idRating).append(comment).append(type).append(idReceiver).append(idSender).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Rating)) {
            return false;
        }
        Rating rhs = ((Rating) other);
        return new EqualsBuilder().append(idTarget, rhs.idTarget).append(rating, rhs.rating).append(idRating, rhs.idRating).append(date, rhs.date).append(comment, rhs.comment).append(type, rhs.type).append(idReceiver, rhs.idReceiver).append(idSender, rhs.idSender).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idRating);
        dest.writeValue(idSender);
        dest.writeValue(idTarget);
        dest.writeValue(idReceiver);
        dest.writeValue(type);
        dest.writeValue(comment);
        dest.writeValue(rating);
        dest.writeValue(date);
    }

    public int describeContents() {
        return 0;
    }
}