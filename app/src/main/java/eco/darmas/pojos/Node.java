package eco.darmas.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Node<T> implements Serializable {

    private T data = null;
    private List<Node<T>> children = new ArrayList<>();
    private List<Node<T>> fullTree = new ArrayList<>();
    private Node<T> parent = null;
    private int id;

    public int getId() {
        return id;
    }

    public void addNodeToTree(Node<T> node) {
        fullTree.add(node);
    }

    public List<Node<T>> getFullTree() {
        return fullTree;
    }

    public Node(T data, int id) {
        this.data = data;
        this.id = id;
        fullTree.add(this);
    }

    public void addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
    }

    /*public void addChild(T data) {
        Node<T> child = new Node<>(data);
        child.setParent(this);
        children.add(child);
    }*/

    public void addChildren(List<Node<T>> children) {
        for(Node<T> t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public Node<T> getParent() {
        return parent;
    }

    /*public eco.darmas.pojo.Node(T data, eco.darmas.pojo.Node<T> parent) {
        this.data = data;
        this.parent = parent;
    }*/

    public List<Node<T>> getChildren() {
        return children;
    }

    public void setChildren(List<Node<T>> children) {
        this.children = children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setParent(Node<T> parent) {
        //parent.addChild(this);
        this.parent = parent;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        return this.children.size() == 0;
    }

    public void removeParent() {
        this.parent = null;
    }
}