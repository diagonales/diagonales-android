package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_listing",
        "id_creator",
        "id_event",
        "title",
        "date",
        "active",
        "text",
        "type",
        "likes",
        "id_category",
        "image",
        "address",
        "price_min",
        "price_max",
        "comments",
        "ranged",
        "stock",
        "max_quantity",
        "completed",
        "shipping_method",
        "shipping_price"
})
public class Listing implements Parcelable {

    public class Type {
        public static final int OFFER = 0;
        public static final int DEMAND = 1;
    }

    public class ShippingMethod {
        public static final int PICKUP = 0;
        public static final int PICKUP_AND_DELIVERY = 1;
        public static final int DELIVERY = 2;
        public static final int DIGITAL = 3;
    }

    @Id(assignable = true)
    @JsonProperty("id_listing")
    private long idListing;
    @JsonProperty("id_creator")
    private int idCreator;
    @JsonProperty("id_event")
    private int idEvent;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("active")
    private String active;
    @JsonProperty("text")
    private String text;
    @JsonProperty("type")
    private int type;
    @JsonProperty("likes")
    private String likes;
    @JsonProperty("id_category")
    private int idCategory;
    @JsonProperty("image")
    private String image;
    @JsonProperty("address")
    private String address;
    @JsonProperty("price_min")
    private double priceMin;
    @JsonProperty("price_max")
    private double priceMax;
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("ranged")
    private String ranged;
    @JsonProperty("stock")
    private int stock;
    @JsonProperty("max_quantity")
    private int maxQuantity;
    @JsonProperty("completed")
    private int completed;
    @JsonProperty("shipping_method")
    private int shippingMethod;
    @JsonProperty("shipping_price")
    private double shippingPrice;


    public final static Parcelable.Creator<Listing> CREATOR = new Creator<Listing>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Listing createFromParcel(Parcel in) {
            return new Listing(in);
        }

        public Listing[] newArray(int size) {
            return (new Listing[size]);
        }

    };

    protected Listing(Parcel in) {
        this.idListing = ((Long) in.readValue((Long.class.getClassLoader())));
        this.idCreator = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idEvent = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.likes = ((String) in.readValue((String.class.getClassLoader())));
        this.idCategory = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.priceMin = ((Double) in.readValue((Double.class.getClassLoader())));
        this.priceMax = ((Double) in.readValue((Double.class.getClassLoader())));
        this.comments = ((String) in.readValue((String.class.getClassLoader())));
        this.ranged = ((String) in.readValue((String.class.getClassLoader())));
        this.stock = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.maxQuantity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.completed = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shippingMethod = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shippingPrice = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    public Listing() {
    }

    /**
     * @param text
     * @param idCreator
     * @param idEvent
     * @param image
     * @param type
     * @param date
     * @param idCategory
     * @param idListing
     * @param title
     * @param priceMin
     * @param address
     * @param likes
     * @param ranged
     * @param priceMax
     * @param active
     * @param comments
     * @param stock
     * @param maxQuantity
     * @param completed
     * @param shippingMethod
     * @param shippingPrice
     */
    public Listing(long idListing, int idCreator, int idEvent, String title, String date, String active, String text, int type, String likes, int idCategory, String image, String address, double priceMin, double priceMax, String comments, String ranged, int stock, int maxQuantity, int completed, int shippingMethod, double shippingPrice) {
        super();
        this.idListing = idListing;
        this.idCreator = idCreator;
        this.idEvent = idEvent;
        this.title = title;
        this.date = date;
        this.active = active;
        this.text = text;
        this.type = type;
        this.likes = likes;
        this.idCategory = idCategory;
        this.image = image;
        this.address = address;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
        this.comments = comments;
        this.ranged = ranged;
        this.stock = stock;
        this.maxQuantity = maxQuantity;
        this.completed = completed;
        this.shippingMethod = shippingMethod;
        this.shippingPrice = shippingPrice;
    }

    @JsonProperty("id_listing")
    public int getIdListing() {
        return (int) idListing;
    }

    @JsonProperty("id_listing")
    public void setIdListing(long idListing) {
        this.idListing = idListing;
    }

    @JsonProperty("id_creator")
    public int getIdCreator() {
        return idCreator;
    }

    @JsonProperty("id_creator")
    public void setIdCreator(int idCreator) {
        this.idCreator = idCreator;
    }

    @JsonProperty("id_event")
    public int getIdEvent() {
        return idEvent;
    }

    @JsonProperty("id_event")
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(int type) {
        this.type = type;
    }

    @JsonProperty("likes")
    public String getLikes() {
        return likes;
    }

    @JsonProperty("likes")
    public void setLikes(String likes) {
        this.likes = likes;
    }

    @JsonProperty("id_category")
    public int getIdCategory() {
        return idCategory;
    }

    @JsonProperty("id_category")
    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("price_min")
    public double getPriceMin() {
        return priceMin;
    }

    @JsonProperty("price_min")
    public void setPriceMin(double priceMin) {
        this.priceMin = priceMin;
    }

    @JsonProperty("price_max")
    public double getPriceMax() {
        return priceMax;
    }

    @JsonProperty("price_max")
    public void setPriceMax(double priceMax) {
        this.priceMax = priceMax;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("ranged")
    public String getRanged() {
        return ranged;
    }

    @JsonProperty("ranged")
    public void setRanged(String ranged) {
        this.ranged = ranged;
    }

    @JsonProperty("stock")
    public int getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(int stock) {
        this.stock = stock;
    }

    @JsonProperty("max_quantity")
    public int getMaxQuantity() {
        return maxQuantity;
    }

    @JsonProperty("max_quantity")
    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    @JsonProperty("completed")
    public int getCompleted() {
        return completed;
    }

    @JsonProperty("completed")
    public void setCompleted(int completed) {
        this.completed = completed;
    }

    @JsonProperty("shipping_method")
    public int getShippingMethod() {
        return shippingMethod;
    }

    @JsonProperty("shipping_method")
    public void setShippingMethod(int shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    @JsonProperty("shipping_price")
    public double getShippingPrice() {
        return shippingPrice;
    }

    @JsonProperty("shipping_price")
    public void setShippingPrice(double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    @Override
    public String toString() {
        return "Listing{" +
                "idListing='" + idListing + '\'' +
                ", idCreator='" + idCreator + '\'' +
                ", idEvent='" + idEvent + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", active='" + active + '\'' +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", likes='" + likes + '\'' +
                ", idCategory='" + idCategory + '\'' +
                ", image='" + image + '\'' +
                ", address='" + address + '\'' +
                ", priceMin='" + priceMin + '\'' +
                ", priceMax='" + priceMax + '\'' +
                ", comments='" + comments + '\'' +
                ", ranged='" + ranged + '\'' +
                ", stock='" + stock + '\'' +
                ", maxQuantity='" + maxQuantity + '\'' +
                ", completed='" + completed + '\'' +
                ", shippingMethod='" + shippingMethod + '\'' +
                ", shippingPrice='" + shippingPrice + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        //return new HashCodeBuilder().append(text).append(idCreator).append(image).append(type).append(date).append(idCategory).append(title).append(idListing).append(priceMin).append(address).append(likes).append(ranged).append(priceMax).append(active).append(comments).toHashCode();
        return new HashCodeBuilder().append(text).append(idCreator).append(idEvent).append(image).append(type).append(idCategory).append(title).append(idListing).append(priceMin).append(address).append(likes).append(ranged).append(priceMax).append(active).append(comments).append(stock).append(maxQuantity).append(completed).append(shippingMethod).append(shippingPrice).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Listing)) {
            return false;
        }
        Listing rhs = ((Listing) other);
        return new EqualsBuilder().append(text, rhs.text).append(idCreator, rhs.idCreator).append(idEvent, rhs.idEvent).append(image, rhs.image).append(type, rhs.type).append(date, rhs.date).append(idCategory, rhs.idCategory).append(title, rhs.title).append(idListing, rhs.idListing).append(priceMin, rhs.priceMin).append(address, rhs.address).append(likes, rhs.likes).append(ranged, rhs.ranged).append(priceMax, rhs.priceMax).append(active, rhs.active).append(comments, rhs.comments).append(stock, rhs.stock).append(maxQuantity, rhs.maxQuantity).append(completed, rhs.completed).append(shippingMethod, rhs.shippingMethod).append(shippingPrice, rhs.shippingPrice).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idListing);
        dest.writeValue(idCreator);
        dest.writeValue(idEvent);
        dest.writeValue(title);
        dest.writeValue(date);
        dest.writeValue(active);
        dest.writeValue(text);
        dest.writeValue(type);
        dest.writeValue(likes);
        dest.writeValue(idCategory);
        dest.writeValue(image);
        dest.writeValue(address);
        dest.writeValue(priceMin);
        dest.writeValue(priceMax);
        dest.writeValue(comments);
        dest.writeValue(ranged);
        dest.writeValue(stock);
        dest.writeValue(maxQuantity);
        dest.writeValue(completed);
        dest.writeValue(shippingMethod);
        dest.writeValue(shippingPrice);
    }

    public int describeContents() {
        return 0;
    }

}