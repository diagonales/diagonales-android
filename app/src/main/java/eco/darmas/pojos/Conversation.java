package eco.darmas.pojos;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Conversation {

    @Id(assignable = true)
    private long idContact;
    @Convert(converter = Message.class, dbType = String.class)
    private Message lastMessage;

    public Conversation(long idContact, Message lastMessage) {
        this.idContact = idContact;
        this.lastMessage = lastMessage;
    }

    public int getIdContact() {
        return (int) idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }
}