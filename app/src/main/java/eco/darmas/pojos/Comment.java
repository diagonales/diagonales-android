package eco.darmas.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_comment",
        "id_creator",
        "date",
        "comment",
        "likes",
        "id_post"
})
public class Comment {

    @JsonProperty("id_comment")
    private long idComment;
    @JsonProperty("id_creator")
    private long idCreator;
    @JsonProperty("date")
    private String date;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("likes")
    private String likes;
    @JsonProperty("id_post")
    private long idPost;

    /**
     * No args constructor for use in serialization
     *
     */
    public Comment() {
    }

    /**
     *
     * @param idCreator
     * @param likes
     * @param idPost
     * @param comment
     * @param date
     * @param idComment
     */
    public Comment(long idComment, long idCreator, String date, String comment, String likes, long idPost) {
        super();
        this.idComment = idComment;
        this.idCreator = idCreator;
        this.date = date;
        this.comment = comment;
        this.likes = likes;
        this.idPost = idPost;
    }

    @JsonProperty("id_comment")
    public int getIdComment() {
        return (int) idComment;
    }

    @JsonProperty("id_comment")
    public void setIdComment(long idComment) {
        this.idComment = idComment;
    }

    @JsonProperty("id_creator")
    public int getIdCreator() {
        return (int) idCreator;
    }

    @JsonProperty("id_creator")
    public void setIdCreator(long idCreator) {
        this.idCreator = idCreator;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonProperty("likes")
    public String getLikes() {
        return likes;
    }

    @JsonProperty("likes")
    public void setLikes(String likes) {
        this.likes = likes;
    }

    @JsonProperty("id_post")
    public long getIdPost() {
        return idPost;
    }

    @JsonProperty("id_post")
    public void setIdPost(long idPost) {
        this.idPost = idPost;
    }

}