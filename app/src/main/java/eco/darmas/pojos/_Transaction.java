package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_transaction",
        "id_sender",
        "id_receiver",
        "id_category",
        "concept",
        "amount",
        "date",
        "status",
        "type",
        "id_listing",
        "date_accepted",
        "date_delivered",
        "date_received",
        "date_rejected",
        "sender_archived",
        "receiver_archived",
        "note",
        "quantity",
        "id_event",
        "shipping_method",
        "shipping_price",
        "shipping_info",
        "contribution"
})
@Entity
public class _Transaction implements Parcelable {

    public class Status {
        public static final int REJECTED = -1;
        public static final int REQUESTED = 0;
        public static final int ACCEPTED = 1;
        public static final int DELIVERED = 2;
        public static final int RECEIVED = 3;
    }

    public class Type {
        public static final int OFFER = 0;
        public static final int DEMAND = 1;
        public static final int TRANSFER = 2;
        public static final int INSCRIPTION = 3;
    }

    @Id(assignable = true)
    @JsonProperty("id_transaction")
    private long idTransaction;
    @JsonProperty("id_sender")
    private int idSender;
    @JsonProperty("id_receiver")
    private int idReceiver;
    @JsonProperty("id_category")
    private int idCategory;
    @JsonProperty("concept")
    private String concept;
    @JsonProperty("amount")
    private double amount;
    @JsonProperty("date")
    private String date;
    @JsonProperty("status")
    private int status;
    @JsonProperty("type")
    private int type;
    @JsonProperty("id_listing")
    private int idListing;
    @JsonProperty("date_accepted")
    private String dateAccepted;
    @JsonProperty("date_delivered")
    private String dateDelivered;
    @JsonProperty("date_received")
    private String dateReceived;
    @JsonProperty("date_rejected")
    private String dateRejected;
    @JsonProperty("sender_archived")
    private int senderArchived;
    @JsonProperty("receiver_archived")
    private int receiverArchived;
    @JsonProperty("note")
    private String note;
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("id_event")
    private int idEvent;
    @JsonProperty("shipping_method")
    private int shippingMethod;
    @JsonProperty("shipping_price")
    private double shippingPrice;
    @JsonProperty("shipping_info")
    private String shippingInfo;
    @JsonProperty("contribution")
    private double contribution;

    public final static Parcelable.Creator<_Transaction> CREATOR = new Creator<_Transaction>() {

        @SuppressWarnings({"unchecked"})
        public _Transaction createFromParcel(Parcel in) {
            return new _Transaction(in);
        }

        public _Transaction[] newArray(int size) {
            return (new _Transaction[size]);
        }

    };

    protected _Transaction(Parcel in) {
        this.idTransaction = ((Long) in.readValue((Long.class.getClassLoader())));
        this.idSender = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idReceiver = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idCategory = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.concept = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Double) in.readValue((Double.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idListing = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateAccepted = ((String) in.readValue((String.class.getClassLoader())));
        this.dateDelivered = ((String) in.readValue((String.class.getClassLoader())));
        this.dateReceived = ((String) in.readValue((String.class.getClassLoader())));
        this.dateRejected = ((String) in.readValue((String.class.getClassLoader())));
        this.senderArchived = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.receiverArchived = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.note = ((String) in.readValue((String.class.getClassLoader())));
        this.quantity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idEvent = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shippingMethod = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shippingPrice = ((Double) in.readValue((Double.class.getClassLoader())));
        this.shippingInfo = ((String) in.readValue((String.class.getClassLoader())));
        this.contribution = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public _Transaction() {
    }

    /**
     * @param concept
     * @param status
     * @param dateReceived
     * @param date
     * @param type
     * @param dateRejected
     * @param idReceiver
     * @param idSender
     * @param idCategory
     * @param amount
     * @param idListing
     * @param dateAccepted
     * @param dateDelivered
     * @param idTransaction
     * @param senderArchived
     * @param receiverArchived
     * @param note
     * @param quantity
     * @param idEvent
     * @param shippingMethod
     * @param shippingPrice
     * @param shippingInfo
     * @param contribution
     */
    public _Transaction(long idTransaction, int idSender, int idReceiver, int idCategory, String concept, double amount, String date, int status, int type, int idListing, String dateAccepted, String dateDelivered, String dateReceived, String dateRejected, int senderArchived, int receiverArchived, String note, int quantity, int idEvent, int shippingMethod, double shippingPrice, String shippingInfo, double contribution) {
        super();
        this.idTransaction = idTransaction;
        this.idSender = idSender;
        this.idReceiver = idReceiver;
        this.idCategory = idCategory;
        this.concept = concept;
        this.amount = amount;
        this.date = date;
        this.status = status;
        this.type = type;
        this.idListing = idListing;
        this.dateAccepted = dateAccepted;
        this.dateDelivered = dateDelivered;
        this.dateReceived = dateReceived;
        this.dateRejected = dateRejected;
        this.senderArchived = senderArchived;
        this.receiverArchived = receiverArchived;
        this.note = note;
        this.quantity = quantity;
        this.idEvent = idEvent;
        this.shippingMethod = shippingMethod;
        this.shippingPrice = shippingPrice;
        this.shippingInfo = shippingInfo;
        this.contribution = contribution;
    }

    @JsonProperty("id_transaction")
    public int getIdTransaction() {
        return (int) idTransaction;
    }

    @JsonProperty("id_transaction")
    public void setIdTransaction(long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public _Transaction withIdTransaction(long idTransaction) {
        this.idTransaction = idTransaction;
        return this;
    }

    @JsonProperty("id_sender")
    public int getIdSender() {
        return idSender;
    }

    @JsonProperty("id_sender")
    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    public _Transaction withIdSender(int idSender) {
        this.idSender = idSender;
        return this;
    }

    @JsonProperty("id_receiver")
    public int getIdReceiver() {
        return idReceiver;
    }

    @JsonProperty("id_receiver")
    public void setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
    }

    public _Transaction withIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
        return this;
    }

    @JsonProperty("id_category")
    public int getIdCategory() {
        return idCategory;
    }

    @JsonProperty("id_category")
    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public _Transaction withIdCategory(int idCategory) {
        this.idCategory = idCategory;
        return this;
    }

    @JsonProperty("concept")
    public String getConcept() {
        return concept;
    }

    @JsonProperty("concept")
    public void setConcept(String concept) {
        this.concept = concept;
    }

    public _Transaction withConcept(String concept) {
        this.concept = concept;
        return this;
    }

    @JsonProperty("amount")
    public double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public _Transaction withAmount(double amount) {
        this.amount = amount;
        return this;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    public _Transaction withDate(String date) {
        this.date = date;
        return this;
    }

    @JsonProperty("status")
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    public _Transaction withStatus(int status) {
        this.status = status;
        return this;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(int type) {
        this.type = type;
    }

    public _Transaction withType(int type) {
        this.type = type;
        return this;
    }

    @JsonProperty("id_listing")
    public int getIdListing() {
        return idListing;
    }

    @JsonProperty("id_listing")
    public void setIdListing(int idListing) {
        this.idListing = idListing;
    }

    public _Transaction withIdListing(int idListing) {
        this.idListing = idListing;
        return this;
    }

    @JsonProperty("date_accepted")
    public String getDateAccepted() {
        return dateAccepted;
    }

    @JsonProperty("date_accepted")
    public void setDateAccepted(String dateAccepted) {
        this.dateAccepted = dateAccepted;
    }

    public _Transaction withDateAccepted(String dateAccepted) {
        this.dateAccepted = dateAccepted;
        return this;
    }

    @JsonProperty("date_delivered")
    public String getDateDelivered() {
        return dateDelivered;
    }

    @JsonProperty("date_delivered")
    public void setDateDelivered(String dateDelivered) {
        this.dateDelivered = dateDelivered;
    }

    public _Transaction withDateDelivered(String dateDelivered) {
        this.dateDelivered = dateDelivered;
        return this;
    }

    @JsonProperty("date_received")
    public String getDateReceived() {
        return dateReceived;
    }

    @JsonProperty("date_received")
    public void setDateReceived(String dateReceived) {
        this.dateReceived = dateReceived;
    }

    public _Transaction withDateReceived(String dateReceived) {
        this.dateReceived = dateReceived;
        return this;
    }

    @JsonProperty("date_rejected")
    public String getDateRejected() {
        return dateRejected;
    }

    @JsonProperty("date_rejected")
    public void setDateRejected(String dateRejected) {
        this.dateRejected = dateRejected;
    }

    public _Transaction withDateRejected(String dateRejected) {
        this.dateRejected = dateRejected;
        return this;
    }

    @JsonProperty("sender_archived")
    public int getSenderArchived() {
        return senderArchived;
    }

    @JsonProperty("sender_archived")
    public void setSenderArchived(int senderArchived) {
        this.senderArchived = senderArchived;
    }

    public _Transaction withSenderArchived(int senderArchived) {
        this.senderArchived = senderArchived;
        return this;
    }

    @JsonProperty("receiver_archived")
    public int getReceiverArchived() {
        return receiverArchived;
    }

    @JsonProperty("receiver_archived")
    public void setReceiverArchived(int receiverArchived) {
        this.receiverArchived = receiverArchived;
    }

    public _Transaction withReceiverArchived(int receiverArchived) {
        this.receiverArchived = receiverArchived;
        return this;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("note")
    public void setNote(String note) {
        this.note = note;
    }

    public _Transaction withNote(String note) {
        this.note = note;
        return this;
    }

    @JsonProperty("quantity")
    public int getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public _Transaction withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("id_event")
    public int getIdEvent() {
        return idEvent;
    }

    @JsonProperty("id_event")
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public _Transaction withIdEvent(int idEvent) {
        this.idEvent = idEvent;
        return this;
    }

    @JsonProperty("shipping_method")
    public int getShippingMethod() {
        return shippingMethod;
    }

    @JsonProperty("shipping_method")
    public void setShippingMethod(int shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public _Transaction withShippingMethod(int shippingMethod) {
        this.shippingMethod = shippingMethod;
        return this;
    }

    @JsonProperty("shipping_price")
    public double  getShippingPrice() {
        return shippingPrice;
    }

    @JsonProperty("shipping_price")
    public void setShippingPrice(double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public _Transaction withShippingPrice(double shippingPrice) {
        this.shippingPrice = shippingPrice;
        return this;
    }

    @JsonProperty("shipping_info")
    public String  getShippingInfo() {
        return shippingInfo;
    }

    @JsonProperty("shipping_info")
    public void setShippingPrice(String shippingInfo) {
        this.shippingInfo = shippingInfo;
    }

    public _Transaction withShippingInfo(String shippingInfo) {
        this.shippingInfo = shippingInfo;
        return this;
    }

    @JsonProperty("contribution")
    public double  getContribution() {
        return contribution;
    }

    @JsonProperty("contribution")
    public void setContribution(double contribution) {
        this.contribution = contribution;
    }

    public _Transaction withContribution(double contribution) {
        this.contribution = contribution;
        return this;
    }

    @Override
    public int hashCode() {
        //return new HashCodeBuilder().append(concept).append(status).append(dateReceived).append(type).append(date).append(dateRejected).append(idReceiver).append(idCategory).append(idSender).append(amount).append(dateAccepted).append(idListing).append(dateDelivered).append(idTransaction).toHashCode();
        return new HashCodeBuilder().append(concept).append(status).append(type).append(idReceiver).append(idCategory).append(idSender).append(amount).append(idListing).append(idTransaction).append(senderArchived).append(receiverArchived).append(note).append(quantity).append(idEvent).append(shippingMethod).append(shippingPrice).append(contribution).append(shippingInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof _Transaction)) {
            return false;
        }
        _Transaction rhs = ((_Transaction) other);
        return new EqualsBuilder().append(concept, rhs.concept).append(status, rhs.status).append(dateReceived, rhs.dateReceived).append(type, rhs.type).append(date, rhs.date).append(dateRejected, rhs.dateRejected).append(idReceiver, rhs.idReceiver).append(idCategory, rhs.idCategory).append(idSender, rhs.idSender).append(amount, rhs.amount).append(dateAccepted, rhs.dateAccepted).append(idListing, rhs.idListing).append(dateDelivered, rhs.dateDelivered).append(idTransaction, rhs.idTransaction).append(senderArchived, rhs.senderArchived).append(receiverArchived, rhs.receiverArchived).append(note, rhs.note).append(quantity, rhs.quantity).append(idEvent, rhs.idEvent).append(shippingMethod, rhs.shippingMethod).append(shippingPrice, rhs.shippingPrice).append(shippingInfo, rhs.shippingInfo).append(contribution, rhs.contribution).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idTransaction);
        dest.writeValue(idSender);
        dest.writeValue(idReceiver);
        dest.writeValue(idCategory);
        dest.writeValue(concept);
        dest.writeValue(amount);
        dest.writeValue(date);
        dest.writeValue(status);
        dest.writeValue(type);
        dest.writeValue(idListing);
        dest.writeValue(dateAccepted);
        dest.writeValue(dateDelivered);
        dest.writeValue(dateReceived);
        dest.writeValue(dateRejected);
        dest.writeValue(senderArchived);
        dest.writeValue(receiverArchived);
        dest.writeValue(note);
        dest.writeValue(quantity);
        dest.writeValue(idEvent);
        dest.writeValue(shippingMethod);
        dest.writeValue(shippingPrice);
        dest.writeValue(shippingInfo);
        dest.writeValue(contribution);
    }

    public int describeContents() {
        return 0;
    }
}