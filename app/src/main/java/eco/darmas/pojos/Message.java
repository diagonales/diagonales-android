package eco.darmas.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;

import eco.darmas.utils.AppLogger;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.converter.PropertyConverter;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_message",
        "message",
        "id_sender",
        "sender_deleted",
        "id_receiver",
        "receiver_deleted",
        "date",
        "status",
        "type"
})
public class Message implements Serializable, PropertyConverter<Message, String> {

    @Transient
    public static final int SENT = 0;
    @Transient
    public static final int RECEIVED = 1;

    @Id(assignable = true)
    @JsonProperty("id_message")
    private long idMessage;
    @JsonProperty("message")
    private String message;
    @JsonProperty("id_sender")
    private int idSender;
    @JsonProperty("sender_deleted")
    private int senderDeleted;
    @JsonProperty("id_receiver")
    private int idReceiver;
    @JsonProperty("receiver_deleted")
    private int receiverDeleted;
    @JsonProperty("date")
    private String date;
    @JsonProperty("status")
    private int status;
    @JsonProperty("type")
    private int type;

    @Transient
    private final static long serialVersionUID = 944868465681135591L;

    /**
     * No args constructor for use in serialization
     */
    public Message() {
    }

    /**
     * @param senderDeleted
     * @param message
     * @param idMessage
     * @param status
     * @param receiverDeleted
     * @param type
     * @param date
     * @param idReceiver
     * @param idSender
     */
    public Message(long idMessage, String message, int idSender, int senderDeleted, int idReceiver, int receiverDeleted, String date, int status, int type) {
        super();
        this.idMessage = idMessage;
        this.message = message;
        this.idSender = idSender;
        this.senderDeleted = senderDeleted;
        this.idReceiver = idReceiver;
        this.receiverDeleted = receiverDeleted;
        this.date = date;
        this.status = status;
        this.type = type;
    }

    @JsonProperty("id_message")
    public int getIdMessage() {
        return (int) idMessage;
    }

    @JsonProperty("id_message")
    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("id_sender")
    public int getIdSender() {
        return idSender;
    }

    @JsonProperty("id_sender")
    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    @JsonProperty("sender_deleted")
    public int getSenderDeleted() {
        return senderDeleted;
    }

    @JsonProperty("sender_deleted")
    public void setSenderDeleted(int senderDeleted) {
        this.senderDeleted = senderDeleted;
    }

    @JsonProperty("id_receiver")
    public int getIdReceiver() {
        return idReceiver;
    }

    @JsonProperty("id_receiver")
    public void setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
    }

    @JsonProperty("receiver_deleted")
    public int getReceiverDeleted() {
        return receiverDeleted;
    }

    @JsonProperty("receiver_deleted")
    public void setReceiverDeleted(int receiverDeleted) {
        this.receiverDeleted = receiverDeleted;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("status")
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            AppLogger.e(e, null);
        }
        return null;
    }

    @Override
    public Message convertToEntityProperty(String databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        try {
            return new ObjectMapper().readValue(databaseValue, Message.class);
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        return null;
    }

    @Override
    public String convertToDatabaseValue(Message entityProperty) {
        return entityProperty == null ? null : entityProperty.toString();
    }
}