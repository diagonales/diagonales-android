package eco.darmas.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_category",
        "id_parent",
        "name",
        "rateable",
        "offer_listings",
        "demand_listings"
})
public class Category implements Serializable {

    @Id(assignable = true)
    @JsonProperty("id_category")
    private long idCategory;
    @JsonProperty("id_parent")
    private int idParent;
    @JsonProperty("name")
    private String name;
    @JsonProperty("rateable")
    private int rateable;
    @JsonProperty("offer_listings")
    private String offerListings;
    @JsonProperty("demand_listings")
    private String demandListings;

    private final static long serialVersionUID = 5025201206845497229L;

    @JsonProperty("demand_listings")
    public String getDemandListings() {
        return demandListings;
    }

    @JsonProperty("demand_listings")
    public void setDemandListings(String demandListings) {
        this.demandListings = demandListings;
    }

    @JsonProperty("offer_listings")
    public String getOfferListings() {
        return offerListings;
    }

    @JsonProperty("offer_listings")
    public void setOfferListings(String offerListings) {
        this.offerListings = offerListings;
    }

    @JsonProperty("id_category")
    public int getIdCategory() {
        return (int) idCategory;
    }

    @JsonProperty("id_category")
    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    @JsonProperty("id_parent")
    public int getIdParent() {
        return idParent;
    }

    @JsonProperty("id_parent")
    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("rateable")
    public int getRateable() {
        return rateable;
    }

    @JsonProperty("rateable")
    public void setRateable(int rateable) {
        this.rateable = rateable;
    }

    @Override
    public String toString() {
        return name;
    }
}