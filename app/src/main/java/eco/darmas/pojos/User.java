package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_user",
        "name",
        "email",
        "address",
        "phone",
        "password",
        "date",
        "funds",
        "active",
        "comment",
        "admin",
        "last_seen",
        "status",
        "token",
        "payment_margin",
        "image"
})
public class User implements Serializable, Parcelable {

    public class Status {
        public static final int GUEST = 0;
        public static final int ACTIVE = 1;
        public static final int MODERATOR = 2;
        public static final int ADMIN = 3;
    }

    @Id(assignable = true)
    @JsonProperty("id_user")
    private long idUser;
    @JsonProperty("name")
    private String name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("address")
    private String address;
    @JsonProperty("phone")
    private String phone;
    //TODO: password should not be retrieved
    @JsonProperty("password")
    @JsonIgnore
    private String password;
    @JsonProperty("date")
    private String date;
    @JsonProperty("funds")
    private double funds;
    @JsonProperty("active")
    private String active;
    @JsonProperty("comment")
    private String comment;
    //TODO: delete this property here and in database
    @JsonProperty("admin")
    private String admin;
    @JsonProperty("last_seen")
    private String lastSeen;
    @JsonProperty("status")
    private int status;
    @JsonProperty("token")
    private String token;
    @JsonProperty("payment_margin")
    private int paymentMargin;
    @JsonProperty("image")
    private String image;
    public final static Parcelable.Creator<User> CREATOR = new Creator<User>() {


        @SuppressWarnings({
                "unchecked"
        })
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return (new User[size]);
        }

    };
    private final static long serialVersionUID = -2561336936080277590L;

    protected User(Parcel in) {
        this.idUser = ((Long) in.readValue((Long.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.funds = ((Double) in.readValue((Double.class.getClassLoader())));
        this.active = ((String) in.readValue((String.class.getClassLoader())));
        this.comment = ((String) in.readValue((String.class.getClassLoader())));
        this.admin = ((String) in.readValue((String.class.getClassLoader())));
        this.lastSeen = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.token = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentMargin = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public User() {
    }

    /**
     * @param lastSeen
     * @param phone
     * @param status
     * @param date
     * @param password
     * @param funds
     * @param token
     * @param email
     * @param address
     * @param admin
     * @param name
     * @param idUser
     * @param active
     * @param comment
     * @param paymentMargin
     * @param image
     */
    public User(int idUser, String name, String email, String address, String phone, String password, String date, double funds, String active, String comment, String admin, String lastSeen, int status, String token, int paymentMargin, String image) {
        super();
        this.idUser = idUser;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.password = password;
        this.date = date;
        this.funds = funds;
        this.active = active;
        this.comment = comment;
        this.admin = admin;
        this.lastSeen = lastSeen;
        this.status = status;
        this.token = token;
        this.paymentMargin = paymentMargin;
        this.image = image;
    }

    @JsonProperty("id_user")
    public int getIdUser() {
        return (int) idUser;
    }

    @JsonProperty("id_user")
    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("funds")
    public double getFunds() {
        return funds;
    }

    @JsonProperty("funds")
    public void setFunds(double funds) {
        this.funds = funds;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonProperty("admin")
    public String getAdmin() {
        return admin;
    }

    @JsonProperty("admin")
    public void setAdmin(String admin) {
        this.admin = admin;
    }

    @JsonProperty("last_seen")
    public String getLastSeen() {
        return lastSeen;
    }

    @JsonProperty("last_seen")
    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    @JsonProperty("status")
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("payment_margin")
    public void setPaymentMargin(int paymentMargin) {
        this.paymentMargin = paymentMargin;
    }

    @JsonProperty("payment_margin")
    public int getPaymentMargin() {
        return paymentMargin;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser='" + idUser + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", date='" + date + '\'' +
                ", funds='" + funds + '\'' +
                ", active='" + active + '\'' +
                ", comment='" + comment + '\'' +
                ", admin='" + admin + '\'' +
                ", lastSeen='" + lastSeen + '\'' +
                ", status='" + status + '\'' +
                ", token='" + token + '\'' +
                ", paymentMargin='" + paymentMargin + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idUser);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(address);
        dest.writeValue(phone);
        dest.writeValue(password);
        dest.writeValue(date);
        dest.writeValue(funds);
        dest.writeValue(active);
        dest.writeValue(comment);
        dest.writeValue(admin);
        dest.writeValue(lastSeen);
        dest.writeValue(status);
        dest.writeValue(token);
        dest.writeValue(paymentMargin);
        dest.writeValue(image);
    }

    public int describeContents() {
        return 0;
    }
}