package eco.darmas.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id_post",
        "id_creator",
        "id_event",
        "title",
        "date",
        "active",
        "text",
        "announce",
        "likes",
        "comments",
        "image",
        "pinned"
})
public class Post implements Serializable, Parcelable {

    @Id(assignable = true)
    @JsonProperty("id_post")
    private long idPost;
    @JsonProperty("id_creator")
    private int idCreator;
    @JsonProperty("id_event")
    private int idEvent;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("active")
    private String active;
    @JsonProperty("text")
    private String text;
    @JsonProperty("announce")
    private String announce;
    @JsonProperty("likes")
    private String likes;
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("image")
    private String image;
    @JsonProperty("pinned")
    private String pinned;
    public final static Parcelable.Creator<Post> CREATOR = new Creator<Post>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        public Post[] newArray(int size) {
            return (new Post[size]);
        }

    };
    private final static long serialVersionUID = -4530727837175748776L;

    protected Post(Parcel in) {
        this.idPost = ((Long) in.readValue((Integer.class.getClassLoader())));
        this.idCreator = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.idEvent = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.announce = ((String) in.readValue((String.class.getClassLoader())));
        this.likes = ((String) in.readValue((String.class.getClassLoader())));
        this.comments = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.pinned = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Post() {
    }

    /**
     * @param text
     * @param title
     * @param idCreator
     * @param idEvent
     * @param pinned
     * @param likes
     * @param announce
     * @param image
     * @param active
     * @param idPost
     * @param date
     * @param comments
     */
    public Post(int idPost, int idCreator, int idEvent, String title, String date, String active, String text, String announce, String likes, String comments, String image, String pinned) {
        super();
        this.idPost = idPost;
        this.idCreator = idCreator;
        this.idEvent = idEvent;
        this.title = title;
        this.date = date;
        this.active = active;
        this.text = text;
        this.announce = announce;
        this.likes = likes;
        this.comments = comments;
        this.image = image;
        this.pinned = pinned;
    }

    @JsonProperty("id_post")
    public int getIdPost() {
        return (int) idPost;
    }

    @JsonProperty("id_post")
    public void setIdPost(long idPost) {
        this.idPost = idPost;
    }

    @JsonProperty("id_creator")
    public int getIdCreator() {
        return idCreator;
    }

    @JsonProperty("id_creator")
    public void setIdCreator(int idCreator) {
        this.idCreator = idCreator;
    }

    @JsonProperty("id_event")
    public int getIdEvent() {
        return idEvent;
    }

    @JsonProperty("id_event")
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("announce")
    public String getAnnounce() {
        return announce;
    }

    @JsonProperty("announce")
    public void setAnnounce(String announce) {
        this.announce = announce;
    }

    @JsonProperty("likes")
    public String getLikes() {
        return likes;
    }

    @JsonProperty("likes")
    public void setLikes(String likes) {
        this.likes = likes;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("pinned")
    public String getPinned() {
        return pinned;
    }

    @JsonProperty("pinned")
    public void setPinned(String pinned) {
        this.pinned = pinned;
    }

    @Override
    public String toString() {
        return "Post{" +
                "idPost='" + idPost + '\'' +
                ", idCreator='" + idCreator + '\'' +
                ", idEvent='" + idEvent + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", active='" + active + '\'' +
                ", text='" + text + '\'' +
                ", announce='" + announce + '\'' +
                ", likes='" + likes + '\'' +
                ", comments='" + comments + '\'' +
                ", image='" + image + '\'' +
                ", pinned='" + pinned + '\'' +
                '}';
    }

    public int hashCode() {
        //return new HashCodeBuilder().append(text).append(title).append(idCreator).append(pinned).append(likes).append(announce).append(image).append(active).append(idPost).append(date).append(comments).toHashCode();
        return new HashCodeBuilder().append(text).append(title).append(idCreator).append(idEvent).append(pinned).append(likes).append(announce).append(image).append(active).append(idPost).append(comments).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Post)) {
            return false;
        }
        Post rhs = ((Post) other);
        return new EqualsBuilder().append(text, rhs.text).append(title, rhs.title).append(idCreator, rhs.idCreator).append(idEvent, rhs.idEvent).append(pinned, rhs.pinned).append(likes, rhs.likes).append(announce, rhs.announce).append(image, rhs.image).append(active, rhs.active).append(idPost, rhs.idPost).append(date, rhs.date).append(comments, rhs.comments).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idPost);
        dest.writeValue(idCreator);
        dest.writeValue(idEvent);
        dest.writeValue(title);
        dest.writeValue(date);
        dest.writeValue(active);
        dest.writeValue(text);
        dest.writeValue(announce);
        dest.writeValue(likes);
        dest.writeValue(comments);
        dest.writeValue(image);
        dest.writeValue(pinned);
    }

    public int describeContents() {
        return 0;
    }

}