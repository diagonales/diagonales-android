package eco.darmas;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;

import com.squareup.leakcanary.LeakCanary;

import eco.darmas.data.DataManager;
import eco.darmas.data.network.NetworkService;
import eco.darmas.data.preferences.PreferencesHelper;
import eco.darmas.utils.AppLogger;

public class App extends Application {

    private static App instance;
    private PreferencesHelper preferencesHelper;
    private NetworkService networkService;
    private DataManager dataManager;
    public static final long startTime = System.currentTimeMillis();

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        //MultiDex.install(this);
        // Required initialization logic here!
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your instance in this process.
                return;
            }
            LeakCanary.install(this);
            AppLogger.init();
        }
        instance = this;
        AppLogger.i("application started");
        AppData.INSTANCE.getSystemDefaultLocale();
        preferencesHelper = new PreferencesHelper(App.getInstance(), "data");
        networkService = new NetworkService(BuildConfig.DEBUG || preferencesHelper.isTestMode());
        dataManager = new DataManager(BuildConfig.DEBUG || preferencesHelper.isTestMode());
        AppLogger.i("application ok: " + (System.currentTimeMillis() - startTime) + "ms");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public static App getInstance() {
        return instance;
    }

    public void logout() {
        dataManager.clearData();
        preferencesHelper.setCurrentUserId(null);
        preferencesHelper.setSelectedEventId(null);
        preferencesHelper.setTestMode(false);
        AppData.INSTANCE.setLoggedUser(null);
    }

    public void startNetworkService() {
        if (BuildConfig.DEBUG || preferencesHelper.isTestMode()) {
            networkService = new NetworkService(true);
            dataManager = new DataManager(true);
        } else {
            networkService = new NetworkService(false);
            dataManager = new DataManager(false);
        }
    }

    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }

    public NetworkService getNetworkService() {
        return networkService;
    }
}