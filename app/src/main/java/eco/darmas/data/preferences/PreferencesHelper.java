package eco.darmas.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

import eco.darmas.AppConfig;

public class PreferencesHelper {

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_SELECTED_EVENT_ID = "PREF_KEY_SELECTED_EVENT_ID";
    private static final String PREF_KEY_TEST_MODE = "PREF_KEY_TEST_MODE";
    private static final String PREF_KEY_SAVED_DATA_TYPE = "PREF_KEY_SAVED_DATA_TYPE";

    private SharedPreferences sharedPreferences;

    public PreferencesHelper(Context context, String preferencesFileName) {
        this.sharedPreferences = context.getSharedPreferences(preferencesFileName, Context.MODE_PRIVATE);
    }

    public Integer getCurrentUserId() {
        int userId = sharedPreferences.getInt(PREF_KEY_CURRENT_USER_ID, AppConfig.NULL_INDEX);
        return userId == AppConfig.NULL_INDEX ? null : userId;
    }

    public void setCurrentUserId(Integer userId) {
        int id = userId == null ? AppConfig.NULL_INDEX : userId;
        sharedPreferences.edit().putInt(PREF_KEY_CURRENT_USER_ID, id).apply();
    }

    public int getSelectedEventId() {
        return sharedPreferences.getInt(PREF_KEY_SELECTED_EVENT_ID, AppConfig.NULL_INDEX);
    }

    public void setSelectedEventId(Integer eventId) {
        int id = eventId == null ? AppConfig.NULL_INDEX : eventId;
        sharedPreferences.edit().putInt(PREF_KEY_SELECTED_EVENT_ID, id).apply();
    }

    public void setCookies(HashSet<String> cookies) {
        sharedPreferences.edit().putStringSet("cookies", cookies).apply();
    }

    public Set<String> getSavedCookies() {
        return sharedPreferences.getStringSet("cookies", new HashSet<>());
    }

    public boolean isTestMode() {
        return sharedPreferences.getBoolean(PREF_KEY_TEST_MODE, false);
    }

    public void setTestMode(boolean test) {
        sharedPreferences.edit().putBoolean(PREF_KEY_TEST_MODE, test).apply();
    }

    public String getCachedDataType() {
        return sharedPreferences.getString(PREF_KEY_SAVED_DATA_TYPE, null);
    }

    public void setCachedDataType(String savedDataType) {
        sharedPreferences.edit().putString(PREF_KEY_SAVED_DATA_TYPE, savedDataType).apply();
    }
}