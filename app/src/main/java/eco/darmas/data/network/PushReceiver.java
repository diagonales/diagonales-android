package eco.darmas.data.network;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;

import eco.darmas.AppConfig;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.activities.user.UserPresenter;
import eco.darmas.auxiliary.CircleTransform;
import eco.darmas.events.NotificationReceivedEvent;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;

import static android.content.ContentValues.TAG;

public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String notificationTitle = "Darmas";
        String notificationText = "Notificación";
        String notificationSummary = "";
        int notificationAlerts = Notification.DEFAULT_ALL;

        if (!intent.hasExtra("type")) {
            AppLogger.d("Received Notification without type!");
            return;
        }
        String type = intent.getStringExtra("type");
        switch (type) {
            case "transfer" : {
                if (AppData.INSTANCE.getLoggedUser() != null) {
                    EventBus.getDefault().postSticky(new NotificationReceivedEvent(NotificationReceivedEvent.TRANSFER_RECEIVED));
                }

                String sender_name = null;
                if (intent.getStringExtra("sender_name") != null) {
                    sender_name = intent.getStringExtra("sender_name");
                }

                String title = null;
                if (intent.getStringExtra("title") != null) {
                    title = intent.getStringExtra("title");
                }

                String amount = null;
                if (intent.getStringExtra("amount") != null) {
                    amount = AppConstants.CURRENCY_FORMAT.format(Double.parseDouble(intent.getStringExtra("amount")));
                }

                String summary = null;
                if (intent.getStringExtra("summary") != null) {
                    summary = intent.getStringExtra("summary");
                }

                notificationTitle = title;
                notificationText = sender_name + " te ha enviado " + amount + " Darmas";
                notificationSummary = summary;

                sendNotification(context, type, 0, notificationTitle, notificationText, notificationSummary, notificationAlerts, null);
                break;
            }
            case "transaction" : {
                if (AppData.INSTANCE.getLoggedUser() != null) {
                    EventBus.getDefault().postSticky(new NotificationReceivedEvent(NotificationReceivedEvent.TRANSACTION_UPDATED));
                }

                String sender_name = null;
                if (intent.getStringExtra("sender_name") != null) {
                    sender_name = intent.getStringExtra("sender_name");
                }

                String title = null;
                if (intent.getStringExtra("title") != null) {
                    title = intent.getStringExtra("title");
                }

                String text = null;
                if (intent.getStringExtra("text") != null) {
                    text = intent.getStringExtra("text");
                }

                notificationTitle = title;
                notificationText = sender_name + text + intent.getStringExtra("summary");

                sendNotification(context, type, 0, notificationTitle, notificationText, notificationSummary, notificationAlerts, null);
                break;
            }
            case "rating" : {
                if (AppData.INSTANCE.getLoggedUser() != null) {
                    EventBus.getDefault().postSticky(new NotificationReceivedEvent(NotificationReceivedEvent.RATING_RECEIVED));
                }

                String title = null;
                if (intent.getStringExtra("title") != null) {
                    title = intent.getStringExtra("title");
                }

                String message = null;
                if (intent.getStringExtra("message") != null) {
                    message = intent.getStringExtra("message");
                }

                notificationTitle = title;
                notificationText = message;
                notificationAlerts = 0;

                sendNotification(context, type, 0, notificationTitle, notificationText, notificationSummary, notificationAlerts, null);
                break;
            }
            case "message" : {
                //TODO: just get the new item without rest calls to update the content please
                if (AppData.INSTANCE.getLoggedUser() != null) {
                    EventBus.getDefault().postSticky(new NotificationReceivedEvent(NotificationReceivedEvent.MESSAGE_RECEIVED));
                }

                int id_sender = 0;
                if (intent.getStringExtra("id_sender") != null) {
                    id_sender = Integer.parseInt(intent.getStringExtra("id_sender"));
                }

                if (UserPresenter.isActivityVisible() && UserPresenter.selectedTab == 2) {
                    if (id_sender == 0 || UserPresenter.idContact == id_sender) {
                        return;
                    }
                }

                String title = null;
                // Attempt to extract the "title" property from the payload
                if (intent.getStringExtra("title") != null) {
                    title = intent.getStringExtra("title");
                }

                String message = null;
                // Attempt to extract the "message" property from the payload
                if (intent.getStringExtra("message") != null) {
                    message = intent.getStringExtra("message");
                }

                notificationTitle = title;
                notificationText = message;
                notificationSummary = "";

                String finalNotificationTitle = notificationTitle;
                String finalNotificationText = notificationText;
                String finalNotificationSummary = notificationSummary;
                int finalNotificationAlerts = notificationAlerts;
                int finalNotificationAlerts1 = notificationAlerts;
                int finalId_sender = id_sender;
                Picasso.get().load(AppConfig.getUserImagesUrl() + id_sender + ".jpg").resize(100, 100).transform(new CircleTransform()).placeholder(R.drawable.ic_account_image).centerCrop().into(new Target() {

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        sendNotification(context, type, finalId_sender, finalNotificationTitle, finalNotificationText, finalNotificationSummary, finalNotificationAlerts, bitmap);
                        AppLogger.d(TAG, "The image was obtained correctly");
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        sendNotification(context, type, finalId_sender, finalNotificationTitle, finalNotificationText, finalNotificationSummary, finalNotificationAlerts1, null);
                        AppLogger.e(e, "The image was not obtained");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        sendNotification(context, type, finalId_sender, finalNotificationTitle, finalNotificationText, finalNotificationSummary, finalNotificationAlerts1, null);
                        AppLogger.i(TAG, "Getting ready to get the image");
                        //Here you should place a loading gif in the ImageView to
                        //while image is being obtained.
                    }
                });
                break;
            }
        }
    }

    private void sendNotification(Context context, String type, int id_target, String notificationTitle, String notificationText, String notificationSummary, int notificationAlerts, Bitmap mBitmap) {
        if (!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("checkbox_preference", true)) {
            return;
        }

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;
        final Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra("tab", type);
        if (id_target != 0) {
            notificationIntent.putExtra("id_target", id_target);
        }

        contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*
        // You need this if starting the activity from a service
            notificationIntent.setAction(Intent.ACTION_MAIN);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            //notificationIntent.putExtra("notification_id", id);
            contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("1", "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Notification.Builder builder;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                if (! notificationSummary.equals("")) {
                    builder = new Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setLargeIcon(mBitmap)
                            .setContentTitle(notificationTitle)
                            .setContentText(notificationText)
                            .setSubText(notificationSummary)
                            .setDefaults(notificationAlerts)
                            .setColor(Color.parseColor("#0084c2"))
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setContentIntent(contentIntent);
                } else {
                    builder = new Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setLargeIcon(mBitmap)
                            .setContentTitle(notificationTitle)
                            .setContentText(notificationText)
                            .setDefaults(notificationAlerts)
                            .setColor(Color.parseColor("#0084c2"))
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setContentIntent(contentIntent);
                }
            } else {
                if (!notificationSummary.equals("")) {
                    builder = new Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setLargeIcon(mBitmap)
                            .setContentTitle(notificationTitle)
                            .setContentText(notificationText)
                            .setSubText(notificationSummary)
                            .setDefaults(notificationAlerts)
                            //.setColor(Color.parseColor("#0084c2"))
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setContentIntent(contentIntent);
                } else {
                    builder = new Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setLargeIcon(mBitmap)
                            .setContentTitle(notificationTitle)
                            .setContentText(notificationText)
                            .setDefaults(notificationAlerts)
                            //.setColor(Color.parseColor("#0084c2"))
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setContentIntent(contentIntent);
                }
            }
            Notification bigNote;
            if (! notificationSummary.equals("")) {
                bigNote = new Notification.BigTextStyle(builder)
                        .setBigContentTitle(notificationTitle)
                        .bigText(notificationText)
                        .setSummaryText(notificationSummary)
                        .build();
            } else {
                bigNote = new Notification.BigTextStyle(builder)
                        .setBigContentTitle(notificationTitle)
                        .bigText(notificationText)
                        .build();
            }

            // Build the notification and display it
            notificationManager.notify(1, bigNote);

            /*Notification notification = new Notification.InboxStyle(builder)
                    .addLine("First conversation").addLine("Second conversation")
                    .addLine("Thrid conversation").addLine("Fourth Message")
                    .setSummaryText("+2 more").build();
            // Put the auto cancel notification flag
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            return notification;*/
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder mBuilder;
            if (! notificationSummary.equals("")) {
                mBuilder = new Notification.Builder(context, "1")
                        .setSmallIcon(R.drawable.ic_logo)
                        .setLargeIcon(mBitmap)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText)
                        .setSubText(notificationSummary)
                        .setContentIntent(contentIntent)
                        .setColor(Color.parseColor("#0084c2"))
                        .setAutoCancel(true);
            } else {
                mBuilder = new Notification.Builder(context, "1")
                        .setSmallIcon(R.drawable.ic_logo)
                        .setLargeIcon(mBitmap)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText)
                        .setContentIntent(contentIntent)
                        .setColor(Color.parseColor("#0084c2"))
                        .setAutoCancel(true);
            }
            notificationManager.notify(1, mBuilder.build());
        } else {
            NotificationCompat.Builder mBuilder;
            if (! notificationSummary.equals("")) {
                mBuilder = new NotificationCompat.Builder(context, String.valueOf(CommonUtils.getRandom(1, 1000)))
                        .setSmallIcon(R.drawable.ic_logo)
                        .setLargeIcon(mBitmap)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText)
                        .setSubText(notificationSummary)
                        .setDefaults(notificationAlerts)
                        .setColor(Color.parseColor("#0084c2"))
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(contentIntent);
            } else {
                mBuilder = new NotificationCompat.Builder(context, String.valueOf(CommonUtils.getRandom(1, 1000)))
                        .setSmallIcon(R.drawable.ic_logo)
                        .setLargeIcon(mBitmap)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText)
                        .setDefaults(notificationAlerts)
                        .setColor(Color.parseColor("#0084c2"))
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(contentIntent);
            }

            // Build the notification and display it
            notificationManager.notify(1, mBuilder.build());
        }

        /*// Prepare a notification with vibration, sound and lights
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setLights(Color.RED, 1000, 1000)
                .setVibrate(new long[]{0, 400, 250, 400})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        // Build the notification and display it
        notificationManager.notify(1, builder.build());*/
    }
}