package eco.darmas.data.network;

import java.util.List;

import eco.darmas.pojos.Category;
import eco.darmas.pojos.Comment;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Message;
import eco.darmas.pojos.Post;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface APIService {

    @GET("get_user_data.php")
    Call<User> getUserData(@Query("id_user") int id_user);

    @FormUrlEncoded
    @POST("test_login.php")
    Call<ResponseBody> testLogin(@Field("name") String name,
                                 @Field("email") String email);

    @POST("login.php")
    Call<ResponseBody> login(@Header("Authorization") String credentials);

    @GET("loggedin.php")
    Call<ResponseBody> checkLoggedIn(@Query("id_user") int id_user);

    @FormUrlEncoded
    @POST("register_device.php")
    Call<ResponseBody> registerDevice(@Field("id_user") int id_user,
                                      @Field("token") String token);

    @GET("get_users.php")
    Observable<List<User>> getUsers();

    @GET("get_posts.php")
    Observable<List<Post>> getPosts(@Query("start") int start,
                                    @Query("quantity") int quantity,
                                    @Query("announce") String announce,
                                    @Query("id_creator") int id_creator);

    @GET("get_listings.php")
    Observable<List<Listing>> getListings(@Query("start") int start,
                                          @Query("quantity") int quantity,
                                          @Query("id_listing") int id_listing,
                                          @Query("id_creator") int id_creator,
                                          @Query("categories") String categories,
                                          @Query("title") String title);

    @GET("get_transactions.php")
    Observable<List<_Transaction>> getTransactions(@Query("start") int start,
                                                   @Query("quantity") int quantity,
                                                   @Query("id_transaction") int id_transaction,
                                                   @Query("id_user") int id_user);

    @GET("get_messages.php")
    Observable<List<Message>> getMessages(@Query("start") int start,
                                          @Query("quantity") int quantity,
                                          @Query("id_message") int id_message,
                                          @Query("id_user") int id_user);

    @GET("rating_get.php")
    Observable<List<Rating>> getRatings(@Query("id_rating") int id_rating,
                                        @Query("id_sender") int id_sender,
                                        @Query("id_receiver") int id_receiver);

    @GET("category_get.php")
    Observable<List<Category>> getCategories();

    @Multipart
    @Streaming
    @POST("event_create.php")
    Call<ResponseBody> createCommunityEvent(@Part("type") RequestBody type,
                                            @Part("id_creator") RequestBody id_creator,
                                            @Part("date_created") RequestBody date_created,
                                            @Part("title") RequestBody title,
                                            @Part("text") RequestBody text,
                                            @Part("date_start") RequestBody date_start,
                                            @Part("date_end") RequestBody date_end,
                                            @Part("address") RequestBody address,
                                            @Part("price_min") RequestBody price_min,
                                            @Part("price_max") RequestBody price_max,
                                            @Part("ranged") RequestBody ranged,
                                            @Part("vacants") RequestBody vacants,
                                            @Part("strict_assistance") RequestBody strict_assistance,
                                            @Part("date_inscription_open") RequestBody date_inscription_open,
                                            @Part("date_inscription_close") RequestBody date_inscription_close,
                                            @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("event_edit.php")
    Call<ResponseBody> editCommunityEvent(@Field("id_event") int id_event,
                                          @Field("type") int type,
                                          @Field("date_edited") String date_edited,
                                          @Field("title") String title,
                                          @Field("text") String text,
                                          @Field("date_start") String date_start,
                                          @Field("date_end") String date_end,
                                          @Field("address") String address,
                                          @Field("vacants") int vacants,
                                          @Field("strict_assistance") int strict_assistance,
                                          @Field("date_inscription_open") String date_inscription_open,
                                          @Field("date_inscription_close") String date_inscription_close);

    @FormUrlEncoded
    @POST("event_update.php")
    Call<ResponseBody> updateCommunityEvent(@Field("id_event") int id_event,
                                            @Field("participants") String participants,
                                            @Field("listings") String listings,
                                            @Field("attendees") String attendees);

    @FormUrlEncoded
    @POST("event_inscription.php")
    Call<ResponseBody> eventInscription(@Field("id_sender") int id_sender,
                                        @Field("concept") String concept,
                                        @Field("date") String date,
                                        @Field("amount") double amount,
                                        @Field("note") String note,
                                        @Field("id_event") int id_event,
                                        @Field("contribution") double contribution);

    @FormUrlEncoded
    @POST("event_inscription_update.php")
    Call<ResponseBody> eventInscriptionUpdate(@Field("id_inscription") int id_inscription,
                                              @Field("id_event") int id_event,
                                              @Field("standOption") String standOption,
                                              @Field("listingsJson") String listingsJson);

    @GET("event_get.php")
    Observable<List<CommunityEvent>> getCommunityEvents();

    @GET("event_inscription_get.php")
    Observable<List<Inscription>> getEventInscriptions(/*@Query("id_event") int id_event*/);

    @FormUrlEncoded
    @POST("logout.php")
    Call<ResponseBody> logout(@Field("id_user") int id_user);

    @FormUrlEncoded
    @POST("execute_transaction.php")
    Call<ResponseBody> executeTransaction(@Field("id_sender") double id_sender,
                                          @Field("id_receiver") double id_receiver,
                                          @Field("concept") String concept,
                                          @Field("amount") double amount,
                                          @Field("id_listing") double id_listing,
                                          @Field("status") int status,
                                          @Field("type") int type,
                                          @Field("contribution") double contribution,
                                          @Field("note") String note,
                                          @Field("id_event") int id_event);

    @FormUrlEncoded
    @POST("transaction_cancel.php")
    Call<ResponseBody> transactionCancel(@Field("id_transaction") int id_transaction,
                                         @Field("id_sender") int id_sender,
                                         @Field("id_receiver") int id_receiver,
                                         @Field("concept") String concept,
                                         @Field("amount") double amount,
                                         @Field("type") int type);

    @FormUrlEncoded
    @POST("process_transaction.php")
    Call<ResponseBody> processTransaction(@Field("id_sender") int id_sender,
                                          @Field("id_receiver") int id_receiver,
                                          @Field("id_category") int id_category,
                                          @Field("concept") String concept,
                                          @Field("amount") double amount,
                                          @Field("id_listing") int id_listing,
                                          @Field("type") int type,
                                          @Field("id_transaction") int id_transaction,
                                          @Field("status") int status,
                                          @Field("note") String note,
                                          @Field("quantity") int quantity,
                                          @Field("shipping_method") int shipping_method,
                                          @Field("shipping_price") double shipping_price,
                                          @Field("shipping_info") String shipping_info,
                                          @Field("id_event") int eventId,
                                          @Field("contribution") double contribution);

    @FormUrlEncoded
    @POST("archive_transaction.php")
    Call<ResponseBody> archiveTransaction(@Field("id_transaction") int id_transaction,
                                          @Field("id_sender") int id_sender,
                                          @Field("id_receiver") int id_receiver);

    @Multipart
    @Streaming
    @POST("upload_profile_image.php")
    Call<ResponseBody> uploadProfileImage(@Part("id_user") RequestBody id_user,
                                          @Part("actual_image") RequestBody actual_image,
                                          @Part MultipartBody.Part file);

    @GET("get_comments_data.php")
    Observable<List<Comment>> getPostComments(@Query("start") int start,
                                              @Query("quantity") int quantity,
                                              @Query("id_post") int id_post,
                                              @Query("id_creator") int id_creator);

    @FormUrlEncoded
    @POST("delete_post.php")
    Call<ResponseBody> deletePost(@Field("id_post") int id_post,
                                  @Field("image") String image);

    @FormUrlEncoded
    @POST("delete_comment.php")
    Call<ResponseBody> deleteComment(@Field("id_post") int id_post,
                                     @Field("id_comment") int id_comment);

    @FormUrlEncoded
    @POST("create_comment.php")
    Call<ResponseBody> createComment(@Field("id_creator") int id_creator,
                                     @Field("id_post") int id_post,
                                     @Field("comment") String comment);

    @FormUrlEncoded
    @POST("update_likes.php")
    Call<ResponseBody> updateLikes(@Field("id_post") int id_post,
                                   @Field("likesJSON") String likesJSON);

    @FormUrlEncoded
    @POST("update_comment_likes.php")
    Call<ResponseBody> updateCommentLikes(@Field("id_comment") int id_comment,
                                          @Field("likesJSON") String likesJSON);

    @Multipart
    @Streaming
    @POST("create_post.php")
    Call<ResponseBody> createPost(@Part("id_creator") RequestBody id_creator,
                                  @Part("id_event") RequestBody id_event,
                                  @Part("title") RequestBody title,
                                  @Part("text") RequestBody text,
                                  @Part("announce") RequestBody announce,
                                  @Part("pinned") RequestBody pinned,
                                  @Part MultipartBody.Part file);

    @Multipart
    @Streaming
    @POST("create_listing.php")
    Call<ResponseBody> createListing(@Part("id_creator") RequestBody id_creator,
                                     @Part("id_event") RequestBody id_event,
                                     @Part("id_category") RequestBody id_category,
                                     @Part("title") RequestBody title,
                                     @Part("text") RequestBody text,
                                     @Part("type") RequestBody type,
                                     @Part("price_min") RequestBody price_min,
                                     @Part("price_max") RequestBody price_max,
                                     @Part("ranged") RequestBody ranged,
                                     @Part("stock") RequestBody stock,
                                     @Part("shipping_method") RequestBody shipping_method,
                                     @Part("address") RequestBody address,
                                     @Part("shipping_price") RequestBody shipping_price,
                                     @Part("max_quantity") RequestBody perBuyMaxStock,
                                     @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("delete_listing.php")
    Call<ResponseBody> deleteListing(@Field("id_user") int id_user,
                                     @Field("id_listing") int id_listing,
                                     @Field("id_category") int id_category,
                                     @Field("id_event") int id_event,
                                     @Field("type") int type,
                                     @Field("image") String image);

    @POST("update_pass.php")
    Call<ResponseBody> updatePass(@Header("Authorization") String credentials,
                                  @Header("new-password") String new_pass);

    @FormUrlEncoded
    @POST("message_send.php")
    Call<ResponseBody> sendMessage(@Field("id_sender") int id_sender,
                                   @Field("id_receiver") int id_receiver,
                                   @Field("message") String message);

    @FormUrlEncoded
    @POST("message_hide.php")
    Call<ResponseBody> hideConversation(@Field("id_user") long idUser,
                                        @Field("id_sender") long idSender,
                                        @Field("id_receiver") long idReceiver);

    @FormUrlEncoded
    @POST("rating_send.php")
    Call<ResponseBody> sendRating(@Field("type") int type,
                                  @Field("id_sender") int id_sender,
                                  @Field("id_target") int id_target,
                                  @Field("id_receiver") int id_receiver,
                                  @Field("rating") float rating,
                                  @Field("comment") String comment,
                                  @Field("type2") int type2,
                                  @Field("id_target2") int id_target2,
                                  @Field("rating2") float rating2,
                                  @Field("comment2") String comment2);

    @FormUrlEncoded
    @POST("rating_send.php")
    Call<ResponseBody> sendRating(@Field("type") int type,
                                  @Field("id_sender") int id_sender,
                                  @Field("id_target") int id_target,
                                  @Field("id_receiver") int id_receiver,
                                  @Field("rating") float rating,
                                  @Field("comment") String comment);

    @FormUrlEncoded
    @POST("modify_post.php")
    Call<ResponseBody> togglePinnedPost(@Field("id_post") int id_post,
                                        @Field("pinned") String pinned);

    @FormUrlEncoded
    @POST("listing_edit.php")
    Call<ResponseBody> listingEdit(@Field("id_listing") int id_listing,
                                   @Field("text") String text,
                                   @Field("price_min") double price_min,
                                   @Field("price_max") double price_max,
                                   @Field("ranged") int ranged,
                                   @Field("stock") int stock,
                                   @Field("shipping_method") int shipping_method,
                                   @Field("address") String address,
                                   @Field("shipping_price") double shipping_price,
                                   @Field("max_quantity") int max_quantity);
}