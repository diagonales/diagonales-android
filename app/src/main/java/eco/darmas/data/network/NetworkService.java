package eco.darmas.data.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.security.cert.CertificateException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import eco.darmas.App;
import eco.darmas.AppConfig;
import eco.darmas.utils.AppLogger;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class  NetworkService {

    private static final String apiVersionUrl = String.format(Locale.US, "%.2f", AppConfig.VERSION) + "/";
    private APIService callbackAPI;
    private APIService observableAPI;
    private boolean testNetwork;

    public NetworkService(boolean testNetwork) {
        this((testNetwork ? AppConfig.TEST_BASE_URL : AppConfig.BASE_URL) + AppConfig.API_BASE_URL + apiVersionUrl, App.getInstance());
        this.testNetwork = testNetwork;
    }

    public NetworkService(String baseUrl, Context context) {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        int timeOut = AppConfig.HTTP_TIME_OUT;
        int cacheSize = AppConfig.HTTP_MAX_CACHE_SIZE;

        File okHttpCacheDirectory = new File(context.getCacheDir(), "okhttp_cache");
        Cache cache = new Cache(okHttpCacheDirectory, cacheSize);

        //OkHttpClient.Builder builder = new OkHttpClient.Builder()
        OkHttpClient.Builder builder = getUnsafeOkHttpClient()//TODO: remove for production
                .connectTimeout(timeOut, TimeUnit.MILLISECONDS)
                .addNetworkInterceptor(provideCacheInterceptor())
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .addInterceptor(new ReadCookiesInterceptor())
                .addInterceptor(new SaveCookiesInterceptor())
                .cache(cache);

        OkHttpClient okHttpClient = builder.build();

        //TODO: This checks whether restarting network service, already is there a Picasso instance to omit the process. Needs rework
        try {
            Picasso picasso = new Picasso.Builder(context)
                    .downloader(new OkHttp3Downloader(okHttpClient))
                    .build();
            Picasso.setSingletonInstance(picasso);
        } catch (IllegalStateException e) {
            AppLogger.e(e, null);
        }

        callbackAPI = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(APIService.class);

        observableAPI = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(APIService.class);
    }

    // 2 minutes internet cache
    private Interceptor provideCacheInterceptor() {
        return chain -> {
            Response response = chain.proceed(chain.request());
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge( 2, TimeUnit.MINUTES )
                    .build();

            return response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build();
        };
    }

    public APIService getCallableAPI() {
        return callbackAPI;
    }

    public APIService getObservableAPI() {
        return observableAPI;
    }

    public boolean isTestNetwork() {
        return testNetwork;
    }

    class SaveCookiesInterceptor implements Interceptor {

        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());

            if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                HashSet<String> cookies = new HashSet<>();
                cookies.addAll(originalResponse.headers("Set-Cookie"));
                //         if (sharedPreferences.getStringSet("cookies", null) == null) {
                AppLogger.i("interceptor saved: " + cookies.toString());
                App.getInstance().getPreferencesHelper().setCookies(cookies);
                //          }
            }

            return originalResponse;
        }
    }

    class ReadCookiesInterceptor implements Interceptor {

        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request.Builder builder = chain.request().newBuilder();
            //Set<String> preferences = PreferenceManager.getDefaultSharedPreferences(context).getStringSet("cookies", new HashSet<>());
            Set<String> preferences = App.getInstance().getPreferencesHelper().getSavedCookies();
            for (String cookie : preferences) {
                builder.addHeader("Cookie", cookie);
                //AppLogger.d("intercept read Header: " + cookie); // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp
            }

            return chain.proceed(builder.build());
        }
    }

    public static OkHttpClient.Builder getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}