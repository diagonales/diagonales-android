package eco.darmas.data;

import android.util.LruCache;

import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Message;
import eco.darmas.pojos.Post;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import io.objectbox.BoxStore;
import io.objectbox.rx.RxBoxStore;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DataRepository {


    //private final NetworkService testNetworkService;
    private final BoxStore boxStore;
    private final LruCache<Class<?>, Observable<?>> apiObservables = new LruCache<>(10);

    DataRepository(/*NetworkService networkService, */BoxStore boxStore) {
        //this.networkService = networkService;
        this.boxStore = boxStore;
    }

    Observable<Class> getDataObservable() {
        return RxBoxStore.observable(boxStore);
    }

    public void saveData(List objectList, Class clazz) {
        if (objectList.size() < boxStore.boxFor(clazz).count()) {
            boxStore.runInTxAsync(() -> {
                boxStore.boxFor(clazz).removeAll();
                boxStore.boxFor(clazz).put(objectList);
                AppLogger.d(boxStore.boxFor(clazz).count() + " saved in box: " + clazz.getName());
            }, null);
        } else {
            boxStore.runInTxAsync(() -> {
                boxStore.boxFor(clazz).put(objectList);
                AppLogger.d(boxStore.boxFor(clazz).count() + " saved in box: " + clazz.getName());
            }, null);
        }
    }

    Observable<User> getUserListObservable(boolean fromServer) {
        Observable<List<User>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getUsers()
                    .retry(3)
                    .doOnNext(users -> saveData(users, User.class));
        } else {
            observable = Observable.just(boxStore.boxFor(User.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(users -> users);
    }

    Observable<Post> getPostListObservable(boolean fromServer) {
        Observable<List<Post>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getPosts(0, 0, "", 0)
                    .retry(3)
                    .doOnNext(posts -> saveData(posts, Post.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Post.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(post -> post);
    }

    Observable<_Transaction> getTransactionListObservable(boolean fromServer) {
        Observable<List<_Transaction>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getTransactions(0, 0, 0, AppData.INSTANCE.getLoggedUser().getIdUser())
                    .retry(3)
                    .doOnNext(transactions -> saveData(transactions, _Transaction.class));
        } else {
            observable = Observable.just(boxStore.boxFor(_Transaction.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(transactions -> transactions);
    }

    Observable<Listing> getListingListObservable(boolean fromServer) {
        Observable<List<Listing>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getListings(0, 0, 0, 0, "", "")
                    .retry(3)
                    .doOnNext(listings -> saveData(listings, Listing.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Listing.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(listings -> listings);
    }

    Observable<Rating> getRatingListObservable(boolean fromServer) {
        Observable<List<Rating>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getRatings(0, 0, 0)
                    .retry(3)
                    .doOnNext(ratings -> saveData(ratings, Rating.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Rating.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(ratings -> ratings);
    }

    Observable<Category> getCategoryListObservable(boolean fromServer) {
        Observable<List<Category>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getCategories()
                    .retry(3)
                    .doOnNext(categories -> saveData(categories, Category.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Category.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(categories -> categories);
    }

    Observable<Message> getMessageListObservable(boolean fromServer) {
        Observable<List<Message>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getMessages(0, 0, 0, AppData.INSTANCE.getLoggedUser().getIdUser())
                    .retry(3)
                    .doOnNext(messages -> saveData(messages, Message.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Message.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(messages -> messages);
    }

    Observable<CommunityEvent> getCommunityEventListObservable(boolean fromServer) {
        Observable<List<CommunityEvent>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getCommunityEvents()
                    .retry(3)
                    .doOnNext(communityEvents -> saveData(communityEvents, CommunityEvent.class));
        } else {
            observable = Observable.just(boxStore.boxFor(CommunityEvent.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(communityEvents -> communityEvents);
    }

    Observable<Inscription> getEventInscriptionListObservable(boolean fromServer) {
        Observable<List<Inscription>> observable;
        if (fromServer) {
            observable = App.getInstance().getNetworkService().getObservableAPI().getEventInscriptions()
                    .retry(3)
                    .doOnNext(inscriptions -> saveData(inscriptions, Inscription.class));
        } else {
            observable = Observable.just(boxStore.boxFor(Inscription.class).getAll());
        }
        return observable
                .subscribeOn(Schedulers.io())
                .flatMapIterable(inscriptions -> inscriptions);
    }

    private Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCachedObservable) {
        Observable<?> preparedObservable = null;

        if (useCachedObservable) //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if (preparedObservable != null)
            return preparedObservable;

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable
                .subscribeOn(Schedulers.newThread());
        //.observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }

        return preparedObservable;
    }
}