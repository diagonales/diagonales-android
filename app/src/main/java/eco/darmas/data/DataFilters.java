package eco.darmas.data;

import eco.darmas.AppConstants;

public final class DataFilters {

    public static int selectedCategoryId;
    public static boolean showEnded = true;
    public static boolean showArchived = false;
    public static String offersConstraint = "";
    public static String demandsConstraint = "";
    public static String ordersConstraint = "";
    public static int selectedOffersListOrder = AppConstants.BY_LISTING_RATING_DESC;
    public static int selectedDemandsListOrder = AppConstants.BY_MEMBER_RATING_DESC;
    public static int selectedOrdersListOrder = AppConstants.BY_CREATION_DATE_DESC;
    public static int selectedPostCommentsListOrder = AppConstants.BY_LIKES_DESC;

    private DataFilters() {
    }

    static void clear() {
        selectedCategoryId = 0;
        showEnded = true;
        showArchived = false;
        offersConstraint = "";
        demandsConstraint = "";
        ordersConstraint = "";
        selectedOffersListOrder = AppConstants.BY_LISTING_RATING_DESC;
        selectedDemandsListOrder = AppConstants.BY_MEMBER_RATING_DESC;
        selectedOrdersListOrder = AppConstants.BY_CREATION_DATE_DESC;
        selectedPostCommentsListOrder = AppConstants.BY_LIKES_DESC;
    }
}