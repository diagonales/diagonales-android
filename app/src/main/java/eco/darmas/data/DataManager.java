package eco.darmas.data;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.events.NotificationReceivedEvent;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Message;
import eco.darmas.pojos.MyObjectBox;
import eco.darmas.pojos.Node;
import eco.darmas.pojos.Post;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import io.objectbox.BoxStore;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DataManager {

    private final DataRepository dataRepository;
    private static BoxStore boxStore;
    private CompositeDisposable disposables = new CompositeDisposable();
    private Disposable dataDisposable;

    private SparseArray<User> userMap = new SparseArray<>();
    private SparseArray<Post> postMap = new SparseArray<>();
    private SparseArray<_Transaction> transactionMap = new SparseArray<>();
    private SparseArray<Node<Category>> categoryNodeMap = new SparseArray<>();
    private SparseArray<Listing> listingMap = new SparseArray<>();
    private SparseArray<Rating> ratingMap = new SparseArray<>();
    private SparseArray<CommunityEvent> communityEventMap = new SparseArray<>();
    private List<CommunityEvent> pastCommunityEventsList = new ArrayList<>();
    private List<CommunityEvent> currentCommunityEventsList = new ArrayList<>();
    private List<CommunityEvent> scheduledCommunityEventsList = new ArrayList<>();
    //id_event - id_user - inscription
    private SparseArray<SparseArray<Inscription>> eventInscriptionMap = new SparseArray<>();
    //id_event - id_listing - JSONObject
    private SparseArray<SparseArray<JSONObject>> eventListingIdList = new SparseArray<>();
    //id_user - id_sender - rating
    private SparseArray<SparseArray<Float>> userRatingsMap = new SparseArray<>();
    //id_user - id_category - id_sender - float
    private SparseArray<SparseArray<SparseArray<Float>>> workRatingsMap = new SparseArray<>();
    //id_listing - id_sender - float
    private SparseArray<SparseArray<Float>> listingRatingsMap = new SparseArray<>();

    //logged user collections (reset after logout)
    private List<CommonUtils.StringWithTag> nameList = new ArrayList<>();
    private SparseArray<List<Message>> messagesMap = new SparseArray<>();
    private List<_Transaction> loggedUserOrdersList = new ArrayList<>();
    //id_event - user inscription
    private SparseArray<Inscription> userEventInscriptionMap = new SparseArray<>();
    //only used as workaround when user adds or removes listings to be available at an event
    private transient SparseArray<JSONObject> transientUserEventListings = new SparseArray<>();

    public DataManager(boolean testData) {
        setupBoxStore(testData);
        dataRepository = new DataRepository(boxStore);
        // Should this be here? what about the unregister? where is it?
        EventBus.getDefault().register(this);
    }

    private void setupBoxStore(boolean testData) {
        if (boxStore == null) {
            boxStore = MyObjectBox.builder().androidContext(App.getInstance()).build();
        }
        if (App.getInstance().getPreferencesHelper().getCachedDataType() == null) {
            App.getInstance().getPreferencesHelper().setCachedDataType(!testData ? "REAL_CACHE" : "TEST_CACHE");
        } else if (!testData && App.getInstance().getPreferencesHelper().getCachedDataType().equals("TEST_CACHE")) {
            boxStore.close();
            boxStore.deleteAllFiles();
            boxStore = MyObjectBox.builder().androidContext(App.getInstance()).build();
            App.getInstance().getPreferencesHelper().setCachedDataType("REAL_CACHE");
        } else if (testData && App.getInstance().getPreferencesHelper().getCachedDataType().equals("REAL_CACHE")) {
            boxStore.close();
            boxStore.deleteAllFiles();
            boxStore = MyObjectBox.builder().androidContext(App.getInstance()).build();
            App.getInstance().getPreferencesHelper().setCachedDataType("TEST_CACHE");
        } else {
            boxStore.close();
            boxStore.deleteAllFiles();
            boxStore = MyObjectBox.builder().androidContext(App.getInstance()).build();
            App.getInstance().getPreferencesHelper().setCachedDataType(null);
            setupBoxStore(testData);
        }
    }

    public void startDataLoad() {
        if (dataDisposable == null || dataDisposable.isDisposed()) {
            disposables = new CompositeDisposable();
        } else {
            return;
        }
        AppLogger.d("load started");
        disposables.add(dataRepository.getDataObservable()
                .subscribeOn(Schedulers.newThread())
                .doOnSubscribe(disposable -> dataDisposable = disposable)
                .doOnNext(aClass -> {
                    //TODO: fix this
                    if (AppData.INSTANCE.getLoggedUser() == null) {
                        return;
                    }

                    // disposables not needed here?
                    if (aClass.getName().equals(User.class.getName())) {
                        AppLogger.d("User data change detected... loading from box");
                        refreshUsers(false);
                    } else if (aClass.getName().equals(Post.class.getName())) {
                        AppLogger.d("Post data change detected... loading from box");
                        refreshPosts(false);
                    } else if (aClass.getName().equals(Listing.class.getName())) {
                        AppLogger.d("Listing data change detected... loading from box");
                        refreshListings(false);
                    } else if (aClass.getName().equals(_Transaction.class.getName())) {
                        AppLogger.d("Transaction data change detected... loading from box");
                        refreshTransactions(false);
                    } else if (aClass.getName().equals(Rating.class.getName())) {
                        AppLogger.d("Rating data change detected... loading from box");
                        refreshRatings(false);
                    } else if (aClass.getName().equals(Category.class.getName())) {
                        AppLogger.d("Category data change detected... loading from box");
                        refreshCategories(false);
                    } else if (aClass.getName().equals(Message.class.getName())) {
                        AppLogger.d("Message data change detected... loading from box");
                        refreshConversations(false);
                    } else if (aClass.getName().equals(CommunityEvent.class.getName())) {
                        AppLogger.d("Events data change detected... loading from box");
                        refreshCommunityEvents(false);
                    } else if (aClass.getName().equals(Inscription.class.getName())) {
                        AppLogger.d("Event listings data change detected... loading from box");
                        refreshEventListings(false);
                    }
                })
                .doOnComplete(() -> dataDisposable.dispose())
                .subscribe());

        // Comment this to test UI load speed without data load
        loadData(true);
    }

    private void loadData(boolean fromServer) {
        refreshUsers(fromServer);
        refreshPosts(fromServer);
        refreshTransactions(fromServer);
        refreshListings(fromServer);
        refreshRatings(fromServer);
        refreshCategories(fromServer);
        refreshConversations(fromServer);
        refreshCommunityEvents(fromServer);
        refreshEventListings(fromServer);
    }

    public void refreshUsers(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getUserListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getUserListObservable(false)
                    .observeOn(Schedulers.computation())
                    .doOnSubscribe(disposable -> {
                        nameList.clear();
                        nameList.add(new CommonUtils.StringWithTag("Selecciona un destinatario...", 0));
                    })
                    .subscribeWith(getUsersObserver()));
        }
    }

//    public Observable refreshUsers(boolean fromServer) {
//        return App.getInstance().getNetworkService().getObservableAPI().getUsers()
//                .retry(3)
//                .doOnNext(users -> DataRepository.saveData(users, User.class));
//    }

    public void refreshPosts(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getPostListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getPostListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getPostsObserver()));
        }
    }

    public void refreshTransactions(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getTransactionListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getTransactionListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getTransactionsObserver()));
        }
    }

    public void refreshListings(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getListingListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getListingListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getListingsObserver()));
        }
    }

    public void refreshRatings(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getRatingListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getRatingListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getRatingsObserver()));
        }
    }

    public void refreshCategories(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getCategoryListObservable(true).subscribe());
        } else {
            SparseArray<Node<Category>> transientCategoryNodeMap = new SparseArray<>();
            Category root = new Category();
            root.setIdCategory(0);
            root.setIdParent(-1);
            root.setName("Categorías");
            disposables.add(Observable.merge(Observable.just(root), dataRepository.getCategoryListObservable(false))
                    .observeOn(Schedulers.computation())
                    .doOnNext(category -> {
                        Node<Category> node = new Node<>(category, category.getIdCategory());
                        transientCategoryNodeMap.put(category.getIdCategory(), node);
                    })
                    .toList()
                    .toObservable()
                    .flatMapIterable(categories -> categories)
                    .doOnNext(category -> {
                        if (category.getIdParent() < 0) {
                            return;
                        }
                        Node<Category> node = transientCategoryNodeMap.get(category.getIdParent());

                        while (true) {
                            node.addNodeToTree(transientCategoryNodeMap.get(category.getIdCategory()));

                            try {
                                // Add child category offer listings ids
                                JSONArray parentOfferListings = new JSONArray();
                                if (node.getData().getOfferListings() != null && !node.getData().getOfferListings().isEmpty()) {
                                    parentOfferListings = new JSONArray(node.getData().getOfferListings());
                                }
                                JSONArray newOfferListings = new JSONArray();
                                if (transientCategoryNodeMap.get(category.getIdCategory()).getData().getOfferListings() != null && !transientCategoryNodeMap.get(category.getIdCategory()).getData().getOfferListings().isEmpty()) {
                                    newOfferListings = new JSONArray(transientCategoryNodeMap.get(category.getIdCategory()).getData().getOfferListings());
                                }
                                for (int i = 0; i < newOfferListings.length(); i++) {
                                    parentOfferListings.put(newOfferListings.getInt(i));
                                }
                                node.getData().setOfferListings(parentOfferListings.toString());

                                // Add child category demand listings ids
                                JSONArray parentDemandListings = new JSONArray();
                                if (node.getData().getDemandListings() != null && !node.getData().getDemandListings().isEmpty()) {
                                    parentDemandListings = new JSONArray(node.getData().getDemandListings());
                                }
                                JSONArray newDemandListings = new JSONArray();
                                if (transientCategoryNodeMap.get(category.getIdCategory()).getData().getDemandListings() != null && !transientCategoryNodeMap.get(category.getIdCategory()).getData().getDemandListings().isEmpty()) {
                                    newDemandListings = new JSONArray(transientCategoryNodeMap.get(category.getIdCategory()).getData().getDemandListings());
                                }
                                for (int i = 0; i < newDemandListings.length(); i++) {
                                    parentDemandListings.put(newDemandListings.getInt(i));
                                }
                                node.getData().setDemandListings(parentDemandListings.toString());
                            } catch (JSONException e) {
                                AppLogger.e(e, null);
                            }

                            //node.getData().setOfferListings(node.getData().getOfferListings() + transientCategoryNodeMap.get(category.getIdCategory()).getData().getOfferListings());
                            //node.getData().setDemandListings(node.getData().getDemandListings() + transientCategoryNodeMap.get(category.getIdCategory()).getData().getDemandListings());
                            if (!node.isRoot()) {
                                node = node.getParent();
                            } else {
                                break;
                            }
                        }
                        transientCategoryNodeMap.get(category.getIdParent()).addChild(transientCategoryNodeMap.get(category.getIdCategory()));
                    })
                    .map(category -> transientCategoryNodeMap.get(category.getIdCategory()))
                    .doOnComplete(() -> {
                        for (int i = 0; i < transientCategoryNodeMap.size(); i++) {
                            Node<Category> categoryNode = transientCategoryNodeMap.valueAt(i);
                            Collections.sort(categoryNode.getChildren(), (t1, t2) -> t1.getData().getName().compareToIgnoreCase(t2.getData().getName()));
                        }
                    })
                    .subscribeWith(getCategoriesObserver()));
        }
    }

    public void refreshConversations(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getMessageListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getMessageListObservable(false)
                    .observeOn(Schedulers.computation())
                    .filter(message -> (message.getIdSender() == AppData.INSTANCE.getLoggedUser().getIdUser() && message.getSenderDeleted() == 0) || (message.getIdReceiver() == AppData.INSTANCE.getLoggedUser().getIdUser() && message.getReceiverDeleted() == 0))
                    .subscribeWith(getMessagesObserver()));
        }
    }

    public void refreshCommunityEvents(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getCommunityEventListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getCommunityEventListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getCommunityEventsObserver()));
        }
    }

    public void refreshEventListings(boolean fromServer) {
        if (fromServer) {
            disposables.add(dataRepository.getEventInscriptionListObservable(true).subscribe());
        } else {
            disposables.add(dataRepository.getEventInscriptionListObservable(false)
                    .observeOn(Schedulers.computation())
                    .subscribeWith(getEventListingsObserver()));
        }
    }

    private DisposableObserver<User> getUsersObserver() {
        final SparseArray<User> transientUserMap = new SparseArray<>();
        return new DisposableObserver<User>() {

            @Override
            public void onNext(User user) {
                transientUserMap.put(user.getIdUser(), user);
                if (user.getIdUser() != AppData.INSTANCE.getLoggedUser().getIdUser()) {
                    nameList.add(new CommonUtils.StringWithTag(user.getName(), user.getIdUser()));
                } else {
                    AppData.INSTANCE.setLoggedUser(user);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.USERS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                userMap = transientUserMap;
                AppLogger.d("users data load complete + " + userMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.USERS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Post> getPostsObserver() {
        final SparseArray<Post> transientPostMap = new SparseArray<>();
        return new DisposableObserver<Post>() {

            @Override
            public void onNext(Post post) {
                transientPostMap.put(post.getIdPost(), post);
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.POSTS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                postMap = transientPostMap;
                AppLogger.d("posts data load complete + " + postMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.POSTS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<_Transaction> getTransactionsObserver() {
        final SparseArray<_Transaction> transientTransactionMap = new SparseArray<>();
        final List<_Transaction> transientLoggedUserOrdersList = new ArrayList<>();
        return new DisposableObserver<_Transaction>() {

            @Override
            public void onNext(_Transaction transaction) {
                transientTransactionMap.put(transaction.getIdTransaction(), transaction);
                if (transaction.getType() != _Transaction.Type.TRANSFER && transaction.getType() != _Transaction.Type.INSCRIPTION) {
                    if (AppData.INSTANCE.getLoggedUser().getIdUser() == transaction.getIdSender() || AppData.INSTANCE.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
                        transientLoggedUserOrdersList.add(transaction);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.TRANSACTIONS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                transactionMap = transientTransactionMap;
                loggedUserOrdersList = transientLoggedUserOrdersList;
                AppLogger.d("transactions data load complete + " + transactionMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.TRANSACTIONS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Listing> getListingsObserver() {
        final SparseArray<Listing> transientListingMap = new SparseArray<>();
        return new DisposableObserver<Listing>() {

            @Override
            public void onNext(Listing listing) {
                transientListingMap.put(listing.getIdListing(), listing);
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.LISTINGS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                listingMap = transientListingMap;
                AppLogger.d("listings data load complete + " + listingMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.LISTINGS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Rating> getRatingsObserver() {
        // is this first map needed?
        final SparseArray<Rating> transientRatingMap = new SparseArray<>();
        //id_user - id_sender - float
        final SparseArray<SparseArray<Float>> transientUserRatingsMap = new SparseArray<>();
        //id_user - id_category - id_sender - float
        final SparseArray<SparseArray<SparseArray<Float>>> transientWorkRatingsMap = new SparseArray<>();
        //id_listing - id_sender - float
        final SparseArray<SparseArray<Float>> transientListingRatingsMap = new SparseArray<>();
        return new DisposableObserver<Rating>() {

            @Override
            public void onNext(Rating rating) {
                transientRatingMap.put(rating.getIdRating(), rating);

                int ratingType = rating.getType();
                int idReceiver = rating.getIdReceiver();
                int idSender = rating.getIdSender();
                int idTarget = rating.getIdTarget();
                float ratingFloat = rating.getRating();

                switch (ratingType) {
                    case Rating.Type.MEMBER: {
                        SparseArray<Float> userRatingFloats = transientUserRatingsMap.get(idReceiver, new SparseArray<>());
                        userRatingFloats.put(idSender, ratingFloat);
                        transientUserRatingsMap.put(idReceiver, userRatingFloats);
                        break;
                    }
                    case Rating.Type.WORK: {
                        SparseArray<SparseArray<Float>> userWorkRatings = transientWorkRatingsMap.get(idReceiver, new SparseArray<>());
                        SparseArray<Float> workRatingFloats = userWorkRatings.get(idTarget, new SparseArray<>());
                        workRatingFloats.put(idSender, ratingFloat);
                        userWorkRatings.put(idTarget, workRatingFloats);
                        transientWorkRatingsMap.put(idReceiver, userWorkRatings);
                        break;
                    }
                    case Rating.Type.LISTING: {
                        SparseArray<Float> listingRatingFloats = transientListingRatingsMap.get(idTarget, new SparseArray<>());
                        listingRatingFloats.put(idSender, ratingFloat);
                        transientListingRatingsMap.put(idTarget, listingRatingFloats);
                        break;
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.RATINGS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                ratingMap = transientRatingMap;
                userRatingsMap = transientUserRatingsMap;
                workRatingsMap = transientWorkRatingsMap;
                listingRatingsMap = transientListingRatingsMap;
                AppLogger.d("ratings data load complete + " + ratingMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.RATINGS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Node<Category>> getCategoriesObserver() {
        SparseArray<Node<Category>> transientCategoryNodeMap = new SparseArray<>();
        return new DisposableObserver<Node<Category>>() {

            @Override
            public void onNext(Node<Category> node) {
                transientCategoryNodeMap.put(node.getId(), node);
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.CATEGORIES, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                categoryNodeMap = transientCategoryNodeMap;
                AppLogger.d("categories data load complete + " + categoryNodeMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.CATEGORIES, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Message> getMessagesObserver() {
        final SparseArray<List<Message>> transientConversationMap = new SparseArray<>();
        return new DisposableObserver<Message>() {

            @Override
            public void onNext(Message message) {
                int id_contact;
                if (AppData.INSTANCE.getLoggedUser().getIdUser() == message.getIdSender()) {
                    id_contact = message.getIdReceiver();
                } else {
                    id_contact = message.getIdSender();
                }

                List<Message> newConversation = transientConversationMap.get(id_contact, new ArrayList<>());
                newConversation.add(message);
                transientConversationMap.put(id_contact, newConversation);
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.MESSAGES, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                messagesMap = transientConversationMap;
                AppLogger.d("conversations data load complete + " + messagesMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.MESSAGES, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<CommunityEvent> getCommunityEventsObserver() {
        final SparseArray<CommunityEvent> transientCommunityEventsMap = new SparseArray<>();
        final List<CommunityEvent> transientPastCommunityEventsList = new ArrayList<>();
        final List<CommunityEvent> transientCurrentCommunityEventsList = new ArrayList<>();
        final List<CommunityEvent> transientScheduledCommunityEventsList = new ArrayList<>();
        return new DisposableObserver<CommunityEvent>() {

            @Override
            public void onNext(CommunityEvent communityEvent) {
                transientCommunityEventsMap.put(communityEvent.getIdEvent(), communityEvent);
                Date startDate = DateUtils.parseFromBackendLocale(communityEvent.getDateStart());
                Date endDate = DateUtils.parseFromBackendLocale(communityEvent.getDateEnd());
                if (endDate.before(DateUtils.now())) {
                    transientPastCommunityEventsList.add(communityEvent);
                } else if (startDate.before(DateUtils.now())) {
                    transientCurrentCommunityEventsList.add(communityEvent);
                } else if (startDate.after(DateUtils.now())) {
                    transientScheduledCommunityEventsList.add(communityEvent);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.COMMUNITY_EVENT, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                communityEventMap = transientCommunityEventsMap;
                pastCommunityEventsList = transientPastCommunityEventsList;
                currentCommunityEventsList = transientCurrentCommunityEventsList;
                scheduledCommunityEventsList = transientScheduledCommunityEventsList;
                Collections.reverse(pastCommunityEventsList);
                Collections.reverse(currentCommunityEventsList);
                Collections.reverse(scheduledCommunityEventsList);
                // TODO: this is a temporal design, must be reworked allowing to stay in past events modes
                if (App.getInstance().getPreferencesHelper().getSelectedEventId() >= 0 && currentCommunityEventsList.isEmpty()) {
                    App.getInstance().getPreferencesHelper().setSelectedEventId(null);
                    FilterEvent event = new FilterEvent(FilterEvent.EVENT_MODE_EXIT);
                    EventBus.getDefault().post(event);
                }
                AppLogger.d("events data load complete + " + communityEventMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.COMMUNITY_EVENT, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    private DisposableObserver<Inscription> getEventListingsObserver() {
        final SparseArray<SparseArray<Inscription>> transientEventInscriptionMap = new SparseArray<>();
        final SparseArray<SparseArray<JSONObject>> transientEventListingIdList = new SparseArray<>();
        final SparseArray<Inscription> transientUserEventInscriptionMap = new SparseArray<>();
        return new DisposableObserver<Inscription>() {

            @Override
            public void onNext(Inscription inscription) {
                int index = transientEventInscriptionMap.indexOfKey(inscription.getIdEvent());
                if (index < 0) {
                    SparseArray<Inscription> eventInscriptionMap = new SparseArray<>();
                    eventInscriptionMap.put(inscription.getIdUser(), inscription);
                    transientEventInscriptionMap.put(inscription.getIdEvent(), eventInscriptionMap);
                } else {
                    SparseArray<Inscription> eventInscriptionMap = transientEventInscriptionMap.valueAt(index);
                    eventInscriptionMap.put(inscription.getIdUser(), inscription);
                }

                if (inscription.getIdUser() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
                    transientUserEventInscriptionMap.put(inscription.getIdEvent(), inscription);
                }
                if (!inscription.getListings().isEmpty()) {
                    try {
                        JSONArray jsonArray = new JSONArray(inscription.getListings());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject eventListingJsonObject = jsonArray.getJSONObject(i);
                            int listingId = Integer.valueOf((String) eventListingJsonObject.get("id_listing"));
                            int index2 = transientEventListingIdList.indexOfKey(inscription.getIdEvent());
                            if (index2 < 0) {
                                SparseArray<JSONObject> eventListingIdMap = new SparseArray<>();
                                eventListingIdMap.put(listingId, eventListingJsonObject);
                                transientEventListingIdList.put(inscription.getIdEvent(), eventListingIdMap);
                            } else {
                                SparseArray<JSONObject> eventListingIdMap = transientEventListingIdList.valueAt(index2);
                                eventListingIdMap.put(listingId, eventListingJsonObject);
                            }
                        }
                    } catch (JSONException e) {
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.EVENT_INSCRIPTIONS, false);
                    EventBus.getDefault().post(event);
                }
                AppLogger.e(e, null);
            }

            @Override
            public void onComplete() {
                eventInscriptionMap = transientEventInscriptionMap;
                eventListingIdList = transientEventListingIdList;
                userEventInscriptionMap = transientUserEventInscriptionMap;
                AppLogger.d("event inscriptions data load complete + " + eventInscriptionMap.size());
                if (EventBus.getDefault().hasSubscriberForEvent(DataLoadedEvent.class)) {
                    DataLoadedEvent event = new DataLoadedEvent(DataLoadedEvent.EVENT_INSCRIPTIONS, true);
                    EventBus.getDefault().post(event);
                }
            }
        };
    }

    public List<CommonUtils.StringWithTag> getNameList() {
        return nameList;
    }

    public SparseArray<User> getUserMap() {
        return userMap;
    }

    public SparseArray<Post> getPostMap() {
        return postMap;
    }

    public SparseArray<_Transaction> getTransactionMap() {
        return transactionMap;
    }

    public SparseArray<Rating> getRatingsMap() {
        return ratingMap;
    }

    public SparseArray<Listing> getListingsMap() {
        return listingMap;
    }

    public SparseArray<Node<Category>> getCategoryNodeMap() {
        return categoryNodeMap;
    }

    public SparseArray<List<Message>> getMessagesMap() {
        return messagesMap;
    }

    @NonNull
    public User getUser(int id) {
        return userMap.get(id);
    }

    @NonNull
    public Post getPost(int id) {
        return postMap.get(id);
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationReceived(NotificationReceivedEvent event) {
        switch (event.getEventCode()) {
            case NotificationReceivedEvent.MESSAGE_RECEIVED: {
                refreshConversations(true);
            }
            case NotificationReceivedEvent.RATING_RECEIVED: {
                refreshRatings(true);
                break;
            }
            case NotificationReceivedEvent.TRANSACTION_UPDATED: {
                refreshUsers(true);
                refreshTransactions(true);
                refreshListings(true);
                break;
            }
            case NotificationReceivedEvent.TRANSFER_RECEIVED: {
                refreshUsers(true);
                refreshTransactions(true);
                break;
            }
            case NotificationReceivedEvent.LIKE_RECEIVED: {
                refreshPosts(true);
                break;
            }
            case NotificationReceivedEvent.COMMENT_RECEIVED: {
                refreshPosts(true);
                break;
            }
        }
    }

    //TODO: study this
    public void toggleUpdates() {
        loadData(true);
        CommonUtils.showSuccessToast("Actualizado");

        /*if (!disposables.isDisposed()) {
            disposables.dispose();
        } else {
            startDataLoad();
        }*/
    }

    public SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return userRatingsMap;
    }

    public SparseArray<SparseArray<SparseArray<Float>>> getWorkRatingsMap() {
        return workRatingsMap;
    }

    public SparseArray<SparseArray<Float>> getListingRatingsMap() {
        return listingRatingsMap;
    }

    public List<_Transaction> getLoggedUserOrdersList() {
        return loggedUserOrdersList;
    }

    public List<_Transaction> getUserOrdersList(int userId) {
        List<_Transaction> userOrdersList = new ArrayList<>();
        for (int i = getTransactionMap().size() - 1; i > -1; i--) {
            _Transaction transaction = getTransactionMap().valueAt(i);
            if (transaction.getType() != _Transaction.Type.TRANSFER) {
                if (userId == transaction.getIdSender() || userId == transaction.getIdReceiver()) {
                    userOrdersList.add(transaction);
                }
            }
        }
        return userOrdersList;
    }

    private double getUserReserveFunds(int userId) {
        double reserveFunds = 0;
        for (_Transaction transaction : getUserOrdersList(userId)) {
            if (userId == transaction.getIdSender() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.REQUESTED || transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    reserveFunds += transaction.getAmount();
                }
            }
            if (userId == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    reserveFunds += transaction.getAmount();
                }
            }
        }
        return reserveFunds;
    }

    private double getUserIncomingFunds(int userId) {
        double incomingFunds = 0;
        for (_Transaction transaction : getUserOrdersList(userId)) {
            if (userId == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    incomingFunds += transaction.getAmount();
                }
            }
            if (userId == transaction.getIdSender() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    incomingFunds += transaction.getAmount();
                }
            }
        }
        return incomingFunds;
    }

    public double getUserMaxAvailableIncome(int userId) {
        return userMap.get(userId).getPaymentMargin() - userMap.get(userId).getFunds() - getUserReserveFunds(userId) - getUserIncomingFunds(userId);
    }

    public void clearSubscriptions() {
        // Null checks for pending intent loading
        if (disposables != null) {
            disposables.clear();
        }
    }

    public void disposeSubscriptions() {
        // Null checks for pending intent loading
        if (disposables != null) {
            disposables.dispose();
        }
    }

    public void clearData() {
        boxStore.close();
        boxStore.deleteAllFiles();
        boxStore = null;
        App.getInstance().getPreferencesHelper().setCachedDataType(null);
        App.getInstance().getDataManager().clearSubscriptions();
        DataFilters.clear();
    }

    public SparseArray<CommunityEvent> getCommunityEventMap() {
        return communityEventMap;
    }

    public List<CommunityEvent> getPastCommunityEventsList() {
        return pastCommunityEventsList;
    }

    public List<CommunityEvent> getCurrentCommunityEventsList() {
        return currentCommunityEventsList;
    }

    public List<CommunityEvent> getScheduledCommunityEventsList() {
        return scheduledCommunityEventsList;
    }

    public SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return eventInscriptionMap;
    }

    public SparseArray<SparseArray<JSONObject>> getEventListingIdList() {
        return eventListingIdList;
    }

    public SparseArray<Inscription> getUserEventInscriptionMap() {
        return userEventInscriptionMap;
    }

    public SparseArray<JSONObject> getTransientUserEventListings() {
        return transientUserEventListings;
    }

    public void setTransientUserEventListings(SparseArray<JSONObject> transientUserEventListings) {
        this.transientUserEventListings = transientUserEventListings;
    }
}