package eco.darmas.activities.post;

interface PostContracts {

    interface Activity {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}
