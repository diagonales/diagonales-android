package eco.darmas.activities.post;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;

public class PostActivity extends BaseActivity implements PostContracts.Activity {

    @Override
    protected PostModel createModel(@NonNull DataManager dataManager, Bundle savedInstanceState) {
        return new PostModel(dataManager, savedInstanceState, getIntent());
    }

    @Override
    protected PostPresenter createPresenter(@NonNull BaseActivityModel model, @NonNull BaseActivityView view) {
        return new PostPresenter((PostModel) model, (PostView) view);
    }

    @Override
    protected PostView createView(@NonNull Context context) {
        return new PostView((PostActivity) context);
    }
}