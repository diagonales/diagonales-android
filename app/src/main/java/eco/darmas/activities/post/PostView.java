package eco.darmas.activities.post;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.pojos.Comment;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

@SuppressLint("ViewConstructor")
class PostView extends BaseActivityView implements PostContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private PostActivity mContext;
    private PostPresenter mPresenter;
    private CommentsAdapter flexibleCommentsAdapter;
    private boolean scrollTop;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_front)
    ImageView iv_front;
    @BindView(R.id.tv_text)
    TextView tv_text;
    @BindView(R.id.iv_creator)
    ImageView iv_creator;
    @BindView(R.id.tv_creator)
    TextView tv_creator;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.iv_delete_post)
    ImageView iv_delete_post;
    @BindView(R.id.tv_likes)
    TextView tv_likes;
    @BindView(R.id.iv_comments)
    ImageView iv_comments;
    @BindView(R.id.tv_comments)
    TextView tv_comments;
    @BindView(R.id.iv_share)
    ImageView iv_share;
    @BindView(R.id.iv_report_post)
    ImageView iv_report_post;
    CheckBox cb_like;
    @BindView(R.id.tv_comments_none)
    TextView tv_comments_none;
    @BindView(R.id.iv_toggle_pinned)
    ImageView iv_toggle_pinned;
    @BindView(R.id.iv_delete)
    ImageView iv_delete;
    @BindView(R.id.iv_delete_permanent)
    ImageView iv_delete_permanent;
    @BindView(R.id.rb_order)
    SimpleRatingBar rb_order;
    @BindView(R.id.iv_order)
    ImageView iv_order;
    @BindView(R.id.iv_direction)
    ImageView iv_direction;
    @BindView(R.id.rv_comments)
    RecyclerView rv_comments;

    protected PostView(PostActivity context) {
        super(context);
        this.mContext = context;
        inflate(mContext, R.layout.post_full_screen, this);
    }

    @Override
    public void setupActionBar(ActionBar supportActionBar) {
    }

    protected void initialize(BaseActivityPresenter presenter) {
        cb_like = findViewById(R.id.cb_like);
        this.mPresenter = (PostPresenter) presenter;

        User creator = mPresenter.getUser(mPresenter.getPost().getIdCreator());
        tv_creator.setText(creator.getName());

        cb_like.setChecked(CommonUtils.containsId(mPresenter.getPostLikes(), AppData.INSTANCE.getLoggedUser().getIdUser()));
        if (mPresenter.getPost().getIdCreator() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
            setLikeButtonEnabled(false);
            Drawable mDrawable = ContextCompat.getDrawable(mContext, R.drawable.ic_favorite_border_disabled);
            cb_like.setBackground(mDrawable);
        } else {
            cb_like.setOnTouchListener((v, event) -> {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    return mPresenter.checksOnLikeClick();
                }
                return false;
            });
            cb_like.setOnCheckedChangeListener((compoundButton, b) -> mPresenter.onLikeClicked(b));
        }

        if (!mPresenter.getPost().getImage().isEmpty()) {
            final float scale = mContext.getResources().getDisplayMetrics().density;
            int pixels = (int) (216 * scale + 0.5f);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels);
            iv_front.setLayoutParams(layoutParams);
            ImageUtils.loadPostImage(mPresenter.getPost(), iv_front);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iv_front.setLayoutParams(layoutParams);
        }

        tv_title.setText(mPresenter.getPost().getTitle());
        tv_text.setText(mPresenter.getPost().getText());
        tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mPresenter.getPost().getDate()), AppConstants.COMPLETE_DATE_FORMAT));
        ImageUtils.loadRoundedProfileImage(mPresenter.getUser(mPresenter.getPost().getIdCreator()), iv_creator);

        if (mPresenter.getPost().getIdCreator() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
            iv_delete_post.setVisibility(View.VISIBLE);
        } else if (mPresenter.getPost().getAnnounce().equals("0")) {
            iv_report_post.setVisibility(View.VISIBLE);
        }

        tv_likes.setText(String.valueOf(mPresenter.getPostLikes().length()));
        tv_comments.setText(mPresenter.getPost().getComments());

        // Admin privileges
        if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
            iv_delete.setVisibility(VISIBLE);
            iv_delete.setOnClickListener(view -> {
                CustomDialog confirmDialog = new CustomDialog(mContext);
                if (mPresenter.getPost().getAnnounce().equals("1")) {
                    confirmDialog.setTitle("Deseas remover este anuncio?");
                } else {
                    confirmDialog.setTitle("Deseas remover este post?");
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onDeletePostClicked());
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            iv_delete_permanent.setVisibility(VISIBLE);
            iv_delete_permanent.setOnClickListener(view -> {
                CustomDialog confirmDialog = new CustomDialog(mContext);
                if (mPresenter.getPost().getAnnounce().equals("1")) {
                    confirmDialog.setTitle("Deseas eliminar este anuncio permanentemente?");
                } else {
                    confirmDialog.setTitle("Deseas eliminar este post permanentemente?");
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onDeletePostClicked());
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            if (mPresenter.getPost().getPinned().equals("0")) {
                iv_toggle_pinned.setImageResource(R.drawable.ic_action_pinn_admin);
            } else {
                iv_toggle_pinned.setImageResource(R.drawable.ic_action_unpinn_admin);
            }
            iv_toggle_pinned.setVisibility(VISIBLE);
            iv_toggle_pinned.setOnClickListener(view -> mPresenter.onTogglePinnedClicked());
        }
        setupAdapters();
        setData();
    }

    @OnClick(R.id.li_order)
    void onOrderClickListener() {
        mPresenter.onOrderClicked();
    }

    @OnClick(R.id.iv_share)
    void onShareClickListener() {
        mPresenter.onShareClicked();
    }

    void setData() {
        tv_comments.setText(mPresenter.getPost().getComments());
        LinearLayout.LayoutParams arrowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        switch (mPresenter.getSelectedOrder()) {
            case AppConstants.BY_MEMBER_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_LIKES_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_favorite_outline_red);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
        }
    }

    void setupAdapters() {
        rv_comments.setHasFixedSize(false);
        rv_comments.setNestedScrollingEnabled(false);

        // use a linear layout manager
        LinearLayoutManager lm_comments = new LinearLayoutManager(getContext());
        rv_comments.setLayoutManager(lm_comments);
        rv_comments.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.comment, 15)
                .withBottomEdge(true));

        // Initialize the Adapter
        flexibleCommentsAdapter = new CommentsAdapter(getFlexibleItemList(mPresenter.getPostCommentList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_comments.setAdapter(flexibleCommentsAdapter);

        flexibleCommentsAdapter
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    @OnClick({R.id.iv_creator, R.id.tv_creator})
    void onProfileClickListener() {
        if (AppData.INSTANCE.getLoggedUser().getIdUser() != mPresenter.getPost().getIdCreator()) {
            User contact = mPresenter.getUser(mPresenter.getPost().getIdCreator());
            Object[] parcelables = new Object[]{contact};
            AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, mContext, parcelables);
        }
    }

    void dataSetChanged(boolean scrollTop) {
        setData();
        this.scrollTop = scrollTop;
        flexibleCommentsAdapter.updateDataSet(getFlexibleItemList(mPresenter.getPostCommentList()));
    }

    void setLikeButtonEnabled(boolean enabled) {
        cb_like.setEnabled(enabled);
    }

    void setLikesCount() {
        tv_likes.setText(String.valueOf(mPresenter.getPostLikes().length()));
    }

    void updatePostView() {
        if (!mPresenter.getPost().getImage().isEmpty()) {
            final float scale = mContext.getResources().getDisplayMetrics().density;
            int pixels = (int) (216 * scale + 0.5f);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels);
            iv_front.setLayoutParams(layoutParams);
            ImageUtils.loadPostImage(mPresenter.getPost(), iv_front);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iv_front.setLayoutParams(layoutParams);
        }

        tv_title.setText(mPresenter.getPost().getTitle());
        tv_text.setText(mPresenter.getPost().getText());
        tv_likes.setText(String.valueOf(mPresenter.getPostLikes().length()));
        tv_comments.setText(mPresenter.getPost().getComments());
        ImageUtils.loadRoundedProfileImage(mPresenter.getUser(mPresenter.getPost().getIdCreator()), iv_creator);
    }

    @OnClick(R.id.li_add_comment)
    void onAddCommentsClickListener() {
        if (mPresenter.isInEventMode() && !mPresenter.isInscribedInEventMode()) {
            CommonUtils.showErrorToast("No puedes comentar sin haberte inscripto en este evento");
            return;
        }
        // add comment
        CustomDialog commentDialog = new CustomDialog(mContext);
        commentDialog.setTitle("Agregar comentario");

        final EditText et_comment = new EditText(mContext);

        commentDialog.setView(et_comment);
        commentDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Enviar", (dialog, which) -> {
                // Reset errors.
                et_comment.setError(null);

                // Store values at the time of the comment creation attempt.
                String comment = et_comment.getText().toString();

                boolean cancel = false;
                View focusView = null;

                // Check for a valid comment, if the user entered one.
                if (TextUtils.isEmpty(comment)) {
                    et_comment.setError("Escribe un comentario");
                    focusView = et_comment;
                    cancel = true;
                }

                if (cancel) {
                    // There was an error; don't attempt to send the comment and focus the
                    // comment field with an error.
                    focusView.requestFocus();
                } else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user password change attempt.
                    // showProgress(true);

                    mPresenter.createComment(comment);
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                    dialog.dismiss();
                }
        });
        commentDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "Cancelar", (dialog, which) -> dialog.dismiss());
        commentDialog.show();
    }

    @OnClick(R.id.iv_delete_post)
    void onDeletePostClickListener() {
        CustomDialog confirmDialog = new CustomDialog(mContext);
        if (mPresenter.getPost().getAnnounce().equals("1")) {
            confirmDialog.setTitle("Deseas remover este anuncio?");
        } else {
            confirmDialog.setTitle("Deseas remover este post?");
        }
        confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
            mPresenter.onDeletePostClicked();
            dialog.dismiss();
        });
        confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
        confirmDialog.show();
    }

    @Override
    public boolean onItemClick(View view2, int position) {
        LayoutInflater factory = LayoutInflater.from(mContext);
        final View v = factory.inflate(R.layout.comment_menu, this, false);

        CustomDialog commentDialog = new CustomDialog(mContext);
        commentDialog.setView(v);

        final ImageView iv_comment_like = v.findViewById(R.id.iv_like_comment);
        final ImageView iv_deleteComment = v.findViewById(R.id.iv_delete_comment);
        final ImageView iv_reportComment = v.findViewById(R.id.iv_report_comment);
        final ImageView iv_delete = v.findViewById(R.id.iv_delete);
        final ImageView iv_delete_permanent = v.findViewById(R.id.iv_delete_permanent);

        if (mPresenter.getPostCommentList().get(position).getIdCreator() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
            iv_deleteComment.setVisibility(View.VISIBLE);
            iv_reportComment.setVisibility(View.GONE);
            iv_comment_like.setEnabled(false);
            Drawable mDrawable = ContextCompat.getDrawable(mContext, R.drawable.like_icon);
            if (mDrawable != null) {
                mDrawable.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.secondaryTextColor), PorterDuff.Mode.SRC_IN));
            }
            iv_comment_like.setImageDrawable(mDrawable);

            iv_deleteComment.setOnClickListener(view1 -> {
                final CustomDialog confirmDeleteDialog = new CustomDialog(mContext);
                confirmDeleteDialog.setTitle("Deseas remover este comentario?");
                confirmDeleteDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
                    mPresenter.deleteComment(position);
                    confirmDeleteDialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDeleteDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> {
                    confirmDeleteDialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDeleteDialog.show();
            });
        } else {
            iv_deleteComment.setVisibility(View.GONE);
            iv_reportComment.setVisibility(View.VISIBLE);

            Comment comment = mPresenter.getPostCommentList().get(position);
            JSONArray commentLikesJsonArray = new JSONArray();
            if (!comment.getLikes().isEmpty()) {
                try {
                    commentLikesJsonArray = new JSONArray(comment.getLikes());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            }
            iv_comment_like.setEnabled(true);

            if (!CommonUtils.containsId(commentLikesJsonArray, AppData.INSTANCE.getLoggedUser().getIdUser())) {
                iv_comment_like.setImageResource(R.drawable.ic_favorite_outline_white);
            } else {
                iv_comment_like.setImageResource(R.drawable.ic_favorite_outline_red);
            }

            iv_comment_like.setOnClickListener(view -> {
                if (mPresenter.commentLikeClicked(position)) {
                    commentDialog.dismiss();
                }
            });

            iv_reportComment.setOnClickListener(view -> {
                final CustomDialog reportDialog = new CustomDialog(mContext);
                reportDialog.setTitle("Deseas reportar este comentario?");
                reportDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
                    //TODO: mPresenter.reportComment();
                    CommonUtils.showSuccessToast("Comentario reportado");
                    reportDialog.dismiss();
                    commentDialog.dismiss();
                });
                reportDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> {
                    reportDialog.dismiss();
                    commentDialog.dismiss();
                });
                reportDialog.show();
            });
        }

        // Admin privileges
        if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
            iv_delete.setVisibility(VISIBLE);
            iv_delete.setOnClickListener(view -> {
                CustomDialog confirmDialog = new CustomDialog(mContext);
                confirmDialog.setTitle("Deseas remover este comentario?");
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
                    mPresenter.deleteComment(position);
                    dialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> {
                    dialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDialog.show();
            });
            iv_delete_permanent.setVisibility(VISIBLE);
            iv_delete_permanent.setOnClickListener(view -> {
                CustomDialog confirmDialog = new CustomDialog(mContext);
                confirmDialog.setTitle("Deseas eliminar este comentario permanentemente?");
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
                    mPresenter.deleteComment(position);
                    dialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> {
                    dialog.dismiss();
                    commentDialog.dismiss();
                });
                confirmDialog.show();
            });
        } else {
            iv_delete.setVisibility(GONE);
            iv_delete_permanent.setVisibility(GONE);
        }

        commentDialog.show();
        return true;
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_comments.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_comments_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_comments.scrollToPosition(0);
        }
    }

    class CommentsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        CommentsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class CommentItemAdapter extends AbstractFlexibleItem<CommentItemAdapter.ViewHolder> {

        private Comment comment;

        CommentItemAdapter(Comment comment) {
            this.comment = comment;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof CommentItemAdapter) {
                Comment inItem = ((CommentItemAdapter) inObject).comment;
                return this.comment.getIdComment() == inItem.getIdComment();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return comment.getIdComment();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.comment;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            User creator = mPresenter.getUser(comment.getIdCreator());

            viewHolder.tv_creator.setText(creator.getName());
            viewHolder.tv_comment.setText(comment.getComment());
            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(comment.getDate()), AppConstants.COMPLETE_DATE_FORMAT));
            viewHolder.iv_badge_new.setVisibility(DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(creator.getDate())) ? View.GONE : View.VISIBLE);
            ImageUtils.loadRoundedProfileImage(creator, viewHolder.iv_creator);

            JSONArray commentLikesJsonArray = new JSONArray();
            if (!comment.getLikes().isEmpty()) {
                try {
                    commentLikesJsonArray = new JSONArray(comment.getLikes());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            }
            viewHolder.tv_comment_likes.setText(String.valueOf(commentLikesJsonArray.length()));

            if (creator.getIdUser() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
                viewHolder.iv_comment_likes.setImageResource(R.drawable.ic_favorite_border_disabled);
            } else {
                if (CommonUtils.containsId(commentLikesJsonArray, AppData.INSTANCE.getLoggedUser().getIdUser())) {
                    viewHolder.iv_comment_likes.setImageResource(R.drawable.ic_favorite_outline_red);
                } else {
                    viewHolder.iv_comment_likes.setImageResource(R.drawable.ic_favorite_outline_white);
                }
            }

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != creator.getIdUser()) {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{creator};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, mContext, parcelables);
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> viewHolder.iv_creator.performClick());
            } else {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    //
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_comment)
            TextView tv_comment;
            @BindView(R.id.iv_creator)
            ImageView iv_creator;
            @BindView(R.id.tv_creator)
            TextView tv_creator;
            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.iv_comment_likes)
            ImageView iv_comment_likes;
            @BindView(R.id.tv_comment_likes)
            TextView tv_comment_likes;
            @BindView(R.id.iv_badge_new)
            ImageView iv_badge_new;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleItemList(List<Comment> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Comment item : list) {
            itemList.add(new CommentItemAdapter(item));
        }
        return itemList;
    }
}