package eco.darmas.activities.post;

import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Comment;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class PostPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> implements PostContracts.Presenter {

    private final PostModel mModel;
    private final PostView mView;

    PostPresenter(PostModel mModel, PostView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onPostCreate() {
        super.onPostCreate();
        updateComments(0, 0, false);
    }

    private void updateComments(int start, int quantity, boolean scrollTop) {
        getDisposables().add(mModel.fetchPostComments(start, quantity)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> mView.dataSetChanged(scrollTop))
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        CommonUtils.showErrorToast("Los comentarios no han sido cargados, el servidor no parece estar respondiendo");
                    } else {
                        CommonUtils.showErrorToast("Los comentarios no han sido cargados, hubo un error en el servidor");
                    }
                    AppLogger.e(throwable, null);
                })
                .subscribe());
    }

    void createComment(String comment) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            return;
        }
        mModel.commitCreateComment(comment, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject JsonResponse;
                    try {
                        JsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (JsonResponse.get("result").equals("true")) {
                            int id = (int) JsonResponse.get("id");
                            mModel.writeCreateComment(id, comment);
                            mView.dataSetChanged(false);
                            CommonUtils.showSuccessToast("Comentario agregado!");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El comentario no pudo ser agregado, el servidor no parece estar respondiendo");
                } else {
                    CommonUtils.showErrorToast("El comentario no pudo ser agregado debido a un error");
                }
                AppLogger.e(t, null);
            }
        });
    }

    void deleteComment(int position) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            return;
        }
        mModel.commitDeleteComment(position, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject JsonResponse;
                    try {
                        JsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (JsonResponse.get("result").equals("true")) {
                            mModel.writeDeleteComment(position);
                            mView.dataSetChanged(false);
                            CommonUtils.showSuccessToast("Comentario eliminado!");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                }
                AppLogger.e(t, null);
            }
        });
    }

    boolean checksOnLikeClick() {
        if (isInEventMode() && !isInscribedInEventMode()) {
            CommonUtils.showErrorToast("No puedes interactuar sin haberte inscripto en este evento");
            return true;
        }
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            return true;
        }
        return false;
    }

    void onLikeClicked(boolean checked) {
        mView.setLikeButtonEnabled(false);
        mModel.setLike(checked);
        mView.setLikesCount();
        mModel.commitUpdateLikeState(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject JsonResponse;
                    try {
                        JsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (JsonResponse.get("result").equals("true")) {
                            mView.setLikeButtonEnabled(true);
                            mModel.writeLikes();
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mView.setLikeButtonEnabled(true);
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("Error en el servidor, por favor contacta un administrador");
                }
                AppLogger.e(t, null);
            }
        });
    }

    void onTogglePinnedClicked() {
        if (NetworkUtils.isNetworkConnected(mView.getContext())) {
            String pinned = mModel.getPost().getPinned().equals("0") ? "1" : "0";
            ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.POST_MODIFIED);
            mModel.commitTogglePinnedPost(pinned, new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    JSONObject JSONResponse;
                    try {
                        JSONResponse = new JSONObject(new String(response.body().bytes()));
                        if (JSONResponse.get("result").equals("true")) {
                            event.setSuccess(true);
                            mModel.writeTogglePinnedPost(pinned);
                            CommonUtils.showSuccessToast(pinned.equals("0") ? "Post desanclado" : "Post anclado");
                        } else {
                            CommonUtils.showSuccessToast(pinned.equals("0") ? "El Post no pudo ser desanclado" : "El Post no pudo ser anclado");
                        }
                    } catch (Exception e) {
                        CommonUtils.showSuccessToast(pinned.equals("0") ? "Error al desanclar el post" : "Error al anclar el post");
                        AppLogger.e(e, null);
                    } finally {
                        EventBus.getDefault().postSticky(event);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    CommonUtils.showSuccessToast(pinned.equals("0") ? "Error al desanclar el post" : "Error al anclar el post");
                    CommonUtils.showSuccessToast("Hubo un error en el servidor, por favor contacta un administrador");
                    if (t instanceof SocketTimeoutException) {
                        CommonUtils.showSuccessToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                    }
                    AppLogger.e(t, null);
                }
            });
            CommonUtils.dismissActivity(mView, true);
        } else {
            CommonUtils.showErrorToast("Comprueba tu conección");
        }
    }

    void onDeletePostClicked() {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            return;
        }
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.POST_MODIFIED);
        mModel.commitDeletePost(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject JSONResponse;
                try {
                    JSONResponse = new JSONObject(new String(response.body().bytes()));
                    if (JSONResponse.get("result").equals("true")) {
                        mModel.writeDeletePost();
                        event.setSuccess(true);
                        event.setDialogTitle("El post ha sido eliminado");
                    } else {
                        event.setDialogTitle("El post no pudo ser eliminado");
                    }
                } catch (Exception e) {
                    event.setDialogTitle("Error al eliminar el post");
                    event.setDialogMessage("Por favor contacta un administrador");
                    AppLogger.e(e, null);
                } finally {
                    EventBus.getDefault().postSticky(event);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                event.setDialogTitle("Error al eliminar el post");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().postSticky(event);
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissActivity(mView, true);
    }

    void commentLikeAdded(int position) {
        // add comment like in client
        JSONArray commentLikesJsonArray = new JSONArray();
        if (mModel.getPostLikes().length() > 0) {
            try {
                commentLikesJsonArray = new JSONArray(mModel.getPostComment(position).getLikes());
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
        commentLikesJsonArray.put(String.valueOf(mModel.getLoggedUser().getIdUser()));
        Comment comment = getPostCommentList().get(position);
        comment.setLikes(commentLikesJsonArray.toString());
        mView.dataSetChanged(false);
        // add comment like in server
        mModel.commitCommentLikesUpdate(comment);
    }

    void commentLikeRemoved(int position) {
        // remove comment like in client
        JSONArray commentLikesJsonArray = new JSONArray();
        try {
            commentLikesJsonArray = new JSONArray(mModel.getPostComment(position).getLikes());
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }
        List<String> list = new ArrayList<>();
        try {
            for (int i = 0, len = commentLikesJsonArray.length(); i < len; i++) {
                String val = commentLikesJsonArray.getString(i);
                if (!val.equals(String.valueOf(mModel.getLoggedUser().getIdUser()))) {
                    list.add(val);
                }
            }
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }
        commentLikesJsonArray = new JSONArray(list);
        Comment comment = getPostCommentList().get(position);
        comment.setLikes(commentLikesJsonArray.toString());
        mView.dataSetChanged(false);
        // remove comment like in server
        mModel.commitCommentLikesUpdate(comment);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoaded(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.POSTS: {
                if (event.isSuccess()) {
                    //TODO: check likes added or removed in real time
                    mModel.reloadPost()
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnComplete(() -> {
                                mView.updatePostView();
                                updateComments(0, 0, false);
                            })
                            .subscribe();
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    mModel.reloadPost()
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnComplete(() -> {
                                mView.updatePostView();
                                //updateComments(0, 0, false);
                            })
                            .subscribe();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.POST_COMMENTS_FILTERED: {
                mView.dataSetChanged(true);
                /*mModel.reloadPost()
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnComplete(() -> {
                            mView.updatePostView();
                            updateComments(0, 0, true);
                        })
                        .subscribe();*/
            }
        }
    }

    Post getPost() {
        return mModel.getPost();
    }

    List<Comment> getPostCommentList() {
        List<Comment> postCommentList = mModel.getPostCommentList();
        Collections.sort(postCommentList, getComparator());
        return postCommentList;
    }

    Comparator<Comment> getComparator() {
        return (t1, t2) -> {
            switch (mModel.getSelectedOrder()) {
                case AppConstants.BY_MEMBER_RATING_DESC:
                    float descMemberRating1 = getUserRating(t1.getIdCreator());
                    float descMemberRating2 = getUserRating(t2.getIdCreator());
                    if (descMemberRating1 > descMemberRating2) {
                        return -1;
                    } else if (descMemberRating1 == descMemberRating2) {
                        return commentDateCompare(t1, t2, true);
                    } else if (descMemberRating1 < descMemberRating2) {
                        return 1;
                    }
                case AppConstants.BY_MEMBER_RATING_ASC:
                    float ascMemberRating1 = getUserRating(t1.getIdCreator());
                    float ascMemberRating2 = getUserRating(t2.getIdCreator());
                    if (ascMemberRating1 < ascMemberRating2) {
                        return -1;
                    } else if (ascMemberRating1 == ascMemberRating2) {
                        return commentDateCompare(t1, t2, true);
                    } else if (ascMemberRating1 > ascMemberRating2) {
                        return 1;
                    }
                case AppConstants.BY_LIKES_DESC:
                    int memberLikes1 = 0;
                    if (!t1.getLikes().isEmpty()) {
                        try {
                            memberLikes1 = (new JSONArray(t1.getLikes()).length());
                        } catch (JSONException e) {
                            AppLogger.e(e, null);
                        }
                    }
                    int memberLikes2 = 0;
                    if (!t2.getLikes().isEmpty()) {
                        try {
                            memberLikes2 = (new JSONArray(t2.getLikes()).length());
                        } catch (JSONException e) {
                            AppLogger.e(e, null);
                        }
                    }
                    if (memberLikes1 > memberLikes2) {
                        return -1;
                    } else if (memberLikes1 == memberLikes2) {
                        return commentDateCompare(t1, t2, true);
                    } else if (memberLikes1 < memberLikes2) {
                        return 1;
                    }
                case AppConstants.BY_CREATION_DATE_DESC:
                    return commentDateCompare(t1, t2, true);
                case AppConstants.BY_CREATION_DATE_ASC:
                    return commentDateCompare(t1, t2, false);
            }
            return 0;
        };
    }

    private int commentDateCompare(Comment t1, Comment t2, boolean lower) {
        Date descDate1 = DateUtils.parseFromBackendLocale(t1.getDate());
        Date descDate2 = DateUtils.parseFromBackendLocale(t2.getDate());
        if (lower) {
            if (descDate1.after(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (descDate1.before(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    private float getUserRating(int userId) {
        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getUserRatingsMap().get(userId, new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    JSONArray getPostLikes() {
        return mModel.getPostLikes();
    }

    int getSelectedOrder() {
        return mModel.getSelectedOrder();
    }

    void onOrderClicked() {
        AppNav.navigateToFragment(AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT, (AppCompatActivity) mView.getContext());
    }

    void onShareClicked() {
        CommonUtils.showToast("Función próxima a implementar");
    }

    boolean commentLikeClicked(int position) {
        if (isInEventMode() && !isInscribedInEventMode()) {
            CommonUtils.showErrorToast("No puedes interactuar sin haberte inscripto en este evento");
            return false;
        }
        Comment comment = mModel.getPostCommentList().get(position);

        JSONArray commentLikesJsonArray = new JSONArray();
        if (!comment.getLikes().isEmpty()) {
            try {
                commentLikesJsonArray = new JSONArray(comment.getLikes());
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }

        if (!CommonUtils.containsId(commentLikesJsonArray, AppData.INSTANCE.getLoggedUser().getIdUser())) {
            commentLikeAdded(position);
        } else {
            commentLikeRemoved(position);
        }
        return true;
    }
}