package eco.darmas.activities.post;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;

import com.thirtydegreesray.dataautoaccess.annotation.AutoAccess;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Comment;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.DateUtils;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class PostModel extends BaseActivityModel implements PostContracts.Model {

    @AutoAccess
    Post post;

    private JSONArray postLikes = new JSONArray();
    private List<Comment> postCommentList = new ArrayList<>();

    PostModel(DataManager dataManager, Bundle savedInstanceState, Intent intent) {
        super(dataManager, savedInstanceState);
        AppData.INSTANCE.loadInstanceState(savedInstanceState);
        getDataManager().startDataLoad();

        if (savedInstanceState == null) {
            post = intent.getParcelableExtra("parcelable#0");
        }
        if (!post.getLikes().isEmpty()) {
            try {
                postLikes = new JSONArray(post.getLikes());
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
    }

    @Override
    protected void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        AppData.INSTANCE.saveInstanceState(outState);
    }

    Completable reloadPost() {
        return Completable.create(e -> {
            post = getDataManager().getPost(post.getIdPost());
            if (!post.getLikes().isEmpty()) {
                try {
                    postLikes = new JSONArray(post.getLikes());
                } catch (JSONException t) {
                    AppLogger.e(t, null);
                }
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation());
    }

    Observable<List<Comment>> fetchPostComments(int start, int quantity) {
        return App.getInstance().getNetworkService().getObservableAPI().getPostComments(start, quantity, post.getIdPost(), 0)
                .subscribeOn(Schedulers.io())
                .doOnNext(commentList -> postCommentList = commentList );
    }

    void setLike(boolean checked) {
        if (checked) {
            postLikes.put(String.valueOf(getLoggedUser().getIdUser()));
        } else {
            try {
                ArrayList<String> list = new ArrayList<>();
                for (int i = 0, len = postLikes.length(); i < len; i++) {
                    String val = postLikes.getString(i);
                    if (!val.equals(String.valueOf(getLoggedUser().getIdUser()))) {
                        list.add(val);
                    }
                }
                postLikes = new JSONArray(list);
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
    }

    void commitUpdateLikeState(Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().updateLikes(post.getIdPost(), postLikes.toString()).enqueue(callback);
    }

    void commitDeletePost(Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().deletePost(post.getIdPost(), post.getImage()).enqueue(callback);
    }

    void commitCreateComment(String comment, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().createComment(getLoggedUser().getIdUser(), post.getIdPost(), comment).enqueue(callback);
    }

    void commitDeleteComment(int position, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().deleteComment(post.getIdPost(), postCommentList.get(position).getIdComment()).enqueue(callback);
    }

    void commitCommentLikesUpdate(Comment comment) {
        App.getInstance().getNetworkService().getCallableAPI().updateCommentLikes(comment.getIdComment(), comment.getLikes()).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void commitTogglePinnedPost(String pinned, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().togglePinnedPost(post.getIdPost(), pinned).enqueue(callback);
    }

    void writeLikes() {
        Post announce = getDataManager().getBoxStore().boxFor(Post.class).get(post.getIdPost());
        announce.setLikes(postLikes.toString());
        getDataManager().getBoxStore().boxFor(Post.class).put(announce);
    }

    void writeDeletePost() {
        getDataManager().getBoxStore().boxFor(Post.class).remove(post);
    }

    void writeTogglePinnedPost(String pinned) {
        post.setPinned(pinned);
        getDataManager().getBoxStore().boxFor(Post.class).put(post);
    }

    void writeCreateComment(int commentId, String comment) {
        Comment comment1 = new Comment(commentId, getLoggedUser().getIdUser(), DateUtils.getTimeStamp(), comment, "", post.getIdPost());
        postCommentList.add(0, comment1);
        String newCommentCount = String.valueOf(Integer.valueOf(post.getComments()) + 1);
        post.setComments(newCommentCount);
        getDataManager().getBoxStore().boxFor(Post.class).put(post);
    }

    void writeDeleteComment(int position) {
        postCommentList.remove(position);
        String newCommentCount = String.valueOf(Integer.valueOf(post.getComments()) - 1);
        post.setComments(newCommentCount);
        getDataManager().getBoxStore().boxFor(Post.class).put(post);
    }

    Post getPost() {
        return post;
    }

    JSONArray getPostLikes() {
        return postLikes;
    }

    List<Comment> getPostCommentList() {
        return postCommentList;
    }

    Comment getPostComment(int position) {
        return postCommentList.get(position);
    }

    public int getSelectedOrder() {
        return DataFilters.selectedPostCommentsListOrder;
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }
}