package eco.darmas.activities.admin;

import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;

class AdminPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> {

    AdminPresenter(AdminModel mModel, AdminView mView) {
        super(mModel, mView);
    }
}
