package eco.darmas.activities.admin;


import android.os.Bundle;

import eco.darmas.AppData;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataManager;

class AdminModel extends BaseActivityModel {

    AdminModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        AppData.INSTANCE.loadInstanceState(savedInstanceState);
        getDataManager().startDataLoad();
    }

    @Override
    protected void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        AppData.INSTANCE.saveInstanceState(outState);
    }
}