package eco.darmas.activities.admin;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;

public final class AdminActivity extends BaseActivity {

    @Override
    protected AdminModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new AdminModel(dataManager, savedInstanceState);
    }

    @Override
    protected AdminPresenter createPresenter(BaseActivityModel model, BaseActivityView view) {
        return new AdminPresenter((AdminModel) model, (AdminView) view);
    }

    @Override
    protected AdminView createView(Context context) {
        return new AdminView(context);
    }
}