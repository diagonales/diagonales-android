package eco.darmas.activities.admin;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import eco.darmas.AppConfig;
import eco.darmas.R;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;

class AdminView extends BaseActivityView {

    @BindView(R.id.wb_admin_panel)
    WebView wb_admin_panel;

    AdminView(Context context) {
        super(context);
        inflate(context, R.layout.admin_activity, this);
    }

    @Override
    protected void initialize(BaseActivityPresenter presenter) {
        wb_admin_panel.getSettings().setJavaScriptEnabled(true);
        wb_admin_panel.getSettings().setSupportZoom(true);
        wb_admin_panel.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, final String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
            @Override
            public void onPageFinished(WebView view,final String url) {
                super.onPageFinished(view, url);
            }
        });
        wb_admin_panel.loadUrl(AppConfig.getUsersUrl());
    }
}
