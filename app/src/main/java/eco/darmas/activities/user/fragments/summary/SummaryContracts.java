package eco.darmas.activities.user.fragments.summary;

interface SummaryContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}