package eco.darmas.activities.user.fragments.messages;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Message;
import eco.darmas.utils.DateUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class MessagesView extends BaseFragmentView implements MessagesContracts.View, FlexibleAdapter.OnUpdateListener {

    private MessagesPresenter mPresenter;
    private Unbinder unBinder;
    private MessagesAdapter flexibleMessagesAdapter;
    private boolean isBottomScrolled;

    @BindView(R.id.tv_messages_none)
    TextView tv_messages_none;
    @BindView(R.id.et_message)
    EditText et_message;
    @BindView(R.id.rv_messages)
    RecyclerView rv_messages;

    MessagesView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.messages_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (MessagesPresenter) presenter;

        tv_messages_none.setText("Aun no tienes mensajes con " + mPresenter.getContact().getName().split(" ")[0]);
        setupAdapters();
    }

    private void setupAdapters() {
        //rv_messages.setHasFixedSize(true);

        /*LinearLayoutManager lm_messages = new LinearLayoutManager(getContext());
        lm_messages.setStackFromEnd(true);*/
        SmoothScrollLinearLayoutManager lm_messages = new SmoothScrollLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        lm_messages.setStackFromEnd(true);
        rv_messages.setLayoutManager(lm_messages);
        rv_messages.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.message, 15));

        flexibleMessagesAdapter = new MessagesAdapter(getFlexibleMessagesItemList(mPresenter.getMessageList()), this, true);
        rv_messages.setAdapter(flexibleMessagesAdapter);

        // Find a better implementation for transcript mode on recyclerview
        rv_messages.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isBottomScrolled = !recyclerView.canScrollVertically(1);
            }
        });
    }

    void dataSetChanged() {
        flexibleMessagesAdapter.updateDataSet(getFlexibleMessagesItemList(mPresenter.getMessageList()));
        if (isBottomScrolled) {
            rv_messages.scrollToPosition(rv_messages.getAdapter().getItemCount() - 1);
        }
    }

    @OnClick(R.id.bt_send_message)
    void onSendMessageClickListener() {
        String messageString = et_message.getText().toString();
        messageString = messageString.trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid comment, if the user entered one.
        if (messageString.isEmpty()) {
            cancel = true;
            focusView = et_message;
        }

        if (cancel) {
            // There was an error; don't attempt to send the comment and focus the
            // comment field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform conversation send attempt.
            // showProgress(true);

            mPresenter.sendMessage(messageString);
            //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.hideSoftInputFromWindow(_v.getWindowToken(), 0);
            et_message.setText("");
        }
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_messages.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_messages_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    class MessagesAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        MessagesAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class MessageItemAdapter extends AbstractFlexibleItem<MessageItemAdapter.ViewHolder> {

        private Message message;

        MessageItemAdapter(Message message) {
            this.message = message;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof MessageItemAdapter) {
                Message inItem = ((MessageItemAdapter) inObject).message;
                return this.message.getIdMessage() == inItem.getIdMessage();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return message.getIdMessage();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.message;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            viewHolder.tv_message.setText(message.getMessage());
            viewHolder.tv_message_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(message.getDate()), AppConstants.COMPLETE_DATE_FORMAT));

            if (mPresenter.getContact().getIdUser() == message.getIdSender()) {
                viewHolder.space_left.setVisibility(View.GONE);
                viewHolder.space_right.setVisibility(View.VISIBLE);
                viewHolder.iv_message_status.setVisibility(View.GONE);
            } else {
                viewHolder.space_left.setVisibility(View.VISIBLE);
                viewHolder.space_right.setVisibility(View.GONE);
                viewHolder.iv_message_status.setVisibility(View.VISIBLE);
                if ((message.getStatus() == Message.SENT)) {
                    viewHolder.iv_message_status.setImageResource(R.drawable.ic_message_sent);
                } else if ((message.getStatus() == Message.RECEIVED)) {
                    viewHolder.iv_message_status.setImageResource(R.drawable.ic_message_received);
                }
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_message_date)
            TextView tv_message_date;
            @BindView(R.id.tv_message)
            TextView tv_message;
            @BindView(R.id.iv_message_status)
            ImageView iv_message_status;
            @BindView(R.id.space_left)
            Space space_left;
            @BindView(R.id.space_right)
            Space space_right;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleMessagesItemList(List<Message> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Message item : list) {
            itemList.add(new MessageItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
