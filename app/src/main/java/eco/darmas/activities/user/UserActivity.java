package eco.darmas.activities.user;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;

public class UserActivity extends BaseActivity implements UserContracts.Activity {

    @Override
    protected UserModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new UserModel(dataManager, savedInstanceState, getIntent());
    }

    @Override
    protected UserPresenter createPresenter(BaseActivityModel model, BaseActivityView view) {
        return new UserPresenter((UserModel) model, (UserView) view);
    }

    @Override
    protected UserView createView(Context context) {
        return new UserView(context);
    }
}