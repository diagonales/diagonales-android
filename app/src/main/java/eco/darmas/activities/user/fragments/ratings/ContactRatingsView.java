package eco.darmas.activities.user.fragments.ratings;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class ContactRatingsView extends BaseFragmentView implements ContactRatingsContracts.View {

    private ContactRatingsPresenter mPresenter;
    private Unbinder unBinder;
    private RatingsAdapter flexibleWorkRatingsAdapter;
    private RatingsAdapter flexibleListingRatingsAdapter;

    @BindView(R.id.rb_rating_profile)
    SimpleRatingBar rb_rating_profile;
    @BindView(R.id.tv_rating_profile)
    TextView tv_rating_profile;
    @BindView(R.id.rv_ratings_work)
    RecyclerView rv_ratings_work;
    @BindView(R.id.rv_ratings_listings)
    RecyclerView rv_ratings_listings;
    @BindView(R.id.tv_ratings_work_none)
    TextView tv_ratings_work_none;
    @BindView(R.id.tv_ratings_listings_none)
    TextView tv_ratings_listings_none;

    ContactRatingsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.ratings_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (ContactRatingsPresenter) presenter;

        setupAdapters();
        setData();
    }

    public void setData() {
        mPresenter.repaintUserRating()
                .subscribe();
        mPresenter.repaintUserWorkRatings()
                .subscribe();
        mPresenter.repaintUserListingRatings()
                .subscribe();
    }

    void setupAdapters() {
        rv_ratings_work.setHasFixedSize(true);
        rv_ratings_listings.setHasFixedSize(true);

        LinearLayoutManager lm_ratings_work = new LinearLayoutManager(getContext());
        rv_ratings_work.setLayoutManager(lm_ratings_work);
        LinearLayoutManager lm_ratings_listings = new LinearLayoutManager(getContext());
        rv_ratings_listings.setLayoutManager(lm_ratings_listings);

        flexibleWorkRatingsAdapter = new RatingsAdapter(new ArrayList<>(), null, true);
        rv_ratings_work.setAdapter(flexibleWorkRatingsAdapter);
        flexibleListingRatingsAdapter = new RatingsAdapter(new ArrayList<>(), null, true);
        rv_ratings_listings.setAdapter(flexibleListingRatingsAdapter);
    }

    void setUserRating(float userRating) {
        rb_rating_profile.setRating(userRating);
    }

    void setUserRatingCount(String userRatingCountText) {
        tv_rating_profile.setText(userRatingCountText);
    }

    void workRatingsDataSetChanged(HashMap<Integer, Float> integerFloatHashMap) {
        flexibleWorkRatingsAdapter.updateDataSet(getFlexibleWorkRatingsItemList(integerFloatHashMap));
    }

    void listingRatingsDataSetChanged(HashMap<Integer, Float> integerFloatHashMap) {
        flexibleListingRatingsAdapter.updateDataSet(getFlexibleListingRatingsItemList(integerFloatHashMap));
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }

    public void workRatingsNone(boolean none) {
        tv_ratings_work_none.setVisibility(none ? View.VISIBLE : View.GONE);
        rv_ratings_work.setVisibility(none ? View.GONE : View.VISIBLE);
    }

    public void listingsRatingsNone(boolean none) {
        tv_ratings_listings_none.setVisibility(none ? View.VISIBLE : View.GONE);
        rv_ratings_listings.setVisibility(none ? View.GONE : View.VISIBLE);
    }

    class RatingsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        RatingsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class RatingItemAdapter extends AbstractFlexibleItem<RatingItemAdapter.ViewHolder> {

        private Map.Entry<Integer, Float> entry;
        private int type;

        RatingItemAdapter(Map.Entry<Integer, Float> entry, int type) {
            this.entry = entry;
            this.type = type;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof RatingItemAdapter) {
                Map.Entry<Integer, Float> inItem = ((RatingItemAdapter) inObject).entry;
                return this.entry.getValue().equals(inItem.getValue()) && this.entry.getKey().equals(inItem.getKey());
            }
            return false;
        }

        @Override
        public int hashCode() {
            return entry.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.rating;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int targetId = entry.getKey();

            viewHolder.rb_rating.setRating(entry.getValue());

            int size = 0;
            String targetName;
            if (type == 0) {
                size = mPresenter.getWorkRatings().get(mPresenter.getContactId()).get(targetId).size();
                targetName = mPresenter.getCategoryNodeMap().get(targetId).getData().getName();
                viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            } else {
                size = mPresenter.getListingRatings().get(targetId).size();
                targetName = mPresenter.getListingMap().get(targetId).getTitle();
                viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.listingRatingBar));
                viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
            }
            viewHolder.tv_rating.setText("(" + size + ")");
            viewHolder.tv_rating_target.setText(targetName);
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_rating)
            TextView tv_rating;
            @BindView(R.id.tv_rating_target)
            TextView tv_rating_target;
            @BindView(R.id.rb_rating)
            SimpleRatingBar rb_rating;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleWorkRatingsItemList(HashMap<Integer, Float> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Map.Entry<Integer, Float> item : list.entrySet()) {
            itemList.add(new RatingItemAdapter(item, 0));
        }
        return itemList;
    }

    private List<AbstractFlexibleItem> getFlexibleListingRatingsItemList(HashMap<Integer, Float> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Map.Entry<Integer, Float> item : list.entrySet()) {
            itemList.add(new RatingItemAdapter(item, 1));
        }
        return itemList;
    }
}
