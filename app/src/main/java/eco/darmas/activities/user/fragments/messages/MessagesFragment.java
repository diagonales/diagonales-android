package eco.darmas.activities.user.fragments.messages;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class MessagesFragment extends BaseFragment implements MessagesContracts.Fragment {

    @Override
    protected MessagesModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new MessagesModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected MessagesPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new MessagesPresenter((MessagesModel) model, (MessagesView) view);
    }

    @Override
    protected MessagesView createView(Context context) {
        return new MessagesView(context);
    }
}
