package eco.darmas.activities.user.fragments.summary;

import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Post;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

class SummaryModel extends BaseFragmentModel implements SummaryContracts.Model {

    private User contact;

    private int transactionsCount = 0;
    private int offersCount = 0;
    private int demandsCount = 0;
    private int postsCount = 0;

    SummaryModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.contact = arguments.getParcelable("contact");
        loadUpdatePostsCount().subscribe();
        loadUpdateTransactionsCount().subscribe();
        loadUpdateListingsCount().subscribe();
    }

    Single loadUpdatePostsCount() {
        return Single.create((SingleOnSubscribe<Integer>) e -> {
            int count = 0;
            for (int i = 0; i < getDataManager().getPostMap().size(); i++) {
                Post post = getDataManager().getPostMap().valueAt(i);
                if (post.getIdCreator() != contact.getIdUser()) {
                    continue;
                }
                if (post.getPinned().equals("2")) {
                    continue;
                }
                count++;
            }
            e.onSuccess(count);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(count -> postsCount = count);
    }

    Single loadUpdateTransactionsCount() {
        return Single.create((SingleOnSubscribe<Integer>) e -> {
            int count = 0;
            for (int i = 0; i < getDataManager().getTransactionMap().size(); i++) {
                _Transaction transaction = getDataManager().getTransactionMap().valueAt(i);
                if (transaction.getIdSender() != contact.getIdUser() && transaction.getIdReceiver() != contact.getIdUser()) {
                    continue;
                }
                if (transaction.getStatus() != _Transaction.Status.RECEIVED) {
                    continue;
                }
                count++;
            }
            e.onSuccess(count);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(count -> transactionsCount = count);
    }

    Single loadUpdateListingsCount() {
        return Single.create((SingleOnSubscribe<int[]>) e -> {
            int[] count = new int[2];
            for (int i = 0; i < getDataManager().getListingsMap().size(); i++) {
                Listing listing = getDataManager().getListingsMap().valueAt(i);
                if (listing.getIdCreator() == contact.getIdUser() && listing.getType() == Listing.Type.OFFER) {
                    count[0]++;
                } else if (listing.getIdCreator() == contact.getIdUser() && listing.getType() == Listing.Type.DEMAND) {
                    count[1]++;
                }
            }
            e.onSuccess(count);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(count -> {
                    offersCount = count[0];
                    demandsCount = count[1];
                });
    }

    int getTransactionsCount() {
        return transactionsCount;
    }

    int getOffersCount() {
        return offersCount;
    }

    int getDemandsCount() {
        return demandsCount;
    }

    int getPostsCount() {
        return postsCount;
    }

    User getContact() {
        return contact;
    }
}
