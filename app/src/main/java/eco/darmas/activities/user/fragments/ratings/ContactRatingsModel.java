package eco.darmas.activities.user.fragments.ratings;

import android.os.Bundle;
import android.util.SparseArray;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.pojos.User;

class ContactRatingsModel extends BaseFragmentModel implements ContactRatingsContracts.Model {

    private User contact;

    ContactRatingsModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.contact = arguments.getParcelable("contact");
    }

    User getContact() {
        return contact;
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorkRatingsMap() {
        return getDataManager().getWorkRatingsMap();
    }

    SparseArray<SparseArray<Float>> getListingRatingsMap() {
        return getDataManager().getListingRatingsMap();
    }

    SparseArray<Listing> getListingsMap() {
        return getDataManager().getListingsMap();
    }

    SparseArray<Node<Category>> getCategoryNodeMap() {
        return getDataManager().getCategoryNodeMap();
    }
}