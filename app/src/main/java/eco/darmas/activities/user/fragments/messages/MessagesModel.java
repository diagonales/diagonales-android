package eco.darmas.activities.user.fragments.messages;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Message;
import eco.darmas.pojos.User;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class MessagesModel extends BaseFragmentModel implements MessagesContracts.Model {

    private List<Message> messageList;
    private User contact;

    MessagesModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.contact = arguments.getParcelable("contact");
        loadData().subscribe();
    }

    Single loadData() {
        return Single.create((SingleOnSubscribe<List<Message>>) e -> {
            List<Message> messageList = getDataManager().getMessagesMap().get(contact.getIdUser(), new ArrayList<>());
            e.onSuccess(messageList);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(messages -> messageList = messages);
    }

    void commitSendMessage(Message message, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().sendMessage(getLoggedUser().getIdUser(), contact.getIdUser(), message.getMessage()).enqueue(callback);
    }

    void writeMessage(Message message) {
        getDataManager().getBoxStore().boxFor(Message.class).put(message);
    }

    List<Message> getMessageList() {
        return messageList;
    }

    Message getMessage(int position) {
        return messageList.get(position);
    }

    User getContact() {
        return contact;
    }
}