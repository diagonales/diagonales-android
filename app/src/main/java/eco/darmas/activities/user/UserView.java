package eco.darmas.activities.user;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.BindView;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.activities.user.fragments.messages.MessagesFragment;
import eco.darmas.activities.user.fragments.ratings.ContactRatingsFragment;
import eco.darmas.activities.user.fragments.summary.SummaryFragment;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.pojos.User;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;

class UserView extends BaseActivityView implements UserContracts.View {

    private UserPresenter mPresenter;

    @BindView(R.id.iv_contact)
    ImageView iv_contact;
    @BindView(R.id.iv_contact_collapsed)
    ImageView iv_contact_collapsed;
    @BindView(R.id.iv_contact_background)
    ImageView iv_contact_background;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_contact_name)
    TextView tv_contact_name;
    @BindView(R.id.tv_contact_name_collapsed)
    TextView tv_contact_name_collapsed;
    @BindView(R.id.tv_last_seen_hour)
    TextView tv_last_seen_hour;
    @BindView(R.id.tv_last_seen_date)
    TextView tv_last_seen_date;
    @BindView(R.id.iv_badge_admin)
    ImageView iv_badge_admin;
    @BindView(R.id.iv_badge_admin_collapsed)
    ImageView iv_badge_admin_collapsed;
    @BindView(R.id.iv_badge_verified)
    ImageView iv_badge_verified;
    @BindView(R.id.iv_badge_verified_collapsed)
    ImageView iv_badge_verified_collapsed;
    @BindView(R.id.iv_badge_new)
    ImageView iv_badge_new;
    @BindView(R.id.iv_badge_new_collapsed)
    ImageView iv_badge_new_collapsed;
    @BindView(R.id.cl_footer)
    CoordinatorLayout cl_footer;
    @BindView(R.id.user_pager)
    ViewPager user_pager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsing_toolbar;
    @BindView(R.id.sliding_tabs)
    TabLayout sliding_tabs;
    @BindView(R.id.app_bar_layout)
    AppBarLayout app_bar_layout;
    @BindView(R.id.rl_user_data)
    RelativeLayout rl_user_data;
    @BindView(R.id.rl_user_data_collapsed)
    LinearLayout rl_user_data_collapsed;

    UserView(Context context) {
        super(context);
        initStatusBar();
        inflate(getContext(), R.layout.user_activity, this);
    }

    @Override
    public void setupActionBar(ActionBar supportActionBar) {
        supportActionBar.setCustomView(toolbar);
        supportActionBar.setDisplayShowHomeEnabled(false);
        supportActionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void initialize(BaseActivityPresenter presenter) {
        this.mPresenter = (UserPresenter) presenter;

        sliding_tabs.setupWithViewPager(user_pager);
        user_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                UserPresenter.selectedTab = position;
                if (position == 2) {
                    app_bar_layout.setExpanded(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                }
            }
        });

        if (((UserActivity) getContext()).getIntent().getIntExtra("tab", 0) == 2) {
            app_bar_layout.setExpanded(false, false);
        }
        app_bar_layout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {

            rl_user_data.setAlpha(1.0f - Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange()));
            float scale = 1 - (0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())) * 0.2f;
            rl_user_data.setScaleX(scale);
            rl_user_data.setScaleY(scale);
            rl_user_data_collapsed.setAlpha(0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange()));
            float scale2 = 1 - (1.0f - Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())) * 0.2f;
            rl_user_data_collapsed.setScaleX(scale2);
            rl_user_data_collapsed.setScaleY(scale2);

            /*float sizeInDp = 68 * (1 - (0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())));
            float scale3 = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (sizeInDp * scale3 + 0.5f);
            cl_footer.setPadding(0, 0, 0, messageBoxHeight + dpAsPixels);*/
        });

        iv_back.setOnClickListener(view -> {
            KeyboardUtils.hideSoftInput((UserActivity) getContext(), getRootView().findFocus());
            ((UserActivity) getContext()).onBackPressed();
        });

        user_pager.setOffscreenPageLimit(2);
        PagerAdapter messagesPagerAdapter = new MessagesSlidePagerAdapter(((UserActivity) getContext()).getSupportFragmentManager());
        user_pager.setAdapter(messagesPagerAdapter);
        user_pager.setCurrentItem(((UserActivity) getContext()).getIntent().getIntExtra("tab", 0));
        setData();
    }

    void setData() {
        tv_contact_name.setText(mPresenter.getContact().getName());
        tv_contact_name_collapsed.setText(mPresenter.getContact().getName());
        tv_last_seen_hour.setText("Ultima actividad: " + DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mPresenter.getContact().getLastSeen()), AppConstants.HOUR_FORMAT));
        tv_last_seen_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mPresenter.getContact().getLastSeen()), AppConstants.DATE_FORMAT));
        ImageUtils.loadRoundedProfileImage(mPresenter.getContact(), iv_contact);
        ImageUtils.loadRoundedProfileImage(mPresenter.getContact(), iv_contact_collapsed);
        ImageUtils.loadProfileBackgroundImage(getContext(), mPresenter.getContact(), iv_contact_background);
        if (!DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mPresenter.getContact().getDate()))) {
            iv_badge_new.setVisibility(View.VISIBLE);
            iv_badge_new_collapsed.setVisibility(View.VISIBLE);
        }

        if (mPresenter.getContact().getStatus() == User.Status.ADMIN) {
            iv_badge_admin.setVisibility(View.VISIBLE);
            iv_badge_admin_collapsed.setVisibility(View.VISIBLE);
        }

    }
    private class MessagesSlidePagerAdapter extends FragmentStatePagerAdapter {

        MessagesSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    SummaryFragment summaryFragment = new SummaryFragment();
                    Bundle arguments = new Bundle();
                    arguments.putParcelable("contact", mPresenter.getContact());
                    summaryFragment.setArguments(arguments);
                    return summaryFragment;
                }
                case 1: {
                    ContactRatingsFragment contactRatingsFragment = new ContactRatingsFragment();
                    Bundle arguments = new Bundle();
                    arguments.putParcelable("contact", mPresenter.getContact());
                    contactRatingsFragment.setArguments(arguments);
                    return contactRatingsFragment;
                }
                case 2: {
                    MessagesFragment messagesFragment = new MessagesFragment();
                    Bundle arguments = new Bundle();
                    arguments.putParcelable("contact", mPresenter.getContact());
                    messagesFragment.setArguments(arguments);
                    return messagesFragment;
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Resumen";
                }
                case 1: {
                    return "Valoración";
                }
                case 2: {
                    return "Mensajes";
                }
            }
            return null;
        }
    }

    private void initStatusBar() {
        Window window = ((UserActivity) getContext()).getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            SystemBarTintManager tintManager = new SystemBarTintManager((UserActivity) getContext());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getContext(), R.color.transparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
