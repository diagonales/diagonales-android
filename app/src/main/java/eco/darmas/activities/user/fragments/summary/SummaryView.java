package eco.darmas.activities.user.fragments.summary;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.utils.DateUtils;

class SummaryView extends BaseFragmentView implements SummaryContracts.View {

    private SummaryPresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.tv_transactions_count)
    TextView tv_transactions_count;
    @BindView(R.id.tv_offers_count)
    TextView tv_offers_count;
    @BindView(R.id.tv_demands_count)
    TextView tv_demands_count;
    @BindView(R.id.tv_posts_count)
    TextView tv_posts_count;
    @BindView(R.id.tv_followers_count)
    TextView tv_followers_count;
    @BindView(R.id.tv_following_count)
    TextView tv_following_count;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_user_since)
    TextView tv_user_since;
    @BindView(R.id.li_transactions)
    LinearLayout li_transactions;
    @BindView(R.id.tv_funds)
    TextView tv_funds;
    @BindView(R.id.sb_balance)
    SeekBar sb_balance;
    @BindView(R.id.tv_vol)
    TextView tv_vol;
    @BindView(R.id.tv_margins)
    TextView tv_margins;

    SummaryView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.summary_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (SummaryPresenter) presenter;

        li_transactions.setOnClickListener(view -> {
            mPresenter.onTransactionsCountClick();
        });
        tv_email.setText(mPresenter.getContact().getEmail());
        tv_user_since.setText("Miembro de la comunidad desde el " + DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mPresenter.getContact().getDate()), AppConstants.PARTIAL_DATE_FORMAT));
        tv_funds.setText(mPresenter.getFundsText());
        tv_margins.setText("-" + mPresenter.getContact().getPaymentMargin() + " / +" + mPresenter.getContact().getPaymentMargin());
        tv_vol.setText(mPresenter.getVol());

        sb_balance.setOnTouchListener((v2, event) -> true);
        sb_balance.setMax(mPresenter.getContact().getPaymentMargin() * 2);

        ValueAnimator anim = ValueAnimator.ofInt(mPresenter.getContact().getPaymentMargin(), (int) mPresenter.getContact().getFunds() + mPresenter.getContact().getPaymentMargin());
        anim.setDuration(1000);
        anim.addUpdateListener(animation -> {
            int animProgress = (Integer) animation.getAnimatedValue();
            sb_balance.setProgress(animProgress);
        });
        anim.start();

        paintPostsCount();
        paintTransactionsCount();
        paintListingsCount();
    }

    void paintPostsCount() {
        tv_transactions_count.setText(mPresenter.getTransactionsCount());
    }

    void paintTransactionsCount() {
        tv_offers_count.setText(mPresenter.getOffersCount());
        tv_demands_count.setText(mPresenter.getDemandsCount());
    }

    void paintListingsCount() {
        tv_posts_count.setText(mPresenter.getPostsCount());
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}