package eco.darmas.activities.user.fragments.summary;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eco.darmas.AppConstants;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import io.reactivex.android.schedulers.AndroidSchedulers;

class SummaryPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements SummaryContracts.Presenter{

    private final SummaryModel mModel;
    private final SummaryView mView;

    SummaryPresenter(SummaryModel mModel, SummaryView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updatePostsCount() {
        getDisposables().add(mModel.loadUpdatePostsCount()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::paintPostsCount)
                .subscribe());
    }

    private void updateTransactionsCount() {
        getDisposables().add(mModel.loadUpdateTransactionsCount()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::paintTransactionsCount)
                .subscribe());
    }

    private void updateListingsCount() {
        getDisposables().add(mModel.loadUpdateListingsCount()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::paintListingsCount)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.POSTS: {
                if (event.isSuccess()) {
                    updatePostsCount();
                }
            }
            case DataLoadedEvent.TRANSACTIONS: {
                if (event.isSuccess()) {
                    updateTransactionsCount();
                }
            }
            case DataLoadedEvent.LISTINGS: {
                if (event.isSuccess()) {
                    updateListingsCount();
                }
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    User getContact() {
        return mModel.getContact();
    }

    String getTransactionsCount() {
        return String.valueOf(mModel.getTransactionsCount());
    }

    String getOffersCount() {
        return String.valueOf(mModel.getOffersCount());
    }

    String getDemandsCount() {
        return String.valueOf(mModel.getDemandsCount());
    }

    String getPostsCount() {
        return String.valueOf(mModel.getPostsCount());
    }

    void onTransactionsCountClick() {
        //TODO: check
    }

    String getFundsText() {
        String sign = "";
        if (getContact().getFunds() > 0) {
            sign = "+";
        }
        return sign + String.valueOf(AppConstants.CURRENCY_FORMAT.format(getContact().getFunds()));
    }

    //TODO: put this somewhere in user balance fragment
    String getVol() {
        //TODO: this should take so much resources... possible paging issue
        double vol = 0;
        for (int i = 0; i < mModel.getDataManager().getTransactionMap().size(); i++) {
            _Transaction transaction = mModel.getDataManager().getTransactionMap().valueAt(i);
            if (transaction.getIdSender() != getContact().getIdUser() && transaction.getIdReceiver() != getContact().getIdUser()) {
                continue;
            }
            if (transaction.getStatus() != _Transaction.Status.RECEIVED) {
                continue;
            }
            vol += transaction.getAmount();
        }
        return AppConstants.CURRENCY_FORMAT.format(vol);
    }
}
