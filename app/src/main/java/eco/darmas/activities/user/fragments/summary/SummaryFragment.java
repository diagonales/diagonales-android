package eco.darmas.activities.user.fragments.summary;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class SummaryFragment extends BaseFragment implements SummaryContracts.Fragment {

    @Override
    protected SummaryModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new SummaryModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected SummaryPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new SummaryPresenter((SummaryModel) model, (SummaryView) view);
    }

    @Override
    protected SummaryView createView(Context context) {
        return new SummaryView(context);
    }
}
