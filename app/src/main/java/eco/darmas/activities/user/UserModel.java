package eco.darmas.activities.user;

import android.content.Intent;
import android.os.Bundle;

import com.thirtydegreesray.dataautoaccess.annotation.AutoAccess;

import eco.darmas.AppData;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.User;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

class UserModel extends BaseActivityModel implements UserContracts.Model {

    @AutoAccess
    User contact;

    UserModel(DataManager dataManager, Bundle savedInstanceState, Intent intent) {
        super(dataManager, savedInstanceState);
        AppData.INSTANCE.loadInstanceState(savedInstanceState);
        getDataManager().startDataLoad();

        if (savedInstanceState == null) {
            contact = intent.getParcelableExtra("parcelable#0");
        }
    }

    Single loadData() {
        return Single.create((SingleOnSubscribe<User>) e -> {
            User updatedContact = getDataManager().getUser(contact.getIdUser());
            e.onSuccess(updatedContact);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(updatedContact -> contact = updatedContact);
    }

    @Override
    protected void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        AppData.INSTANCE.saveInstanceState(outState);
    }

    User getContact() {
        return contact;
    }
}