package eco.darmas.activities.user.fragments.messages;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.Message;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class MessagesPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements MessagesContracts.Presenter{

    private final MessagesModel mModel;
    private final MessagesView mView;

    MessagesPresenter(MessagesModel mModel, MessagesView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateLists() {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::dataSetChanged)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.MESSAGES: {
                if (event.isSuccess()) {
                    updateLists();
                }
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    void sendMessage(String messageText) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("No se puede enviar, comprueba tu conección");
            return;
        }

        //TODO: Check this behavior
        Message message = new Message(0, messageText, mModel.getLoggedUser().getIdUser(), 0, mModel.getContact().getIdUser(), 0, DateUtils.getTimeStamp(), Message.SENT, 0);
        mModel.getMessageList().add(message);
        mModel.writeMessage(message);
        mModel.commitSendMessage(message, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject jsonResponse;
                try {
                    jsonResponse = new JSONObject(new String(response.body().bytes()));
                    if (jsonResponse.get("result").equals("true")) {
                        int id = (int) jsonResponse.get("id");
                        Message message1 = mModel.getMessageList().get(mModel.getMessageList().size() - 1);
                        message1.setStatus(Message.RECEIVED);
                        message1.setIdMessage(id);
                        mView.dataSetChanged();
                    } else {
                        CommonUtils.showErrorToast("Error al enviar el mensaje");
                    }
                } catch (Exception e) {
                    AppLogger.e(e, null);
                    CommonUtils.showErrorToast("Error al enviar el mensaje, por favor contacta un administrador");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("El mensaje no fue enviado por un error en el servidor, por favor contacta un administrador");
                }
                AppLogger.e(t, null);
            }
        });
    }

    User getContact() {
        return mModel.getContact();
    }

    List<Message> getMessageList() {
        return mModel.getMessageList();
    }
}
