package eco.darmas.activities.user.fragments.ratings;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class ContactRatingsFragment extends BaseFragment implements ContactRatingsContracts.Fragment {

    @Override
    protected ContactRatingsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new ContactRatingsModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected ContactRatingsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new ContactRatingsPresenter((ContactRatingsModel) model, (ContactRatingsView) view);
    }

    @Override
    protected ContactRatingsView createView(Context context) {
        return new ContactRatingsView(context);
    }
}
