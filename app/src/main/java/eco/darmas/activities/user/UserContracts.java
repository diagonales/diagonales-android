package eco.darmas.activities.user;

interface UserContracts {

    interface Activity {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}
