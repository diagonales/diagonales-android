package eco.darmas.activities.user;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.User;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class UserPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> implements UserContracts.Presenter {

    private final UserModel mModel;
    private final UserView mView;
    public static int idContact;
    public static int selectedTab;

    UserPresenter(UserModel mModel, UserView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
        idContact = mModel.getContact().getIdUser();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void updateData() {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::setData)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContentModified(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.USERS:
                updateData();
        }
    }

    User getContact() {
        return mModel.getContact();
    }
}
