package eco.darmas.activities.user.fragments.ratings;

interface ContactRatingsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}