package eco.darmas.activities.user.fragments.messages;

interface MessagesContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}