package eco.darmas.activities.login;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;

public class LoginActivity extends BaseActivity implements LoginContracts.Activity {

    private LoginView mView;

    @Override
    protected LoginModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new LoginModel(dataManager, savedInstanceState);
    }

    @Override
    protected LoginPresenter createPresenter(BaseActivityModel model, BaseActivityView view) {
        return new LoginPresenter((LoginModel) model, (LoginView) view);
    }

    @Override
    protected LoginView createView(Context context) {
        mView = new LoginView((LoginActivity) context);
        return mView;
    }

    @Override
    public void onBackPressed() {
        if (mView.dismissPermissionRationale() == 0) {
            moveTaskToBack(true);
        }
    }
}