package eco.darmas.activities.login;

import android.annotation.SuppressLint;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.transition.TransitionManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import eco.darmas.AppConfig;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.BuildConfig;
import eco.darmas.R;
import eco.darmas.auxiliary.Action;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.auxiliary.CustomScrollView;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.KeyboardUtils;
import eco.darmas.utils.NetworkUtils;

@SuppressLint("ViewConstructor")
class LoginView extends BaseActivityView implements LoginContracts.View, LoaderCallbacks<Cursor> {

    private LoginActivity mContext;
    private LoginPresenter mPresenter;

    @BindView(R.id.cl_login)
    ConstraintLayout cl_login;
    @BindView(R.id.tv_logo)
    TextView tv_logo;
    @BindView(R.id.et_email)
    AutoCompleteTextView et_email;
    @BindView(R.id.et_password)
    TextInputEditText et_password;
    @BindView(R.id.ti_password)
    TextInputLayout ti_password;
    @BindView(R.id.ti_name)
    TextInputLayout ti_name;
    @BindView(R.id.et_name)
    TextInputEditText et_name;
    @BindView(R.id.pb_progress)
    ProgressBar pb_progress;
    @BindView(R.id.sv_login_form)
    CustomScrollView sv_login_form;
    @BindView(R.id.cb_guest)
    CheckBox cb_guest;
    @BindView(R.id.tv_ver)
    TextView tv_ver;

    protected LoginView(@NonNull LoginActivity context) {
        super(context);
        this.mContext = context;
        initStatusBar();
        inflate(mContext, R.layout.login, this);
    }

    @Override
    protected void initialize(@NonNull BaseActivityPresenter presenter) {
        this.mPresenter = (LoginPresenter) presenter;
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.lobster_two_bold_italic);
        tv_logo.setTypeface(typeface, 1);
        pb_progress.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.primaryTextColor), android.graphics.PorterDuff.Mode.MULTIPLY);

        //TODO: simplify this code in methods please
        if (BuildConfig.DEBUG) {
            tv_ver.setText("v" + AppConfig.VERSION + " (beta)");
            tv_ver.setVisibility(VISIBLE);
            cb_guest.setChecked(true);
            cb_guest.setEnabled(false);
            ti_password.setVisibility(View.GONE);
            ti_name.setVisibility(View.VISIBLE);
        } else if (mPresenter.isTestMode()) {
            cb_guest.setChecked(true);
            cb_guest.setEnabled(true);
            ti_password.setVisibility(View.GONE);
            ti_name.setVisibility(View.VISIBLE);
        } else {
            cb_guest.setChecked(false);
            cb_guest.setEnabled(true);
            ti_password.setVisibility(View.VISIBLE);
            ti_name.setVisibility(View.GONE);
        }

        cb_guest.setOnCheckedChangeListener((compoundButton, b) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition(cl_login);
            }
            if (b) {
                ti_password.setVisibility(View.GONE);
                ti_name.setVisibility(View.VISIBLE);
            } else {
                ti_password.setVisibility(View.VISIBLE);
                ti_name.setVisibility(View.GONE);
            }
            mPresenter.setTestMode(b);
        });

        // TODO: will leave this in a fixed test mode, until test and production mode selection is working properly
//        cb_guest.setChecked(true);
//        cb_guest.setEnabled(false);
    }

    @OnFocusChange({R.id.et_password, R.id.et_email})
    void onFocusChangeListener() {
        final Handler handler;
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if (sv_login_form != null) {
                    sv_login_form.smoothScrollTo(0, 500);
                    sv_login_form.setEnableScrolling(false);
                    handler.post(this);
                }
            }
        };
        handler.post(r);
    }

    @OnEditorAction(R.id.et_password)
    boolean onEditorActionListener(int id) {
        if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
            attemptLogin();
            return true;
        }
        return false;
    }

    @OnClick(R.id.bt_sign_in)
    void onSignInClickListener(View view) {
        if (!NetworkUtils.isNetworkConnected(mContext)) {
            CommonUtils.showWarningToast(getContext().getString(R.string.check_connection));
            return;
        }
        attemptLogin();
    }

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        et_email.setError(null);
        et_password.setError(null);
        et_name.setError(null);

        // Store values at the time of the login attempt.
        String name = et_name.getText().toString();
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one and if is not a guest.
        if (!cb_guest.isChecked() && (TextUtils.isEmpty(password) || !CommonUtils.isPasswordValid(password))) {
            et_password.setError(mContext.getString(R.string.error_invalid_password));
            focusView = et_password;
            cancel = true;
        }
        if (cb_guest.isChecked() && TextUtils.isEmpty(name)) {
            et_name.setError(mContext.getString(R.string.error_field_required));
            focusView = et_name;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            et_email.setError(mContext.getString(R.string.error_field_required));
            focusView = et_email;
            cancel = true;
        } else if (!CommonUtils.isEmailValid(email)) {
            et_email.setError(mContext.getString(R.string.error_invalid_email));
            focusView = et_email;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            KeyboardUtils.hideSoftInput(mContext, getRootView().findFocus());
            showProgress(true);

            if (mPresenter.isTestMode()) {
                mPresenter.testLogin(name, email);
            } else {
                mPresenter.logIn(email, password);
            }
        }
    }

    void showLoginResult(String welcomeMessageTitle, String welcomeMessageText) {
        if (AppData.INSTANCE.getLoggedUser() != null) {
            if (!welcomeMessageTitle.isEmpty() || !welcomeMessageText.isEmpty()) {
                showWelcomeMessage(welcomeMessageTitle, welcomeMessageText);
            } else {
                Object[] parcelables = new Object[]{AppData.INSTANCE.getLoggedUser()};
                AppNav.navigateToActivity(AppConstants.MAIN_ACTIVITY, mContext, parcelables);
                mContext.finish();
            }
        } else {
            showProgress(false);
            et_password.setError(mContext.getString(R.string.error_incorrect_password));
            et_password.requestFocus();
            KeyboardUtils.showSoftInput(et_password, mContext);
        }
    }

    void showOutdatedClient(String requiredVersion, String downloadLink) {
        CustomDialog outdatedDialog = new CustomDialog(mContext);
        outdatedDialog.setCanceledOnTouchOutside(false);
        outdatedDialog.setTitle(getContext().getString(R.string.outdated_version));
        outdatedDialog.setMessage("Debes actualizar la aplicación a la version " + requiredVersion + " para poder continuar");
        outdatedDialog.setButton(CustomDialog.BUTTON_NEUTRAL, "Descargar", (dialog, which) -> {
            CommonUtils.openInBrowser(mContext, downloadLink);
            mContext.finish();
        });
        outdatedDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "Salir", (dialog, which) -> {
            dialog.dismiss();
            mContext.finish();
        });
        pb_progress.setVisibility(GONE);
        outdatedDialog.show();
    }

    void showWelcomeMessage(String welcomeMessageTitle, String welcomeMessageText) {
        CustomDialog outdatedDialog = new CustomDialog(mContext);
        outdatedDialog.setCanceledOnTouchOutside(false);
        outdatedDialog.setTitle(welcomeMessageTitle);
        outdatedDialog.setMessage(welcomeMessageText);
        outdatedDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Continuar", (dialog, which) -> {
            dialog.dismiss();
            pb_progress.setVisibility(VISIBLE);
            Object[] parcelables = new Object[]{AppData.INSTANCE.getLoggedUser()};
            AppNav.navigateToActivity(AppConstants.MAIN_ACTIVITY, mContext, parcelables);
            mContext.finish();
        });
        pb_progress.setVisibility(GONE);
        outdatedDialog.show();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(cl_login);
        }
        sv_login_form.setVisibility(show ? View.GONE : View.VISIBLE);
        pb_progress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(mContext,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY), LoginView.ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE + " = ?", new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null) {
            return;
        }
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(LoginView.ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

private interface ProfileQuery {
    String[] PROJECTION = {ContactsContract.CommonDataKinds.Email.ADDRESS, ContactsContract.CommonDataKinds.Email.IS_PRIMARY};

    int ADDRESS = 0;
    //int IS_PRIMARY = 1;
}

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_dropdown_item_1line, emailAddressCollection);
        et_email.setAdapter(adapter);
    }

    void populateAutoComplete() {
        mContext.getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void requestPermissions() {
        getPermissionPresenter().requestReadContactsPermission();
    }

    @Override
    public void permissionAccepted(int actionCode) {
        if (actionCode == Action.ACTION_CODE_READ_CONTACTS) {
            populateAutoComplete();
            getPermissionPresenter().requestWriteExternalStoragePermission();
        }
    }

    @Override
    public void permissionDenied(int actionCode) {
        if (actionCode == Action.ACTION_CODE_READ_CONTACTS) {
            getPermissionPresenter().requestWriteExternalStoragePermission();
        } else if (actionCode == Action.ACTION_CODE_SAVE_IMAGE) {
            if (dismissPermissionRationale() == 0) {
                showSnackBarPermissionMessage(actionCode);
            }
        }
    }

    @Override
    protected void showSnackBarPermissionMessage(int action) {
        switch (action) {
            case Action.ACTION_CODE_SAVE_IMAGE:
                super.showSnackBarPermissionMessage(R.string.snackbar_save_image);
                break;
        }
    }

    private void initStatusBar() {
        Window window = ((LoginActivity) getContext()).getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            SystemBarTintManager tintManager = new SystemBarTintManager((LoginActivity) getContext());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getContext(), R.color.transparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}