package eco.darmas.activities.login;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import eco.darmas.App;
import eco.darmas.AppConfig;
import eco.darmas.AppData;
import eco.darmas.BuildConfig;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class LoginPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> implements LoginContracts.Presenter {

    private final LoginModel mModel;
    private final LoginView mView;

    LoginPresenter(LoginModel mModel, LoginView mView/*, Object[] IntentParams*/) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;

        if (mModel.isClosedSession()) {
            mView.showProgress(false);
            mView.requestPermissions();
        } else {
            logInCheck();
            getDisposables().add(Completable.timer(3, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(() -> mView.showProgress(true)));
        }
    }

    void testLogin(String name, String email) {
        if (!BuildConfig.DEBUG) {
            App.getInstance().startNetworkService();
        }
        mModel.testLogin(name, email).enqueue(new LoginCallback());
    }

    void logIn(String email, String password) {
        if (App.getInstance().getNetworkService().isTestNetwork()) {
            App.getInstance().startNetworkService();
        }
        String credentials = Credentials.basic(email, password);
        mModel.logIn(credentials).enqueue(new LoginCallback());
    }

    private void logInCheck() {
        mModel.logInCheck().enqueue(new LogInCheckCallback());
    }

    //Todo: correct this behaviour
    void setTestMode(boolean b) {
        if (!BuildConfig.DEBUG) {
            App.getInstance().getPreferencesHelper().setTestMode(b);
        }
    }

    private class LoginCallback implements Callback<ResponseBody> {


        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            getDisposables().dispose();
            if (response.isSuccessful()) {
                try {
                    JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                    if (responseJSON.get("result").equals("true")) {
                        if (Double.parseDouble((String) responseJSON.get("required_version")) <= AppConfig.VERSION) {

                            User loggedUser = new User();
                            loggedUser.setIdUser(Integer.parseInt((String) responseJSON.get("id_user")));
                            loggedUser.setName((String) responseJSON.get("name"));
                            loggedUser.setEmail((String) responseJSON.get("email"));
                            loggedUser.setAddress((String) responseJSON.get("address"));
                            loggedUser.setPhone((String) responseJSON.get("phone"));
                            loggedUser.setDate((String) responseJSON.get("date"));
                            loggedUser.setFunds(Double.parseDouble((String) responseJSON.get("funds")));
                            loggedUser.setActive((String) responseJSON.get("active"));
                            loggedUser.setComment((String) responseJSON.get("comment"));
                            loggedUser.setAdmin((String) responseJSON.get("admin"));
                            loggedUser.setLastSeen((String) responseJSON.get("last_seen"));
                            loggedUser.setStatus(Integer.parseInt((String) responseJSON.get("status")));
                            loggedUser.setToken((String) responseJSON.get("token"));
                            loggedUser.setPaymentMargin(Integer.parseInt((String) responseJSON.get("payment_margin")));
                            loggedUser.setImage((String) responseJSON.get("image"));

                            AppData.INSTANCE.setLoggedUser(loggedUser);
                            AppData.INSTANCE.setRefreshInterval(Integer.parseInt((String) responseJSON.get("refresh_interval")));
                            App.getInstance().getPreferencesHelper().setCurrentUserId(loggedUser.getIdUser());

                            String welcomeMessageTitle = responseJSON.isNull("message_title") ? "" : (String) responseJSON.get("message_title");
                            String welcomeMessageText = responseJSON.isNull("message_text") ? "" : (String) responseJSON.get("message_text");
                            mView.showLoginResult(welcomeMessageTitle, welcomeMessageText);
                        } else {
                            mView.showOutdatedClient((String) responseJSON.get("required_version"), (String) responseJSON.get("download_link"));
                        }
                    } else if (responseJSON.get("result").equals("false")) {
                        mView.showLoginResult("", "");
                    }
                } catch (Exception e) {
                    mView.showProgress(false);
                    CommonUtils.showErrorToast("Se ha producido un error, por favor contacta un administrador");
                    AppLogger.e(e, null);
                } finally {
                    response.body().close();
                }
            } else {
                mView.showProgress(false);
                CommonUtils.showErrorToast("Se ha producido un error en el servidor, por favor contacta un administrador");
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            getDisposables().dispose();
            mView.showProgress(false);
            if (t instanceof SocketTimeoutException) {
                CommonUtils.showErrorToast("El servidor no parece responder, vuelve a intentarlo mas tarde.");
            } else {
                CommonUtils.showErrorToast("Se ha producido un error, por favor contacta un administrador");
            }
            AppLogger.e(t, null);
        }
    }

    private class LogInCheckCallback implements Callback<ResponseBody> {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            getDisposables().dispose();
            if (response.isSuccessful()) {
                try {
                    JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                    if (responseJSON.get("result").equals("true")) {
                        if (Double.parseDouble((String) responseJSON.get("required_version")) <= AppConfig.VERSION) {

                            User loggedUser = new User();
                            loggedUser.setIdUser(Integer.parseInt((String) responseJSON.get("id_user")));
                            loggedUser.setName((String) responseJSON.get("name"));
                            loggedUser.setEmail((String) responseJSON.get("email"));
                            loggedUser.setAddress((String) responseJSON.get("address"));
                            loggedUser.setPhone((String) responseJSON.get("phone"));
                            loggedUser.setDate((String) responseJSON.get("date"));
                            loggedUser.setFunds(Double.parseDouble((String) responseJSON.get("funds")));
                            loggedUser.setActive((String) responseJSON.get("active"));
                            loggedUser.setComment((String) responseJSON.get("comment"));
                            loggedUser.setAdmin((String) responseJSON.get("admin"));
                            loggedUser.setLastSeen((String) responseJSON.get("last_seen"));
                            loggedUser.setStatus(Integer.parseInt((String) responseJSON.get("status")));
                            loggedUser.setToken((String) responseJSON.get("token"));
                            loggedUser.setPaymentMargin(Integer.parseInt((String) responseJSON.get("payment_margin")));
                            loggedUser.setImage((String) responseJSON.get("image"));

                            AppData.INSTANCE.setLoggedUser(loggedUser);
                            AppData.INSTANCE.setRefreshInterval(Integer.parseInt((String) responseJSON.get("refresh_interval")));

                            String welcomeMessageTitle = responseJSON.isNull("message_title") ? "" : (String) responseJSON.get("message_title");
                            String welcomeMessageText = responseJSON.isNull("message_text") ? "" : (String) responseJSON.get("message_text");
                            mView.showLoginResult(welcomeMessageTitle, welcomeMessageText);
                        } else {
                            mView.showOutdatedClient((String) responseJSON.get("required_version"), (String) responseJSON.get("download_link"));
                        }
                    } else if (responseJSON.get("result").equals("expired")) {
                        App.getInstance().getPreferencesHelper().setCurrentUserId(null);
                        mView.showProgress(false);
                        CommonUtils.showInfoToast("Sesión terminada, ingresa nuevamente");
                    } else if (responseJSON.get("result").equals("false")) {
                        App.getInstance().getPreferencesHelper().setCurrentUserId(null);
                        mView.showProgress(false);
                    }
                } catch (Exception e) {
                    App.getInstance().getPreferencesHelper().setCurrentUserId(null);
                    mView.showProgress(false);
                    CommonUtils.showErrorToast("Se ha producido un error, por favor contacta un administrador");
                    AppLogger.e(e, null);
                } finally {
                    response.body().close();
                }
            } else {
                mView.showProgress(false);
                CommonUtils.showErrorToast("Se ha producido un error en el servidor, por favor contacta un administrador");
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            getDisposables().dispose();
            App.getInstance().getPreferencesHelper().setCurrentUserId(null);
            mView.showProgress(false);
            if (t instanceof SocketTimeoutException) {
                CommonUtils.showErrorToast("El servidor no parece responder...");
            } else {
                CommonUtils.showErrorToast("No se pudo contactar el servidor...");
            }
            AppLogger.e(t, null);
        }
    }
}