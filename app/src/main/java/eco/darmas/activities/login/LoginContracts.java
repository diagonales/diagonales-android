package eco.darmas.activities.login;

interface LoginContracts {

    interface Activity {
    }

    interface View {

        void requestPermissions();
    }

    interface Presenter {
    }

    interface Model {
    }
}
