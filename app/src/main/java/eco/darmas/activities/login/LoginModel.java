package eco.darmas.activities.login;

import android.os.Bundle;

import eco.darmas.App;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

class LoginModel extends BaseActivityModel implements LoginContracts.Model {

    LoginModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    Call<ResponseBody> testLogin(final String name, final String email) {
        return App.getInstance().getNetworkService().getCallableAPI().testLogin(name, email);
    }

    Call<ResponseBody> logIn(final String credentials) {
        return App.getInstance().getNetworkService().getCallableAPI().login(credentials);
    }

    Call<ResponseBody> logInCheck() {
        return App.getInstance().getNetworkService().getCallableAPI().checkLoggedIn(App.getInstance().getPreferencesHelper().getCurrentUserId());
    }

    boolean isClosedSession() {
        return App.getInstance().getPreferencesHelper().getCurrentUserId() == null;
    }
}