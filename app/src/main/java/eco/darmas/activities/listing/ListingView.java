package eco.darmas.activities.listing;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Rating;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eco.darmas.utils.ScreenUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class ListingView extends BaseActivityView implements FlexibleAdapter.OnUpdateListener {

    ListingPresenter mPresenter;
    private CommentsAdapter flexibleCommentsAdapter;
    private boolean scrollTop;
    private int selectedShippingMethod = -1;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_front)
    ImageView iv_front;
    @BindView(R.id.tv_text)
    TextView tv_text;
    @BindView(R.id.rv_comments)
    RecyclerView rv_comments;
    @BindView(R.id.iv_creator)
    ImageView iv_creator;
    @BindView(R.id.tv_creator)
    TextView tv_creator;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_comments)
    TextView tv_comments;
    @BindView(R.id.iv_share)
    ImageView iv_share;
    @BindView(R.id.iv_delete)
    ImageView iv_delete;
    @BindView(R.id.tv_price_min)
    TextView tv_price_min;
    @BindView(R.id.tv_price_max)
    TextView tv_price_max;
    @BindView(R.id.tv_divider)
    TextView tv_divider;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.iv_diag_max)
    ImageView iv_diag_max;
    @BindView(R.id.iv_diag_min)
    ImageView iv_diag_min;
    @BindView(R.id.bt_order)
    Button bt_order;
    @BindView(R.id.tv_category)
    TextView tv_category;
    @BindView(R.id.rb_rating)
    SimpleRatingBar rb_rating;
    @BindView(R.id.rb_rating_2)
    SimpleRatingBar rb_rating_2;
    @BindView(R.id.rb_rating_3)
    SimpleRatingBar rb_rating_3;
    @BindView(R.id.tv_rating)
    TextView tv_rating;
    @BindView(R.id.tv_rating_2)
    TextView tv_rating_2;
    @BindView(R.id.tv_rating_3)
    TextView tv_rating_3;
    @BindView(R.id.li_rating)
    LinearLayout li_rating;
    @BindView(R.id.li_rating_2)
    LinearLayout li_rating_2;
    @BindView(R.id.li_rating_3)
    LinearLayout li_rating_3;
    @BindView(R.id.iv_badge_verified)
    ImageView iv_badge_verified;
    @BindView(R.id.iv_badge_new)
    ImageView iv_badge_new;
    @BindView(R.id.tv_stock)
    TextView tv_stock;
    @BindView(R.id.gl_details)
    GridLayout gl_details;
    @BindView(R.id.iv_edit)
    ImageView iv_edit;
    @BindView(R.id.iv_report)
    ImageView iv_report;
    @BindView(R.id.tv_comments_none)
    TextView tv_comments_none;
    @BindView(R.id.tv_completed)
    TextView tv_completed;
    @BindView(R.id.li_event_desc)
    LinearLayout li_event_desc;
    @BindView(R.id.tv_deadline)
    TextView tv_deadline;
    @BindView(R.id.cv_percent_discount)
    CardView cv_percent_discount;
    @BindView(R.id.tv_discount_percent)
    TextView tv_discount_percent;
    @BindView(R.id.li_primary_delivery_info)
    LinearLayout li_primary_delivery_info;
    @BindView(R.id.iv_address)
    ImageView iv_address;
    @BindView(R.id.tv_event_desc)
    TextView tv_event_desc;
    @BindView(R.id.li_secondary_delivery_info)
    LinearLayout li_secondary_delivery_info;
    @BindView(R.id.tv_secondary_delivery_info)
    TextView tv_secondary_delivery_info;
    @BindView(R.id.li_tertiary_delivery_info)
    LinearLayout li_tertiary_delivery_info;
    @BindView(R.id.tv_tertiary_delivery_info)
    TextView tv_tertiary_delivery_info;

    ListingView(Context context) {
        super(context);
        inflate(context, R.layout.listing_full_screen, this);
    }

    @Override
    protected void initialize(BaseActivityPresenter presenter) {
        this.mPresenter = (ListingPresenter) presenter;

        // Checks the orientation of the screen
        /*int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iv_front.setLayoutParams(layoutParams);
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT){
            int imageHeight = ScreenUtils.getDisplayWidthPixels(getContext()) / 2;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imageHeight);
            iv_front.setLayoutParams(layoutParams);
            if (ScreenUtils.getDisplayWidthPixels(getContext()) < 480) {
                CommonUtils.adaptGridLayoutColumnCount(1, gl_details);
            } else {
                CommonUtils.adaptGridLayoutColumnCount(2, gl_details);
            }
        }*/
        int imageHeight = ScreenUtils.getDisplayWidthPixels(getContext()) / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imageHeight);
        iv_front.setLayoutParams(layoutParams);

        if (ScreenUtils.getDisplayWidthPixels(getContext()) < 480) {
            CommonUtils.adaptGridLayoutColumnCount(1, gl_details);
        } else {
            CommonUtils.adaptGridLayoutColumnCount(2, gl_details);
        }

        if (mPresenter.isCreatedByLoggedUser()) {
            iv_delete.setVisibility(VISIBLE);
            iv_edit.setVisibility(VISIBLE);
            iv_report.setVisibility(GONE);
            bt_order.setEnabled(false);
            bt_order.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        } else {
            iv_delete.setVisibility(GONE);
            iv_edit.setVisibility(GONE);
            iv_report.setVisibility(VISIBLE);
            bt_order.setEnabled(true);
            bt_order.setTextColor(getResources().getColor(R.color.accentColor));

            iv_creator.setOnClickListener(view1 -> mPresenter.onMemberClicked(mPresenter.getCreatorId()));
            tv_creator.setOnClickListener(view1 -> iv_creator.performClick());
        }

        setData();
        setupAdapters();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gl_details.setColumnCount(2);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (ScreenUtils.getDisplayWidthPixels(getContext()) < 480) {
                CommonUtils.adaptGridLayoutColumnCount(1, gl_details);
            } else {
                CommonUtils.adaptGridLayoutColumnCount(2, gl_details);
            }
        }
    }

    void setupAdapters() {
        rv_comments.setHasFixedSize(false);
        rv_comments.setNestedScrollingEnabled(false);

        // use a linear layout manager
        LinearLayoutManager lm_comments = new LinearLayoutManager(getContext());
        rv_comments.setLayoutManager(lm_comments);
        rv_comments.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.comment_listing, 15)
                .withBottomEdge(true));

        // Initialize the Adapter
        flexibleCommentsAdapter = new CommentsAdapter(getFlexibleItemList(mPresenter.getListingCommentList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_comments.setAdapter(flexibleCommentsAdapter);

        flexibleCommentsAdapter
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    void setData() {
        if (mPresenter.isInEventMode()) {
            li_event_desc.setVisibility(GONE);
            li_secondary_delivery_info.setVisibility(GONE);
            li_tertiary_delivery_info.setVisibility(GONE);
            tv_address.setTextColor(getResources().getColor(R.color.textFair));
            tv_stock.setTextColor(getResources().getColor(R.color.textFair));
            tv_date.setTextColor(getResources().getColor(R.color.textFair));
            tv_deadline.setTextColor(getResources().getColor(R.color.textFair));
            iv_address.setImageResource(R.drawable.ic_stand);
        } else {
            if (mPresenter.getCurrentEventsWithListingAvailability().size() > 0) {
                li_event_desc.setVisibility(VISIBLE);
                tv_event_desc.setText(mPresenter.getEventInfoText());
            } else {
                li_event_desc.setVisibility(GONE);
            }
            tv_address.setTextColor(getResources().getColor(R.color.primaryTextColor));
            tv_stock.setTextColor(getResources().getColor(R.color.primaryTextColor));
            tv_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
            tv_deadline.setTextColor(getResources().getColor(R.color.primaryTextColor));
            switch (mPresenter.getShippingMethod()) {
                case Listing.ShippingMethod.PICKUP:
                    li_primary_delivery_info.setVisibility(VISIBLE);
                    li_secondary_delivery_info.setVisibility(GONE);
                    li_tertiary_delivery_info.setVisibility(GONE);
                    break;
                case Listing.ShippingMethod.DELIVERY:
                    li_primary_delivery_info.setVisibility(GONE);
                    li_secondary_delivery_info.setVisibility(VISIBLE);
                    li_tertiary_delivery_info.setVisibility(GONE);
                    tv_secondary_delivery_info.setText(mPresenter.getSecondaryShippingText());
                    break;
                case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                    li_primary_delivery_info.setVisibility(VISIBLE);
                    li_secondary_delivery_info.setVisibility(VISIBLE);
                    li_tertiary_delivery_info.setVisibility(GONE);
                    tv_secondary_delivery_info.setText(mPresenter.getSecondaryShippingText());
                    break;
                case Listing.ShippingMethod.DIGITAL:
                    li_primary_delivery_info.setVisibility(GONE);
                    li_secondary_delivery_info.setVisibility(GONE);
                    li_tertiary_delivery_info.setVisibility(VISIBLE);
                    tv_tertiary_delivery_info.setText("Envío digital");
                    break;
            }
        }
        tv_address.setText(mPresenter.getListingAddressText());
        tv_stock.setText(mPresenter.getListingStockText());
        tv_date.setText(mPresenter.getListingEndDateText());
        if (mPresenter.hasDiscount()) {
            tv_discount_percent.setText(mPresenter.getPercentageDiscountText());
            cv_percent_discount.setVisibility(VISIBLE);
            //viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.textFair));
            //viewHolder.tv_divider.setTextColor(getResources().getColor(R.color.textFair));
            //viewHolder.tv_price_max.setTextColor(getResources().getColor(R.color.textFair));
        } else {
            cv_percent_discount.setVisibility(GONE);
            //viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
            //viewHolder.tv_divider.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
            //viewHolder.tv_price_max.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
        }

        tv_title.setText(mPresenter.getListingTitle());
        tv_text.setText(mPresenter.getListingDescription());
        iv_badge_new.setVisibility(mPresenter.showBadgeNew() ? VISIBLE : GONE);
        rb_rating_2.setRating(mPresenter.getUserRating());
        tv_rating_2.setText(mPresenter.getUserRatingsCount());

        if (mPresenter.getType() == Listing.Type.OFFER) {
            bt_order.setText("Encargar");

            rb_rating.setRating(mPresenter.getListingRating());
            tv_rating.setText(mPresenter.getListingRatingsCount());
            rb_rating_3.setRating(mPresenter.getWorkRating());
            tv_rating_3.setText(mPresenter.getWorkRatingsCount());
            li_rating.setVisibility(VISIBLE);
            li_rating_3.setVisibility(VISIBLE);

            if (mPresenter.hasRangedPrice()) {
                tv_price_min.setText(mPresenter.getPriceMinText());
                tv_price_min.setVisibility(VISIBLE);
                iv_diag_min.setVisibility(VISIBLE);
                tv_divider.setVisibility(VISIBLE);
                tv_price_max.setText(mPresenter.getPriceMaxText());
                tv_price_max.setVisibility(VISIBLE);
                iv_diag_max.setVisibility(VISIBLE);
            } else {
                tv_price_min.setText(mPresenter.getPriceMinText());
                tv_price_min.setVisibility(VISIBLE);
                iv_diag_min.setVisibility(VISIBLE);
                tv_divider.setVisibility(GONE);
                tv_price_max.setVisibility(GONE);
                iv_diag_max.setVisibility(GONE);
            }
        } else {
            bt_order.setText("Ofrecer");

            li_rating.setVisibility(GONE);
            li_rating_3.setVisibility(GONE);

            if (mPresenter.hasRangedPrice()) {
                tv_price_min.setText(mPresenter.getPriceMinText());
                tv_price_min.setVisibility(VISIBLE);
                iv_diag_min.setVisibility(VISIBLE);
                tv_divider.setVisibility(VISIBLE);
                tv_price_max.setText(mPresenter.getPriceMaxText());
                tv_price_max.setVisibility(VISIBLE);
                iv_diag_max.setVisibility(VISIBLE);
            } else {
                tv_price_max.setText(mPresenter.getPriceMaxText());
                tv_price_max.setVisibility(VISIBLE);
                iv_diag_max.setVisibility(VISIBLE);
                tv_divider.setVisibility(GONE);
                tv_price_min.setVisibility(GONE);
                iv_diag_min.setVisibility(GONE);
            }
        }
        tv_category.setText(mPresenter.getCategoryTree());
        tv_creator.setText(mPresenter.getCreatorName());
        tv_comments.setText(mPresenter.getCommentsCount());
        tv_completed.setText(mPresenter.getCompletedCount());
        mPresenter.loadProfileImage(iv_creator);
        mPresenter.loadListingFrontImage(iv_front);
    }

    @OnClick(R.id.bt_order)
    void onOrderClickListener() {
        if (mPresenter.isInEventMode() && !mPresenter.isInscribedInEventMode()) {
            CommonUtils.showDialog(getContext(), "Sin inscripción al evento!", "Debes completar tu inscripción antes de poder realizar este encargo");
            return;
        }
        if (!mPresenter.hasStock()) {
            LayoutInflater factory = LayoutInflater.from(getContext());
            final View view = factory.inflate(R.layout.order_no_stock_dialog, null);
            Button bt_ok = view.findViewById(R.id.bt_ok);
            SwitchCompat sw_receive_notification = view.findViewById(R.id.sw_receive_notification);

            CustomDialog noStockDialog = new CustomDialog(getContext());
            noStockDialog.setView(view);

            sw_receive_notification.setOnCheckedChangeListener((compoundButton, b) -> {
                //TODO: implement
                CommonUtils.showNotImplementedToast();
            });
            bt_ok.setOnClickListener(view1 -> noStockDialog.dismiss());
            noStockDialog.show();
            return;
        }

        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.order_confirm_dialog, null);
        final Button bt_cancel = view.findViewById(R.id.bt_cancel);
        final Button bt_ok = view.findViewById(R.id.bt_ok);
        final EditText et_pay = view.findViewById(R.id.et_pay);
        final SwitchCompat sw_include_note = view.findViewById(R.id.sw_include_note);
        final EditText et_note = view.findViewById(R.id.et_note);
        final NumberPicker np_quantity = view.findViewById(R.id.np_quantity);
        final TextView tv_total = view.findViewById(R.id.tv_total);
        final CheckBox cb_donate = view.findViewById(R.id.cb_donate);

        final RadioButton rb_pickup = view.findViewById(R.id.rb_pickup);
        final RadioButton rb_delivery = view.findViewById(R.id.rb_delivery);
        final RadioButton rb_digital = view.findViewById(R.id.rb_digital);
        final TextView tv_pickup_address = view.findViewById(R.id.tv_pickup_address);
        final LinearLayout li_address = view.findViewById(R.id.li_address);
        final EditText et_address = view.findViewById(R.id.et_address);
        final LinearLayout li_digital_address = view.findViewById(R.id.li_digital_address);
        final EditText et_digital_address = view.findViewById(R.id.et_digital_address);
        final LinearLayout li_shipping_price = view.findViewById(R.id.li_shipping_price);
        final TextView tv_shipping_price = view.findViewById(R.id.tv_shipping_price);
        final LinearLayout li_pickup = view.findViewById(R.id.li_pickup);
        final LinearLayout li_delivery = view.findViewById(R.id.li_delivery);
        final LinearLayout li_digital = view.findViewById(R.id.li_digital);
        final ImageView iv_pickup = view.findViewById(R.id.iv_pickup);
        final ImageView iv_delivery = view.findViewById(R.id.iv_delivery);
        final ImageView iv_digital = view.findViewById(R.id.iv_digital);

        if (mPresenter.getType() == Listing.Type.OFFER) {
            et_pay.setText(mPresenter.getPriceMinText());
        } else {
            et_pay.setText(mPresenter.getPriceMaxText());
        }
        if (mPresenter.hasRangedPrice()) {
            et_pay.setEnabled(true);
        } else {
            et_pay.setEnabled(false);
        }
        if (mPresenter.getType() == Listing.Type.OFFER) {
            if (mPresenter.getPriceMin() == 0) {
                cb_donate.setEnabled(false);
            } else {
                cb_donate.setEnabled(true);
            }
        } else {
            if (mPresenter.getPriceMax() == 0) {
                cb_donate.setEnabled(false);
            } else {
                cb_donate.setEnabled(true);
            }
        }

        et_pay.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    tv_total.setText("-.");
                    cb_donate.setChecked(false);
                    cb_donate.setEnabled(false);
                    return;
                }
                double newPrice = Double.parseDouble(s.toString());
                if (newPrice == 0) {
                    cb_donate.setChecked(false);
                    cb_donate.setEnabled(false);
                } else {
                    cb_donate.setEnabled(true);
                }
                double total;
                if (selectedShippingMethod == Listing.ShippingMethod.DELIVERY) {
                    total = (newPrice + mPresenter.getShippingPrice()) * np_quantity.getValue();
                } else {
                    total = newPrice * np_quantity.getValue();
                }
                if (cb_donate.isChecked()) {
                    total = total * 1.01;
                }
                tv_total.setText(AppConstants.CURRENCY_FORMAT.format(total));
            }
        });

        // Set value
        np_quantity.setMaxValue(mPresenter.getMaxAvailableQuantity());
        np_quantity.setMinValue(1);
        np_quantity.setValue(1);
        np_quantity.setOnValueChangedListener((picker, oldVal, newVal) -> {
            if (et_pay.getText().toString().isEmpty()) {
                tv_total.setText("-.");
                return;
            }
            double newPrice = Double.parseDouble(et_pay.getText().toString());
            double total;
            if (selectedShippingMethod == Listing.ShippingMethod.DELIVERY) {
                total = (newPrice + mPresenter.getShippingPrice()) * newVal;
            } else {
                total = newPrice * newVal;
            }
            if (cb_donate.isChecked()) {
                total = total * 1.01;
            }
            tv_total.setText(AppConstants.CURRENCY_FORMAT.format(total));
        });

        cb_donate.setOnCheckedChangeListener((compoundButton, b) -> {
            if (et_pay.getText().toString().isEmpty()) {
                tv_total.setText("-.");
                return;
            }
            double newPrice = Double.parseDouble(et_pay.getText().toString());
            double total;
            if (selectedShippingMethod == Listing.ShippingMethod.DELIVERY) {
                total = (newPrice + mPresenter.getShippingPrice()) * np_quantity.getValue();
            } else {
                total = newPrice * np_quantity.getValue();
            }
            if (b) {
                total = total * 1.01;
            }
            tv_total.setText(AppConstants.CURRENCY_FORMAT.format(total));
        });

        sw_include_note.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                et_note.setVisibility(VISIBLE);
                KeyboardUtils.showSoftInput(et_note, getContext());
            } else {
                et_note.setVisibility(GONE);
                KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
            }
        });

        //Set delivery method
        if (mPresenter.isInEventMode()) {
            selectedShippingMethod = Listing.ShippingMethod.PICKUP;
            iv_pickup.setImageResource(R.drawable.ic_stand_big);
            rb_pickup.setChecked(true);
            rb_pickup.setText("Evento");
            li_delivery.setVisibility(GONE);
            li_digital.setVisibility(GONE);
            tv_pickup_address.setTextColor(getResources().getColor(R.color.textFair));
            tv_pickup_address.setText(mPresenter.getListingAddress());
            li_address.setVisibility(GONE);
            li_shipping_price.setVisibility(INVISIBLE);
            li_digital_address.setVisibility(GONE);
        } else {
            iv_pickup.setImageResource(R.drawable.ic_location_big);
            tv_pickup_address.setTextColor(getResources().getColor(R.color.secondaryTextColor));
            rb_pickup.setText("Dirección");
            switch (mPresenter.getShippingMethod()) {
                case Listing.ShippingMethod.PICKUP:
                    selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                    rb_pickup.setChecked(true);
                    li_delivery.setVisibility(GONE);
                    li_digital.setVisibility(GONE);
                    tv_pickup_address.setText(mPresenter.getListingAddress());
                    li_address.setVisibility(GONE);
                    li_shipping_price.setVisibility(INVISIBLE);
                    li_digital_address.setVisibility(GONE);
                    break;
                case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                    selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                    rb_pickup.setChecked(true);
                    li_digital.setVisibility(GONE);
                    tv_pickup_address.setText(mPresenter.getListingAddress());
                    tv_shipping_price.setText(mPresenter.getShippingPriceText());
                    li_address.setVisibility(GONE);
                    li_shipping_price.setVisibility(INVISIBLE);
                    li_digital_address.setVisibility(GONE);
                    break;
                case Listing.ShippingMethod.DELIVERY:
                    selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                    rb_delivery.setChecked(true);
                    li_pickup.setVisibility(GONE);
                    li_digital.setVisibility(GONE);
                    tv_shipping_price.setText(mPresenter.getShippingPriceText());
                    tv_pickup_address.setVisibility(GONE);
                    li_digital_address.setVisibility(GONE);
                    break;
                case Listing.ShippingMethod.DIGITAL:
                    selectedShippingMethod = Listing.ShippingMethod.DIGITAL;
                    rb_digital.setChecked(true);
                    li_pickup.setVisibility(GONE);
                    li_delivery.setVisibility(GONE);
                    li_address.setVisibility(GONE);
                    li_shipping_price.setVisibility(INVISIBLE);
                    tv_pickup_address.setVisibility(GONE);
                    break;
            }
            iv_pickup.setOnClickListener(view12 -> {
                if (rb_pickup.isEnabled()) {
                    rb_pickup.performClick();
                }
            });
            rb_pickup.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    rb_delivery.setChecked(false);
                    rb_digital.setChecked(false);
                    selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                    tv_total.setText(mPresenter.getTotalPriceText(selectedShippingMethod, cb_donate.isChecked()));

                    tv_pickup_address.setVisibility(VISIBLE);
                    li_address.setVisibility(GONE);
                    li_shipping_price.setVisibility(INVISIBLE);
                    li_digital_address.setVisibility(GONE);
                }
            });
            iv_delivery.setOnClickListener(view12 -> {
                if (rb_delivery.isEnabled()) {
                    rb_delivery.performClick();
                }
            });
            rb_delivery.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    rb_pickup.setChecked(false);
                    rb_digital.setChecked(false);
                    selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                    tv_total.setText(mPresenter.getTotalPriceText(selectedShippingMethod, cb_donate.isChecked()));

                    li_address.setVisibility(VISIBLE);
                    li_shipping_price.setVisibility(VISIBLE);
                    tv_pickup_address.setVisibility(GONE);
                    li_digital_address.setVisibility(GONE);
                }
            });
            iv_digital.setOnClickListener(view12 -> {
                if (rb_digital.isEnabled()) {
                    rb_digital.performClick();
                }
            });
            rb_digital.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    rb_pickup.setChecked(false);
                    rb_delivery.setChecked(false);
                    selectedShippingMethod = Listing.ShippingMethod.DIGITAL;
                    tv_total.setText(mPresenter.getTotalPriceText(selectedShippingMethod, cb_donate.isChecked()));

                    li_digital_address.setVisibility(VISIBLE);
                    li_address.setVisibility(GONE);
                    li_shipping_price.setVisibility(INVISIBLE);
                    tv_pickup_address.setVisibility(GONE);
                }
            });
        }
        tv_total.setText(mPresenter.getTotalPriceText(selectedShippingMethod, cb_donate.isChecked()));

        CustomDialog orderDialog = new CustomDialog(getContext());
        orderDialog.setView(view);

        bt_cancel.setOnClickListener(view1 -> {
            orderDialog.dismiss();
            KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
        });
        bt_ok.setOnClickListener(v2 -> {
            et_pay.setError(null);
            View focusView = null;
            String price = et_pay.getText().toString();
            int quantity = np_quantity.getValue();
            String note = et_note.getText().toString().trim();
            String shippingInfo;
            if (mPresenter.getShippingMethod() == Listing.ShippingMethod.DIGITAL) {
                shippingInfo = et_digital_address.getText().toString().trim();
            } else {
                shippingInfo = et_address.getText().toString().trim();
            }
            if (!mPresenter.confirmOrder(price, quantity, sw_include_note.isChecked(), note, selectedShippingMethod, shippingInfo, cb_donate.isChecked())) {
                switch (mPresenter.getErrorCode()) {
                    case AppConstants.INVALID_MIN_PRICE:
                        focusView = et_pay;
                        et_pay.setError(mPresenter.getError());
                        break;
                    case AppConstants.INVALID_MAX_PRICE:
                        focusView = et_pay;
                        et_pay.setError(mPresenter.getError());
                        break;
                    case AppConstants.EMPTY_SHIPPING_INFO:
                        if (selectedShippingMethod == Listing.ShippingMethod.DIGITAL) {
                            focusView = et_digital_address;
                            et_digital_address.setError(mPresenter.getError());
                        } else {
                            focusView = et_address;
                            et_address.setError(mPresenter.getError());
                        }
                        break;
                    case AppConstants.EMPTY_TEXT_FIELD:
                        focusView = et_note;
                        et_note.setError(mPresenter.getError());
                        break;
                }
                if (focusView != null) {
                    focusView.requestFocus();
                }
            } else {
                KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
                orderDialog.dismiss();
            }
        });
        ViewGroup layout = view.findViewById(R.id.li_content);
        LayoutTransition layoutTransition = layout.getLayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);

        ViewGroup layout2 = view.findViewById(R.id.li_footer);
        LayoutTransition layoutTransition2 = layout2.getLayoutTransition();
        layoutTransition2.enableTransitionType(LayoutTransition.CHANGING);
        orderDialog.show();
    }

    @OnClick(R.id.iv_delete)
    void onDeleteClickListener() {
        CustomDialog confirmDialog = new CustomDialog(getContext());
        confirmDialog.setTitle("Deseas eliminar tu listado?");
        confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
            mPresenter.onDeleteClicked();
        });
        confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
        confirmDialog.show();
    }

    @OnClick(R.id.iv_edit)
    void onEditClickListener(View view) {
        LayoutInflater li = LayoutInflater.from(getContext());
        final View pView = li.inflate(R.layout.listing_edit, (ViewGroup) view.getRootView(), false);

        CustomDialog editDialog = new CustomDialog(getContext());
        editDialog.setView(pView);

        final NumberPicker np_quantity = pView.findViewById(R.id.np_quantity);
        final EditText et_text = pView.findViewById(R.id.et_text);
        final EditText et_price_min = pView.findViewById(R.id.et_price_min);
        final EditText et_price_max = pView.findViewById(R.id.et_price_max);
        final TextView tv_divider = pView.findViewById(R.id.tv_divider);
        final Switch sw_ranged = pView.findViewById(R.id.sw_ranged);
        final Switch sw_max_stock = pView.findViewById(R.id.sw_max_stock);
        final EditText et_max_stock = pView.findViewById(R.id.et_max_stock);
        final Button bt_apply = pView.findViewById(R.id.bt_apply);
        final Button bt_cancel = pView.findViewById(R.id.bt_cancel);

        final ImageView iv_pickup = pView.findViewById(R.id.iv_pickup);
        final CheckBox cb_pickup = pView.findViewById(R.id.cb_pickup);
        final ImageView iv_delivery = pView.findViewById(R.id.iv_delivery);
        final CheckBox cb_delivery = pView.findViewById(R.id.cb_delivery);
        final ImageView iv_digital = pView.findViewById(R.id.iv_digital);
        final CheckBox cb_digital = pView.findViewById(R.id.cb_digital);
        final LinearLayout li_delivery_price = pView.findViewById(R.id.li_delivery_price);
        final LinearLayout li_address = pView.findViewById(R.id.li_address);
        final EditText et_address = pView.findViewById(R.id.et_address);
        final EditText et_delivery_price = pView.findViewById(R.id.et_delivery_price);

        //Set text
        et_text.setText(mPresenter.getListingDescription());

        // Set Price
        if (mPresenter.hasRangedPrice()) {
            et_price_min.setText(AppConstants.CURRENCY_FORMAT.format(mPresenter.getPriceMin()));
            et_price_min.setVisibility(View.VISIBLE);
            tv_divider.setVisibility(View.VISIBLE);
            et_price_max.setText(AppConstants.CURRENCY_FORMAT.format(mPresenter.getPriceMax()));
            et_price_max.setVisibility(View.VISIBLE);
            sw_ranged.setChecked(true);
        } else {
            if (mPresenter.getType() == Listing.Type.OFFER) {
                et_price_min.setText(AppConstants.CURRENCY_FORMAT.format(mPresenter.getPriceMin()));
                et_price_min.setVisibility(View.VISIBLE);
                tv_divider.setVisibility(View.INVISIBLE);
                et_price_max.setVisibility(View.INVISIBLE);
                et_price_max.setVisibility(View.INVISIBLE);
                sw_ranged.setChecked(false);
            } else {
                et_price_max.setText(AppConstants.CURRENCY_FORMAT.format(mPresenter.getPriceMax()));
                et_price_max.setVisibility(View.VISIBLE);
                tv_divider.setVisibility(View.GONE);
                et_price_min.setVisibility(View.GONE);
                et_price_min.setVisibility(View.GONE);
                sw_ranged.setChecked(false);
            }
        }
        sw_ranged.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                tv_divider.setVisibility(View.VISIBLE);
                et_price_min.setHint("Precio min.");
                et_price_min.setVisibility(View.VISIBLE);
                et_price_max.setHint("Precio max.");
                et_price_max.setVisibility(View.VISIBLE);
            } else {
                tv_divider.setVisibility(View.GONE);
                if (mPresenter.getType() == Listing.Type.OFFER) {
                    et_price_min.setHint("Precio");
                    et_price_min.setVisibility(View.VISIBLE);
                    et_price_max.setVisibility(View.INVISIBLE);
                } else {
                    et_price_max.setHint("Precio");
                    et_price_max.setVisibility(View.VISIBLE);
                    et_price_min.setVisibility(View.GONE);
                }
            }
        });

        // Set Stock and limit if any
        np_quantity.setMinValue(0);
        np_quantity.setMaxValue(99);
        np_quantity.setValue(mPresenter.getGlobalStock());
        if (mPresenter.getMaxQuantity() != -1) {
            sw_max_stock.setChecked(true);
            et_max_stock.setText(String.valueOf(mPresenter.getMaxQuantity()));
            et_max_stock.setVisibility(View.VISIBLE);
        } else {
            sw_max_stock.setChecked(false);
            et_max_stock.setText("");
            et_max_stock.setVisibility(View.INVISIBLE);
        }
        sw_max_stock.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                et_max_stock.setVisibility(View.VISIBLE);
            } else {
                et_max_stock.setVisibility(View.INVISIBLE);
            }
        });

        //Set delivery method
        switch (mPresenter.getShippingMethod()) {
            case Listing.ShippingMethod.PICKUP:
                selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                cb_pickup.setChecked(true);
                et_address.setText(mPresenter.getListingAddress());
                li_delivery_price.setVisibility(GONE);
                break;
            case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                cb_pickup.setChecked(true);
                cb_delivery.setChecked(true);
                et_address.setText(mPresenter.getListingAddress());
                et_delivery_price.setText(mPresenter.getShippingPriceText());
                break;
            case Listing.ShippingMethod.DELIVERY:
                selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                cb_delivery.setChecked(true);
                et_delivery_price.setText(mPresenter.getShippingPriceText());
                li_address.setVisibility(GONE);
                break;
            case Listing.ShippingMethod.DIGITAL:
                selectedShippingMethod = Listing.ShippingMethod.DIGITAL;
                cb_digital.setChecked(true);
                li_delivery_price.setVisibility(GONE);
                li_address.setVisibility(GONE);
                break;
        }
        iv_pickup.setOnClickListener(view12 -> cb_pickup.performClick());
        cb_pickup.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                li_address.setVisibility(VISIBLE);
                if (selectedShippingMethod == Listing.ShippingMethod.DIGITAL) {
                    cb_digital.setChecked(false);
                    selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                } else {
                    if (cb_delivery.isChecked()) {
                        selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                    } else {
                        selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                    }
                }
            } else {
                li_address.setVisibility(GONE);
                if (cb_delivery.isChecked()) {
                    selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                } else {
                    selectedShippingMethod = -1;
                }
            }
        });
        iv_delivery.setOnClickListener(view12 -> cb_delivery.performClick());
        cb_delivery.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                li_delivery_price.setVisibility(VISIBLE);
                if (selectedShippingMethod == Listing.ShippingMethod.DIGITAL) {
                    cb_digital.setChecked(false);
                    selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                } else {
                    if (cb_pickup.isChecked()) {
                        selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                    } else {
                        selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                    }
                }
            } else {
                li_delivery_price.setVisibility(GONE);
                if (cb_pickup.isChecked()) {
                    selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                } else {
                    selectedShippingMethod = -1;
                }
            }
        });
        iv_digital.setOnClickListener(view12 -> cb_digital.performClick());
        cb_digital.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (selectedShippingMethod == Listing.ShippingMethod.PICKUP) {
                    cb_pickup.setChecked(false);
                } else if (selectedShippingMethod == Listing.ShippingMethod.PICKUP_AND_DELIVERY) {
                    cb_pickup.setChecked(false);
                    cb_delivery.setChecked(false);
                } else if (selectedShippingMethod == Listing.ShippingMethod.DELIVERY) {
                    cb_delivery.setChecked(false);
                }
                selectedShippingMethod = Listing.ShippingMethod.DIGITAL;
            } else {
                selectedShippingMethod = -1;
            }
        });

        bt_apply.setOnClickListener(v -> {
            final String text = et_text.getText().toString().trim();
            final String priceMin = et_price_min.getText().toString();
            final String priceMax = et_price_max.getText().toString();
            final int stock = np_quantity.getValue();
            final String address = et_address.getText().toString().trim();
            final String shippingPrice = et_delivery_price.getText().toString().trim();
            final int perBuyMaxStock;
            if (sw_max_stock.isChecked()) {
                perBuyMaxStock = Integer.valueOf(et_max_stock.getText().toString());
            } else {
                perBuyMaxStock = -1;
            }
            if (mPresenter.onApplyEdits(text, priceMin, priceMax, sw_ranged.isChecked(), stock, selectedShippingMethod, address, shippingPrice, perBuyMaxStock)) {
                KeyboardUtils.hideSoftInput((ListingActivity) getContext(), pView.findFocus());
                editDialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(v -> {
            KeyboardUtils.hideSoftInput((ListingActivity) getContext(), pView.findFocus());
            editDialog.dismiss();
        });
        ViewGroup layout = pView.findViewById(R.id.li_listing_edit);
        LayoutTransition layoutTransition = layout.getLayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        //layoutTransition.setStartDelay(LayoutTransition.CHANGE_DISAPPEARING, 0);

        ViewGroup layout2 = pView.findViewById(R.id.li_content);
        LayoutTransition layoutTransition2 = layout2.getLayoutTransition();
        layoutTransition2.enableTransitionType(LayoutTransition.CHANGING);
        editDialog.show();
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_comments.setVisibility(size == 0 ? View.GONE : View.VISIBLE);
        tv_comments_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_comments.scrollToPosition(0);
        }
    }

    void dataSetChanged(boolean scrollTop) {
        setData();
        this.scrollTop = scrollTop;
        flexibleCommentsAdapter.updateDataSet(getFlexibleItemList(mPresenter.getListingCommentList()));
    }

    class CommentsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        CommentsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class CommentItemAdapter extends AbstractFlexibleItem<CommentItemAdapter.ViewHolder> {

        private Rating rating;

        CommentItemAdapter(Rating rating) {
            this.rating = rating;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof CommentItemAdapter) {
                Rating inItem = ((CommentItemAdapter) inObject).rating;
                return this.rating.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return rating.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.comment_listing;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            if (DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mPresenter.getUser(rating.getIdSender()).getDate()))) {
                viewHolder.iv_badge_new.setVisibility(View.GONE);
            } else {
                viewHolder.iv_badge_new.setVisibility(View.VISIBLE);
            }

            viewHolder.rb_rating.setRating(rating.getRating());
            viewHolder.tv_creator.setText(mPresenter.getUser(rating.getIdSender()).getName());
            viewHolder.tv_comment.setText(rating.getComment());
            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(rating.getDate()), AppConstants.COMPLETE_DATE_FORMAT));

            /*String[] likes_array = data.get(_position).get("likes").replaceAll("/", "").split(",");
            if (likes_array[0].equals("")) {
                tv_likes.setText("0");
            } else {
                tv_likes.setText(String.valueOf(likes_array.length));
            }

            List<String> myList = new ArrayList<String>();
            myList = Arrays.asList(likes_array);
            if (myList.contains("\"" + id_user + "\"")) {
                iv_likes.setImageResource(R.drawable.ic_favorite_outline_red);
            } else {
                iv_likes.setImageResource(R.drawable.ic_favorite_outline_white);
            }*/

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(rating.getIdSender()), viewHolder.iv_creator);

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != rating.getIdSender()) {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(rating.getIdSender())};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (ListingActivity) getContext(), parcelables);
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> viewHolder.iv_creator.performClick());
            } else {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    //
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_comment)
            TextView tv_comment;
            @BindView(R.id.iv_creator)
            ImageView iv_creator;
            @BindView(R.id.tv_creator)
            TextView tv_creator;
            @BindView(R.id.tv_date)
            TextView tv_date;
            //final ImageView iv_likes = _v.findViewById(R.id.iv_likes);
            //final TextView tv_likes = _v.findViewById(R.id.tv_likes);
            @BindView(R.id.rb_rating)
            SimpleRatingBar rb_rating;
            @BindView(R.id.iv_badge_new)
            ImageView iv_badge_new;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleItemList(List<Rating> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Rating item : list) {
            itemList.add(new CommentItemAdapter(item));
        }
        return itemList;
    }
}