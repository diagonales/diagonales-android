package eco.darmas.activities.listing;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.SparseArray;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ListingPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> {

    private ListingModel mModel;
    private ListingView mView;
    private int errorCode;
    private String error;

    ListingPresenter(ListingModel model, ListingView view) {
        super(model, view);
        this.mModel = model;
        this.mView = view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void updateData() {
        getDisposables().add(mModel.reloadListing()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> mView.setData())
                .subscribe());
    }

    private void updateComments() {
        getDisposables().add(mModel.fetchListingComments()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(ratings -> mView.dataSetChanged(false))
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.LISTINGS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData();
                    updateComments();
                }
            }
            case DataLoadedEvent.RATINGS: {
                if (event.isSuccess()) {
                    updateData();
                    updateComments();
                }
            }
        }
    }

    void onDeleteClicked() {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            return;
        }
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.LISTING_MODIFIED);
        mModel.commitDeleteListing(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject JSONResponse;
                try {
                    JSONResponse = new JSONObject(new String(response.body().bytes()));
                    if (JSONResponse.get("result").equals("true")) {
                        mModel.writeDeleteListing();
                        event.setSuccess(true);
                        event.setDialogTitle("El listado ha sido eliminado");
                    } else {
                        event.setDialogTitle("El listado no pudo ser eliminado");
                    }
                } catch (Exception e) {
                    event.setDialogTitle("Error al eliminar el listado");
                    event.setDialogMessage("Por favor contacta un administrador");
                    AppLogger.e(e, null);
                } finally {
                    EventBus.getDefault().postSticky(event);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                event.setDialogTitle("Error al eliminar el listado");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().postSticky(event);
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissActivity(mView, true);
    }

    String getListingAddressText() {
        Listing listing = mModel.getListing();
        if (mModel.getEventModeId() != -1) {
            int eventIndex = mModel.getEventInscriptionMap().indexOfKey(mModel.getEventModeId());
            if (eventIndex >= 0) {
                int stand = mModel.getEventInscriptionMap().valueAt(eventIndex).get(listing.getIdCreator(), new Inscription()).getStand();
                if (stand > 0) {
                    return "Stand #" + stand + " del evento " + "\"" + mModel.getCommunityEvent(mModel.getEventModeId()).getTitle() + "\"";
                }
            }
            return "Sin stand, en el evento";
        }
        return listing.getAddress();
    }

    int getShippingMethod() {
        Listing listing = mModel.getListing();
        return listing.getShippingMethod();
    }

    String getListingAddress() {
        Listing listing = mModel.getListing();
        if (mModel.getEventModeId() > -1) {
            int eventIndex = mModel.getEventInscriptionMap().indexOfKey(mModel.getEventModeId());
            if (eventIndex >= 0) {
                int stand = mModel.getEventInscriptionMap().valueAt(eventIndex).get(listing.getIdCreator(), new Inscription()).getStand();
                if (stand > 0) {
                    return "Stand #" + stand;
                }
            }
            return "Sin stand, contactar";
        }
        return listing.getAddress();
    }

    String getShippingPriceText() {
        return AppConstants.CURRENCY_FORMAT.format(getShippingPrice());
    }

    double getShippingPrice() {
        Listing listing = mModel.getListing();
        return listing.getShippingPrice();
    }

    boolean showBadgeNew() {
        return DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mModel.getUser(mModel.getListing().getIdCreator()).getDate()));
    }

    float getUserRating() {
        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getUserRatingsMap().get(mModel.getListing().getIdCreator(), new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    String getUserRatingsCount() {
        int size = mModel.getUserRatingsMap().get(mModel.getListing().getIdCreator(), new SparseArray<>()).size();
        return "(" + String.valueOf(size) + ")";
    }

    float getListingRating() {
        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getListingRatingsMap().get(mModel.getListing().getIdListing(), new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    String getListingRatingsCount() {
        int count = mModel.getListingRatingsMap().get(mModel.getListing().getIdListing(), new SparseArray<>()).size();
        return "(" + String.valueOf(count) + ")";
    }

    float getWorkRating() {
        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getWorksRatingsMap().get(mModel.getListing().getIdCreator(), new SparseArray<>()).get(mModel.getListing().getIdCategory(), new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    String getWorkRatingsCount() {
        int count = mModel.getWorksRatingsMap().get(mModel.getListing().getIdCreator(), new SparseArray<>()).get(mModel.getListing().getIdCategory(), new SparseArray<>()).size();
        return "(" + String.valueOf(count) + ")";
    }

    Spannable getCategoryTree() {
        Spannable textToSpan = new SpannableString(mModel.getCategoryTree());
        int start = mModel.getCategoryTree().lastIndexOf(mModel.getListingCategoryNode().getData().getName());
        int end = mModel.getCategoryTree().length();
        if (start != -1 && end != -1) {
            textToSpan.setSpan(new StyleSpan(Typeface.ITALIC), 0, start, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            textToSpan.setSpan(new ForegroundColorSpan(mView.getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        return textToSpan;
    }

    String getCreatorName() {
        return mModel.getUser(mModel.getListing().getIdCreator()).getName();
    }

    void loadProfileImage(ImageView iv_creator) {
        ImageUtils.loadRoundedProfileImage(getUser(mModel.getListing().getIdCreator()), iv_creator);
    }

    void loadListingFrontImage(ImageView iv_front) {
        ImageUtils.loadListingImage(mModel.getListing(), iv_front);
    }

    String getListingEndDateText() {
        if (mModel.getEventModeId() != -1) {
            //TODO: allow user to establish an particular end time on all listings at the event if he wants to go before close
            return DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mModel.getCommunityEvent(mModel.getEventModeId()).getDateEnd()), AppConstants.ABBREVIATED_DATE_FORMAT);
        }
        return DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mModel.getListing().getDate()), AppConstants.ABBREVIATED_DATE_FORMAT);
    }

    String getListingTitle() {
        return mModel.getListing().getTitle();
    }

    String getListingDescription() {
        return mModel.getListing().getText();
    }

    boolean hasRangedPrice() {
        return mModel.getListing().getRanged().equals("1");
    }

    boolean hasDiscount() {
        return getEventDiscount() > 0;
    }

    double getEventDiscount() {
        Listing listing = mModel.getListing();
        double discount = 0;
        int eventMode = mModel.getEventModeId();
        if (eventMode >= 0) {
            int eventIndex = mModel.getEventListingJsonObjectMap().indexOfKey(eventMode);
            if (eventIndex >= 0) {
                int listingIndex = mModel.getEventListingJsonObjectMap().get(eventMode).indexOfKey(listing.getIdListing());
                if (listingIndex >= 0) {
                    if (mModel.getEventListingJsonObjectMap().get(eventMode).get(listing.getIdListing()).has("discount")) {
                        try {
                            discount = Double.valueOf((String) mModel.getEventListingJsonObjectMap().get(eventMode).get(listing.getIdListing()).get("discount"));
                        } catch (JSONException e) {
                            AppLogger.e(e, null);
                        }
                    }
                }
            }
        }
        return discount;
    }

    String getPercentageDiscountText() {
        double percentage = (getEventDiscount() * 100) / mModel.getListing().getPriceMin();
        return AppConstants.PERCENTAGE_FORMAT.format(percentage) + " OFF";
    }

    String getPriceMinText() {
        double discount = getEventDiscount();
        if (discount > 0) {
            return AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMin() - discount);
        }
        return AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMin());
    }

    String getPriceMaxText() {
        double discount = getEventDiscount();
        if (discount > 0) {
            return AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMax() - discount);
        }
        return AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMax());
    }

    double getPriceMin() {
        return mModel.getListing().getPriceMin();
    }

    double getPriceMax() {
        return mModel.getListing().getPriceMax();
    }

    boolean isCreatedByLoggedUser() {
        return mModel.getLoggedUser().getIdUser() == mModel.getListing().getIdCreator();
    }

    public int getType() {
        return mModel.getListing().getType();
    }

    boolean confirmOrder(String priceText, int quantity, boolean includeNote, String note, int shippingMethod, String shippingInfo, boolean contribution) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            setErrorResponse(AppConstants.OFFLINE_MODE, null);
            return false;
        }
        if (priceText.isEmpty()) {
            setErrorResponse(AppConstants.INVALID_MIN_PRICE, "Debes ingresar un precio");
            return false;
        }
        double price = Double.valueOf(priceText);
        if ((shippingMethod == Listing.ShippingMethod.DELIVERY || shippingMethod == Listing.ShippingMethod.DIGITAL) && shippingInfo.isEmpty()) {
            setErrorResponse(AppConstants.EMPTY_SHIPPING_INFO, "Debes ingresar la dirección del envío");
            return false;
        }
        double totalPrice;
        double contributionPrice = 0;
        if (shippingMethod == Listing.ShippingMethod.DELIVERY) {
            totalPrice = (price + getShippingPrice()) * quantity;
        } else {
            totalPrice = price * quantity;
        }
        if (contribution) {
            contributionPrice = totalPrice * 0.01;
            totalPrice = totalPrice + contributionPrice;
        }

        double priceMin = mModel.getListing().getPriceMin();
        double priceMax = mModel.getListing().getPriceMax();
        if (isInEventMode()) {
            priceMin = priceMin - getEventDiscount();
            priceMax = priceMax - getEventDiscount();
        }
        // Check for a valid price, if the user entered one.
        if (hasRangedPrice() && price < priceMin) {
            setErrorResponse(AppConstants.INVALID_MIN_PRICE, "El precio no puede ser inferior a " + AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMin()));
            return false;
        } else if (hasRangedPrice() && price > priceMax) {
            setErrorResponse(AppConstants.INVALID_MAX_PRICE, "El precio no puede ser superior a " + AppConstants.CURRENCY_FORMAT.format(mModel.getListing().getPriceMax()));
            return false;
        } else if (mModel.getListing().getType() == Listing.Type.OFFER && (mModel.getLoggedUser().getFunds() - totalPrice) < (0 - mModel.getLoggedUser().getPaymentMargin())) {
            CommonUtils.showErrorToast("Este pago excedería tu margen de saldo actual: (-" + mModel.getLoggedUser().getPaymentMargin() + " +" + mModel.getLoggedUser().getPaymentMargin() + ")");
            return false;
        } else if (mModel.getListing().getType() == Listing.Type.DEMAND && (mModel.getLoggedUser().getFunds() + totalPrice) > mModel.getLoggedUser().getPaymentMargin()) {
            CommonUtils.showErrorToast("Este ingreso excedería tu margen de saldo actual: (-" + mModel.getLoggedUser().getPaymentMargin() + " +" + mModel.getLoggedUser().getPaymentMargin() + ")");
            return false;
        } else if (mModel.getListing().getType() == Listing.Type.OFFER && totalPrice > mModel.getUserMaxAvailableIncome(mModel.getListing().getIdCreator())) {
            CommonUtils.showErrorToast("El anunciante no cuenta con margen suficiente para aceptar esta operación");
            return false;
        } else if (mModel.getListing().getType() == Listing.Type.DEMAND && mModel.getUser(mModel.getListing().getIdCreator()).getFunds() - totalPrice < 0 - mModel.getUser(mModel.getListing().getIdCreator()).getPaymentMargin()) {
            CommonUtils.showErrorToast("El anunciante no cuenta con margen suficiente para aceptar esta operación");
            return false;
        } else if (includeNote && note.isEmpty()) {
            setErrorResponse(AppConstants.EMPTY_TEXT_FIELD, "La nota adicional no esta escrita");
            return false;
        } else if (!includeNote) {
            note = "";
        }
        resetErrors();
        startTransaction(totalPrice, quantity, note, shippingMethod, shippingInfo, contributionPrice);
        return true;
    }

    private void setErrorResponse(int errorCode, String error) {
        this.errorCode = errorCode;
        this.error = error;
    }

    private void resetErrors() {
        errorCode = AppConstants.CLEARED;
        error = "";
    }

    int getErrorCode() {
        return errorCode;
    }

    String getError() {
        return error;
    }

    String getCommentsCount() {
        int count = 0;
        for (Rating rating : mModel.getListingCommentList()) {
            if (!rating.getComment().isEmpty()) {
                count++;
            }
        }
        return String.valueOf(count);
    }

    void onMemberClicked(int userId) {
        Object[] parcelables = new Object[]{mModel.getUser(userId)};
        AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (ListingActivity) mView.getContext(), parcelables);
    }

    int getCreatorId() {
        return mModel.getListing().getIdCreator();
    }

    private void startTransaction(final double price, int quantity, String note, int shippingMethod, String shippingInfo, double contribution) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.TRANSACTION_UPDATED);
        double shippingPrice = getShippingPrice();
        int eventId = mModel.getEventModeId();
        /*int inscriptionId = 0;
        String stockAtEventString = "";
        if (eventId > -1) {
            Inscription inscription = mModel.getDataManager().getEventInscriptionMap().get(eventId).get(mModel.listing.getIdCreator());
            inscriptionId = inscription.getIdInscription();
            try {
                JSONObject jsonObject = mModel.getDataManager().getEventListingIdList().get(eventId).get(mModel.listing.getIdListing());
                int stockAtEvent = Integer.valueOf((String) jsonObject.get("stock"));
                stockAtEvent = stockAtEvent - quantity;
                jsonObject.put("stock", stockAtEvent);
                stockAtEventString = jsonObject.toString();
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }*/
        mModel.commitCreateTransaction(price, quantity, note, shippingMethod, shippingPrice, shippingInfo, eventId, contribution, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            event.setSuccess(true);

                            _Transaction transaction = new _Transaction()
                                    .withIdTransaction((int) responseJSON.get("id"))
                                    .withIdSender(mModel.getLoggedUser().getIdUser())
                                    .withIdReceiver(mModel.getListing().getIdCreator())
                                    .withIdCategory(mModel.getListing().getIdCategory())
                                    .withConcept(mModel.getListing().getTitle())
                                    .withAmount(price)
                                    .withQuantity(quantity)
                                    .withNote(note)
                                    .withDate(DateUtils.getTimeStamp())
                                    .withStatus(_Transaction.Status.REQUESTED)
                                    .withType(mModel.getListing().getType())
                                    .withIdListing(mModel.getListing().getIdListing())
                                    .withShippingMethod(shippingMethod)
                                    .withContribution(contribution)
                                    .withShippingPrice(shippingPrice)
                                    .withIdEvent(eventId);
                            mModel.writeTransaction(transaction);

                            if (mModel.getListing().getType() == _Transaction.Type.OFFER) {
                                mModel.writePaymentReserve(price);
                                event.setDialogTitle("Encargo enviado!");
                                event.setDialogMessage("El monto se ha reservado hasta que recibas una respuesta, en caso de ser rechazado, se reintegrará a tu saldo.");
                            } else {
                                event.setDialogTitle("Oferta enviada!");
                                event.setDialogMessage("Ahora debes esperar una respuesta");
                            }
                        } else {
                            event.setDialogTitle("El encargo no pudo ser enviado");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        event.setDialogTitle("El encargo no pudo ser enviado");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.d(t, null);
                event.setDialogTitle("El encargo no pudo ser enviado");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, los fondos no fueron enviados. Por favor vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.dismissActivity(mView, true);
    }

    List<Integer> getCurrentEventsWithListingAvailability() {
        List<Integer> eventsList = new ArrayList<>();
        Listing listing = mModel.getListing();
        for (CommunityEvent communityEvent : mModel.getCurrentCommunityEventList()) {
            int eventId = communityEvent.getIdEvent();
            int eventIndex = mModel.getEventListingJsonObjectMap().indexOfKey(eventId);
            if (eventIndex >= 0) {
                int listingIndex = mModel.getEventListingJsonObjectMap().get(eventId).indexOfKey(listing.getIdListing());
                if (listingIndex >= 0) {
                    eventsList.add(communityEvent.getIdEvent());
                }
            }
        }
        return eventsList;
    }

    String getListingStockText() {
        Listing listing = mModel.getListing();
        int stock = 0;
        int eventMode = mModel.getEventModeId();
        if (eventMode >= 0) {
            int eventIndex = mModel.getEventListingJsonObjectMap().indexOfKey(eventMode);
            if (eventIndex >= 0) {
                int listingIndex = mModel.getEventListingJsonObjectMap().get(eventMode).indexOfKey(listing.getIdListing());
                if (listingIndex >= 0) {
                    try {
                        stock = Integer.valueOf((String) mModel.getEventListingJsonObjectMap().get(eventMode).get(listing.getIdListing()).get("stock"));
                    } catch (JSONException e) {
                        AppLogger.e(e, null);
                    }
                    return "Disponib.: " + stock;
                }
            }
        }
        //TODO: to show stock or availability in case of services
        return "Disponib.: " + String.valueOf(mModel.getListing().getStock());
    }

    boolean hasStock() {
        return getStock() > 0;
    }

    int getStock() {
        Listing listing = mModel.getListing();
        int stock = 0;
        int eventMode = mModel.getEventModeId();
        if (eventMode >= 0) {
            int eventIndex = mModel.getEventListingJsonObjectMap().indexOfKey(eventMode);
            if (eventIndex >= 0) {
                int listingIndex = mModel.getEventListingJsonObjectMap().get(eventMode).indexOfKey(listing.getIdListing());
                if (listingIndex >= 0) {
                    try {
                        stock = Integer.valueOf((String) mModel.getEventListingJsonObjectMap().get(eventMode).get(listing.getIdListing()).get("stock"));
                    } catch (JSONException e) {
                        AppLogger.e(e, null);
                    }
                    return stock;
                }
            }
        }
        stock = mModel.getListing().getStock();
        return stock;
    }

    int getGlobalStock() {
        return mModel.getListing().getStock();
    }

    int getMaxAvailableQuantity() {
        int maxQuantity;
        if (mModel.getListing().getMaxQuantity() != -1 && mModel.getListing().getMaxQuantity() < getStock()) {
            maxQuantity = mModel.getListing().getMaxQuantity();
        } else {
            maxQuantity = getStock();
        }
        return maxQuantity;
    }

    int getMaxQuantity() {
        return mModel.getListing().getMaxQuantity();
    }

    boolean onApplyEdits(String text, String priceMinText, String priceMaxText, boolean ranged, int stock, int shippingMethod, String address, String shippingPriceText, int perBuyMaxStock) {
        if (!text.equals("")) {
            double priceMin = 0;
            double priceMax = 0;
            if (getType() == Listing.Type.OFFER) {
                if (!priceMinText.equals("")) {
                    priceMin = Double.parseDouble(priceMinText);
                } else {
                    CommonUtils.showInfoToast("Debes ingresar un precio");
                    return false;
                }
            } else {
                if (!priceMaxText.equals("")) {
                    priceMax = Double.parseDouble(priceMaxText);
                } else {
                    CommonUtils.showInfoToast("Debes ingresar un precio");
                    return false;
                }
            }
            if (perBuyMaxStock == 0) {
                CommonUtils.showErrorToast("El limite de unidades por orden debe ser mayor a 0");
                return false;
            }

            double shippingPrice = 0;
            switch (shippingMethod) {
                case Listing.ShippingMethod.PICKUP:
                    if (address.isEmpty()) {
                        CommonUtils.showInfoToast("Debes ingresar la dirección de referencia");
                        return false;
                    }
                    break;
                case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                    if (address.isEmpty()) {
                        CommonUtils.showInfoToast("Debes ingresar la dirección de referencia");
                        return false;
                    }
                    if (shippingPriceText.isEmpty()) {
                        CommonUtils.showInfoToast("Ingresa el precio de envío (0 para envíos sin cargo)");
                        return false;
                    }
                    shippingPrice = Double.valueOf(shippingPriceText);
                    break;
                case Listing.ShippingMethod.DELIVERY:
                    if (shippingPriceText.isEmpty()) {
                        CommonUtils.showInfoToast("Ingresa el precio de envío (0 para envíos sin cargo)");
                        return false;
                    }
                    shippingPrice = Double.valueOf(shippingPriceText);
                    break;
                case Listing.ShippingMethod.DIGITAL:
                    break;
                default:
                    CommonUtils.showInfoToast("Debes seleccionar al menos un metodo de entrega");
                    return false;
            }

            if (!ranged) {
                if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                    applyEdits(text, priceMin, priceMax, 0, stock, shippingMethod, address, shippingPrice, perBuyMaxStock);
                    return true;
                } else {
                    CommonUtils.showErrorToast("Comprueba tu conección");
                    return false;
                }
            } else {
                if (getType() == Listing.Type.OFFER) {
                    if (!priceMaxText.equals("")) {
                        priceMax = Double.parseDouble(priceMaxText);
                    } else {
                        CommonUtils.showInfoToast("Debes ingresar un precio maximo");
                        return false;
                    }
                } else {
                    if (!priceMinText.equals("")) {
                        priceMin = Double.parseDouble(priceMinText);
                    } else {
                        CommonUtils.showInfoToast("Debes ingresar un precio minimo");
                        return false;
                    }
                }
                if (priceMax > priceMin) {
                    if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                        applyEdits(text, priceMin, priceMax, 1, stock, shippingMethod, address, shippingPrice, perBuyMaxStock);
                        return true;
                    } else {
                        CommonUtils.showErrorToast("Comprueba tu conección");
                        return false;
                    }
                } else {
                    CommonUtils.showInfoToast("El precio maximo debe ser mayor que el minimo");
                }
            }
        } else {
            CommonUtils.showInfoToast("Ingresa una descripción de lo que ofreces");
        }
        return false;
    }

    private void applyEdits(String text, double priceMin, double priceMax, int ranged, int newQuantity, int shippingMethod, String address, double shippingPrice, int perBuyMaxStock) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.LISTING_MODIFIED);
        mModel.commitEditListing(text, priceMin, priceMax, ranged, newQuantity, shippingMethod, address, shippingPrice, perBuyMaxStock, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            event.setSuccess(true);

                            mModel.writeListingEdit(text, priceMin, priceMax, String.valueOf(ranged), newQuantity, shippingMethod, address, shippingPrice, perBuyMaxStock);
                            if (mModel.getListing().getType() == _Transaction.Type.OFFER) {
                                CommonUtils.showSuccessToast("Tu oferta ha sido actualizada");
                            } else {
                                CommonUtils.showSuccessToast("Tu demanda ha sido actualizada");
                            }
                        } else {
                            AppLogger.d((String) responseJSON.get("error"));
                            CommonUtils.showErrorToast("El listado no pudo ser actualizado");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CommonUtils.showErrorToast("El listado no pudo ser actualizado, por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.d(t, null);
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("El listado no pudo ser actualizado, debido a un error, por favor contacta un administrador");
                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    List<Rating> getListingCommentList() {
        return mModel.getListingCommentList();
    }

    String getCompletedCount() {
        return String.valueOf(mModel.getListing().getCompleted());
    }

    double getTotalPrice(int shippingMethod, boolean contribution) {
        double totalPrice;
        if (getType() == Listing.Type.OFFER) {
            totalPrice = mModel.getListing().getPriceMin();
        } else {
            totalPrice = mModel.getListing().getPriceMax();
        }

        if (shippingMethod != Listing.ShippingMethod.DELIVERY) {
            double discount = getEventDiscount();
            if (discount > 0) {
                totalPrice = totalPrice - discount;
            }
        } else {
            totalPrice = totalPrice + mModel.getListing().getShippingPrice();
        }

        if (contribution) {
            totalPrice = totalPrice * 1.01;
        }
        return totalPrice;
    }

    String getTotalPriceText(int shippingMethod, boolean contribution) {
        return AppConstants.CURRENCY_FORMAT.format(getTotalPrice(shippingMethod, contribution));
    }

    /*boolean isInscribed() {
        int eventMode = mModel.getEventModeId();
        int eventIndex = mModel.getEventInscriptionMap().indexOfKey(eventMode);
        return eventIndex > -1 && mModel.getEventInscriptionMap().get(eventMode).indexOfKey(mModel.getLoggedUser().getIdUser()) > -1;
    }*/

    String getEventInfoText() {
        return "* Este producto o servicio tambien se encuentra disponible ahora mismo en el evento";
    }

    String getSecondaryShippingText() {
        String secondaryShippingText = "Envío ";
        if (getShippingMethod() == Listing.ShippingMethod.PICKUP_AND_DELIVERY) {
            secondaryShippingText = secondaryShippingText.concat("opcional, ");
        }
        if (getShippingPrice() == 0) {
            secondaryShippingText = secondaryShippingText.concat("sin cargo");
        } else {
            secondaryShippingText = secondaryShippingText.concat("con cargo");
        }
        return secondaryShippingText;
    }
}