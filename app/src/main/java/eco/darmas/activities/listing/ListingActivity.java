package eco.darmas.activities.listing;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;

public class ListingActivity extends BaseActivity {

    @Override
    protected ListingModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new ListingModel(dataManager, savedInstanceState, getIntent());
    }

    @Override
    protected ListingPresenter createPresenter(BaseActivityModel model, BaseActivityView view) {
        return new ListingPresenter((ListingModel) model, (ListingView) view);
    }

    @Override
    protected ListingView createView(Context context) {
        return new ListingView(context);
    }
}
