package eco.darmas.activities.listing;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;

import com.thirtydegreesray.dataautoaccess.annotation.AutoAccess;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class ListingModel extends BaseActivityModel {

    @AutoAccess
    Listing listing;
    private List<Rating> listingCommentList;

    ListingModel(DataManager dataManager, Bundle savedInstanceState, Intent intent) {
        super(dataManager, savedInstanceState);
        AppData.INSTANCE.loadInstanceState(savedInstanceState);
        getDataManager().startDataLoad();

        if (savedInstanceState == null) {
            listing = intent.getParcelableExtra("parcelable#0");
        }
        fetchListingComments().subscribe();
    }

    @Override
    protected void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        AppData.INSTANCE.saveInstanceState(outState);
    }

    Completable reloadListing() {
        return Completable.create(e -> {
            listing = getDataManager().getListingsMap().get(listing.getIdListing());
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation());
    }

    Single<List<Rating>> fetchListingComments() {
        List<Rating> ratingList = new ArrayList<>();
        return Single.create((SingleOnSubscribe<List<Rating>>) e -> {
            for (int i = 0; i < getDataManager().getRatingsMap().size(); i++) {
                Rating rating = getDataManager().getRatingsMap().valueAt(i);
                if (rating.getType() != Rating.Type.LISTING || rating.getIdTarget() != listing.getIdListing() || rating.getComment().isEmpty()) {
                    continue;
                }
                ratingList.add(rating);
            }
            e.onSuccess(ratingList);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(ratings -> listingCommentList = ratings);
    }

    Listing getListing() {
        return listing;
    }

    void commitDeleteListing(Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().deleteListing(listing.getIdCreator(), listing.getIdListing(), listing.getIdCategory(), listing.getIdEvent(), listing.getType(), listing.getImage()).enqueue(callback);
    }

    void writeDeleteListing() {
        getDataManager().getBoxStore().boxFor(Listing.class).remove(listing);
        if (listing.getIdEvent() >= 0) {
            Inscription inscription = getDataManager().getEventInscriptionMap().get(listing.getIdEvent()).get(listing.getIdCreator());
            try {
                JSONArray list = new JSONArray();
                JSONArray jsonArray = new JSONArray(inscription.getListings());
                int len = jsonArray.length();
                for (int i = 0; i < len ; i++) {
                    //Excluding the item at position
                    if (!jsonArray.getJSONObject(i).get("id_listing").equals(String.valueOf(listing.getIdListing()))) {
                        list.put(jsonArray.get(i));
                    }
                }
                inscription.setListings(list.toString());
                getDataManager().getBoxStore().boxFor(Inscription.class).put(inscription);
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        } else {
            Category category = getDataManager().getBoxStore().boxFor(Category.class).get(listing.getIdCategory());
            if (listing.getType() == Listing.Type.OFFER) {
                try {
                    JSONArray offerListings = new JSONArray();
                    if (!category.getOfferListings().isEmpty()) {
                        offerListings = new JSONArray(category.getOfferListings());
                    }

                    JSONArray list = new JSONArray();
                    for (int i = 0; i < offerListings.length(); i++) {
                        //Excluding the item at position
                        int listingId = offerListings.getInt(i);
                        if (listingId != listing.getIdListing()) {
                            list.put(offerListings.get(i));
                        }
                    }
                    category.setOfferListings(list.toString());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            } else {
                try {
                    JSONArray demandListings = new JSONArray();
                    if (!category.getDemandListings().isEmpty()) {
                        demandListings = new JSONArray(category.getDemandListings());
                    }

                    JSONArray list = new JSONArray();
                    for (int i = 0; i < demandListings.length(); i++) {
                        //Excluding the item at position
                        int listingId = demandListings.getInt(i);
                        if (listingId != listing.getIdListing()) {
                            list.put(demandListings.get(i));
                        }
                    }
                    category.setDemandListings(list.toString());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            }
            getDataManager().getBoxStore().boxFor(Category.class).put(category);
        }
    }

    Node<Category> getListingCategoryNode() {
        return getDataManager().getCategoryNodeMap().get(listing.getIdCategory());
    }

    SparseArray<SparseArray<Float>> getListingRatingsMap() {
        return getDataManager().getListingRatingsMap();
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorksRatingsMap() {
        return getDataManager().getWorkRatingsMap();
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    void commitCreateTransaction(double payment, int quantity, String note, int shippingMethod, double shippingPrice, String shippingInfo, int eventId, double contribution, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().processTransaction(getLoggedUser().getIdUser(), listing.getIdCreator(), listing.getIdCategory(), listing.getTitle(), payment, listing.getIdListing(), listing.getType(), 0, _Transaction.Status.REQUESTED, note, quantity, shippingMethod, shippingPrice, shippingInfo, eventId, contribution).enqueue(callback);
    }

    String getCategoryTree() {
            List<String> tree = new ArrayList<>();
            String categoryTree = "";
            Node<Category> categoryNode = getListingCategoryNode();
            tree.add(categoryNode.getData().getName());
            while (true) {
                if (!categoryNode.isRoot()) {
                    categoryNode = categoryNode.getParent();
                    tree.add(categoryNode.getData().getName());
                } else {
                    break;
                }
            }
            Collections.reverse(tree);
            for (int i = 0; i < tree.size(); i++) {
                String categoryName = tree.get(i);
                if (i == 1) {
                    categoryTree = categoryTree.concat(categoryName);
                } else if (i > 1) {
                    categoryTree = categoryTree.concat(" > " + categoryName);
                }
            }
            return categoryTree;
    }

    void writePaymentReserve(double payment) {
        User user = getLoggedUser();
        user.setFunds(user.getFunds() - payment);
        getDataManager().getBoxStore().boxFor(User.class).put(user);
    }

    void writeTransaction(_Transaction transaction) {
        getDataManager().getBoxStore().boxFor(_Transaction.class).put(transaction);
        /*getDataManager().getBoxStore().runInTxAsync(() -> {
            getDataManager().getBoxStore().boxFor(_Transaction.class).put(transaction);
        }, null);*/
    }

    double getUserMaxAvailableIncome(int idCreator) {
        return getDataManager().getUserMaxAvailableIncome(idCreator);
    }

    void commitEditListing(String text, double priceMin, double priceMax, int ranged, int newQuantity, int shippingMethod, String address, double shippingPrice, int perBuyMaxStock, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().listingEdit(listing.getIdListing(), text, priceMin, priceMax, ranged, newQuantity, shippingMethod, address, shippingPrice, perBuyMaxStock).enqueue(callback);
    }

    void writeListingEdit(String text, double priceMin, double priceMax, String ranged, int stock, int shippingMethod, String address, double shippingPrice, int perBuyMaxStock) {
        listing.setText(text);
        listing.setPriceMin(priceMin);
        listing.setPriceMax(priceMax);
        listing.setRanged(ranged);
        listing.setStock(stock);
        listing.setShippingMethod(shippingMethod);
        listing.setAddress(address);
        listing.setShippingPrice(shippingPrice);
        listing.setMaxQuantity(perBuyMaxStock);
        getDataManager().getBoxStore().boxFor(Listing.class).put(listing);
    }

    List<Rating> getListingCommentList() {
        return listingCommentList;
    }

    SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return getDataManager().getEventInscriptionMap();
    }

    CommunityEvent getCommunityEvent(int eventModeId) {
        return getDataManager().getCommunityEventMap().get(eventModeId);
    }

    SparseArray<SparseArray<JSONObject>> getEventListingJsonObjectMap() {
        return getDataManager().getEventListingIdList();
    }

    List<CommunityEvent> getCurrentCommunityEventList() {
        return getDataManager().getCurrentCommunityEventsList();
    }
}