package eco.darmas.activities.preferences;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.KeyboardUtils;
import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        PrefsFragment mPrefsFragment = new PrefsFragment();
        mFragmentTransaction.replace(android.R.id.content, mPrefsFragment);
        mFragmentTransaction.commit();
    }

    public static class PrefsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            Preference button = findPreference(getString(R.string.change_password_button));
            button.setOnPreferenceClickListener(preference -> {
                // update pass
                LayoutInflater li = LayoutInflater.from(preference.getContext());
                final View pView = li.inflate(R.layout.update_pass, null);

                CustomDialog updatePassDialog = new CustomDialog(preference.getContext());
                updatePassDialog.setView(pView);

                final EditText et_old_pass = pView.findViewById(R.id.password_old);
                final EditText et_new_pass = pView.findViewById(R.id.et_password);
                final EditText et_rep_pass = pView.findViewById(R.id.password_repeat);
                final Button button_cancel = pView.findViewById(R.id.button_cancel);
                final Button button_ok = pView.findViewById(R.id.button_ok);

                button_ok.setOnClickListener(view -> {

                    // Reset errors.
                    et_old_pass.setError(null);
                    et_new_pass.setError(null);
                    et_rep_pass.setError(null);

                    // Store values at the time of the password change attempt.
                    String password = et_old_pass.getText().toString();
                    String new_password = et_new_pass.getText().toString();
                    String new_password_repeat = et_rep_pass.getText().toString();

                    boolean cancel = false;
                    View focusView = null;

                    // Check for a valid password, if the user entered one.
                    if (password.isEmpty() || !isPasswordValid(password)) {
                        et_old_pass.setError(getString(R.string.error_invalid_password));
                        focusView = et_old_pass;
                        cancel = true;
                    } else

                        // Check for a valid new password, if the user entered one.
                        if (new_password.isEmpty() || !isPasswordValid(new_password)) {
                            et_new_pass.setError(getString(R.string.error_invalid_password));
                            focusView = et_new_pass;
                            cancel = true;
                        }

                    // Check for a match in new passwords.
                    if (!new_password.equals(new_password_repeat)) {
                        et_rep_pass.setError(getString(R.string.passwords_no_match));
                        focusView = et_rep_pass;
                        cancel = true;
                    }

                    if (cancel) {
                        // There was an error; don't attempt to change password and focus the first
                        // form field with an error.
                        focusView.requestFocus();
                    } else {
                        // Show a progress spinner, and kick off a background task to
                        // perform the user password change attempt.
                        // showProgress(true);
                        String email = AppData.INSTANCE.getLoggedUser().getEmail();
                        _updatePass(email, password, new_password, preference.getContext());
                        KeyboardUtils.hideSoftInput((AppCompatActivity) preference.getContext(), view.getRootView().findFocus());
                        updatePassDialog.dismiss();
                    }
                });
                button_cancel.setOnClickListener(view -> {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) preference.getContext(), view.getRootView().findFocus());
                    updatePassDialog.dismiss();
                });

                updatePassDialog.show();
                return true;
            });
        }
    }

    private static void _updatePass(String email, String oldPass, String newPass, Context context) {
        String credentials = Credentials.basic(email, oldPass);
        App.getInstance().getNetworkService().getCallableAPI().updatePass(credentials, newPass).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonResponse;
                    try {
                        jsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (jsonResponse.get("result").equals("true")) {
                            CustomDialog dialog = new CustomDialog(context);
                            dialog.setTitle("Tu contraseña ha sido cambiada!");
                            dialog.setButton(CustomDialog.BUTTON_POSITIVE, "Ok", (dialog1, which) -> {
                                dialog1.dismiss();
                                ((AppCompatActivity) context).finish();
                            });
                            dialog.show();
                        } else if (jsonResponse.get("result").equals("false")) {
                            CommonUtils.showErrorToast("La contraseña ingresada no es la correcta");
                        } else {
                            CommonUtils.showErrorToast("La contraseña no se ha actualizado debido a un error");
                        }
                    } catch (Exception e) {
                        CommonUtils.showErrorToast("La contraseña no se ha actualizado debido a un error");
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                CommonUtils.showErrorToast("La contraseña no se ha actualizado debido a un error");
                AppLogger.e(t, null);
            }
        });
    }

    private static boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}