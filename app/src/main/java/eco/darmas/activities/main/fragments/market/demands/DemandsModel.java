package eco.darmas.activities.main.fragments.market.demands;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class DemandsModel extends BaseFragmentModel implements DemandsContracts.Model {

    private List<Listing> demandList = new ArrayList<>();

    DemandsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadDemandsData().subscribe();
    }

    Single loadDemandsData() {
        return Observable.create((ObservableOnSubscribe<Listing>) e -> {
            for (int i = getDataManager().getListingsMap().size() - 1; i > -1; i--) {
                Listing listing = getDataManager().getListingsMap().valueAt(i);
                if (listing.getType() == Listing.Type.DEMAND) {
                    int eventMode = getEventModeId();
                    int eventIndex = getDataManager().getEventListingIdList().indexOfKey(eventMode);
                    if ((eventMode >= 0 && eventIndex < 0) || (eventMode >= 0 && getDataManager().getEventListingIdList().get(eventMode).indexOfKey(listing.getIdListing()) < 0)) {
                        continue;
                    } else if (eventMode < 0 && listing.getIdEvent() >= 0) {
                        continue;
                    }
                    Node<Category> categoryNode = getDataManager().getCategoryNodeMap().get(listing.getIdCategory());
                    if (!getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getFullTree().contains(categoryNode)) {
                        continue;
                    }
                    if (!CommonUtils.containsText(listing.getTitle(), DataFilters.demandsConstraint)) {
                        continue;
                    }
                    e.onNext(listing);
                }
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(demands -> demandList = demands);
    }

    Category getCategory(int categoryId) {
        return getDataManager().getCategoryNodeMap().get(categoryId).getData();
    }

    List<Listing> getDemandList() {
        return demandList;
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    int getSelectedCategory() {
        return DataFilters.selectedCategoryId;
    }

    String getSelectedCategoryName() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getData().getName();
    }

    String getDemandListings() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getData().getDemandListings();
    }

    SparseArray<SparseArray<JSONObject>> getEventListingIdList() {
        return getDataManager().getEventListingIdList();
    }

    Listing getListingFromId(int listingId) {
        return getDataManager().getListingsMap().get(listingId);
    }

    SparseArray<SparseArray<JSONObject>> getEventListingJsonObjectMap() {
        return getDataManager().getEventListingIdList();
    }

    void addSearchConstraint(String search) {
        DataFilters.demandsConstraint = search;
    }

    int getSelectedOrder() {
        return DataFilters.selectedDemandsListOrder;
    }

    Listing getListing(int position) {
        return demandList.get(position);
    }

    SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return getDataManager().getEventInscriptionMap();
    }

    void refreshListings() {
        getDataManager().refreshListings(true);
        getDataManager().refreshEventListings(true);
        getDataManager().refreshRatings(true);
    }
}