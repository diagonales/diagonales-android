package eco.darmas.activities.main.fragments.market.category;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.pojos._Transaction;

@SuppressWarnings(value = "unchecked")
class CategoryModel extends BaseFragmentModel {

    private Node<Category> treeItem;
    private int type;
    private String categoryTree;

    CategoryModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        treeItem = getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId);
        type = arguments.getInt("integer#0");
        loadData();
    }

    private void loadData() {
        List<String> tree = new ArrayList<>();
        String categoryTree = "";
        Node<Category> categoryNode = treeItem;
        tree.add(categoryNode.getData().getName());
        while (true) {
            if (!categoryNode.isRoot()) {
                categoryNode = categoryNode.getParent();
                tree.add(categoryNode.getData().getName());
            } else {
                break;
            }
        }
        Collections.reverse(tree);
        for (int i = 0; i < tree.size(); i++) {
            String categoryName = tree.get(i);
            if (i == 1) {
                categoryTree = categoryTree.concat(categoryName);
            } else if (i > 1) {
                categoryTree = categoryTree.concat(" > " + categoryName);
            }
        }
        this.categoryTree = categoryTree;
    }

    Node<Category> getTreeItem() {
        return treeItem;
    }

    void setChildTreeItem(int id) {
        treeItem = getDataManager().getCategoryNodeMap().get(id);
        if (categoryTree.length() == 0) {
            categoryTree = categoryTree.concat(treeItem.getData().getName());
        } else {
            categoryTree = categoryTree.concat(" > " + treeItem.getData().getName());
        }
    }

    void setParentTreeItem(int id) {
        Node<Category> node = getDataManager().getCategoryNodeMap().get(id);
        if (!node.isRoot()) {
            treeItem = node.getParent();
            if (categoryTree.contains(">")) {
                categoryTree = categoryTree.substring(0, categoryTree.lastIndexOf(" > "));
            } else {
                categoryTree = "";
            }
        } else {
            treeItem = getDataManager().getCategoryNodeMap().get(0);
        }
    }

    int getSelectedCategory() {
        return DataFilters.selectedCategoryId;
    }

    int getType() {
        return type;
    }

    String getCategoryTree() {
        return categoryTree;
    }

    List<_Transaction> getLoggedUserOrdersList() {
        return getDataManager().getLoggedUserOrdersList();
    }

    List<Node<Category>> getCategoryNodeTree(int id) {
        return getDataManager().getCategoryNodeMap().get(id).getFullTree();
    }

    SparseArray<SparseArray<JSONObject>> getEventListingIdList() {
        return getDataManager().getEventListingIdList();
    }

    Listing getListingFromId(int listingId) {
        return getDataManager().getListingsMap().get(listingId);
    }

    SparseArray<SparseArray<JSONObject>> getEventListingJsonObjectMap() {
        return getDataManager().getEventListingIdList();
    }
}