package eco.darmas.activities.main.fragments.comunity.posts;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Post;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class PostsModel extends BaseFragmentModel implements PostsContracts.Model {

    private List<Post> postList = new ArrayList<>();
    private List<Post> headerList = new ArrayList<>();

    PostsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadData().subscribe();
    }

    Single loadData() {
        return Observable.create((ObservableOnSubscribe<Post>) e -> {
            for (int i = getDataManager().getPostMap().size() - 1; i > -1; i--) {
                Post post = getDataManager().getPostMap().valueAt(i);
                if (Integer.parseInt(post.getAnnounce()) != 0) {
                    continue;
                }
                if (post.getIdEvent() != getEventModeId()) {
                    continue;
                }
                e.onNext(post);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toSortedList((post1, post2) -> new PinnedComparator().compare(post1, post2))
                .doOnSuccess(sortedPosts -> {
                    postList = sortedPosts;
                    headerList.clear();
                    for (Post post : sortedPosts) {
                        if (post.getPinned().equals("2")) {
                            headerList.add(post);
                        }
                    }
                });
    }

    // Should be this method inside Post class?
    class PinnedComparator implements Comparator<Post> {

        @Override
        public int compare(Post post1, Post post2) {
            return Integer.parseInt(post2.getPinned()) - Integer.parseInt(post1.getPinned());
        }
    }

    List<Post> getPostList() {
        return postList;
    }

    Post getPost(int position) {
        return postList.get(position);
    }

    List<Post> getHeaderList() {
        return headerList;
    }

    Post getHeader(int position) {
        return headerList.get(position);
    }

    void refreshPosts() {
        getDataManager().refreshPosts(true);
    }
}