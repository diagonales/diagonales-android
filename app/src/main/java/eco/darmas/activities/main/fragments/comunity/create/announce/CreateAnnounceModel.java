package eco.darmas.activities.main.fragments.comunity.create.announce;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.SparseArray;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class CreateAnnounceModel extends BaseFragmentModel implements CreateAnnounceContracts.Model {


    private File announceImageFile;
    private Uri tempImageUri;

    CreateAnnounceModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    void commitCreatePost(final String title, final String text, String pinned, Callback<ResponseBody> callback) {
        MultipartBody.Part body = null;
        if (announceImageFile != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), announceImageFile);
            body = MultipartBody.Part.createFormData("uploaded_file", announceImageFile.getName(), reqFile);
        }

        RequestBody idCreatorBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getLoggedUser().getIdUser()));
        RequestBody idEventBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getEventModeId()));
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody announceBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(1));
        RequestBody pinnedBody = RequestBody.create(MediaType.parse("text/plain"), pinned);

        App.getInstance().getNetworkService().getCallableAPI().createPost(idCreatorBody, idEventBody, titleBody, textBody, announceBody, pinnedBody, body).enqueue(callback);

        /*@Override
        public void onProgress(long bytesWritten, long totalSize) {
            double mbWritten = (double) bytesWritten / 1048576;
            double mbTotalSize = (double) totalSize / 1048576;
            progress.setTitle("Cargando " + dfLoadImage.format(mbWritten) + "m de " + dfLoadImage.format(mbTotalSize) + "m");
            super.onProgress(bytesWritten, totalSize);
        }*/
    }

    void writePost(Post announce) {
        getDataManager().getBoxStore().boxFor(Post.class).put(announce);
    }

    File getAnnounceImageFile() {
        Calendar cal = Calendar.getInstance();
        announceImageFile = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/" + ("IMG_" + cal.getTimeInMillis() + ".jpg"));
        return announceImageFile;
    }

    void setAnnounceImageFile(String path) {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(App.getInstance().getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        announceImageFile = new File(CommonUtils.getRealImagePath(photo));
    }

    Uri getTempImageUri() {
        return tempImageUri;
    }

    void setTempImageUri(Uri tempImageUri) {
        this.tempImageUri = tempImageUri;
    }

    SparseArray<CommunityEvent> getCommunityEventMap() {
        return getDataManager().getCommunityEventMap();
    }
}