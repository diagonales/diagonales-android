package eco.darmas.activities.main.fragments.comunity.create.event;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class CreateCommunityEventFragment extends BaseFragment {

    @Override
    protected CreateCommunityEventModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new CreateCommunityEventModel(dataManager, savedInstanceState);
    }

    @Override
    protected CreateCommunityEventPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new CreateCommunityEventPresenter((CreateCommunityEventModel) model, (CreateCommunityEventView) view);
    }

    @Override
    protected CreateCommunityEventView createView(Context context) {
        return new CreateCommunityEventView(context);
    }
}