package eco.darmas.activities.main.fragments.comunity.posts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.items.AbstractHeaderItem;
import eu.davidea.flexibleadapter.items.IFlexible;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.viewholders.FlexibleViewHolder;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;

@SuppressLint("ViewConstructor")
class PostsView extends BaseFragmentView implements PostsContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private PostsPresenter mPresenter;
    private PostsAdapter flexiblePostsAdapter;
    private HeadersAdapter flexibleFootersAdapter;
    private Unbinder unBinder;
    private boolean scrollTop;

    @BindView(R.id.li_add_post)
    LinearLayout li_add_post;
    @BindView(R.id.iv_add_post)
    ImageView iv_add_post;
    @BindView(R.id.tv_add_post)
    TextView tv_add_post;
    @BindView(R.id.tv_posts_none)
    TextView tv_posts_none;
    @BindView(R.id.rv_posts)
    RecyclerView rv_posts;
    @BindView(R.id.rv_headers)
    RecyclerView rv_headers;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    PostsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.posts_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (PostsPresenter) presenter;
        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshItems());
        swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor);
        setupAdapters();
    }

    void setupAdapters() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rv_posts.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_posts = new LinearLayoutManager(getContext());
        rv_posts.setLayoutManager(lm_posts);
        rv_posts.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.post_pinned, 15)
                .withBottomEdge(true));
        rv_posts.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.post, 15)
                .withBottomEdge(true));

        LinearLayoutManager lm_footers = new LinearLayoutManager(getContext());
        rv_headers.setLayoutManager(lm_footers);

        // Initialize the Adapter
        flexiblePostsAdapter = new PostsAdapter(getFlexiblePostsItemList(mPresenter.getPostList()), this, true);

        flexibleFootersAdapter = new HeadersAdapter(getFlexibleHeadersItemList(mPresenter.getHeaderList()), null, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_posts.setAdapter(flexiblePostsAdapter);
        rv_headers.setAdapter(flexibleFootersAdapter);

        flexiblePostsAdapter
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
        flexibleFootersAdapter
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    @OnClick({R.id.iv_add_post, R.id.tv_add_post})
    void onAddPostClickListener() {
        mPresenter.onAddPostClicked();
    }

    void dataSetChanged(boolean scrollTop) {
        this.scrollTop = scrollTop;
        flexiblePostsAdapter.updateDataSet(getFlexiblePostsItemList(mPresenter.getPostList()));
        flexibleFootersAdapter.updateDataSet(getFlexibleHeadersItemList(mPresenter.getHeaderList()));
    }

    void stopRefreshAnimation() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onPostItemClick(position);
    }

    void scrollTop() {
        rv_posts.smoothScrollToPosition(0);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_posts.setVisibility(mPresenter.getPostList().size() == 0 && mPresenter.getHeaderList().size() == 0 ? View.GONE : View.VISIBLE);
        tv_posts_none.setVisibility(mPresenter.getPostList().size() == 0 && mPresenter.getHeaderList().size() == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_posts.scrollToPosition(0);
        }
    }

    class PostsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        PostsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class PostItemAdapter extends AbstractFlexibleItem<PostItemAdapter.ViewHolder> {

        private Post post;

        PostItemAdapter(Post post) {
            this.post = post;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof PostItemAdapter) {
                Post inItem = ((PostItemAdapter) inObject).post;
                return this.post.getIdPost() == inItem.getIdPost();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return post.getIdPost();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.post;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int id_creator = post.getIdCreator();

            viewHolder.tv_creator.setText(mPresenter.getUser(id_creator).getName());
            viewHolder.tv_title.setText(post.getTitle());

            viewHolder.tv_comments.setText(post.getComments());

            JSONArray likesJsonArray = new JSONArray();
            if (!post.getLikes().isEmpty()) {
                try {
                    likesJsonArray = new JSONArray(post.getLikes());
                } catch (Exception e) {
                    AppLogger.e(e, null);
                }
            }

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != id_creator) {
                if (CommonUtils.containsId(likesJsonArray, AppData.INSTANCE.getLoggedUser().getIdUser())) {
                    viewHolder.iv_likes.setImageResource(R.drawable.ic_favorite_outline_red);
                } else {
                    viewHolder.iv_likes.setImageResource(R.drawable.ic_favorite_outline_white);
                }
            } else {
                viewHolder.iv_likes.setImageResource(R.drawable.ic_favorite_border_disabled);
            }
            viewHolder.tv_likes.setText(String.valueOf(likesJsonArray.length()));

            if (!post.getImage().equals("")) {
                final float scale = getResources().getDisplayMetrics().density;
                int pixels = (int) (216 * scale + 0.5f);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels);
                viewHolder.iv_front.setLayoutParams(layoutParams);
                ImageUtils.loadPostImage(post, viewHolder.iv_front);
            } else {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                viewHolder.iv_front.setLayoutParams(layoutParams);
            }

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(post.getIdCreator()), viewHolder.iv_creator);

            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(post.getDate()), AppConstants.COMPLETE_DATE_FORMAT));
            if (post.getText().length() > 240) {
                viewHolder.tv_text.setText(post.getText().substring(0, 240).concat("..."));
            } else {
                viewHolder.tv_text.setText(post.getText());
            }

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != id_creator) {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(id_creator)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> viewHolder.iv_creator.performClick());
            } else {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    //
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_title)
            TextView tv_title;
            @BindView(R.id.iv_front)
            ImageView iv_front;
            @BindView(R.id.tv_text)
            TextView tv_text;
            @BindView(R.id.iv_creator)
            ImageView iv_creator;
            @BindView(R.id.tv_creator)
            TextView tv_creator;
            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.iv_likes)
            ImageView iv_likes;
            @BindView(R.id.tv_likes)
            TextView tv_likes;
            @BindView(R.id.tv_comments)
            TextView tv_comments;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    class PinnedPostItemAdapter extends AbstractFlexibleItem<PinnedPostItemAdapter.PinnedViewHolder> {

        Post post;

        PinnedPostItemAdapter(Post post) {
            this.post = post;
            setHidden(false);
            setSelectable(true);
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof PinnedPostItemAdapter) {
                Post inItem = ((PinnedPostItemAdapter) inObject).post;
                return this.post.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return post.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.post_pinned;
        }

        @Override
        public PinnedViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new PinnedViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, PinnedViewHolder viewHolder, int position, List<Object> payloads) {
            int id_creator = post.getIdCreator();

            viewHolder.tv_creator.setText(mPresenter.getUser(id_creator).getName());
            viewHolder.tv_title.setText(post.getTitle());
            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(post.getDate()), AppConstants.COMPLETE_DATE_FORMAT));
            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(post.getIdCreator()), viewHolder.iv_creator);

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != id_creator) {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(id_creator)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> viewHolder.iv_creator.performClick());
            } else {
                viewHolder.iv_creator.setOnClickListener(view1 -> {
                    //
                });
                viewHolder.tv_creator.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class PinnedViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_title)
            TextView tv_title;
            @BindView(R.id.iv_creator)
            ImageView iv_creator;
            @BindView(R.id.tv_creator)
            TextView tv_creator;
            @BindView(R.id.tv_date)
            TextView tv_date;

            PinnedViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    class HeadersAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        HeadersAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class HeaderItemAdapter extends AbstractHeaderItem<HeaderItemAdapter.HeaderViewHolder> implements IHeader<HeaderItemAdapter.HeaderViewHolder> {

        Post post;

        HeaderItemAdapter(Post post) {
            this.post = post;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof HeaderItemAdapter) {
                Post inItem = ((HeaderItemAdapter) inObject).post;
                return this.post.getIdPost() == inItem.getIdPost();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return post.getIdPost();
        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public void setEnabled(boolean enabled) {

        }

        @Override
        public boolean isHidden() {
            return false;
        }

        @Override
        public void setHidden(boolean hidden) {

        }

        @Override
        public int getSpanSize(int spanCount, int position) {
            return 1;
        }

        @Override
        public boolean shouldNotifyChange(IFlexible newItem) {
            return true;
        }

        @Override
        public boolean isSelectable() {
            return false;
        }

        @Override
        public void setSelectable(boolean selectable) {

        }

        @Override
        public String getBubbleText(int position) {
            return null;
        }

        @Override
        public boolean isDraggable() {
            return false;
        }

        @Override
        public void setDraggable(boolean draggable) {

        }

        @Override
        public boolean isSwipeable() {
            return false;
        }

        @Override
        public void setSwipeable(boolean swipeable) {

        }

        @Override
        public int getItemViewType() {
            return 0;
        }

        @Override
        public int getLayoutRes() {
            return R.layout.post_header;
        }

        @Override
        public HeaderViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new HeaderViewHolder(view, adapter);
        }

        @Override
        public void unbindViewHolder(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void onViewAttached(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void onViewDetached(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, HeaderViewHolder viewHolder, int position, List<Object> payloads) {
            Completable.timer(5, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> viewHolder.tv_title.setSelected(false))
                    .subscribe(() -> viewHolder.tv_title.setSelected(true));
            viewHolder.tv_title.setText(post.getTitle() + " " + post.getText());
            viewHolder.tv_title.setOnClickListener(view -> mPresenter.onHeaderItemClick(position));
        }

        class HeaderViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_title)
            TextView tv_title;

            HeaderViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter, true);
                ButterKnife.bind(this, view);
                Completable.timer(5, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe(() -> tv_title.setSelected(true));
            }
        }
    }

    class HeaderWorkaroundItemAdapter extends AbstractHeaderItem<HeaderWorkaroundItemAdapter.HeaderViewHolder> implements IHeader<HeaderWorkaroundItemAdapter.HeaderViewHolder> {

        Post post;

        HeaderWorkaroundItemAdapter(Post post) {
            this.post = post;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof HeaderWorkaroundItemAdapter) {
                Post inItem = ((HeaderWorkaroundItemAdapter) inObject).post;
                return this.post.getIdPost() == inItem.getIdPost();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return post.getIdPost();
        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public void setEnabled(boolean enabled) {
        }

        @Override
        public boolean isHidden() {
            return false;
        }

        @Override
        public void setHidden(boolean hidden) {
        }

        @Override
        public int getSpanSize(int spanCount, int position) {
            return 1;
        }

        @Override
        public boolean shouldNotifyChange(IFlexible newItem) {
            return false;
        }

        @Override
        public boolean isSelectable() {
            return false;
        }

        @Override
        public void setSelectable(boolean selectable) {
        }

        @Override
        public String getBubbleText(int position) {
            return null;
        }

        @Override
        public boolean isDraggable() {
            return false;
        }

        @Override
        public void setDraggable(boolean draggable) {
        }

        @Override
        public boolean isSwipeable() {
            return false;
        }

        @Override
        public void setSwipeable(boolean swipeable) {
        }

        @Override
        public int getItemViewType() {
            return 0;
        }

        @Override
        public int getLayoutRes() {
            return R.layout.post_header_workaround;
        }

        @Override
        public HeaderViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new HeaderViewHolder(view, adapter);
        }

        @Override
        public void unbindViewHolder(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void onViewAttached(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void onViewDetached(FlexibleAdapter adapter, HeaderViewHolder holder, int position) {

        }

        @Override
        public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, HeaderViewHolder viewHolder, int position, List<Object> payloads) {
        }

        class HeaderViewHolder extends FlexibleViewHolder {

            HeaderViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter, true);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexiblePostsItemList(List<Post> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Post item : list) {
            switch (item.getPinned()) {
                case "0":
                    itemList.add(new PostItemAdapter(item));
                    break;
                case "1":
                    itemList.add(new PinnedPostItemAdapter(item));
                    break;
                case "2":
                    itemList.add(new HeaderWorkaroundItemAdapter(item));
                    break;
            }
        }
        return itemList;
    }

    private List<AbstractFlexibleItem> getFlexibleHeadersItemList(List<Post> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Post item : list) {
            itemList.add(new HeaderItemAdapter(item));
        }
        return itemList;
    }

    protected Unbinder getUnBinder() {
        return unBinder;
    }
}