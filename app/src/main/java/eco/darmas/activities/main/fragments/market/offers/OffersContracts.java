package eco.darmas.activities.main.fragments.market.offers;

interface OffersContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}