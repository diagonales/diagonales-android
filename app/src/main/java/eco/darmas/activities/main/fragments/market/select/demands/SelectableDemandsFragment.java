package eco.darmas.activities.main.fragments.market.select.demands;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class SelectableDemandsFragment extends BaseFragment implements SelectableDemandsContracts.Fragment {

    @Override
    protected SelectableDemandsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new SelectableDemandsModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected SelectableDemandsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new SelectableDemandsPresenter((SelectableDemandsModel) model, (SelectableDemandsView) view);
    }

    @Override
    protected SelectableDemandsView createView(Context context) {
        return new SelectableDemandsView(context);
    }
}
