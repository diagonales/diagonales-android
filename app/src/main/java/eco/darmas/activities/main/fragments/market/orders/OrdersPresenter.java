package eco.darmas.activities.main.fragments.market.orders;

import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class OrdersPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements OrdersContracts.Presenter {

    private final OrdersModel mModel;
    private final OrdersView mView;

    OrdersPresenter(OrdersModel mModel, OrdersView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData(boolean scrollTop) {
        getDisposables().add(Single.merge(mModel.loadTransactionsData(), mModel.loadRatingsData())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> mView.dataSetChanged(scrollTop))
                .doAfterTerminate(() -> CommonUtils.showProgress(mView.getContext(), false))
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.TRANSACTIONS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.RATINGS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.LISTINGS_FILTERED: {
                updateData(true);
            }
            case FilterEvent.EVENT_MODE_ENTERED: {
                updateData(true);
            }
            case FilterEvent.EVENT_MODE_EXIT: {
                updateData(true);
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    String getCategoryName(int categoryId) {
        return mModel.getCategory(categoryId).getName();
    }

    List<_Transaction> getOrderList() {
        List<_Transaction> offerList = mModel.getOrderList();
        Collections.sort(offerList, getComparator());
        return offerList;
    }

    Comparator<_Transaction> getComparator() {
        return (t1, t2) -> {
            switch (mModel.getSelectedOrder()) {
                case AppConstants.BY_SKILL_RATING_DESC:
                    float descSkillRating1 = getWorkRating(t1);
                    float descSkillRating2 = getWorkRating(t2);
                    if (descSkillRating1 > descSkillRating2) {
                        return -1;
                    } else if (descSkillRating1 == descSkillRating2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (descSkillRating1 < descSkillRating2) {
                        return 1;
                    }
                case AppConstants.BY_SKILL_RATING_ASC:
                    float ascSkillRating1 = getWorkRating(t1);
                    float ascSkillRating2 = getWorkRating(t2);
                    if (ascSkillRating1 < ascSkillRating2) {
                        return -1;
                    } else if (ascSkillRating1 == ascSkillRating2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (ascSkillRating1 > ascSkillRating2) {
                        return 1;
                    }
                case AppConstants.BY_MEMBER_RATING_DESC:
                    float descMemberRating1 = getUserRating(t1);
                    float descMemberRating2 = getUserRating(t2);
                    if (descMemberRating1 > descMemberRating2) {
                        return -1;
                    } else if (descMemberRating1 == descMemberRating2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (descMemberRating1 < descMemberRating2) {
                        return 1;
                    }
                case AppConstants.BY_MEMBER_RATING_ASC:
                    float ascMemberRating1 = getUserRating(t1);
                    float ascMemberRating2 = getUserRating(t2);
                    if (ascMemberRating1 < ascMemberRating2) {
                        return -1;
                    } else if (ascMemberRating1 == ascMemberRating2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (ascMemberRating1 > ascMemberRating2) {
                        return 1;
                    }
                case AppConstants.BY_PRICE_DESC:
                    double descPrice1 = t1.getAmount();
                    double descPrice2 = t2.getAmount();
                    if (descPrice1 < descPrice2) {
                        return -1;
                    } else if (descPrice1 == descPrice2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (descPrice1 > descPrice2) {
                        return 1;
                    }
                case AppConstants.BY_PRICE_ASC:
                    double ascPrice1 = t1.getAmount();
                    double ascPrice2 = t2.getAmount();
                    if (ascPrice1 > ascPrice2) {
                        return -1;
                    } else if (ascPrice1 == ascPrice2) {
                        return transactionCreationDateCompare(t1, t2, true);
                    } else if (ascPrice1 < ascPrice2) {
                        return 1;
                    }
                case AppConstants.BY_CREATION_DATE_DESC:
                    return transactionCreationDateCompare(t1, t2, true);
                case AppConstants.BY_CREATION_DATE_ASC:
                    return transactionCreationDateCompare(t1, t2, false);
                case AppConstants.BY_END_DATE_DESC:
                    return transactionEndDateCompare(t1, t2, true);
                case AppConstants.BY_END_DATE_ASC:
                    return transactionEndDateCompare(t1, t2, false);
            }
            return 0;
        };
    }

    private int transactionCreationDateCompare(_Transaction t1, _Transaction t2, boolean recent) {
        Date descDate1 = DateUtils.parseFromBackendLocale(t1.getDate());
        Date descDate2 = DateUtils.parseFromBackendLocale(t2.getDate());
        if (recent) {
            if (descDate1.after(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (descDate1.before(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    private int transactionEndDateCompare(_Transaction t1, _Transaction t2, boolean recent) {
        //TODO design a better expiration system and methods for each user
        Date descDate1 = DateUtils.parseFromBackendLocale(t1.getDateReceived());
        Date descDate2 = DateUtils.parseFromBackendLocale(t2.getDateReceived());
        if (recent) {
            if (descDate1.after(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (descDate1.before(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    private float getWorkRating(_Transaction transaction) {
        int transactionType = transaction.getType();
        int idContact = 0;
        if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            idContact = transaction.getIdReceiver();
        } else if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            idContact = transaction.getIdSender();
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            idContact = transaction.getIdReceiver();
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            idContact = transaction.getIdSender();
        }

        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getWorksRatingsMap().get(idContact, new SparseArray<>()).get(transaction.getIdCategory(), new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    private float getUserRating(_Transaction transaction) {
        int transactionType = transaction.getType();
        int idContact = 0;
        if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            idContact = transaction.getIdReceiver();
        } else if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            idContact = transaction.getIdSender();
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            idContact = transaction.getIdReceiver();
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            idContact = transaction.getIdSender();
        }

        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getUserRatingsMap().get(idContact, new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    void onCategoryClicked() {
        Object[] arguments = new Object[]{_Transaction.Type.TRANSFER};
        AppNav.navigateToFragment(AppConstants.CATEGORY_FRAGMENT, (AppCompatActivity) mView.getContext(), arguments);
    }

    void onOrderClicked() {
        AppNav.navigateToFragment(AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT, (AppCompatActivity) mView.getContext());
    }

    String getSelectedCategoryName() {
        return mModel.getSelectedCategoryName();
    }

    int getSelectedCategoryId() {
        return mModel.getSelectedCategory();
    }

    void addSearchConstraint(String search) {
        mModel.addSearchConstraint(search);
        updateData(false);
    }

    int getSelectedOrder() {
        return mModel.getSelectedOrder();
    }

    boolean onOrderItemClick(int position) {
        //TODO
        return true;
    }

    void onRateClicked(int position) {
        _Transaction transaction = mModel.getOrderList().get(position);

        Rating[] ratings = getRatings(transaction);
        Object[] parcelables = new Object[]{ratings[0], ratings[1]};
        AppNav.navigateToFragment(AppConstants.RATE_FRAGMENT, (MainActivity) mView.getContext(), parcelables);
    }

    Rating[] getRatings(_Transaction transaction) {
        int transactionType = transaction.getType();

        Rating[] ratings = new Rating[2];
        if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            ratings[0] = mModel.getUserToListingsRatingList().get(transaction.getIdListing(), new Rating()
                    .withType(Rating.Type.LISTING)
                    .withIdTarget(transaction.getIdListing())
                    .withIdReceiver(transaction.getIdReceiver())
                    .withRating(0)
                    .withComment(""));
            ratings[1] = mModel.getUserToWorksRatingList().get(transaction.getIdReceiver(), new SparseArray<>()).get(transaction.getIdCategory(), new Rating()
                    .withType(Rating.Type.WORK)
                    .withIdTarget(transaction.getIdCategory())
                    .withIdReceiver(transaction.getIdReceiver())
                    .withRating(0)
                    .withComment(""));
        } else if (transactionType == _Transaction.Type.OFFER && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            ratings[0] = mModel.getUserToMembersRatingList().get(transaction.getIdSender(), new Rating()
                    .withType(Rating.Type.MEMBER)
                    .withIdTarget(transaction.getIdSender())
                    .withIdReceiver(transaction.getIdSender())
                    .withRating(0)
                    .withComment(""));
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdSender()) {
            ratings[0] = mModel.getUserToMembersRatingList().get(transaction.getIdReceiver(), new Rating()
                    .withType(Rating.Type.MEMBER)
                    .withIdTarget(transaction.getIdReceiver())
                    .withIdReceiver(transaction.getIdReceiver())
                    .withRating(0)
                    .withComment(""));
        } else if (transactionType == _Transaction.Type.DEMAND && mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver()) {
            ratings[0] = mModel.getUserToWorksRatingList().get(transaction.getIdSender(), new SparseArray<>()).get(transaction.getIdCategory(), new Rating()
                    .withType(Rating.Type.WORK)
                    .withIdTarget(transaction.getIdCategory())
                    .withIdReceiver(transaction.getIdSender())
                    .withRating(0)
                    .withComment(""));
        }
        return ratings;
    }

    void onCancelTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }

        mModel.commitTransactionCancel(order, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonResponse;
                    try {
                        jsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (jsonResponse.get("result").equals("true")) {
                            mModel.writeTransactionCancel(order);
                            if (order.getType() == _Transaction.Type.OFFER) {
                                mModel.writePaymentRefund(order.getIdSender(), order.getAmount());
                                CommonUtils.showSuccessToast("La transacción ha sido cancelada");
                            }
                        } else {
                            CommonUtils.showErrorToast("La transacción no pudo ser cancelada debido a un error");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                } else {
                    CommonUtils.showErrorToast("La transacción no pudo ser cancelada debido a un error");
                    response.body().close();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("La transacción no pudo ser cancelada debido a un error");
                }
                AppLogger.e(t, null);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    void onAcceptTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        order.setStatus(_Transaction.Status.ACCEPTED);
        order.setDateAccepted(DateUtils.getTimeStamp());
        processTransaction(order);
    }

    void onRejectTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        order.setStatus(_Transaction.Status.REJECTED);
        order.setDateRejected(DateUtils.getTimeStamp());
        order.setReceiverArchived(1);
        processTransaction(order);
    }

    void onDeliveredTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        order.setStatus(_Transaction.Status.DELIVERED);
        order.setDateDelivered(DateUtils.getTimeStamp());
        processTransaction(order);
    }

    void onReceivedTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        order.setStatus(_Transaction.Status.RECEIVED);
        order.setDateReceived(DateUtils.getTimeStamp());
        processTransaction(order);
    }

    void onArchiveTransaction(_Transaction order) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        if (mModel.getLoggedUser().getIdUser() == order.getIdSender()) {
            order.setSenderArchived(1);
        } else if (mModel.getLoggedUser().getIdUser() == order.getIdReceiver()) {
            order.setReceiverArchived(1);
        }
        archiveTransaction(order);
    }

    private void processTransaction(_Transaction order) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.TRANSACTION_UPDATED);
        mModel.commitTransactionProcess(order, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonResponse;
                    try {
                        jsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (jsonResponse.get("result").equals("true")) {
                            event.setSuccess(true);

                            int status = Integer.parseInt((String) jsonResponse.get("status"));
                            switch (status) {
                                case _Transaction.Status.ACCEPTED:
                                    mModel.writeTransactionProcess(order);
                                    mModel.writeListingStock(order);
                                    if (order.getType() == _Transaction.Type.DEMAND) {
                                        mModel.writePaymentReserve(order.getAmount());
                                        event.setDialogTitle("La oferta fue aceptada");
                                        event.setDialogMessage("Se ha reservado el monto, ahora solo debes esperar a recibir tu encargo!");
                                    } else if (order.getType() == _Transaction.Type.OFFER) {
                                        event.setDialogTitle("El encargo fue aceptado");
                                        event.setDialogMessage("Ya puedes preparar el encargo y luego entregarlo!");
                                    }
                                    break;
                                case _Transaction.Status.REJECTED:
                                    mModel.writeTransactionProcess(order);
                                    if (order.getType() == _Transaction.Type.OFFER) {
                                        mModel.writePaymentRefund(order.getIdSender(), order.getAmount());
                                        event.setDialogTitle("La transacción ha sido rechazada");
                                        event.setDialogMessage("Se reintegraron los fondos reservados");
                                    } else {
                                        event.setDialogTitle("La transacción ha sido rechazada");
                                    }
                                    break;
                                case _Transaction.Status.DELIVERED:
                                    mModel.writeTransactionProcess(order);
                                    switch (order.getShippingMethod()) {
                                        case Listing.ShippingMethod.PICKUP:
                                            event.setDialogTitle("Has marcado el encargo como listo");
                                            event.setDialogMessage("Ahora espera que sea retirado satisfactoriamente para que se libere tu pago, y puedas calificar la operación");
                                            break;
                                        case Listing.ShippingMethod.DELIVERY:
                                            event.setDialogTitle("Has marcado el encargo como enviado");
                                            event.setDialogMessage("Ahora espera que sea recibido satisfactoriamente para que se libere tu pago, y puedas calificar la operación");
                                            break;
                                        case Listing.ShippingMethod.DIGITAL:
                                            event.setDialogTitle("Has marcado el encargo como enviado");
                                            event.setDialogMessage("Ahora espera que sea recibido satisfactoriamente para que se libere tu pago, y puedas calificar la operación");
                                            break;
                                    }
                                    break;
                                case _Transaction.Status.RECEIVED:
                                    if (order.getType() == _Transaction.Type.OFFER) {
                                        mModel.writeTransactionProcess(order);
                                        mModel.writeReleasePayment(order.getIdReceiver(), order.getAmount());
                                        event.setDialogTitle("Felicidades!");
                                        event.setDialogMessage("La transacción ha sido completada, recordá que podes calificar la conducta de este usuario o modificarla si es que ya lo habias calificado anteriormente");
                                    } else if (order.getType() == _Transaction.Type.DEMAND) {
                                        mModel.writeTransactionProcess(order);
                                        mModel.writeReleasePayment(order.getIdSender(), order.getAmount());
                                        event.setDialogTitle("Felicidades!");
                                        event.setDialogMessage("La transacción ha sido completada, recordá que podes valorar tu experiencia con este producto o servicio, o bien modificar tu valoración si es que ya la habias enviado anteriormente");
                                    }
                                    break;
                            }
                        } else {
                            CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        }
                    } catch (Exception e) {
                        CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        AppLogger.e(e, null);
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                    response.body().close();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                EventBus.getDefault().post(event);
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                }
                AppLogger.e(t, null);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    private void archiveTransaction(_Transaction order) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.TRANSACTION_UPDATED);
        mModel.commitTransactionArchive(order, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonResponse;
                    try {
                        jsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (jsonResponse.get("result").equals("true")) {
                            event.setSuccess(true);
                            mModel.writeTransactionProcess(order);
                            CommonUtils.showSuccessToast("Transacción archivada");
                        } else {
                            CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        }
                    } catch (Exception e) {
                        CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        AppLogger.e(e, null);
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                    response.body().close();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                EventBus.getDefault().post(event);
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                }
                AppLogger.e(t, null);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    void onShowEndedClicked(boolean b) {
        mModel.setShowEnded(b);
        updateData(true);
    }

    Listing getListing(int idListing) {
        return mModel.getListing(idListing);
    }

    String getOrderCount() {
        return "(" + String.valueOf(mModel.getOrderList().size()) + ")";
    }

    void onConceptClicked(Listing listing) {
        if (listing == null) {
            CommonUtils.showInfoToast("Este listado no esta disponible");
            return;
        }
        Object[] parcelables = new Object[]{listing};
        AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }
}

