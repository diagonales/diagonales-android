package eco.darmas.activities.main.fragments.market.order;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.User;
import eco.darmas.utils.CommonUtils;

import static android.support.v7.widget.GridLayout.UNDEFINED;

class ListOrderView extends BaseFragmentView {

    private ListOrderPresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.li_order_member_desc)
    LinearLayout li_order_member_desc;
    @BindView(R.id.li_order_member_asc)
    LinearLayout li_order_member_asc;
    @BindView(R.id.li_order_listing_desc)
    LinearLayout li_order_listing_desc;
    @BindView(R.id.li_order_listing_asc)
    LinearLayout li_order_listing_asc;
    @BindView(R.id.li_order_skill_desc)
    LinearLayout li_order_skill_desc;
    @BindView(R.id.li_order_skill_asc)
    LinearLayout li_order_skill_asc;
    @BindView(R.id.li_order_price_desc)
    LinearLayout li_order_price_desc;
    @BindView(R.id.li_order_price_asc)
    LinearLayout li_order_price_asc;
    @BindView(R.id.li_order_date_desc)
    LinearLayout li_order_date_desc;
    @BindView(R.id.li_order_date_asc)
    LinearLayout li_order_date_asc;
    @BindView(R.id.li_order_creation_date_desc)
    LinearLayout li_order_creation_date_desc;
    @BindView(R.id.li_order_creation_date_asc)
    LinearLayout li_order_creation_date_asc;
    @BindView(R.id.li_order_skill)
    LinearLayout li_order_skill;
    @BindView(R.id.li_order_listing)
    LinearLayout li_order_listing;

    @BindView(R.id.li_price)
    LinearLayout li_price;
    @BindView(R.id.li_end_date)
    LinearLayout li_end_date;

    @BindView(R.id.li_like)
    LinearLayout li_like;
    @BindView(R.id.li_rating)
    LinearLayout li_rating;

    ListOrderView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.list_order_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (ListOrderPresenter) presenter;
        if (mPresenter.getCaller() == AppConstants.SELECT_OFFERS_LIST_ORDER_FRAGMENT) {
            li_like.setVisibility(GONE);
            if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
                li_order_listing_asc.setVisibility(VISIBLE);
                li_order_skill_asc.setVisibility(VISIBLE);
                li_order_member_asc.setVisibility(VISIBLE);
            }
        } else if (mPresenter.getCaller() == AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT) {
            li_like.setVisibility(GONE);
            li_order_skill.setVisibility(GONE);
            li_order_listing.setVisibility(GONE);
            if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
                li_order_member_asc.setVisibility(VISIBLE);
            }
            /*GridLayout.LayoutParams itemLP = (GridLayout.LayoutParams) cv_rating.getLayoutParams();
            itemLP.columnSpec = GridLayout.spec(0, 1);
            cv_rating.setLayoutParams(itemLP);*/
        } else if (mPresenter.getCaller() == AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT) {
            li_like.setVisibility(GONE);
            li_order_listing.setVisibility(GONE);
        } else if (mPresenter.getCaller() == AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT) {
            li_order_skill.setVisibility(GONE);
            li_order_listing.setVisibility(GONE);
            if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
                li_order_member_asc.setVisibility(VISIBLE);
            }
            GridLayout.LayoutParams itemLP = (GridLayout.LayoutParams) li_rating.getLayoutParams();
            itemLP.columnSpec = GridLayout.spec(UNDEFINED, 1);
            li_rating.setLayoutParams(itemLP);
            li_price.setVisibility(GONE);
            li_end_date.setVisibility(GONE);
            li_like.setVisibility(VISIBLE);
        }

        /*switch (mPresenter.getInitialOrder()) {
            case AppConstants.BY_LISTING_RATING_DESC:
                //li_order_listing_desc.setAlpha(1);
                break;
            case AppConstants.BY_LISTING_RATING_ASC:
                //li_order_listing_asc.setAlpha(1);
                break;
            case AppConstants.BY_SKILL_RATING_DESC:
                //li_order_skill_desc.setAlpha(1);
                break;
            case AppConstants.BY_SKILL_RATING_ASC:
                //li_order_skill_asc.setAlpha(1);
                break;
            case AppConstants.BY_MEMBER_RATING_DESC:
                //li_order_member_desc.setAlpha(1);
                break;
            case AppConstants.BY_MEMBER_RATING_ASC:
                //li_order_member_asc.setAlpha(1);
                break;
            case AppConstants.BY_PRICE_DESC:
                //li_order_price_desc.setAlpha(1);
                break;
            case AppConstants.BY_PRICE_ASC:
                //li_order_price_asc.setAlpha(1);
                break;
            case AppConstants.BY_END_DATE_DESC:
                //li_order_date_desc.setAlpha(1);
                break;
            case AppConstants.BY_END_DATE_ASC:
                //li_order_date_asc.setAlpha(1);
                break;
        }*/
    }

    @OnClick({R.id.li_order_member_desc, R.id.li_order_member_asc, R.id.li_order_listing_desc, R.id.li_order_listing_asc, R.id.li_order_skill_desc, R.id.li_order_skill_asc, R.id.li_order_price_desc, R.id.li_order_price_asc, R.id.li_order_date_desc, R.id.li_order_date_asc, R.id.li_order_like, R.id.li_order_creation_date_desc, R.id.li_order_creation_date_asc})
    void onApplyClickListener(View view) {
        int selectedOrder = 0;
        switch (view.getId()) {
            case R.id.li_order_listing_desc:
                selectedOrder = AppConstants.BY_LISTING_RATING_DESC;
                break;
            case R.id.li_order_listing_asc:
                selectedOrder = AppConstants.BY_LISTING_RATING_ASC;
                break;
            case R.id.li_order_skill_desc:
                selectedOrder = AppConstants.BY_SKILL_RATING_DESC;
                break;
            case R.id.li_order_skill_asc:
                selectedOrder = AppConstants.BY_SKILL_RATING_ASC;
                break;
            case R.id.li_order_member_desc:
                selectedOrder = AppConstants.BY_MEMBER_RATING_DESC;
                break;
            case R.id.li_order_member_asc:
                selectedOrder = AppConstants.BY_MEMBER_RATING_ASC;
                break;
            case R.id.li_order_price_desc:
                selectedOrder = AppConstants.BY_PRICE_DESC;
                break;
            case R.id.li_order_price_asc:
                selectedOrder = AppConstants.BY_PRICE_ASC;
                break;
            case R.id.li_order_date_desc:
                selectedOrder = AppConstants.BY_END_DATE_DESC;
                break;
            case R.id.li_order_date_asc:
                selectedOrder = AppConstants.BY_END_DATE_ASC;
                break;
            case R.id.li_order_like:
                selectedOrder = AppConstants.BY_LIKES_DESC;
                break;
            case R.id.li_order_creation_date_desc:
                selectedOrder = AppConstants.BY_CREATION_DATE_DESC;
                break;
            case R.id.li_order_creation_date_asc:
                selectedOrder = AppConstants.BY_CREATION_DATE_ASC;
                break;
        }
        mPresenter.onApplyClicked(selectedOrder);
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG, false);
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}