package eco.darmas.activities.main.fragments.comunity.create.announce;

interface CreateAnnounceContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}