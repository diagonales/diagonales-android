package eco.darmas.activities.main.fragments.market.select.offers;

interface SelectableOffersContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}