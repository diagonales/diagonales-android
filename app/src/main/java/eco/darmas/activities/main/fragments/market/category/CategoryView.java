package eco.darmas.activities.main.fragments.market.category;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Node;
import eco.darmas.utils.ScreenUtils;

class CategoryView extends BaseFragmentView {

    private CategoryPresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.li_categories)
    LinearLayout li_categories;
    @BindView(R.id.li_tree)
    LinearLayout li_tree;
    @BindView(R.id.elv_category)
    ExpandableListView elv_category;
    @BindView(R.id.tv_category_tree)
    TextView tv_category_tree;

    protected CategoryView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.category_selection, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        mPresenter = (CategoryPresenter) presenter;
        li_tree.getLayoutParams().height = ScreenUtils.getDisplayHeightPixels(getContext()) / 3;
        Double minimumWidth = ScreenUtils.getDisplayWidthPixels(getContext()) * 0.8;
        li_categories.setMinimumWidth(minimumWidth.intValue());
        setData();
        setupAdapters();
    }

    void setData() {
        tv_category_tree.setText(mPresenter.getTree());
    }

    void setupAdapters() {
        elv_category.setAdapter(new CategoryExpandableListViewAdapter(mPresenter.getTreeItem()));
        elv_category.expandGroup(0, true);
        // setOnChildClickListener listener for child row click
        elv_category.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> mPresenter.onChildClicked((int) id));
        // setOnGroupClickListener listener for group heading click
        elv_category.setOnGroupClickListener((parent, v, groupPosition, id) -> mPresenter.onGroupClicked((int) id));
    }

    void dataSetChanged() {
        ((CategoryExpandableListViewAdapter) elv_category.getExpandableListAdapter()).swapData(mPresenter.getTreeItem());
        elv_category.expandGroup(0, true);
        setData();
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }

    @OnClick(R.id.bt_show)
    void onShowClickListener() {
        mPresenter.onShowClicked();
    }

    @OnClick(R.id.bt_reset)
    void onResetClickListener() {
        mPresenter.onResetClicked();
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        mPresenter.onCancelClicked();
    }

    class CategoryExpandableListViewAdapter extends BaseExpandableListAdapter {
        Node<Category> data;

        CategoryExpandableListViewAdapter(Node<Category> data) {
            this.data = data;
        }

        void swapData(Node<Category> categoryNode) {
            this.data = categoryNode;
            notifyDataSetChanged();
        }

        @Override
        public int getGroupCount() {
            return 1;
        }

        @Override
        public int getChildrenCount(int i) {
            return data.getChildren().size();
        }

        @Override
        public Node<Category> getGroup(int i) {
            return data;
        }

        @Override
        public Node<Category> getChild(int i, int i1) {
            return data.getChildren().get(i1);
        }

        @Override
        public long getGroupId(int i) {
            return data.getId();
        }

        @Override
        public long getChildId(int i, int i1) {
            return data.getChildren().get(i1).getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }

        @Override
        public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = view;
            if (v == null) {
                assert inflater != null;
                v = inflater.inflate(R.layout.category_tree_node, viewGroup, false);
            }

            TextView tv_category = v.findViewById(R.id.tv_category);
            TextView tv_listings_count = v.findViewById(R.id.tv_listings_count);
            CardView cv_category = v.findViewById(R.id.cv_category);

            Category category = getGroup(i).getData();
            tv_category.setTextSize(18);
            tv_category.setText(category.getName());
            if (!getGroup(i).isRoot()) {
                tv_listings_count.setTextSize(18);
                if (mPresenter.getType() == 0) {
                    tv_listings_count.setText(mPresenter.getOfferListingsText(category));
                } else if (mPresenter.getType() == 1) {
                    tv_listings_count.setText(mPresenter.getDemandListingsText(category));
                } else if (mPresenter.getType() == 2) {
                    tv_listings_count.setText(mPresenter.getTransactionsCountText(category));
                }
                tv_listings_count.setVisibility(View.VISIBLE);
                cv_category.setCardElevation(5);
                cv_category.setCardBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            } else {
                tv_listings_count.setVisibility(View.GONE);
                cv_category.setCardElevation(0);
                cv_category.setCardBackgroundColor(0);
            }

            return v;
        }

        @Override
        public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = view;
            if (v == null) {
                assert inflater != null;
                v = inflater.inflate(R.layout.category_tree_node, viewGroup, false);
            }

            TextView tv_category = v.findViewById(R.id.tv_category);
            TextView tv_listings_count = v.findViewById(R.id.tv_listings_count);
            CardView cv_category = v.findViewById(R.id.cv_category);

            Category category = getGroup(i).getChildren().get(i1).getData();
            tv_category.setTextSize(14);
            tv_listings_count.setTextSize(14);
            tv_category.setText(category.getName());
            tv_category.setPadding(35, 0, 0, 0);
            if (mPresenter.getType() == 0) {
                tv_listings_count.setText(mPresenter.getOfferListingsText(category));
            } else if (mPresenter.getType() == 1) {
                tv_listings_count.setText(mPresenter.getDemandListingsText(category));
            } else if (mPresenter.getType() == 2) {
                tv_listings_count.setText(mPresenter.getTransactionsCountText(category));
            }
            cv_category.setCardElevation(0);
            cv_category.setCardBackgroundColor(0);

            return v;
        }
    }
}
