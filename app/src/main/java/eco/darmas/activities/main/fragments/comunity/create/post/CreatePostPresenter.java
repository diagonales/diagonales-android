package eco.darmas.activities.main.fragments.comunity.create.post;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import eco.darmas.AppConstants;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.NetworkUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CreatePostPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements CreatePostContracts.Presenter {

    private final CreatePostModel mModel;
    private final CreatePostView mView;

    CreatePostPresenter(CreatePostModel mModel, CreatePostView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        mView.initialize(this);
        EventBus.getDefault().register(this);
    }

    void onPublishClicked(String title, String text, String pinned) {
        if (!title.equals("")) {
            if (!text.equals("")) {
                if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                    createPost(title, text, pinned);
                } else {
                    CommonUtils.showErrorToast("Comprueba tu conección");
                }
            } else {
                CommonUtils.showErrorToast("Debes ingresar el contenido");
            }
        } else {
            CommonUtils.showErrorToast("Debes escribir un titulo");
        }
    }

    private void createPost(String title, String text, String pinned) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.POST_CREATED);
        mModel.commitCreatePost(title, text, pinned, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            int id = (int) responseJSON.get("id");
                            String image = (String) responseJSON.get("image");
                            Post post = new Post(id, mModel.getLoggedUser().getIdUser(), mModel.getEventModeId(), title, DateUtils.getTimeStamp(), "1", text, "0", "", "0", image, pinned);
                            mModel.writePost(post);
                            event.setSuccess(true);
                            event.setDialogTitle("El post ha sido creado!");
                        } else {
                            event.setDialogTitle("Error al crear el post");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                        event.setDialogTitle("Error al crear el post");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().postSticky(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                event.setDialogTitle("Error al crear el post");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogTitle("Error al crear el post");
                    event.setDialogMessage("El servidor no parece estar respondiendo, por favor intentalo mas tarde");
                }
                EventBus.getDefault().postSticky(event);
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissFragment(mView, AppConstants.CREATE_POST_FRAGMENT_TAG, true);
    }

    void onAddCameraImageClicked() {
        mModel.setTempImageUri(ImageUtils.takePictureFromCamera((MainActivity) mView.getContext(), AppConstants.POST_CAMERA_REQUEST, mModel.getPostImageFile()));
    }

    void onAddFileImageClicked() {
        ImageUtils.chooseImageFromFile((MainActivity) mView.getContext(), AppConstants.POST_CHOOSE_FILE);
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActivityResult(ActivityResultEvent event) {
        if (event.resultCode != AppConstants.RESULT_OK) {
            return;
        }
        switch (event.eventCode) {
            case AppConstants.POST_CAMERA_REQUEST: {
                Picasso.get().load(mModel.getTempImageUri()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_front.getWidth(), mView.iv_front.getHeight()).centerCrop().into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al descargar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setPostImageFile(mModel.getTempImageUri().getPath());
                break;
            }
            case AppConstants.POST_CHOOSE_FILE: {
                String path = event.data.getDataString();
                Picasso.get().load(Uri.parse(path)).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_front.getWidth(), mView.iv_front.getHeight()).centerCrop().into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al descargar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setPostImageFile(path);
            }
        }
    }

    String getEventInfoText() {
        return "Esta publicación solo estará disponible para el evento " + "\"" + mModel.getCommunityEventMap().get(mModel.getEventModeId()).getTitle() + "\"";
    }
}