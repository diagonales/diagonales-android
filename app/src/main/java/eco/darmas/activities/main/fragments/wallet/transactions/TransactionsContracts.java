package eco.darmas.activities.main.fragments.wallet.transactions;

interface TransactionsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}