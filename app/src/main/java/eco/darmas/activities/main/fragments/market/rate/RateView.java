package eco.darmas.activities.main.fragments.market.rate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.ScreenUtils;

class RateView extends BaseFragmentView implements RateContracts.View {

    private RatePresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.li_rating)
    LinearLayout li_rating;
    @BindView(R.id.rb_rating)
    SimpleRatingBar rb_rating;
    @BindView(R.id.et_comment)
    EditText et_comment;
    @BindView(R.id.cb_rating_work)
    CheckBox cb_rating_work;
    @BindView(R.id.li_rating_2)
    LinearLayout li_rating_2;
    @BindView(R.id.rb_rating_2)
    SimpleRatingBar rb_rating_2;
    @BindView(R.id.et_comment_2)
    EditText et_comment_2;
    @BindView(R.id.tv_rating_info)
    TextView tv_rating_info;
    @BindView(R.id.bt_cancel)
    Button bt_cancel;
    @BindView(R.id.bt_send)
    Button bt_send;

    protected RateView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.send_rating, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (RatePresenter) presenter;

        li_rating.setMinimumWidth(ScreenUtils.getDisplayWidthPixels(getContext()));

        if (mPresenter.getRating() != 0) {
            rb_rating.setRating(mPresenter.getRating());
        } else {
            rb_rating.setRating(0.5f);
        }
        switch (mPresenter.getRatingType()) {
            case 0 : {
                tv_rating_info.setText("Esta es tu valoracón de la conducta económica actual de este miembro. Por favor utiliza un criterio justo y comprensivo al emitir o modificar esta valoración, ya que podría afectar su participación en la comunidad.");
                rb_rating.setFillColor(getResources().getColor(R.color.userRatingBar));
                rb_rating.setPressedFillColor(getResources().getColor(R.color.userRatingBar));
                rb_rating.setPressedBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_rating.setStarBackgroundColor(getResources().getColor(R.color.userBackgroundRatingBar));
                rb_rating.setPressedStarBackgroundColor(getResources().getColor(R.color.userBackgroundRatingBar));
                rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                break;
            }
            case 1 : {
                tv_rating_info.setText("Esta es tu valoracón actual del trabajo en la categoría hecho por este miembro. Por favor utiliza un criterio justo y objetivo al emitir o modificar esta valoración, ya que podría tener un impacto en sus próximas operaciones.");
                rb_rating.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                rb_rating.setPressedFillColor(getResources().getColor(R.color.categoryRatingBar));
                rb_rating.setPressedBorderColor(getResources().getColor(R.color.categoryRatingBar));
                rb_rating.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
                rb_rating.setPressedStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
                rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                break;
            }
            case 2 : {
                tv_rating_info.setText("Esta es tu valoracón actual del producto o servicio que te ha ofrecido este miembro. Por favor utiliza un criterio justo y objetivo al emitir o modificar esta valoración, ya que podría tener un impacto en sus próximas operaciones.");
                rb_rating.setFillColor(getResources().getColor(R.color.listingRatingBar));
                rb_rating.setPressedFillColor(getResources().getColor(R.color.listingRatingBar));
                rb_rating.setPressedBorderColor(getResources().getColor(R.color.listingRatingBar));
                rb_rating.setStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
                rb_rating.setPressedStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
                rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                break;
            }
        }
        rb_rating.setOnRatingBarChangeListener((simpleRatingBar, rating, fromUser) -> {
            if(rating < 0.5f) {
                simpleRatingBar.setRating(0.5f);
            }
        });
        et_comment.setText(mPresenter.getComment());

        if (mPresenter.getRatingType() == 2) {
            cb_rating_work.setVisibility(View.VISIBLE);
            rb_rating_2.setOnRatingBarChangeListener((simpleRatingBar, rating, fromUser) -> {
                if(rating < 0.5f) {
                    simpleRatingBar.setRating(0.5f);
                }
            });

            rb_rating_2.setFillColor(getResources().getColor(R.color.categoryRatingBar));
            rb_rating_2.setPressedFillColor(getResources().getColor(R.color.categoryRatingBar));
            rb_rating_2.setPressedBorderColor(getResources().getColor(R.color.categoryRatingBar));
            rb_rating_2.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            rb_rating_2.setPressedStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            rb_rating_2.setBorderColor(getResources().getColor(R.color.transparent));

            if (mPresenter.getRating2() != 0) {
                rb_rating_2.setRating(mPresenter.getRating2());
            } else {
                rb_rating_2.setRating(0.5f);
            }
            et_comment_2.setText(mPresenter.getComment2());
        } else {
            rb_rating_2.setRating(0);
        }
    }

    @OnCheckedChanged(R.id.cb_rating_work)
    void onSecondRatingClickListener(boolean b) {
        if (b) {
            li_rating_2.setVisibility(View.VISIBLE);
        } else {
            li_rating_2.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.RATE_FRAGMENT_TAG, false);
    }

    @OnClick(R.id.bt_send)
    void onSendClickListener() {
        mPresenter.sendRating(rb_rating.getRating(), et_comment.getText().toString().trim(), rb_rating_2.getRating(), et_comment_2.getText().toString().trim());
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}