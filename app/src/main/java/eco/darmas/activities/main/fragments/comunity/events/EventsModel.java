package eco.darmas.activities.main.fragments.comunity.events;

import android.os.Bundle;
import android.util.SparseArray;

import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class EventsModel extends BaseFragmentModel implements EventsContracts.Model {

    EventsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    void commitInscription(int userId, String concept, String date, double amount, String note, int eventId, double contribution, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().eventInscription(userId, concept, date, amount, note, eventId, contribution).enqueue(callback);
    }

    List<CommunityEvent> getPastCommunityEventsList() {
        return getDataManager().getPastCommunityEventsList();
    }

    List<CommunityEvent> getCurrentCommunityEventList() {
        return getDataManager().getCurrentCommunityEventsList();
    }

    List<CommunityEvent> getScheduledCommunityEventList() {
        return getDataManager().getScheduledCommunityEventsList();
    }

    void writeTransaction(_Transaction transaction) {
        getDataManager().getBoxStore().boxFor(_Transaction.class).put(transaction);
    }

    void writePayment(double payment) {
        User user = getLoggedUser();
        user.setFunds(user.getFunds() - payment);
        getDataManager().getBoxStore().boxFor(User.class).put(user);
    }

    void writeCommunityEvent(double income, int eventId) {
        CommunityEvent communityEvent = getDataManager().getCommunityEventMap().get(eventId);
        communityEvent.setInscriptions(communityEvent.getInscriptions() + 1);
        communityEvent.setIncome(communityEvent.getIncome() + income);
        getDataManager().getBoxStore().boxFor(CommunityEvent.class).put(communityEvent);
    }

    void writeInscription(Inscription inscription) {
        getDataManager().getBoxStore().boxFor(Inscription.class).put(inscription);
    }

    SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return getDataManager().getEventInscriptionMap();
    }

    void setEventModeId(int eventId) {
        App.getInstance().getPreferencesHelper().setSelectedEventId(eventId);
    }

    void commitInscriptionEdit(Inscription inscription, String standOption, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().eventInscriptionUpdate(inscription.getIdInscription(), inscription.getIdEvent(), standOption, inscription.getListings()).enqueue(callback);
    }

    void refreshEvents() {
        getDataManager().refreshCommunityEvents(true);
    }
}
