package eco.darmas.activities.main.fragments.market.orders;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class OrdersFragment extends BaseFragment implements OrdersContracts.Fragment {

    @Override
    protected OrdersModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new OrdersModel(dataManager, savedInstanceState);
    }

    @Override
    protected OrdersPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new OrdersPresenter((OrdersModel) model, (OrdersView) view);
    }

    @Override
    protected OrdersView createView(Context context) {
        return new OrdersView(context);
    }
}
