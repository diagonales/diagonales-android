package eco.darmas.activities.main.fragments.market.create;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class CreateListingFragment extends BaseFragment {

    @Override
    protected CreateListingModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new CreateListingModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected CreateListingPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new CreateListingPresenter((CreateListingModel) model, (CreateListingView) view);
    }

    @Override
    protected CreateListingView createView(Context context) {
        return new CreateListingView(context);
    }
}