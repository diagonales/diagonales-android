package eco.darmas.activities.main.fragments.comunity.announces;

interface AnnouncesContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}