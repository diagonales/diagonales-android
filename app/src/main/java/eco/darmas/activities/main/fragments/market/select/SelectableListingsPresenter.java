package eco.darmas.activities.main.fragments.market.select;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import eco.darmas.AppConstants;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class SelectableListingsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements SelectableListingsContracts.Presenter {

    private final SelectableListingsModel mModel;
    private final SelectableListingsView mView;

    SelectableListingsPresenter(SelectableListingsModel mModel, SelectableListingsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
    }

    int getEventId() {
        return mModel.getEventId();
    }

    void onConfirmClicked() {
        confirmEventListingsUpdate();
    }

    private void confirmEventListingsUpdate() {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.COMMUNITY_EVENT_MODIFIED);
        mModel.commitEventListingsUpdate(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonResponse;
                    try {
                        jsonResponse = new JSONObject(new String(response.body().bytes()));
                        if (jsonResponse.get("result").equals("true")) {
                            mModel.writeEventListing();
                            event.setSuccess(true);
                            event.setDialogTitle("Listados actualizados");
                            event.setDialogMessage("Tus listados para el evento " + "\"" + mModel.getCommunityEventsMap().get(mModel.getEventId()).getTitle() + "\"" + " han sido actualizados correctamente");
                        } else {
                            CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        }
                    } catch (Exception e) {
                        CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                        AppLogger.e(e, null);
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                    response.body().close();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                EventBus.getDefault().post(event);
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("La operación no pudo ser procesada debido a un error");
                }
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissFragment(mView, AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT_TAG, true);
    }
}