package eco.darmas.activities.main.fragments.comunity.create.event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;

class CreateCommunityEventView extends BaseFragmentView {

    private MainActivity mContext;
    private CreateCommunityEventPresenter mPresenter;
    private Unbinder unBinder;
    private DatePickerDialog.OnDateSetListener date_start_listener, date_end_listener, date_inscription_open_listener, date_inscription_close_listener;
    private TimePickerDialog.OnTimeSetListener time_start_listener, time_end_listener, time_inscription_open_listener, time_inscription_close_listener;
    private String date_start = "", date_end = "", date_inscription_open = "", date_inscription_close = "";
    private String time_start = "", time_end = "", time_inscription_open = "", time_inscription_close = "";

    @BindView(R.id.et_title)
    EditText et_title;
    @BindView(R.id.et_text)
    EditText et_text;
    @BindView(R.id.sp_event_type)
    Spinner sp_event_type;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_price_min)
    EditText et_price_min;
    @BindView(R.id.et_price_max)
    EditText et_price_max;
    @BindView(R.id.sw_ranged)
    Switch sw_ranged;
    @BindView(R.id.et_date_start)
    EditText et_date_start;
    @BindView(R.id.et_date_end)
    EditText et_date_end;
    @BindView(R.id.et_time_start)
    EditText et_time_start;
    @BindView(R.id.et_time_end)
    EditText et_time_end;
    @BindView(R.id.sw_limited)
    Switch sw_limited;
    @BindView(R.id.iv_image)
    ImageView iv_image;
    @BindView(R.id.et_vacants)
    EditText et_vacants;
    @BindView(R.id.et_date_inscription_open)
    EditText et_date_inscription_open;
    @BindView(R.id.et_time_inscription_open)
    EditText et_time_inscription_open;
    @BindView(R.id.et_date_inscription_close)
    EditText et_date_inscription_close;
    @BindView(R.id.et_time_inscription_close)
    EditText et_time_inscription_close;

    public CreateCommunityEventView(Context context) {
        super(context);
        this.mContext = (MainActivity) context;
    }

    @Override
    protected View inflate(@NonNull LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.create_community_event, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (CreateCommunityEventPresenter) presenter;

        et_vacants.setVisibility(View.GONE);
        et_date_inscription_open.setVisibility(View.GONE);
        et_time_inscription_open.setVisibility(View.GONE);
        et_date_inscription_close.setVisibility(View.GONE);
        et_time_inscription_close.setVisibility(View.GONE);
        et_price_max.setVisibility(View.GONE);
        sp_event_type.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, mPresenter.getCommunityEventTypes()));

        date_start_listener = (arg0, arg1, arg2, arg3) -> {
            Date date = DateUtils.parsedSelectedDate(String.valueOf(arg1) + "-" + String.valueOf(arg2 + 1) + "-" + String.valueOf(arg3));
            date_start = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_DATE_FORMAT);
            et_date_start.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.DATE_FORMAT));
        };
        date_end_listener = (arg0, arg1, arg2, arg3) -> {
            Date date = DateUtils.parsedSelectedDate(String.valueOf(arg1) + "-" + String.valueOf(arg2 + 1) + "-" + String.valueOf(arg3));
            date_end = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_DATE_FORMAT);
            et_date_end.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.DATE_FORMAT));
        };
        date_inscription_open_listener = (arg0, arg1, arg2, arg3) -> {
            Date date = DateUtils.parsedSelectedDate(String.valueOf(arg1) + "-" + String.valueOf(arg2 + 1) + "-" + String.valueOf(arg3));
            date_inscription_open = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_DATE_FORMAT);
            et_date_inscription_open.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.DATE_FORMAT));
        };
        date_inscription_close_listener = (arg0, arg1, arg2, arg3) -> {
            Date date = DateUtils.parsedSelectedDate(String.valueOf(arg1) + "-" + String.valueOf(arg2 + 1) + "-" + String.valueOf(arg3));
            date_inscription_close = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_DATE_FORMAT);
            et_date_inscription_close.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.DATE_FORMAT));
        };
        time_start_listener = (timePicker, i, i1) -> {
            Date date = DateUtils.parsedSelectedTime(String.valueOf(i) + ":" + String.valueOf(i1) + ":00");
            time_start = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_TIME_FORMAT);
            et_time_start.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.HOUR_FORMAT));
        };
        time_end_listener = (timePicker, i, i1) -> {
            Date date = DateUtils.parsedSelectedTime(String.valueOf(i) + ":" + String.valueOf(i1) + ":00");
            time_end = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_TIME_FORMAT);
            et_time_end.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.HOUR_FORMAT));
        };
        time_inscription_open_listener = (timePicker, i, i1) -> {
            Date date = DateUtils.parsedSelectedTime(String.valueOf(i) + ":" + String.valueOf(i1) + ":00");
            time_inscription_open = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_TIME_FORMAT);
            et_time_inscription_open.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.HOUR_FORMAT));
        };
        time_inscription_close_listener = (timePicker, i, i1) -> {
            Date date = DateUtils.parsedSelectedTime(String.valueOf(i) + ":" + String.valueOf(i1) + ":00");
            time_inscription_close = DateUtils.formatToFrontEndLocale(date, AppConstants.TIMESTAMP_DATABASE_TIME_FORMAT);
            et_time_inscription_close.setText(DateUtils.formatToFrontEndLocale(date, AppConstants.HOUR_FORMAT));
        };
        setData();
    }

    @OnItemSelected(R.id.sp_event_type)
    void onCommunityEventTypeSelected(int pos, long id) {

    }

    @OnCheckedChanged(R.id.sw_ranged)
    void onRangeSwitchCheckedListener(boolean b) {
        if (b) {
            et_price_min.setHint("Precio min.");
            et_price_min.setVisibility(View.VISIBLE);
            et_price_max.setHint("Precio max.");
            et_price_max.setVisibility(View.VISIBLE);
        } else {
            et_price_min.setHint("Precio");
            et_price_min.setVisibility(View.VISIBLE);
            et_price_max.setVisibility(View.INVISIBLE);
        }
    }

    @OnCheckedChanged(R.id.sw_limited)
    void onLimitedSwitchCheckedListener(boolean b) {
        if (b) {
            et_vacants.setVisibility(View.VISIBLE);
            et_date_inscription_open.setVisibility(View.VISIBLE);
            et_time_inscription_open.setVisibility(View.VISIBLE);
            et_date_inscription_close.setVisibility(View.VISIBLE);
            et_time_inscription_close.setVisibility(View.VISIBLE);
        } else {
            et_vacants.setVisibility(View.GONE);
            et_date_inscription_open.setVisibility(View.GONE);
            et_time_inscription_open.setVisibility(View.GONE);
            et_date_inscription_close.setVisibility(View.GONE);
            et_time_inscription_close.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.iv_image)
    void onImageClickListener() {
        if (!mContext.getPermissionPresenter().checkGrantedWriteExternalStoragePermission()) {
            mContext.getPermissionPresenter().requestWriteExternalStoragePermissionAfterRationale();
            return;
        }

        LayoutInflater factory = LayoutInflater.from(mContext);
        final View view = factory.inflate(R.layout.picture_source_dialog, null);

        CustomDialog picture_source_dialog = new CustomDialog(mContext);
        picture_source_dialog.setView(view);

        final ImageView iv_camera = view.findViewById(R.id.iv_camera);
        final ImageView iv_file = view.findViewById(R.id.iv_file);

        iv_camera.setOnClickListener(view1 -> {
            mPresenter.onAddCameraImageClicked();
            picture_source_dialog.dismiss();
        });
        iv_file.setOnClickListener(view12 -> {
            mPresenter.onAddFileImageClicked();
            picture_source_dialog.dismiss();
        });
        picture_source_dialog.show();
    }

    @OnClick(R.id.et_date_start)
    void onDateStartSelectClickListener() {
        int startYear = DateUtils.getCurrentYear();
        int startMonth = DateUtils.getCurrentMonth();
        int startDay = DateUtils.getCurrentDay();
        if (!date_start.isEmpty()) {
            startYear = Integer.valueOf(date_start.split("-")[0]);
            startMonth = Integer.valueOf(date_start.split("-")[1]) - 1;
            startDay = Integer.valueOf(date_start.split("-")[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date_start_listener, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.et_date_end)
    void onDateEndSelectClickListener() {
        int startYear = DateUtils.getCurrentYear();
        int startMonth = DateUtils.getCurrentMonth();
        int startDay = DateUtils.getCurrentDay();
        if (!date_end.isEmpty()) {
            startYear = Integer.valueOf(date_end.split("-")[0]);
            startMonth = Integer.valueOf((date_end.split("-")[1])) - 1;
            startDay = Integer.valueOf(date_end.split("-")[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date_end_listener, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.et_date_inscription_open)
    void onDateInscriptionOpenSelectClickListener() {
        int startYear = DateUtils.getCurrentYear();
        int startMonth = DateUtils.getCurrentMonth();
        int startDay = DateUtils.getCurrentDay();
        if (!date_inscription_open.isEmpty()) {
            startYear = Integer.valueOf(date_inscription_open.split("-")[0]);
            startMonth = Integer.valueOf(date_inscription_open.split("-")[1]) - 1;
            startDay = Integer.valueOf(date_inscription_open.split("-")[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date_inscription_open_listener, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.et_date_inscription_close)
    void onDateInscriptionCloseSelectClickListener() {
        int startYear = DateUtils.getCurrentYear();
        int startMonth = DateUtils.getCurrentMonth();
        int startDay = DateUtils.getCurrentDay();
        if (!date_inscription_close.isEmpty()) {
            startYear = Integer.valueOf(date_inscription_close.split("-")[0]);
            startMonth = Integer.valueOf(date_inscription_close.split("-")[1]) - 1;
            startDay = Integer.valueOf(date_inscription_close.split("-")[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date_inscription_close_listener, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.et_time_start)
    void onTimeStartSelectClickListener() {
        int startHour = DateUtils.getCurrentHour();
        int startMinute = DateUtils.getCurrentMinute();
        if (!time_start.isEmpty()) {
            startHour = Integer.valueOf(time_start.split(":")[0]);
            startMinute = Integer.valueOf(time_start.split(":")[1]);
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), time_start_listener, startHour, startMinute, true);
        timePickerDialog.show();
    }

    @OnClick(R.id.et_time_end)
    void onTimeEndSelectClickListener() {
        int startHour = DateUtils.getCurrentHour();
        int startMinute = DateUtils.getCurrentMinute();
        if (!time_end.isEmpty()) {
            startHour = Integer.valueOf(time_end.split(":")[0]);
            startMinute = Integer.valueOf(time_end.split(":")[1]);
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), time_end_listener, startHour, startMinute, true);
        timePickerDialog.show();
    }

    @OnClick(R.id.et_time_inscription_open)
    void onTimeInscriptionOpenSelectClickListener() {
        int startHour = DateUtils.getCurrentHour();
        int startMinute = DateUtils.getCurrentMinute();
        if (!time_inscription_open.isEmpty()) {
            startHour = Integer.valueOf(time_inscription_open.split(":")[0]);
            startMinute = Integer.valueOf(time_inscription_open.split(":")[1]);
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), time_inscription_open_listener, startHour, startMinute, true);
        timePickerDialog.show();
    }

    @OnClick(R.id.et_time_inscription_close)
    void onTimeInscriptionCloseSelectClickListener() {
        int startHour = DateUtils.getCurrentHour();
        int startMinute = DateUtils.getCurrentMinute();
        if (!time_inscription_close.isEmpty()) {
            startHour = Integer.valueOf(time_inscription_close.split(":")[0]);
            startMinute = Integer.valueOf(time_inscription_close.split(":")[1]);
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), time_inscription_close_listener, startHour, startMinute, true);
        timePickerDialog.show();
    }

    @OnClick(R.id.bt_publish)
    void onPublishClickListener() {
        final int type = (int) ((CommonUtils.StringWithTag) sp_event_type.getSelectedItem()).tag;
        final String title = et_title.getText().toString().trim();
        final String text = et_text.getText().toString().trim();
        final String address = et_address.getText().toString().trim();

        final String price_min = et_price_min.getText().toString().trim();
        final String price_max;
        final int ranged;
        if (sw_ranged.isChecked()) {
            ranged = 1;
            price_max = et_price_max.getText().toString().trim();
        } else {
            ranged = 0;
            price_max = "0";
        }

        final String date_start = this.date_start.concat(" " + this.time_start).trim();
        final String date_end = this.date_end.concat(" " + this.time_end).trim();
        final int limited;
        final String vacants;
        final String date_inscription_open;
        final String date_inscription_close;
        if (sw_limited.isChecked()) {
            limited = 1;
            vacants = et_vacants.getText().toString().trim();
            date_inscription_open = this.date_inscription_open.concat(" " + this.time_inscription_open).trim();
            date_inscription_close = this.date_inscription_close.concat(" " + this.time_inscription_close).trim();
        } else {
            limited = 0;
            vacants = "0";
            date_inscription_open = null;
            date_inscription_close = null;
        }
        mPresenter.onPublishClicked(type, title, text, date_start, date_end, address, price_min, price_max, ranged, vacants, limited, date_inscription_open, date_inscription_close);
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.CREATE_COMMUNITY_EVENT_FRAGMENT_TAG, false);
    }

    void setData() {
    }

    void loadPostImage(Bitmap bitmap) {
        iv_image.setImageBitmap(bitmap);
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}