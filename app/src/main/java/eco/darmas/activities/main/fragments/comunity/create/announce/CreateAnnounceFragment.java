package eco.darmas.activities.main.fragments.comunity.create.announce;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class CreateAnnounceFragment extends BaseFragment implements CreateAnnounceContracts.Fragment {

    @Override
    protected CreateAnnounceModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new CreateAnnounceModel(dataManager, savedInstanceState);
    }

    @Override
    protected CreateAnnouncePresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new CreateAnnouncePresenter((CreateAnnounceModel) model, (CreateAnnounceView) view);
    }

    @Override
    protected CreateAnnounceView createView(Context context) {
        return new CreateAnnounceView(context);
    }
}