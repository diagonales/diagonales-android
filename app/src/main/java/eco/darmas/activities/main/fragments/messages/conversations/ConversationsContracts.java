package eco.darmas.activities.main.fragments.messages.conversations;

interface ConversationsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}