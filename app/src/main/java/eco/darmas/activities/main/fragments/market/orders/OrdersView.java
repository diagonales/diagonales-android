package eco.darmas.activities.main.fragments.market.orders;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eco.darmas.utils.ScreenUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.utils.FlexibleUtils;
import eu.davidea.viewholders.FlexibleViewHolder;

class OrdersView extends BaseFragmentView implements OrdersContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private OrdersPresenter mPresenter;
    private OrdersAdapter flexibleOrdersAdapter;
    private Unbinder unBinder;
    private boolean scrollTop;

    // layout
    @BindView(R.id.li_market_orders)
    LinearLayout li_market_orders;

    // toolbar
    @BindView(R.id.li_order)
    LinearLayout li_order;
    @BindView(R.id.rb_order)
    SimpleRatingBar rb_order;
    @BindView(R.id.iv_order)
    ImageView iv_order;
    @BindView(R.id.iv_direction)
    ImageView iv_direction;
    @BindView(R.id.iv_search_orders)
    ImageView iv_search_orders;
    @BindView(R.id.li_categories)
    LinearLayout li_categories;
    @BindView(R.id.cv_category)
    CardView cv_category;
    @BindView(R.id.tv_category_orders_count)
    TextView tv_category_orders_count;
    @BindView(R.id.tv_category_orders)
    TextView tv_category_orders;
    @BindView(R.id.cb_show_ended)
    CheckBox cb_show_ended;

    // search box
    @BindView(R.id.li_search)
    LinearLayout li_search;
    @BindView(R.id.et_search)
    EditText et_search;

    // components
    @BindView(R.id.rv_orders)
    RecyclerView rv_orders;
    @BindView(R.id.tv_orders_none)
    TextView tv_orders_none;

    OrdersView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.orders_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (OrdersPresenter) presenter;
        setupAdapters();
        setData();
    }

    @OnClick(R.id.li_order)
    void onOrderClickListener() {
        mPresenter.onOrderClicked();
    }

    @OnTextChanged(R.id.et_search)
    void onSearchTextChangedListener(CharSequence s) {
        mPresenter.addSearchConstraint(s.toString());
        flexibleOrdersAdapter.setFilter(s.toString());
    }

    @OnClick(R.id.iv_clear)
    void onSearchClearClickListener() {
        et_search.setText("");
        mPresenter.addSearchConstraint("");
        flexibleOrdersAdapter.setFilter("");
    }

    @OnClick(R.id.li_categories)
    void onCategoryClickListener() {
        mPresenter.onCategoryClicked();
    }

    @OnCheckedChanged(R.id.cb_show_ended)
    void onShowEndedClickListener(boolean b) {
        mPresenter.onShowEndedClicked(b);
    }

    @OnClick(R.id.iv_search_orders)
    void onSearchOrdersClickListener() {
        if (li_search.getVisibility() == GONE) {
            et_search.setText("");
            li_search.setVisibility(VISIBLE);
            KeyboardUtils.showSoftInput(et_search, getContext());
        } else {
            KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), et_search);
            flexibleOrdersAdapter.setFilter("");
            mPresenter.addSearchConstraint("");
            li_search.setVisibility(GONE);
        }
    }

    void setData() {
        tv_category_orders.setText(mPresenter.getSelectedCategoryName());
        if (mPresenter.getSelectedCategoryId() == 0) {
            et_search.setHint("Buscar en todas las categorías");
            cv_category.setCardElevation(0);
            cv_category.setCardBackgroundColor(0);
            tv_category_orders_count.setVisibility(View.INVISIBLE);
        } else {
            Spannable hint = CommonUtils.getBoldItalicTextSpan("Buscar en " + mPresenter.getSelectedCategoryName(), mPresenter.getSelectedCategoryName());
            et_search.setHint(hint);
            tv_category_orders_count.setText(mPresenter.getOrderCount());
            cv_category.setCardElevation(5);
            cv_category.setCardBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            tv_category_orders_count.setVisibility(View.VISIBLE);
        }
        LinearLayout.LayoutParams arrowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        switch (mPresenter.getSelectedOrder()) {
            case AppConstants.BY_SKILL_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.categoryRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_SKILL_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.categoryRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
        }
    }

    void setupAdapters() {
        rv_orders.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_orders = new LinearLayoutManager(getContext());
        rv_orders.setLayoutManager(lm_orders);
        rv_orders.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.order, 15));

        // Initialize the Adapter
        flexibleOrdersAdapter = new OrdersAdapter(getFlexibleOrdersItemList(mPresenter.getOrderList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_orders.setAdapter(flexibleOrdersAdapter);
    }

    void dataSetChanged(boolean scrollTop) {
        setData();
        this.scrollTop = scrollTop;
        flexibleOrdersAdapter.updateDataSet(getFlexibleOrdersItemList(mPresenter.getOrderList()));
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onOrderItemClick(position);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_orders.setVisibility(size == 0 ? View.GONE : View.VISIBLE);
        tv_orders_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_orders.scrollToPosition(0);
        }
    }

    class OrdersAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        OrdersAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class OrderItemAdapter extends AbstractFlexibleItem<OrderItemAdapter.ViewHolder> {

        private _Transaction transaction;

        OrderItemAdapter(_Transaction transaction) {
            this.transaction = transaction;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof OrderItemAdapter) {
                _Transaction inItem = ((OrderItemAdapter) inObject).transaction;
                return this.transaction.equals(inItem) && mPresenter.getRatings(transaction)[0].equals(mPresenter.getRatings(inItem)[0]);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return transaction.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.order;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int transactionType = transaction.getType();
            int status = transaction.getStatus();
            int senderId = transaction.getIdSender();
            int receiverId = transaction.getIdReceiver();

            // Top buttons
            if (transaction.getContribution() > 0) {
                viewHolder.li_contribution.setVisibility(VISIBLE);
                viewHolder.bt_contribution.setOnClickListener(view -> CommonUtils.showDialog(getContext(), "Contribución a la comunidad: ", AppConstants.CURRENCY_FORMAT.format(transaction.getContribution())));
            } else {
                viewHolder.li_contribution.setVisibility(GONE);
            }
            if (!transaction.getNote().isEmpty()) {
                viewHolder.li_notes.setVisibility(VISIBLE);
                viewHolder.bt_notes.setOnClickListener(view -> CommonUtils.showDialog(getContext(), "Notas adicionales:", transaction.getNote()));
            } else {
                viewHolder.li_notes.setVisibility(GONE);
            }
            if (transaction.getIdEvent() >= 0) {
                viewHolder.bt_shipping.setImageResource(R.drawable.ic_stand_small);
                viewHolder.bt_shipping.setOnClickListener(v -> CommonUtils.showDialog(getContext(), "Información de entrega:", "Retiro en stand"));
                // Paint seek bar to event color
                viewHolder.sb_status.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.textFair), PorterDuff.Mode.MULTIPLY));
                // Adapt status display
                viewHolder.tv_delivered.setText("Listo");
                viewHolder.tv_received.setText("Retirado");
            } else {
                // Paint seek bar to global color
                viewHolder.sb_status.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.accentColor), PorterDuff.Mode.MULTIPLY));
                switch (transaction.getShippingMethod()) {
                    case Listing.ShippingMethod.PICKUP:
                        viewHolder.bt_shipping.setImageResource(R.drawable.ic_location_small);
                        viewHolder.bt_shipping.setOnClickListener(v -> CommonUtils.showDialog(getContext(), "Información de entrega:", "Retiro en dirección"));
                        // Adapt status display
                        viewHolder.tv_delivered.setText("Listo");
                        viewHolder.tv_received.setText("Retirado");
                        break;
                    case Listing.ShippingMethod.DELIVERY:
                        viewHolder.bt_shipping.setImageResource(R.drawable.ic_delivery_small);
                        viewHolder.bt_shipping.setOnClickListener(v -> CommonUtils.showDialog(getContext(), "Información de entrega:", transaction.getShippingInfo()));
                        // Adapt status display
                        viewHolder.tv_delivered.setText("Enviado");
                        viewHolder.tv_received.setText("Recibido");
                        break;
                    case Listing.ShippingMethod.DIGITAL:
                        viewHolder.bt_shipping.setImageResource(R.drawable.ic_digital_small);
                        viewHolder.bt_shipping.setOnClickListener(v -> CommonUtils.showDialog(getContext(), "Información de entrega:", transaction.getShippingInfo()));
                        // Adapt status display
                        viewHolder.tv_delivered.setText("Enviado");
                        viewHolder.tv_received.setText("Recibido");
                        break;
                    default:
                        break;
                }
            }

            viewHolder.tv_concept_left.setMaxWidth(ScreenUtils.getDisplayWidthPixels(getContext()) / 3);
            viewHolder.tv_concept_right.setMaxWidth(ScreenUtils.getDisplayWidthPixels(getContext()) / 3);

            Listing listing = mPresenter.getListing(transaction.getIdListing());
            viewHolder.tv_concept_left.setOnClickListener(view -> mPresenter.onConceptClicked(listing));
            viewHolder.tv_concept_right.setOnClickListener(view -> mPresenter.onConceptClicked(listing));

            viewHolder.iv_more.setOnClickListener(view -> {
                PopupMenu popup = new PopupMenu(getContext(), view);

                // Reflection workaround to show icons in menu
                try {
                    Field[] fields = popup.getClass().getDeclaredFields();
                    for (Field field : fields) {
                        if ("mPopup".equals(field.getName())) {
                            field.setAccessible(true);
                            Object menuPopupHelper = field.get(popup);
                            Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                            Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                            setForceIcons.invoke(menuPopupHelper, true);
                            break;
                        }
                    }
                } catch (Exception e) {
                    AppLogger.e(e, null);
                }

                popup.getMenuInflater().inflate(R.menu.order_popup, popup.getMenu());
                /*if (transaction.getNote().isEmpty()) {
                    popup.getMenu().findItem(R.id.specifications).setEnabled(false);
                    popup.getMenu().findItem(R.id.specifications).setTitle("Sin notas adicionales");
                }*/

                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        default:
                            CommonUtils.showToast("Característica a implementar");
                    }
                    return true;
                });
                popup.show();
            });

            Rating rating = mPresenter.getRatings(transaction)[0];
            viewHolder.sb_status.setOnTouchListener((v2, event) -> true);
            viewHolder.tv_ordered_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDate()), AppConstants.HOUR_FORMAT));
            viewHolder.tv_ordered_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDate()), AppConstants.DATE_FORMAT));
            viewHolder.tv_id_right.setText("ID: " + transaction.getIdTransaction());

            viewHolder.rb_rating.setOnTouchListener((v2, event) -> {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    mPresenter.onRateClicked(position);
                    v2.setPressed(false);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v2.setPressed(true);
                }
                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v2.setPressed(false);
                }
                return true;
            });
            //viewHolder.bt_rate.setOnClickListener(v2 -> mPresenter.onRateClicked(position));

            viewHolder.bt_expand.setImageResource(R.drawable.ic_keyboard_arrow_down);
            if (status == _Transaction.Status.RECEIVED) {
                float ratingFloat = 0;
                if (rating != null) {
                    ratingFloat = rating.getRating();
                }
                viewHolder.rb_rating.setRating(ratingFloat);
                // Hide status info
                viewHolder.group_expanded.setVisibility(GONE);
                viewHolder.group_expanded_2.setVisibility(GONE);
                if (transactionType == _Transaction.Type.OFFER && AppData.INSTANCE.getLoggedUser().getIdUser() == senderId) {
                    if (ratingFloat != 0) {
                        viewHolder.tv_rating.setText("Tu valoración de este producto: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.listingRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.VISIBLE);
                        //viewHolder.bt_rate.setVisibility(View.GONE);
                    } else {
                        viewHolder.tv_rating.setText("Aún no has valorado este producto: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.listingRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.GONE);
                        //viewHolder.bt_rate.setVisibility(View.VISIBLE);
                    }
                } else if (transactionType == _Transaction.Type.DEMAND && AppData.INSTANCE.getLoggedUser().getIdUser() == receiverId) {
                    if (ratingFloat != 0) {
                        viewHolder.tv_rating.setText("Tu valoración de su trabajo: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.VISIBLE);
                        //viewHolder.bt_rate.setVisibility(View.GONE);
                    } else {
                        viewHolder.tv_rating.setText("Aún no has valorado su trabajo: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.GONE);
                        //viewHolder.bt_rate.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (ratingFloat != 0) {
                        viewHolder.tv_rating.setText("Tu valoración de su conducta: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.userRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.userBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.VISIBLE);
                        //viewHolder.bt_rate.setVisibility(View.GONE);
                    } else {
                        viewHolder.tv_rating.setText("Aún no has valorado su conducta: ");
                        viewHolder.rb_rating.setFillColor(getResources().getColor(R.color.userRatingBar));
                        viewHolder.rb_rating.setStarBackgroundColor(getResources().getColor(R.color.userBackgroundRatingBar));
                        viewHolder.rb_rating.setBorderColor(getResources().getColor(R.color.transparent));
                        viewHolder.rb_rating.setVisibility(View.GONE);
                        //viewHolder.bt_rate.setVisibility(View.VISIBLE);
                    }
                }
                viewHolder.group_rating.setVisibility(View.VISIBLE);
            } else {
                viewHolder.group_rating.setVisibility(View.GONE);
                // show status info
                viewHolder.group_expanded.setVisibility(GONE);
                viewHolder.group_expanded_2.setVisibility(VISIBLE);
                //viewHolder.bt_rate.setVisibility(View.GONE);
            }

            viewHolder.bt_expand.setOnClickListener(v2 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    //TODO: while animation is happening if user scroll the recycler view, visual bugs and crashes occur -> https://github.com/davideas/FlexibleAdapter/issues/82
                    TransitionManager.beginDelayedTransition(rv_orders);
                }
                if (transaction.getStatus() == _Transaction.Status.RECEIVED) {
                    viewHolder.group_expanded_2.setVisibility(viewHolder.group_expanded_2.getVisibility() == GONE ? VISIBLE : GONE);
                }
                viewHolder.group_expanded.setVisibility(viewHolder.group_expanded.getVisibility() == GONE ? VISIBLE : GONE);
                viewHolder.bt_expand.setImageResource(viewHolder.group_expanded.getVisibility() == GONE ? R.drawable.ic_keyboard_arrow_down : R.drawable.ic_keyboard_arrow_up);
            });
            viewHolder.bt_cancel.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                if (transaction.getType() == _Transaction.Type.OFFER) {
                    confirmDialog.setTitle("Deseas cancelar este encargo?");
                    confirmDialog.setMessage("Una vez cancelado el pago reservado se reintegrará a tus fondos");
                } else if (transaction.getType() == _Transaction.Type.OFFER) {
                    confirmDialog.setTitle("Deseas cancelar esta oferta?");
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onCancelTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            viewHolder.bt_accept.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                if (transactionType == _Transaction.Type.OFFER) {
                    confirmDialog.setTitle("Deseas aceptar este encargo?");
                } else if (transactionType == _Transaction.Type.DEMAND) {
                    confirmDialog.setTitle("Deseas aceptar esta oferta?");
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onAcceptTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            viewHolder.bt_reject.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                if (transactionType == _Transaction.Type.OFFER) {
                    confirmDialog.setTitle("Deseas rechazar este encargo?");
                } else if (transactionType == _Transaction.Type.DEMAND) {
                    confirmDialog.setTitle("Deseas rechazar esta oferta?");
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onRejectTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            viewHolder.bt_delivered.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                switch (transaction.getShippingMethod()) {
                    case Listing.ShippingMethod.PICKUP:
                        confirmDialog.setTitle("Marcar como listo?");
                        break;
                    case Listing.ShippingMethod.DELIVERY:
                        confirmDialog.setTitle("Marcar como enviado?");
                        break;
                    case Listing.ShippingMethod.DIGITAL:
                        confirmDialog.setTitle("Marcar como enviado?");
                        break;
                }
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onDeliveredTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            viewHolder.bt_received.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                switch (transaction.getShippingMethod()) {
                    case Listing.ShippingMethod.PICKUP:
                        confirmDialog.setTitle("Marcar como retirado?");
                        break;
                    case Listing.ShippingMethod.DELIVERY:
                        confirmDialog.setTitle("Marcar como recibido?");
                        break;
                    case Listing.ShippingMethod.DIGITAL:
                        confirmDialog.setTitle("Marcar como recibido?");
                        break;
                }
                confirmDialog.setMessage("Antes revisa que todo este bien, luego de hacerlo liberaremos el pago y la transacción quedará completada.");
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onReceivedTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });
            viewHolder.bt_archive.setOnClickListener(v2 -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                confirmDialog.setTitle("Archivar operación?");
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> mPresenter.onArchiveTransaction(transaction));
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });

            switch (status) {
                case _Transaction.Status.REQUESTED:
                    viewHolder.bt_archive.setVisibility(View.GONE);
                    if (AppData.INSTANCE.getLoggedUser().getIdUser() == senderId) {
                        viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderAwaiting));
                        viewHolder.tv_order_status.setText("En espera");
                        viewHolder.bt_cancel.setVisibility(View.VISIBLE);
                        viewHolder.bt_accept.setVisibility(View.GONE);
                        viewHolder.bt_reject.setVisibility(View.GONE);
                        viewHolder.bt_delivered.setVisibility(View.GONE);
                        viewHolder.bt_received.setVisibility(View.GONE);
                        viewHolder.bt_waiting.setVisibility(View.GONE);
                    } else if (AppData.INSTANCE.getLoggedUser().getIdUser() == receiverId) {
                        viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderPending));
                        viewHolder.tv_order_status.setText("Pendiente");
                        viewHolder.bt_cancel.setVisibility(View.GONE);
                        viewHolder.bt_accept.setVisibility(View.VISIBLE);
                        viewHolder.bt_reject.setVisibility(View.VISIBLE);
                        viewHolder.bt_delivered.setVisibility(View.GONE);
                        viewHolder.bt_received.setVisibility(View.GONE);
                        viewHolder.bt_waiting.setVisibility(View.GONE);
                    }

                    /*ValueAnimator anim = ValueAnimator.ofInt(viewHolder.sb_status.getProgress() * 100, 1 * 100);
                    anim.setDuration(1000);
                    anim.addUpdateListener(animation -> {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        viewHolder.sb_status.setProgress(animProgress);
                    });
                    anim.start();*/
                    viewHolder.sb_status.setProgress(1);

                    viewHolder.tv_accepted.setText("Aceptado");
                    viewHolder.tv_accepted_hour.setText("--:--");
                    viewHolder.tv_accepted_date.setText("--/--/--");
                    viewHolder.tv_delivered_hour.setText("--:--");
                    viewHolder.tv_delivered_date.setText("--/--/--");
                    viewHolder.tv_received_hour.setText("--:--");
                    viewHolder.tv_received_date.setText("--/--/--");
                    viewHolder.tv_accepted.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_accepted_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_accepted_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    break;
                case _Transaction.Status.REJECTED:
                    //viewHolder.cv_order_status.setCardBackgroundColor(getResources().getColor(R.color.orderRejected));
                    viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderRejected));
                    viewHolder.tv_order_status.setText("Rechazado");

                    viewHolder.bt_cancel.setVisibility(View.GONE);
                    viewHolder.bt_accept.setVisibility(View.GONE);
                    viewHolder.bt_reject.setVisibility(View.GONE);
                    viewHolder.bt_delivered.setVisibility(View.GONE);
                    viewHolder.bt_received.setVisibility(View.GONE);
                    viewHolder.bt_waiting.setVisibility(View.GONE);
                    viewHolder.bt_archive.setVisibility(View.VISIBLE);

                    /*ValueAnimator anim2 = ValueAnimator.ofInt(viewHolder.sb_status.getProgress() * 100, 2 * 100);
                    anim2.setDuration(1000);
                    anim2.addUpdateListener(animation -> {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        viewHolder.sb_status.setProgress(animProgress);
                    });
                    anim2.start();*/
                    viewHolder.sb_status.setProgress(2);

                    viewHolder.tv_accepted.setText("Rechazado");
                    viewHolder.tv_accepted_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateRejected()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_accepted_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateRejected()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_delivered_hour.setText("--:--");
                    viewHolder.tv_delivered_date.setText("--/--/--");
                    viewHolder.tv_received_hour.setText("--:--");
                    viewHolder.tv_received_date.setText("--/--/--");
                    viewHolder.tv_accepted.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    break;
                case _Transaction.Status.ACCEPTED:
                    viewHolder.bt_archive.setVisibility(View.GONE);
                    if (AppData.INSTANCE.getLoggedUser().getIdUser() == senderId) {
                        if (transactionType == _Transaction.Type.OFFER) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderInProcess));
                            viewHolder.tv_order_status.setText("En proceso");
                            viewHolder.bt_cancel.setVisibility(View.GONE);
                            viewHolder.bt_accept.setVisibility(View.GONE);
                            viewHolder.bt_reject.setVisibility(View.GONE);
                            viewHolder.bt_delivered.setVisibility(View.GONE);
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.VISIBLE);
                        } else if (transactionType == _Transaction.Type.DEMAND) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderPending));
                            viewHolder.tv_order_status.setText("Pendiente");
                            viewHolder.bt_cancel.setVisibility(View.GONE);
                            viewHolder.bt_accept.setVisibility(View.GONE);
                            viewHolder.bt_reject.setVisibility(View.GONE);
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.bt_delivered.setText("Listo");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.bt_delivered.setText("Enviado");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.bt_delivered.setText("Enviado");
                                    break;
                            }
                            viewHolder.bt_delivered.setVisibility(View.VISIBLE);
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.GONE);
                        }
                    } else if (AppData.INSTANCE.getLoggedUser().getIdUser() == receiverId) {
                        if (transactionType == _Transaction.Type.OFFER) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderPending));
                            viewHolder.tv_order_status.setText("Pendiente");
                            viewHolder.bt_cancel.setVisibility(View.GONE);
                            viewHolder.bt_accept.setVisibility(View.GONE);
                            viewHolder.bt_reject.setVisibility(View.GONE);
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.bt_delivered.setText("Listo");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.bt_delivered.setText("Enviado");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.bt_delivered.setText("Enviado");
                                    break;
                            }
                            viewHolder.bt_delivered.setVisibility(View.VISIBLE);
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.GONE);
                        } else if (transactionType == _Transaction.Type.DEMAND) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderInProcess));
                            viewHolder.tv_order_status.setText("En proceso");
                            viewHolder.bt_cancel.setVisibility(View.GONE);
                            viewHolder.bt_accept.setVisibility(View.GONE);
                            viewHolder.bt_reject.setVisibility(View.GONE);
                            viewHolder.bt_delivered.setVisibility(View.GONE);
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.VISIBLE);
                        }
                    }

                    /*ValueAnimator anim3 = ValueAnimator.ofInt(viewHolder.sb_status.getProgress() * 100, 2 * 100);
                    anim3.setDuration(1000);
                    anim3.addUpdateListener(animation -> {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        viewHolder.sb_status.setProgress(animProgress);
                    });
                    anim3.start();*/
                    viewHolder.sb_status.setProgress(2);

                    viewHolder.tv_accepted.setText("Aceptado");
                    viewHolder.tv_accepted_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_accepted_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_delivered_hour.setText("--:--");
                    viewHolder.tv_delivered_date.setText("--/--/--");
                    viewHolder.tv_received_hour.setText("--:--");
                    viewHolder.tv_received_date.setText("--/--/--");
                    viewHolder.tv_accepted.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_delivered_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    break;
                case _Transaction.Status.DELIVERED:
                    viewHolder.bt_archive.setVisibility(View.GONE);
                    viewHolder.bt_cancel.setVisibility(View.GONE);
                    viewHolder.bt_accept.setVisibility(View.GONE);
                    viewHolder.bt_reject.setVisibility(View.GONE);
                    viewHolder.bt_delivered.setVisibility(View.GONE);
                    if (AppData.INSTANCE.getLoggedUser().getIdUser() == senderId) {
                        if (transactionType == _Transaction.Type.OFFER) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderPending));
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.tv_order_status.setText("Retirar");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.tv_order_status.setText("Recepción");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.tv_order_status.setText("Recepción");
                                    break;
                            }
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.bt_received.setText("Retirado");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.bt_received.setText("Recibido");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.bt_received.setText("Recibido");
                                    break;
                            }
                            viewHolder.bt_received.setVisibility(View.VISIBLE);
                            viewHolder.bt_waiting.setVisibility(View.GONE);
                        } else if (transactionType == _Transaction.Type.DEMAND) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderAwaiting));
                            viewHolder.tv_order_status.setText("En espera");
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.VISIBLE);
                        }
                    } else if (AppData.INSTANCE.getLoggedUser().getIdUser() == receiverId) {
                        if (transactionType == _Transaction.Type.OFFER) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderAwaiting));
                            viewHolder.tv_order_status.setText("En espera");
                            viewHolder.bt_received.setVisibility(View.GONE);
                            viewHolder.bt_waiting.setVisibility(View.VISIBLE);
                        } else if (transactionType == _Transaction.Type.DEMAND) {
                            viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderPending));
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.tv_order_status.setText("Retirar");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.tv_order_status.setText("Recepción");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.tv_order_status.setText("Recepción");
                                    break;
                            }
                            switch (transaction.getShippingMethod()) {
                                case Listing.ShippingMethod.PICKUP:
                                    viewHolder.bt_received.setText("Retirado");
                                    break;
                                case Listing.ShippingMethod.DELIVERY:
                                    viewHolder.bt_received.setText("Recibido");
                                    break;
                                case Listing.ShippingMethod.DIGITAL:
                                    viewHolder.bt_received.setText("Recibido");
                                    break;
                            }
                            viewHolder.bt_received.setVisibility(View.VISIBLE);
                            viewHolder.bt_waiting.setVisibility(View.GONE);
                        }
                    }
                    /*ValueAnimator anim4 = ValueAnimator.ofInt(viewHolder.sb_status.getProgress() * 100, 3 * 100);
                    anim4.setDuration(1000);
                    anim4.addUpdateListener(animation -> {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        viewHolder.sb_status.setProgress(animProgress);
                    });
                    anim4.start();*/
                    viewHolder.sb_status.setProgress(3);

                    viewHolder.tv_accepted.setText("Aceptado");
                    viewHolder.tv_accepted_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_accepted_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_delivered_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateDelivered()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_delivered_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateDelivered()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_received_hour.setText("--:--");
                    viewHolder.tv_received_date.setText("--/--/--");
                    viewHolder.tv_accepted.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_received.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_hour.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    viewHolder.tv_received_date.setTextColor(getResources().getColor(R.color.secondaryTextColor));
                    break;
                case _Transaction.Status.RECEIVED:
                    viewHolder.tv_order_status.setBackgroundColor(getResources().getColor(R.color.orderCompleted));
                    viewHolder.tv_order_status.setText("Completado");

                    /*ValueAnimator anim5 = ValueAnimator.ofInt(viewHolder.sb_status.getProgress() * 100, 4 * 100);
                    anim5.setDuration(1000);
                    anim5.addUpdateListener(animation -> {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        viewHolder.sb_status.setProgress(animProgress);
                    });
                    anim5.start();*/
                    viewHolder.sb_status.setProgress(4);

                    viewHolder.tv_accepted.setText("Aceptado");
                    viewHolder.tv_accepted_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_accepted_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateAccepted()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_delivered_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateDelivered()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_delivered_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateDelivered()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_received_hour.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateReceived()), AppConstants.HOUR_FORMAT));
                    viewHolder.tv_received_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDateReceived()), AppConstants.DATE_FORMAT));
                    viewHolder.tv_accepted.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_accepted_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_delivered_date.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_received.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_received_hour.setTextColor(getResources().getColor(R.color.primaryTextColor));
                    viewHolder.tv_received_date.setTextColor(getResources().getColor(R.color.primaryTextColor));

                    viewHolder.bt_archive.setVisibility(View.GONE);
                    viewHolder.bt_cancel.setVisibility(View.GONE);
                    viewHolder.bt_accept.setVisibility(View.GONE);
                    viewHolder.bt_reject.setVisibility(View.GONE);
                    viewHolder.bt_delivered.setVisibility(View.GONE);
                    viewHolder.bt_received.setVisibility(View.GONE);
                    viewHolder.bt_waiting.setVisibility(View.GONE);
                    break;
            }

            String quantity = "";
            if (transaction.getQuantity() > 1) {
                quantity = "x" + String.valueOf(transaction.getQuantity()) + " ";
            }
            String concept = quantity + "\"" + transaction.getConcept() + "\"";
            String prefix = "";
            if (transactionType == _Transaction.Type.OFFER) {
                if (AppData.INSTANCE.getLoggedUser().getIdUser() == transaction.getIdSender()) {
                    prefix = "-";
                } else {
                    prefix = "+";
                }
            } else if (transactionType == _Transaction.Type.DEMAND) {
                if (AppData.INSTANCE.getLoggedUser().getIdUser() == transaction.getIdSender()) {
                    prefix = "+";
                } else {
                    prefix = "-";
                }
            }
            String amount = prefix + AppConstants.CURRENCY_FORMAT.format(transaction.getAmount());
            Spannable conceptSpan;
            Spannable amountSpan;

            if (transaction.getIdEvent() >= 0) {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.textFair));
                viewHolder.iv_currency_left.setImageResource(R.drawable.ic_darmas_currency_fair);
                viewHolder.iv_currency_right.setImageResource(R.drawable.ic_darmas_currency_fair);
                if (listing != null) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                }
            } else {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.secondaryAccentColor));
                viewHolder.iv_currency_left.setImageResource(R.drawable.ic_darmas_currency);
                viewHolder.iv_currency_right.setImageResource(R.drawable.ic_darmas_currency);
                if (listing != null) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                }
            }

            if (AppData.INSTANCE.getLoggedUser().getIdUser() == receiverId) {
                if (transactionType == _Transaction.Type.OFFER) {
                    viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_back_white);
                    viewHolder.tv_ordered.setText("Encargado");
                    viewHolder.tv_concept_left.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_left.setVisibility(View.GONE);
                    viewHolder.tv_amount_left.setVisibility(View.GONE);
                    viewHolder.tv_amount_right.setText(amountSpan);
                    viewHolder.tv_amount_right.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_right.setVisibility(View.VISIBLE);
                    viewHolder.tv_concept_right.setVisibility(View.GONE);
                    if (flexibleOrdersAdapter.hasFilter()) {
                        FlexibleUtils.highlightText(viewHolder.tv_concept_left, quantity + "\"" + transaction.getConcept() + "\"", flexibleOrdersAdapter.getFilter(String.class));
                    } else {
                        viewHolder.tv_concept_left.setText(conceptSpan);
                    }
                } else if (transactionType == _Transaction.Type.DEMAND) {
                    viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_back_white);
                    viewHolder.tv_ordered.setText("Ofrecido");
                    viewHolder.tv_concept_right.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_right.setVisibility(View.GONE);
                    viewHolder.tv_amount_right.setVisibility(View.GONE);
                    viewHolder.tv_amount_left.setText(amountSpan);
                    viewHolder.tv_amount_left.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_left.setVisibility(View.VISIBLE);
                    viewHolder.tv_concept_left.setVisibility(View.GONE);
                    if (flexibleOrdersAdapter.hasFilter()) {
                        FlexibleUtils.highlightText(viewHolder.tv_concept_right, quantity + "\"" + transaction.getConcept() + "\"", flexibleOrdersAdapter.getFilter(String.class));
                    } else {
                        viewHolder.tv_concept_right.setText(conceptSpan);
                    }
                }
                viewHolder.iv_right.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(senderId)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> viewHolder.iv_right.performClick());
                viewHolder.tv_right_name.setText(mPresenter.getUser(senderId).getName());
                ImageUtils.loadRoundedProfileImage(mPresenter.getUser(senderId), viewHolder.iv_right);
            } else if (AppData.INSTANCE.getLoggedUser().getIdUser() == senderId) {
                if (transactionType == _Transaction.Type.OFFER) {
                    viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_forward_white);
                    viewHolder.tv_ordered.setText("Encargado");
                    viewHolder.tv_concept_right.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_right.setVisibility(View.GONE);
                    viewHolder.tv_amount_right.setVisibility(View.GONE);
                    viewHolder.tv_amount_left.setText(amountSpan);
                    viewHolder.tv_amount_left.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_left.setVisibility(View.VISIBLE);
                    viewHolder.tv_concept_left.setVisibility(View.GONE);
                    if (flexibleOrdersAdapter.hasFilter()) {
                        FlexibleUtils.highlightText(viewHolder.tv_concept_right, quantity + "\"" + transaction.getConcept() + "\"", flexibleOrdersAdapter.getFilter(String.class));
                    } else {
                        viewHolder.tv_concept_right.setText(conceptSpan);
                    }
                } else if (transactionType == _Transaction.Type.DEMAND) {
                    viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_forward_white);
                    viewHolder.tv_ordered.setText("Ofrecido");
                    viewHolder.tv_concept_left.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_left.setVisibility(View.GONE);
                    viewHolder.tv_amount_left.setVisibility(View.GONE);
                    viewHolder.tv_amount_right.setText(amountSpan);
                    viewHolder.tv_amount_right.setVisibility(View.VISIBLE);
                    viewHolder.iv_currency_right.setVisibility(View.VISIBLE);
                    viewHolder.tv_concept_right.setVisibility(View.GONE);
                    if (flexibleOrdersAdapter.hasFilter()) {
                        FlexibleUtils.highlightText(viewHolder.tv_concept_left, quantity + "\"" + transaction.getConcept() + "\"", flexibleOrdersAdapter.getFilter(String.class));
                    } else {
                        viewHolder.tv_concept_left.setText(conceptSpan);
                    }
                }

                viewHolder.iv_right.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(receiverId)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> viewHolder.iv_right.performClick());
                viewHolder.tv_right_name.setText(mPresenter.getUser(receiverId).getName());
                ImageUtils.loadRoundedProfileImage(mPresenter.getUser(receiverId), viewHolder.iv_right);
            }
            ImageUtils.loadTransactionBackgroundImage(listing, viewHolder.iv_listing_front);
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.iv_arrow)
            ImageView iv_arrow;
            @BindView(R.id.iv_right)
            ImageView iv_right;
            @BindView(R.id.tv_id_right)
            TextView tv_id_right;
            @BindView(R.id.tv_right_name)
            TextView tv_right_name;
            @BindView(R.id.tv_concept_right)
            TextView tv_concept_right;
            @BindView(R.id.tv_concept_left)
            TextView tv_concept_left;
            @BindView(R.id.tv_amount_right)
            TextView tv_amount_right;
            @BindView(R.id.tv_amount_left)
            TextView tv_amount_left;
            @BindView(R.id.tv_accepted)
            TextView tv_accepted;
            @BindView(R.id.tv_accepted_hour)
            TextView tv_accepted_hour;
            @BindView(R.id.tv_accepted_date)
            TextView tv_accepted_date;
            @BindView(R.id.tv_delivered)
            TextView tv_delivered;
            @BindView(R.id.tv_delivered_hour)
            TextView tv_delivered_hour;
            @BindView(R.id.tv_delivered_date)
            TextView tv_delivered_date;
            @BindView(R.id.tv_received)
            TextView tv_received;
            @BindView(R.id.tv_received_hour)
            TextView tv_received_hour;
            @BindView(R.id.tv_received_date)
            TextView tv_received_date;
            @BindView(R.id.bt_expand)
            FloatingActionButton bt_expand;
            /*@BindView(R.id.tbr_hour)
            TableRow tbr_hour;
            @BindView(R.id.tbr_date)
            TableRow tbr_date;*/
            @BindView(R.id.bt_cancel)
            Button bt_cancel;
            @BindView(R.id.bt_accept)
            Button bt_accept;
            @BindView(R.id.bt_reject)
            Button bt_reject;
            @BindView(R.id.bt_delivered)
            Button bt_delivered;
            @BindView(R.id.bt_received)
            Button bt_received;
            @BindView(R.id.bt_waiting)
            Button bt_waiting;
            //@BindView(R.id.bt_rate)
            //Button bt_rate;
            @BindView(R.id.bt_archive)
            Button bt_archive;
            @BindView(R.id.iv_currency_right)
            ImageView iv_currency_right;
            @BindView(R.id.iv_currency_left)
            ImageView iv_currency_left;
            @BindView(R.id.sb_status)
            SeekBar sb_status;
            @BindView(R.id.tv_ordered)
            TextView tv_ordered;
            @BindView(R.id.tv_ordered_hour)
            TextView tv_ordered_hour;
            @BindView(R.id.tv_ordered_date)
            TextView tv_ordered_date;
            /*@BindView(R.id.li_rating)
            LinearLayout li_rating;*/
            /*@BindView(R.id.li_diagram)
            LinearLayout li_diagram;*/
            @BindView(R.id.tv_rating)
            TextView tv_rating;
            @BindView(R.id.rb_rating)
            SimpleRatingBar rb_rating;
            @BindView(R.id.iv_listing_front)
            ImageView iv_listing_front;
            /*@BindView(R.id.tv_category)
            TextView tv_category;*/
            @BindView(R.id.iv_more)
            ImageView iv_more;
            @BindView(R.id.tv_order_status)
            TextView tv_order_status;
            /*@BindView(R.id.cv_order_status)
            CardView cv_order_status;*/
            /*@BindView(R.id.li_contact)
            LinearLayout li_contact;*/
            @BindView(R.id.bt_shipping)
            FloatingActionButton bt_shipping;
            @BindView(R.id.li_notes)
            LinearLayout li_notes;
            @BindView(R.id.bt_notes)
            FloatingActionButton bt_notes;
            @BindView(R.id.bt_contribution)
            FloatingActionButton bt_contribution;
            @BindView(R.id.li_contribution)
            LinearLayout li_contribution;

            @BindView(R.id.cl_order)
            ConstraintLayout cl_order;
            @BindView(R.id.group_expanded)
            Group group_expanded;
            @BindView(R.id.group_expanded_2)
            Group group_expanded_2;
            @BindView(R.id.group_rating)
            Group group_rating;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleOrdersItemList(List<_Transaction> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (_Transaction item : list) {
            itemList.add(new OrderItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
