package eco.darmas.activities.main.fragments.market.select;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class SelectableListingsFragment extends BaseFragment implements SelectableListingsContracts.Fragment {

    @Override
    protected SelectableListingsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new SelectableListingsModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected SelectableListingsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new SelectableListingsPresenter((SelectableListingsModel) model, (SelectableListingsView) view);
    }

    @Override
    protected SelectableListingsView createView(Context context) {
        return new SelectableListingsView(context, getChildFragmentManager());
    }
}
