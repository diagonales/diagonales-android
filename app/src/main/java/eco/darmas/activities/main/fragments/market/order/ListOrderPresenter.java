package eco.darmas.activities.main.fragments.market.order;

import org.greenrobot.eventbus.EventBus;

import eco.darmas.AppConstants;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataFilters;
import eco.darmas.events.FilterEvent;
import eco.darmas.utils.CommonUtils;

class ListOrderPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> {

    private ListOrderModel mModel;
    private ListOrderView mView;

    ListOrderPresenter(ListOrderModel model, ListOrderView view) {
        super(model, view);
        this.mModel = model;
        this.mView = view;
    }

    int getInitialOrder() {
        return DataFilters.selectedOffersListOrder;
    }

    void onApplyClicked(int newSelectedOrder) {
        if (mModel.getSelectedOrder() != newSelectedOrder) {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG, true);
            FilterEvent event = null;
            if (mModel.getCaller() == AppConstants.SELECT_OFFERS_LIST_ORDER_FRAGMENT) {
                DataFilters.selectedOffersListOrder = newSelectedOrder;
                event = new FilterEvent(FilterEvent.LISTINGS_FILTERED);
            } else if (mModel.getCaller() == AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT) {
                DataFilters.selectedDemandsListOrder = newSelectedOrder;
                event = new FilterEvent(FilterEvent.LISTINGS_FILTERED);
            } else if (mModel.getCaller() == AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT) {
                DataFilters.selectedOrdersListOrder = newSelectedOrder;
                event = new FilterEvent(FilterEvent.LISTINGS_FILTERED);
            } else if (mModel.getCaller() == AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT) {
                DataFilters.selectedPostCommentsListOrder = newSelectedOrder;
                event = new FilterEvent(FilterEvent.POST_COMMENTS_FILTERED);
            }
            EventBus.getDefault().post(event);
        } else {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG, false);
        }
    }

    int getCaller() {
        return mModel.getCaller();
    }
}
