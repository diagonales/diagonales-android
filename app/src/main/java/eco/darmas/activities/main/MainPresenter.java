package eco.darmas.activities.main;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eco.darmas.App;
import eco.darmas.AppConstants;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.utils.CommonUtils;

class MainPresenter extends BaseActivityPresenter<BaseActivityModel, BaseActivityView> implements MainContracts.Presenter {

    private final MainModel mModel;
    private final MainView mView;

    MainPresenter(MainModel mModel, MainView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onPostCreate() {
        super.onPostCreate();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //App.getInstance().getDataManager().startDataLoad();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        App.getInstance().getDataManager().disposeSubscriptions();
    }

    public void toggleUpdates() {
        mModel.toggleUpdates();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContentModified(ContentModifiedEvent event) {
        if (event.getDialogTitle() != null || event.getDialogMessage() != null) {
            CommonUtils.showDialog(mView.getContext(), event.getDialogTitle(), event.getDialogMessage());
        }
        switch (event.getEventCode()) {
            case ContentModifiedEvent.POST_CREATED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.POST_MODIFIED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.LISTING_CREATED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.LISTING_MODIFIED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.TRANSACTION_UPDATED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.TRANSFER_SENT:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.RATING_MODIFIED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.PROFILE_IMAGE_UPDATED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.COMMUNITY_EVENT_CREATED:
                mView.showProgress(false);
                break;
            case ContentModifiedEvent.COMMUNITY_EVENT_MODIFIED:
                mView.showProgress(false);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActivityResult(ActivityResultEvent event) {
        //if (event.eventCode == AppConstants.POST_ACTIVITY) {
        if (event.resultCode == AppConstants.RESULT_PROGRESS) {
            mView.showProgress(true);
        }
        //}
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.EVENT_MODE_ENTERED:
                mView.setData();
                mView.showEventModeTransition(true);
                break;
            case FilterEvent.EVENT_MODE_EXIT: {
                mView.setData();
                mView.showEventModeTransition(false);
                break;
            }
        }
    }

    //TODO: check if this is necessary
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.COMMUNITY_EVENT: {
                if (event.isSuccess()) {
                    mView.setData();
                }
            }
        }
    }

    boolean ongoingEvents() {
        return !mModel.getCurrentCommunityEventsList().isEmpty();
    }

    int getOngoingEventType() {
        return mModel.getCurrentCommunityEventsList().get(0).getType();
    }

    int getEventModeType() {
        if (mModel.getEventModeId() >= 0) {
            return mModel.getCommunityEvent(mModel.getEventModeId()).getType();
        } else {
            return -1;
        }
    }

    void enterEventMode() {
        /*if (mModel.getCurrentCommunityEventsList().isEmpty()) {
            mView.setData();
            CommonUtils.showErrorToast("Este evento ha finalizado");
        }*/
        mModel.setEventModeId(mModel.getCurrentCommunityEventsList().get(0).getIdEvent());
        FilterEvent event = new FilterEvent(FilterEvent.EVENT_MODE_ENTERED);
        EventBus.getDefault().post(event);
    }

    public void resetEventMode() {
        mModel.resetSelectedEventModeId();
        FilterEvent event = new FilterEvent(FilterEvent.EVENT_MODE_EXIT);
        EventBus.getDefault().post(event);
    }
}