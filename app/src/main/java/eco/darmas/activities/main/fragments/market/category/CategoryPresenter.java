package eco.darmas.activities.main.fragments.market.category;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataFilters;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Node;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;

class CategoryPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> {

    private CategoryView mView;
    private CategoryModel mModel;

    CategoryPresenter(CategoryModel mModel, CategoryView mView) {
        super(mModel, mView);
        this.mView = mView;
        this.mModel = mModel;
    }

    void onCancelClicked() {
        CommonUtils.dismissFragment(mView.getRootView(), AppConstants.CATEGORY_FRAGMENT_TAG, false);
    }

    void onShowClicked() {
        int newSelectedId = mModel.getTreeItem().getId();
        if (mModel.getSelectedCategory() != newSelectedId) {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.CATEGORY_FRAGMENT_TAG, true);
            DataFilters.selectedCategoryId = newSelectedId;
            FilterEvent event = new FilterEvent(FilterEvent.LISTINGS_FILTERED);
            EventBus.getDefault().post(event);
        } else {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.CATEGORY_FRAGMENT_TAG, false);
        }
    }

    void onResetClicked() {
        if (mModel.getSelectedCategory() != 0) {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.CATEGORY_FRAGMENT_TAG, true);
            DataFilters.selectedCategoryId = 0;
            FilterEvent event = new FilterEvent(FilterEvent.LISTINGS_FILTERED);
            EventBus.getDefault().post(event);
        } else {
            CommonUtils.dismissFragment(mView.getRootView(), AppConstants.CATEGORY_FRAGMENT_TAG, false);
        }
    }

    boolean onChildClicked(int id) {
        mModel.setChildTreeItem(id);
        mView.dataSetChanged();
        return false;
    }

    boolean onGroupClicked(int id) {
        mModel.setParentTreeItem(id);
        mView.dataSetChanged();
        return true;
    }

    Spannable getTree() {
        Spannable textToSpan = new SpannableString(mModel.getCategoryTree());
        int start = mModel.getCategoryTree().lastIndexOf(mModel.getTreeItem().getData().getName());
        int end = mModel.getCategoryTree().length();
        if (start != -1 && end != -1) {
            textToSpan.setSpan(new StyleSpan(Typeface.ITALIC), 0, start, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            textToSpan.setSpan(new ForegroundColorSpan(mView.getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        return textToSpan;
    }

    Node<Category> getTreeItem() {
        return mModel.getTreeItem();
    }

    int getType() {
        return mModel.getType();
    }

    String getOfferListingsText(Category category) {
        int offerListingsCount = 0;
        int eventMode = mModel.getEventModeId();
        try {
            JSONArray offerListings = new JSONArray();
            if (!category.getOfferListings().isEmpty()) {
                offerListings = new JSONArray(category.getOfferListings());
            }
            for (int j = 0; j < offerListings.length(); j++) {
                int listingId = offerListings.getInt(j);
                int eventIndex = mModel.getEventListingIdList().indexOfKey(eventMode);
                if ((eventMode >= 0 && eventIndex < 0) || (eventMode >= 0 && mModel.getEventListingIdList().get(eventMode).indexOfKey(listingId) < 0)) {
                    continue;
                }
                offerListingsCount++;
            }
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }

        // Count event exclusive listings as well
        if (eventMode >= 0 && mModel.getEventListingJsonObjectMap().indexOfKey(eventMode) >= 0) {
            try {
                for (int i = 0; i < mModel.getEventListingJsonObjectMap().get(eventMode).size(); i++) {
                    JSONObject jsonObject = mModel.getEventListingJsonObjectMap().get(eventMode).valueAt(i);
                    if (mModel.getListingFromId(jsonObject.getInt("id_listing")) != null && mModel.getListingFromId(jsonObject.getInt("id_listing")).getIdEvent() == eventMode) {
                        offerListingsCount++;
                    }
                }
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
        return "(" + offerListingsCount + ")";
    }

    String getDemandListingsText(Category category) {
        int demandListingsCount = 0;
        int eventMode = mModel.getEventModeId();
        try {
            JSONArray demandListings = new JSONArray();
            if (!category.getDemandListings().isEmpty()) {
                demandListings = new JSONArray(category.getDemandListings());
            }
            for (int j = 0; j < demandListings.length(); j++) {
                int listingId = demandListings.getInt(j);
                int eventIndex = mModel.getEventListingIdList().indexOfKey(eventMode);
                if ((eventMode >= 0 && eventIndex < 0) || (eventMode >= 0 && mModel.getEventListingIdList().get(eventMode).indexOfKey(listingId) < 0)) {
                    continue;
                }
                demandListingsCount++;
            }
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }

        // Count event exclusive listings
        if (eventMode >= 0 && mModel.getEventListingJsonObjectMap().indexOfKey(eventMode) >= 0) {
            try {
                for (int i = 0; i < mModel.getEventListingJsonObjectMap().get(eventMode).size(); i++) {
                    JSONObject jsonObject = mModel.getEventListingJsonObjectMap().get(eventMode).valueAt(i);
                    if (mModel.getListingFromId(jsonObject.getInt("id_listing")) != null && mModel.getListingFromId(jsonObject.getInt("id_listing")).getIdEvent() == eventMode) {
                        demandListingsCount++;
                    }
                }
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
        return "(" + demandListingsCount + ")";
    }

    String getTransactionsCountText(Category category) {
        int count = 0;
        for (_Transaction transaction : mModel.getLoggedUserOrdersList()) {
            int eventMode = mModel.getEventModeId();
            if (eventMode != transaction.getIdEvent()) {
                continue;
            }
            if (!DataFilters.showEnded && transaction.getStatus() == _Transaction.Status.RECEIVED) {
                continue;
            }
            for (int j = 0; j < mModel.getCategoryNodeTree(category.getIdCategory()).size(); j++) {
                Node node = mModel.getCategoryNodeTree(category.getIdCategory()).get(j);
                if (node.getId() == transaction.getIdCategory()) {
                    count++;
                    break;
                }
            }
        }
        return "(" + count + ")";
    }
}