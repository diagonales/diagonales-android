package eco.darmas.activities.main.fragments.comunity.create.post;

interface CreatePostContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}