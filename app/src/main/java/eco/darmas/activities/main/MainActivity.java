package eco.darmas.activities.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.data.DataManager;
import eco.darmas.utils.CommonUtils;

public class MainActivity extends BaseActivity implements MainContracts.Activity {

    public MainView mView;
    public MainPresenter mPresenter;

    @Override
    protected MainModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new MainModel(dataManager, savedInstanceState);
    }

    @Override
    protected MainPresenter createPresenter(BaseActivityModel model, BaseActivityView view) {
        mPresenter = new MainPresenter((MainModel) model, (MainView) view);
        return mPresenter;
    }

    @Override
    protected MainView createView(Context context) {
        mView = new MainView((MainActivity) context);
        return mView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppData.INSTANCE.getLoggedUser() == null) {
            AppNav.navigateToActivity(AppConstants.LOGIN_ACTIVITY, MainActivity.this);
            finish();
        }
        /*if (AppData.INSTANCE.getLoggedUser() == null) {
            Object[] parcelables = new Object[]{getIntent().getIntExtra("id_target", 0)};
            AppNav.navigateToActivity(AppConstants.LOGIN_ACTIVITY, MainActivity.this, parcelables);
            finish();
        } else if (getIntent().getIntExtra("id_target", 0) != 0) {
            User contact = mPresenter.getUser(getIntent().getIntExtra("id_target", 0));
            Object[] parcelables = new Object[]{contact};
            AppNav.navigateToActivity(AppConstants.USER_MESSAGES_ACTIVITY, MainActivity.this, parcelables);
        }*/
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (mView.dismissPermissionRationale() == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            CommonUtils.showInfoToast("Presiona nuevamente atras para salir...");
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
    }

    @Override
    public void showProgress(boolean show) {
        mView.showProgress(show);
    }
}