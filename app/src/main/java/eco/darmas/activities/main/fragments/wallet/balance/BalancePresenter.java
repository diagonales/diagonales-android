package eco.darmas.activities.main.fragments.wallet.balance;

import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class BalancePresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements BalanceContracts.Presenter {

    private final BalanceModel mModel;
    private final BalanceView mView;

    BalancePresenter(BalanceModel mModel, BalanceView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData() {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::dataSetChanged)
                //.doAfterTerminate(mView::stopRefreshAnimation)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.TRANSACTIONS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.EVENT_MODE_ENTERED: {
                updateData();
            }
            case FilterEvent.EVENT_MODE_EXIT: {
                updateData();
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    void executeTransaction(final int id_receiver, final String concept, final double amount, final double contribution, final String note, final int eventModeId) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }

        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.TRANSFER_SENT);
        _Transaction transaction = new _Transaction(0,
                mModel.getLoggedUser().getIdUser(),
                id_receiver,
                0,
                concept,
                amount,
                DateUtils.getTimeStamp(),
                _Transaction.Status.RECEIVED,
                _Transaction.Type.TRANSFER,
                0,
                null,
                null,
                null,
                null,
                0,
                0,
                note,
                1,
                eventModeId,
                -1,
                0,
                "",
                contribution);
        mModel.commitTransfer(transaction, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            transaction.setIdTransaction((int) responseJSON.get("id"));
                            mModel.writeTransaction(transaction);
                            event.setSuccess(true);
                            event.setDialogTitle("Darmas enviados!");
                            event.setDialogMessage("Los fondos fueron enviados correctamente");
                        } else {
                            event.setDialogTitle("Los fondos no pudieron ser enviados");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        event.setDialogTitle("Los fondos no pudieron ser enviados");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.e(t, null);
                event.setDialogTitle("Los fondos no pudieron ser enviados");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, los fondos no fueron enviados. Por favor vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    List<CommonUtils.StringWithTag> getNameList() {
        return mModel.getNameList();
    }

    List<_Transaction> getTransactionList() {
        return mModel.getTransactionList();
    }

    boolean onTransactionItemClick(int position) {
        // TODO: write this
        return true;
    }

    void onQrClicked() {
        CommonUtils.showToast("Función próxima a implementar");
    }

    double getCounterPartMaxAvailableIncome(int userId) {
        return mModel.getCounterPartMaxAvailableIncome(userId);
    }

    boolean hasReserveFunds() {
        for (_Transaction transaction : mModel.getLoggedUserOrdersList()) {
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdSender() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.REQUESTED || transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    return true;
                }
            } else if (mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    return true;
                }
            }
        }
        return false;
    }

    String getReserveFunds() {
        double reserveFunds = 0;
        for (_Transaction transaction : mModel.getLoggedUserOrdersList()) {
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdSender() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.REQUESTED || transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    reserveFunds += transaction.getAmount();
                }
            }
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    reserveFunds += transaction.getAmount();
                }
            }
        }
        return AppConstants.CURRENCY_FORMAT.format(reserveFunds);
    }

    boolean hasIncomingFunds() {
        for (_Transaction transaction : mModel.getLoggedUserOrdersList()) {
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    return true;
                }
            } else if (mModel.getLoggedUser().getIdUser() == transaction.getIdSender() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    return true;
                }
            }
        }
        return false;
    }

    String getIncomingFunds() {
        double incomingFunds = 0;
        for (_Transaction transaction : mModel.getLoggedUserOrdersList()) {
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdReceiver() && transaction.getType() == _Transaction.Type.OFFER && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    incomingFunds += transaction.getAmount();
                }
            }
            if (mModel.getLoggedUser().getIdUser() == transaction.getIdSender() && transaction.getType() == _Transaction.Type.DEMAND && transaction.getAmount() > 0) {
                if (transaction.getStatus() == _Transaction.Status.ACCEPTED || transaction.getStatus() == _Transaction.Status.DELIVERED) {
                    incomingFunds += transaction.getAmount();
                }
            }
        }
        return AppConstants.CURRENCY_FORMAT.format(incomingFunds);
    }

    void onConceptClicked(Listing listing) {
        if (listing == null) {
            CommonUtils.showInfoToast("Este listado no esta disponible");
            return;
        }
        Object[] parcelables = new Object[]{listing};
        AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }

    void onConceptClicked(CommunityEvent communityEvent) {
        if (communityEvent == null) {
            CommonUtils.showInfoToast("Este evento no esta disponible");
            return;
        }
        Object[] parcelables = new Object[]{communityEvent};
        CommonUtils.showNotImplementedToast();
        //AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }

    Listing getListing(int listingId) {
        return mModel.getListing(listingId);
    }

    CommunityEvent getCommunityEvent(int eventId) {
        return mModel.getCommunityEvent(eventId);
    }

    SparseArray<Inscription> getEventInscriptions(int eventId) {
        return mModel.getEventInscriptions(eventId);
    }

    int getEventType(int eventModeId) {
        return mModel.getCommunityEvent(eventModeId).getType();
    }

    String getFundsText() {
        String sign = "";
        if (AppData.INSTANCE.getLoggedUser().getFunds() > 0) {
            sign = "+";
        }
        return sign + String.valueOf(AppConstants.CURRENCY_FORMAT.format(AppData.INSTANCE.getLoggedUser().getFunds()));
    }

    String getVol() {
        //TODO: this should take so much resources... possible paging issue
        double vol = 0;
        for (int i = 0; i < mModel.getDataManager().getTransactionMap().size(); i++) {
            _Transaction transaction = mModel.getDataManager().getTransactionMap().valueAt(i);
            if (transaction.getIdSender() != AppData.INSTANCE.getLoggedUser().getIdUser() && transaction.getIdReceiver() != AppData.INSTANCE.getLoggedUser().getIdUser()) {
                continue;
            }
            if (transaction.getStatus() != _Transaction.Status.RECEIVED) {
                continue;
            }
            vol += transaction.getAmount();
        }
        return AppConstants.CURRENCY_FORMAT.format(vol);
    }
}