package eco.darmas.activities.main.fragments.wallet.transactions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.CommonUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;

class TransactionsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements TransactionsContracts.Presenter{

    private final TransactionsModel mModel;
    private final TransactionsView mView;

    TransactionsPresenter(TransactionsModel mModel, TransactionsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData() {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::dataSetChanged)
                .doAfterTerminate(mView::stopRefreshAnimation)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.TRANSACTIONS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.EVENT_MODE_ENTERED: {
                updateData();
            }
            case FilterEvent.EVENT_MODE_EXIT: {
                updateData();
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    List<CommonUtils.StringWithTag> getNameList() {
        return mModel.getNameList();
    }

    List<_Transaction> getTransactionList() {
        return mModel.getTransactionList();
    }

    boolean onTransactionItemClick(int position) {
        // TODO: write this
        return true;
    }

    Listing getListing(int listingId) {
        return mModel.getListing(listingId);
    }

    void onConceptClicked(Listing listing) {
        if (listing == null) {
            CommonUtils.showInfoToast("Este listado no esta disponible");
            return;
        }
        Object[] parcelables = new Object[]{listing};
        AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }

    void onConceptClicked(CommunityEvent communityEvent) {
        if (communityEvent == null) {
            CommonUtils.showInfoToast("Este evento no esta disponible");
            return;
        }
        Object[] parcelables = new Object[]{communityEvent};
        CommonUtils.showNotImplementedToast();
        //AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }

    CommunityEvent getCommunityEvent(int eventId) {
        return mModel.getCommunityEvent(eventId);
    }

    void refreshItems() {
        mModel.getDataManager().refreshUsers(true);
        mModel.getDataManager().refreshTransactions(true);
    }
}
