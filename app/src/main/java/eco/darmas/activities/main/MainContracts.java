package eco.darmas.activities.main;

interface MainContracts {

    interface Activity {
    }

    interface View {

        void requestReadContacts();

        void requestSaveImage();

        void requestSendSMS();
    }

    interface Presenter {
    }

    interface Model {
    }
}
