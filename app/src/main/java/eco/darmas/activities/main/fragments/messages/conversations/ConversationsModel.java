package eco.darmas.activities.main.fragments.messages.conversations;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Conversation;
import eco.darmas.pojos.Message;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class ConversationsModel extends BaseFragmentModel implements ConversationsContracts.Model {

    private List<Conversation> conversationList;

    ConversationsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadData().subscribe();
    }

    Single loadData() {
        return Observable.create((ObservableOnSubscribe<Conversation>) e -> {
            for (int i = getDataManager().getMessagesMap().size() - 1; i > -1; i--) {
                List<Message> messageList = getDataManager().getMessagesMap().valueAt(i);
                Message lastMessage = messageList.get(messageList.size() - 1);
                int contactId;
                if (getLoggedUser().getIdUser() == lastMessage.getIdSender()) {
                    contactId = lastMessage.getIdReceiver();
                } else {
                    contactId = lastMessage.getIdSender();
                }
                Conversation conversation = new Conversation(contactId, lastMessage);
                e.onNext(conversation);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(conversations -> conversationList = conversations);
    }

    void hideConversation(int position, Callback<ResponseBody> callback) {
        int senderId = conversationList.get(position).getLastMessage().getIdSender();
        int receiverId = conversationList.get(position).getLastMessage().getIdReceiver();
        App.getInstance().getNetworkService().getCallableAPI().hideConversation(getLoggedUser().getIdUser(), senderId, receiverId).enqueue(callback);
    }

    List<Conversation> getConversationList() {
        return conversationList;
    }

    void writeHideConversation(int position) {
        int contactId;
        if (getLoggedUser().getIdUser() == conversationList.get(position).getLastMessage().getIdSender()) {
            contactId = conversationList.get(position).getLastMessage().getIdReceiver();
        } else {
            contactId = conversationList.get(position).getLastMessage().getIdSender();
        }
        List<Message> modifiedMessageList = new ArrayList<>();
        for (Message message : getDataManager().getBoxStore().boxFor(Message.class).getAll()) {
            if (contactId == message.getIdReceiver()) {
                message.setSenderDeleted(1);
                modifiedMessageList.add(message);
            } else if (contactId == message.getIdSender()) {
                message.setReceiverDeleted(1);
                modifiedMessageList.add(message);
            }
        }
        getDataManager().getBoxStore().boxFor(Message.class).put(modifiedMessageList);
    }
}