package eco.darmas.activities.main.fragments.market.create;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Listing;
import eco.darmas.utils.CommonUtils;

class CreateListingView extends BaseFragmentView {

    private MainActivity mContext;
    private CreateListingPresenter mPresenter;
    private Unbinder unBinder;
    private int selectedShippingMethod = -1;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_title)
    EditText et_title;
    @BindView(R.id.et_text)
    EditText et_text;
    @BindView(R.id.et_price_min)
    EditText et_price_min;
    @BindView(R.id.et_price_max)
    EditText et_price_max;
    @BindView(R.id.sw_ranged)
    Switch sw_range;
    @BindView(R.id.tv_category)
    TextView tv_category;
    @BindView(R.id.cv_category)
    CardView cv_category;
    @BindView(R.id.iv_image)
    ImageView iv_image;
    @BindView(R.id.et_stock)
    EditText et_stock;
    @BindView(R.id.sw_max_stock)
    Switch sw_max_stock;
    @BindView(R.id.et_max_stock)
    EditText et_max_stock;
    @BindView(R.id.tv_divider)
    TextView tv_divider;

    @BindView(R.id.cb_pickup)
    CheckBox cb_pickup;
    @BindView(R.id.iv_pickup)
    ImageView iv_pickup;
    @BindView(R.id.li_delivery)
    LinearLayout li_delivery;
    @BindView(R.id.cb_delivery)
    CheckBox cb_delivery;
    @BindView(R.id.li_digital)
    LinearLayout li_digital;
    @BindView(R.id.cb_digital)
    CheckBox cb_digital;
    @BindView(R.id.li_delivery_price)
    LinearLayout li_delivery_price;
    @BindView(R.id.li_address)
    LinearLayout li_address;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_delivery_price)
    EditText et_delivery_price;

    @BindView(R.id.li_event_info)
    LinearLayout li_event_info;
    @BindView(R.id.tv_event_info)
    TextView tv_event_info;

    public CreateListingView(Context context) {
        super(context);
        this.mContext = (MainActivity) context;
    }

    @Override
    protected View inflate(@NonNull LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.create_listing, container, false);
        ViewGroup layout = view.findViewById(R.id.li_content2);
        LayoutTransition layoutTransition = layout.getLayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (CreateListingPresenter) presenter;
        if (mPresenter.getEventModeId() >= 0) {
            li_event_info.setVisibility(VISIBLE);
            tv_event_info.setText(mPresenter.getEventInfoText());

            selectedShippingMethod = Listing.ShippingMethod.PICKUP;
            iv_pickup.setImageResource(R.drawable.ic_stand_big);
            cb_pickup.setText("Evento*");
            cb_pickup.setChecked(true);
            et_address.setText(mPresenter.getPickupData());
            et_address.setMinEms(0);
            et_address.setEnabled(false);
            cb_pickup.setEnabled(false);
            iv_pickup.setEnabled(false);
            li_delivery.setVisibility(GONE);
            li_digital.setVisibility(GONE);
        } else {
            iv_pickup.setImageResource(R.drawable.ic_location_big);
            li_event_info.setVisibility(GONE);
            li_address.setVisibility(GONE);
        }
        if (mPresenter.getType() == Listing.Type.OFFER) {
            tv_title.setText("Agregar Oferta");
            et_price_max.setHint("Precio");
            et_price_min.setVisibility(View.VISIBLE);
            et_price_max.setVisibility(View.INVISIBLE);
            tv_divider.setVisibility(View.INVISIBLE);
        } else {
            tv_title.setText("Agregar Demanda");
            et_price_max.setHint("Precio");
            et_price_max.setVisibility(View.VISIBLE);
            tv_divider.setVisibility(View.GONE);
            et_price_min.setVisibility(View.GONE);
        }
        li_delivery_price.setVisibility(GONE);
        setData();
    }

    @OnCheckedChanged(R.id.sw_ranged)
    void onRangeSwitchCheckedListener(boolean b) {
        if (b) {
            et_price_min.setHint("Precio min.");
            et_price_min.setVisibility(View.VISIBLE);
            tv_divider.setVisibility(View.VISIBLE);
            et_price_max.setHint("Precio max.");
            et_price_max.setVisibility(View.VISIBLE);
        } else {
            if (mPresenter.getType() == Listing.Type.OFFER) {
                et_price_min.setHint("Precio");
                et_price_min.setVisibility(View.VISIBLE);
                et_price_max.setVisibility(View.INVISIBLE);
                tv_divider.setVisibility(View.INVISIBLE);
            } else {
                et_price_max.setHint("Precio");
                et_price_max.setVisibility(View.VISIBLE);
                tv_divider.setVisibility(View.GONE);
                et_price_min.setVisibility(View.GONE);
            }
        }
    }

    @OnCheckedChanged(R.id.sw_max_stock)
    void onMaxStockSwitchCheckedListener(boolean b) {
        if (b) {
            et_max_stock.setVisibility(View.VISIBLE);
        } else {
            et_max_stock.setVisibility(View.INVISIBLE);
        }
    }

    @OnCheckedChanged({R.id.cb_pickup, R.id.cb_delivery, R.id.cb_digital})
    void onDeliveryMethodCheckedListener(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.cb_pickup:
                if (b) {
                    li_address.setVisibility(VISIBLE);
                    if (selectedShippingMethod == Listing.ShippingMethod.DIGITAL) {
                        cb_digital.setChecked(false);
                        selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                    } else {
                        if (cb_delivery.isChecked()) {
                            selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                        } else {
                            selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                        }
                    }
                } else {
                    li_address.setVisibility(GONE);
                    if (cb_delivery.isChecked()) {
                        selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                    } else {
                        selectedShippingMethod = -1;
                    }
                }
                break;
            case R.id.cb_delivery:
                if (b) {
                    li_delivery_price.setVisibility(VISIBLE);
                    if (selectedShippingMethod == Listing.ShippingMethod.DIGITAL) {
                        cb_digital.setChecked(false);
                        selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                    } else {
                        if (cb_pickup.isChecked()) {
                            selectedShippingMethod = Listing.ShippingMethod.PICKUP_AND_DELIVERY;
                        } else {
                            selectedShippingMethod = Listing.ShippingMethod.DELIVERY;
                        }
                    }
                } else {
                    li_delivery_price.setVisibility(GONE);
                    if (cb_pickup.isChecked()) {
                        selectedShippingMethod = Listing.ShippingMethod.PICKUP;
                    } else {
                        selectedShippingMethod = -1;
                    }
                }
                break;
            case R.id.cb_digital:
                if (b) {
                    if (selectedShippingMethod == Listing.ShippingMethod.PICKUP) {
                        cb_pickup.setChecked(false);
                    } else if (selectedShippingMethod == Listing.ShippingMethod.PICKUP_AND_DELIVERY) {
                        cb_pickup.setChecked(false);
                        cb_delivery.setChecked(false);
                    } else if (selectedShippingMethod == Listing.ShippingMethod.DELIVERY) {
                        cb_delivery.setChecked(false);
                    }
                    selectedShippingMethod = Listing.ShippingMethod.DIGITAL;
                } else {
                    selectedShippingMethod = -1;
                }
                break;
        }
    }

    @OnClick({R.id.iv_pickup, R.id.iv_delivery, R.id.iv_digital})
    void onDeliveryMethodImageClickListener(View view) {
        switch (view.getId()) {
            case R.id.iv_pickup:
                cb_pickup.setChecked(!cb_pickup.isChecked());
                break;
            case R.id.iv_delivery:
                cb_delivery.setChecked(!cb_delivery.isChecked());
                break;
            case R.id.iv_digital:
                cb_digital.setChecked(!cb_digital.isChecked());
        }
    }

    @OnClick(R.id.cv_category)
    void onCategoryClickListener() {
        mPresenter.onCategoryClicked();
    }

    @OnClick(R.id.iv_image)
    void onImageClickListener() {
        if (!mContext.getPermissionPresenter().checkGrantedWriteExternalStoragePermission()) {
            mContext.getPermissionPresenter().requestWriteExternalStoragePermissionAfterRationale();
            return;
        }

        LayoutInflater factory = LayoutInflater.from(mContext);
        final View view = factory.inflate(R.layout.picture_source_dialog, null);

        CustomDialog picture_source_dialog = new CustomDialog(mContext);
        picture_source_dialog.setView(view);

        final ImageView iv_camera = view.findViewById(R.id.iv_camera);
        final ImageView iv_file = view.findViewById(R.id.iv_file);

        iv_camera.setOnClickListener(view1 -> {
            mPresenter.onAddCameraImageClicked();
            picture_source_dialog.dismiss();
        });
        iv_file.setOnClickListener(view12 -> {
            mPresenter.onAddFileImageClicked();
            picture_source_dialog.dismiss();
        });
        picture_source_dialog.show();
    }

    @OnClick(R.id.bt_publish)
    void onPublishClickListener() {
        final String title = et_title.getText().toString().trim();
        final String text = et_text.getText().toString().trim();
        final String priceMin = et_price_min.getText().toString();
        final String priceMax = et_price_max.getText().toString();
        final String stock = et_stock.getText().toString();
        final String address = et_address.getText().toString().trim();
        final String shippingPrice = et_delivery_price.getText().toString().trim();
        final String perBuyMaxStock;
        if (sw_max_stock.isChecked()) {
            perBuyMaxStock = et_max_stock.getText().toString();
        } else {
            perBuyMaxStock = "-1";
        }
        mPresenter.onPublishClicked(title, text, priceMin, priceMax, sw_range.isChecked(), stock, selectedShippingMethod, address, shippingPrice, perBuyMaxStock);
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.CREATE_LISTING_FRAGMENT_TAG, false);
    }

    void setData() {
        if (!mPresenter.isSelectedCategoryRoot()) {
            cv_category.setCardBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            tv_category.setText(mPresenter.getSelectedCategoryName());
        } else {
            cv_category.setCardBackgroundColor(getResources().getColor(R.color.orderAwaiting));
            tv_category.setText("Seleccionar...");
        }
    }

    void loadPostImage(Bitmap bitmap) {
        iv_image.setImageBitmap(bitmap);
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}