package eco.darmas.activities.main.fragments.market.create;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.NetworkUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CreateListingPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> {

    private CreateListingModel mModel;
    private CreateListingView mView;

    CreateListingPresenter(CreateListingModel mModel, CreateListingView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        mView.initialize(this);
        EventBus.getDefault().register(this);
    }

    void onPublishClicked(String title, String text, String priceMinText, String priceMaxText, boolean ranged, String stockText, int shippingMethod, String address, String shippingPriceText, String perBuyMaxStockText) {
        if (mModel.getSelectedCategoryNode().getId() > 0) {
            if (!title.equals("")) {
                if (!text.equals("")) {
                    double priceMin = 0;
                    double priceMax = 0;
                    if (mModel.getType() == Listing.Type.OFFER) {
                        if (!priceMinText.equals("")) {
                            priceMin = Double.parseDouble(priceMinText);
                        } else {
                            CommonUtils.showInfoToast("Debes ingresar un precio");
                            return;
                        }
                    } else {
                        if (!priceMaxText.equals("")) {
                            priceMax = Double.parseDouble(priceMaxText);
                        } else {
                            CommonUtils.showInfoToast("Debes ingresar un precio");
                            return;
                        }
                    }
                    int stock;
                    int perBuyMaxStock;
                    if (!stockText.isEmpty()) {
                        stock = Integer.valueOf(stockText);
                        perBuyMaxStock = Integer.valueOf(perBuyMaxStockText);
                        if (perBuyMaxStock == 0) {
                            CommonUtils.showInfoToast("El limite de unidades por orden debe ser mayor a 0");
                            return;
                        }
                    } else {
                        CommonUtils.showInfoToast("Debes ingresar la cantidad de unidades disponibles");
                        return;
                    }

                    double shippingPrice = 0;
                    switch (shippingMethod) {
                        case Listing.ShippingMethod.PICKUP:
                            if (address.isEmpty()) {
                                CommonUtils.showInfoToast("Debes ingresar la dirección de referencia");
                                return;
                            }
                            break;
                        case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                            if (address.isEmpty()) {
                                CommonUtils.showInfoToast("Debes ingresar la dirección de referencia");
                                return;
                            }
                            if (shippingPriceText.isEmpty()) {
                                CommonUtils.showInfoToast("Ingresa el precio de envío (0 para envíos sin cargo)");
                                return;
                            }
                            shippingPrice = Double.valueOf(shippingPriceText);
                            break;
                        case Listing.ShippingMethod.DELIVERY:
                            if (shippingPriceText.isEmpty()) {
                                CommonUtils.showInfoToast("Ingresa el precio de envío (0 para envíos sin cargo)");
                                return;
                            }
                            shippingPrice = Double.valueOf(shippingPriceText);
                            break;
                        case Listing.ShippingMethod.DIGITAL:
                            break;
                        default:
                            CommonUtils.showInfoToast("Debes seleccionar al menos un metodo de entrega");
                            return;
                    }

                    if (!ranged) {
                        if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                            createListing(title, text, priceMin, priceMax, 0, stock, shippingMethod, address, shippingPrice, perBuyMaxStock);
                        } else {
                            CommonUtils.showErrorToast("Comprueba tu conección");
                        }
                    } else {
                        if (mModel.getType() == Listing.Type.OFFER) {
                            if (!priceMaxText.equals("")) {
                                priceMax = Double.parseDouble(priceMaxText);
                            } else {
                                CommonUtils.showInfoToast("Debes ingresar un precio maximo");
                                return;
                            }
                        } else {
                            if (!priceMinText.equals("")) {
                                priceMin = Double.parseDouble(priceMinText);
                            } else {
                                CommonUtils.showInfoToast("Debes ingresar un precio minimo");
                                return;
                            }
                        }
                        if (priceMax > priceMin) {
                            if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                                createListing(title, text, priceMin, priceMax, 1, stock, shippingMethod, address, shippingPrice, perBuyMaxStock);
                            } else {
                                CommonUtils.showErrorToast("Comprueba tu conección");
                            }
                        } else {
                            CommonUtils.showInfoToast("El precio maximo debe ser mayor que el minimo");
                        }
                    }
                } else {
                    CommonUtils.showInfoToast("Ingresa una descripción de lo que ofreces");
                }
            } else {
                CommonUtils.showInfoToast("Debes escribir un titulo");
            }
        } else {
            CommonUtils.showInfoToast("Debes seleccionar una categoría");
        }
    }

    private void createListing(String title, String text, double priceMin, double priceMax, int ranged, int stock, int shippingMethod, String address, double shippingPrice, int perBuyMaxStock) {
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.LISTING_CREATED);
        mModel.commitCreateListing(title, text, priceMin, priceMax, ranged, stock, shippingMethod, address, shippingPrice, perBuyMaxStock, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            int id = (int) responseJSON.get("id");
                            String image = (String) responseJSON.get("image");
                            Listing listing = new Listing(id,
                                    mModel.getLoggedUser().getIdUser(),
                                    mModel.getEventModeId(),
                                    title, DateUtils.getTimeStamp(),
                                    "1",
                                    text,
                                    mModel.getType(),
                                    "",
                                    mModel.getSelectedCategoryNode().getId(),
                                    image,
                                    address,
                                    priceMin,
                                    priceMax,
                                    "",
                                    String.valueOf(ranged),
                                    stock,
                                    perBuyMaxStock,
                                    0,
                                    shippingMethod,
                                    shippingPrice);
                            mModel.writeListing(listing);
                            event.setSuccess(true);
                            if (mModel.getType() == Listing.Type.OFFER) {
                                event.setDialogTitle("Tu oferta ha sido creada!");
                            } else {
                                event.setDialogTitle("Tu demanda ha sido creada!");
                            }
                            event.setDialogMessage("Pronto la veras publicada");
                        } else {
                            event.setDialogTitle("El listado no pudo ser publicado debido a un error");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                        event.setDialogTitle("El listado no pudo ser publicado debido a un error");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().postSticky(event);
                        //TODO: check this
                        //capturedImageFile = null;
                        //capturedImageUri = null;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                event.setDialogTitle("Error al crear el listado");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().postSticky(event);
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissFragment(mView, AppConstants.CREATE_LISTING_FRAGMENT_TAG, true);
    }

    int getType() {
        return mModel.getType();
    }

    String getSelectedCategoryName() {
        return mModel.getSelectedCategoryNode().getData().getName();
    }

    boolean isSelectedCategoryRoot() {
        return mModel.getSelectedCategoryNode().isRoot();
    }

    void onCategoryClicked() {
        Object[] arguments = new Object[]{mModel.getType()};
        AppNav.navigateToFragment(AppConstants.CATEGORY_FRAGMENT, (AppCompatActivity) mView.getContext(), arguments);
    }

    void onAddCameraImageClicked() {
        mModel.setTempImageUri(ImageUtils.takePictureFromCamera((MainActivity) mView.getContext(), AppConstants.LISTING_CAMERA_REQUEST, mModel.getListingImageFile()));
    }

    void onAddFileImageClicked() {
        ImageUtils.chooseImageFromFile((MainActivity) mView.getContext(), AppConstants.LISTING_CHOOSE_FILE);
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.LISTINGS_FILTERED: {
                mView.setData();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActivityResult(ActivityResultEvent event) {
        if (event.resultCode != AppConstants.RESULT_OK) {
            return;
        }
        switch (event.eventCode) {
            case AppConstants.LISTING_CAMERA_REQUEST: {
                Picasso.get().load(mModel.getTempImageUri()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_image.getWidth(), mView.iv_image.getHeight()).centerCrop().into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al procesar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setListingImageFile(mModel.getTempImageUri().getPath());
                break;
            }
            case AppConstants.LISTING_CHOOSE_FILE: {
                String path = event.data.getDataString();
                Picasso.get().load(Uri.parse(path)).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_image.getWidth(), mView.iv_image.getHeight()).centerCrop().into(new Target() {

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al procesar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setListingImageFile(path);
            }
        }
    }

    String getEventInfoText() {
        return "Esta publicación solo estará disponible para el evento " + "\"" + mModel.getCommunityEventMap().get(mModel.getEventModeId()).getTitle() + "\"";
    }

    public String getPickupData() {
        if (mModel.getEventModeId() > -1) {
            int eventIndex = mModel.getEventInscriptionMap().indexOfKey(mModel.getEventModeId());
            if (eventIndex >= 0) {
                int stand = mModel.getEventInscriptionMap().valueAt(eventIndex).get(mModel.getLoggedUser().getIdUser(), new Inscription()).getStand();
                if (stand > 0) {
                    return "*Stand #" + stand;
                }
            }
        }
        return "*Sin stand, contactar";
    }
}