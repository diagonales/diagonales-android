package eco.darmas.activities.main.fragments.comunity.announces;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class AnnouncesFragment extends BaseFragment implements AnnouncesContracts.Fragment {

    @Override
    protected AnnouncesModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new AnnouncesModel(dataManager, savedInstanceState);
    }

    @Override
    protected AnnouncesPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new AnnouncesPresenter((AnnouncesModel) model, (AnnouncesView) view);
    }

    @Override
    protected AnnouncesView createView(Context context) {
        return new AnnouncesView(context);
    }
}