package eco.darmas.activities.main;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.BindView;
import butterknife.OnLongClick;
import eco.darmas.App;
import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.fragments.comunity.announces.AnnouncesFragment;
import eco.darmas.activities.main.fragments.comunity.events.EventsFragment;
import eco.darmas.activities.main.fragments.comunity.info.InfoFragment;
import eco.darmas.activities.main.fragments.comunity.posts.PostsFragment;
import eco.darmas.activities.main.fragments.market.demands.DemandsFragment;
import eco.darmas.activities.main.fragments.market.offers.OffersFragment;
import eco.darmas.activities.main.fragments.market.orders.OrdersFragment;
import eco.darmas.activities.main.fragments.messages.conversations.ConversationsFragment;
import eco.darmas.activities.main.fragments.messages.notifications.NotificationsFragment;
import eco.darmas.activities.main.fragments.profile.account.AccountFragment;
import eco.darmas.activities.main.fragments.profile.ratings.RatingsFragment;
import eco.darmas.activities.main.fragments.wallet.balance.BalanceFragment;
import eco.darmas.activities.main.fragments.wallet.transactions.TransactionsFragment;
import eco.darmas.auxiliary.Action;
import eco.darmas.auxiliary.RegisterForPushNotificationsAsync;
import eco.darmas.base.activity.BaseActivityPresenter;
import eco.darmas.base.activity.BaseActivityView;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.User;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.KeyboardUtils;
import eco.darmas.utils.NetworkUtils;
import me.pushy.sdk.Pushy;

@SuppressLint("ViewConstructor")
class MainView extends BaseActivityView implements MainContracts.View {

    private MainActivity mContext;
    private MainPresenter mPresenter;

    @BindView(R.id.cv_test)
    CardView cv_test;
    @BindView(R.id.app_bar_layout)
    AppBarLayout app_bar_layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.iv_event)
    ImageView iv_event;
    @BindView(R.id.tv_event_type)
    TextView tv_event_type;
    @BindView(R.id.cv_event)
    CardView cv_event;
    @BindView(R.id.iv_event_status)
    ImageView iv_event_status;
    @BindView(R.id.iv_bg_normal)
    ImageView iv_bg_normal;
    @BindView(R.id.iv_bg_fair)
    ImageView iv_bg_fair;

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;

    @BindView(R.id.li_tabs)
    LinearLayout li_tabs;

    @BindView(R.id.community_pager)
    ViewPager community_pager;
    PagerAdapter communityPagerAdapter;
    @BindView(R.id.community_tabs)
    TabLayout community_tabs;

    @BindView(R.id.wallet_pager)
    ViewPager wallet_pager;
    PagerAdapter walletPagerAdapter;
    @BindView(R.id.wallet_tabs)
    TabLayout wallet_tabs;

    @BindView(R.id.market_pager)
    ViewPager market_pager;
    PagerAdapter marketPagerAdapter;
    @BindView(R.id.market_tabs)
    TabLayout market_tabs;

    @BindView(R.id.messages_pager)
    ViewPager messages_pager;
    PagerAdapter messagesPagerAdapter;
    @BindView(R.id.messages_tabs)
    TabLayout messages_tabs;

    @BindView(R.id.profile_pager)
    ViewPager profile_pager;
    PagerAdapter profilePagerAdapter;
    @BindView(R.id.profile_tabs)
    TabLayout profile_tabs;

    private ProgressDialog progress;
    //private ProgressBar progressBar;
    private boolean started;

    MainView(MainActivity context) {
        super(context);
        this.mContext = context;
        initStatusBar();
        inflate(mContext, R.layout.main_activity, this);
    }

    @Override
    public void initialize(BaseActivityPresenter presenter) {
        this.mPresenter = (MainPresenter) presenter;

        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.lobster_two_bold_italic);
        title_text.setTypeface(typeface, 1);
        cv_test.setVisibility(mPresenter.isTestMode() ? VISIBLE : GONE);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.primaryTextColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        refreshButton.setOnClickListener(view -> {
            if (NetworkUtils.isNetworkConnected(mContext)) {
                //showProgress(true);
                //TODO: remove this
                mPresenter.toggleUpdates();
                //updateAll(true);
            } else {
                CommonUtils.showWarningToast("Comprueba tu conección");
            }
        });

        app_bar_layout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {

            float alpha = 1.0f - Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange());
            if (alpha == 0) {
                li_tabs.setVisibility(GONE);
            } else {
                li_tabs.setVisibility(VISIBLE);
                li_tabs.setAlpha(alpha);
            }

            /*float scale = 1 - (0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())) * 0.2f;
            rl_user_data.setScaleX(scale);
            rl_user_data.setScaleY(scale);
            rl_user_data_collapsed.setAlpha(0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange()));
            float scale2 = 1 - (1.0f - Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())) * 0.2f;
            rl_user_data_collapsed.setScaleX(scale2);
            rl_user_data_collapsed.setScaleY(scale2);*/

            /*float sizeInDp = 68 * (1 - (0.0f + Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange())));
            float scale3 = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (sizeInDp * scale3 + 0.5f);
            cl_footer.setPadding(0, 0, 0, messageBoxHeight + dpAsPixels);*/
        });

        community_tabs.setupWithViewPager(community_pager);
        community_pager.setOffscreenPageLimit(3);
        communityPagerAdapter = new CommunitySlidePagerAdapter(mContext.getSupportFragmentManager());
        community_pager.setAdapter(communityPagerAdapter);
        community_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    app_bar_layout.setExpanded(true, true);
                }
            }
        });

        wallet_tabs.setupWithViewPager(wallet_pager);
        wallet_pager.setOffscreenPageLimit(2);
        walletPagerAdapter = new WalletSlidePagerAdapter(mContext.getSupportFragmentManager());
        wallet_pager.setAdapter(walletPagerAdapter);
        wallet_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    app_bar_layout.setExpanded(true, true);
                }
            }
        });

        market_tabs.setupWithViewPager(market_pager);
        market_pager.setOffscreenPageLimit(2);
        marketPagerAdapter = new MarketSlidePagerAdapter(mContext.getSupportFragmentManager());
        market_pager.setAdapter(marketPagerAdapter);
        market_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    app_bar_layout.setExpanded(true, true);
                }
            }
        });

        messages_tabs.setupWithViewPager(messages_pager);
        messages_pager.setOffscreenPageLimit(2);
        messagesPagerAdapter = new MessagesSlidePagerAdapter(mContext.getSupportFragmentManager());
        messages_pager.setAdapter(messagesPagerAdapter);
        messages_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    app_bar_layout.setExpanded(true, true);
                }
            }
        });

        profile_tabs.setupWithViewPager(profile_pager);
        profile_pager.setOffscreenPageLimit(2);
        profilePagerAdapter = new ProfileSlidePagerAdapter(mContext.getSupportFragmentManager());
        profile_pager.setAdapter(profilePagerAdapter);
        profile_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), getRootView().findFocus());
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    app_bar_layout.setExpanded(true, true);
                }
            }
        });

        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Comunidad", R.drawable.ic_community);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Billetera", R.drawable.ic_account_balance_wallet_white);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Mercado", R.drawable.ic_swap_horiz_white);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Mensajes", R.drawable.ic_textsms_white);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("Usuario", R.drawable.ic_account_image);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Change colors
        bottomNavigation.setAccentColor(getResources().getColor(R.color.primaryTextColor));
        bottomNavigation.setInactiveColor(Color.parseColor("#64ffffff"));

        // Set listeners
        //TODO: fire event to the fragments to go up on each list on reselect
        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            app_bar_layout.setExpanded(true, true);
            switch (position) {
                case 0:
                    wallet_tabs.setVisibility(View.GONE);
                    wallet_pager.setVisibility(View.GONE);
                    market_tabs.setVisibility(View.GONE);
                    market_pager.setVisibility(View.GONE);
                    messages_tabs.setVisibility(View.GONE);
                    messages_pager.setVisibility(View.GONE);
                    profile_tabs.setVisibility(View.GONE);
                    profile_pager.setVisibility(View.GONE);
                    community_tabs.setVisibility(View.VISIBLE);
                    community_pager.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    wallet_tabs.setVisibility(View.VISIBLE);
                    wallet_pager.setVisibility(View.VISIBLE);
                    market_tabs.setVisibility(View.GONE);
                    market_pager.setVisibility(View.GONE);
                    messages_tabs.setVisibility(View.GONE);
                    messages_pager.setVisibility(View.GONE);
                    profile_tabs.setVisibility(View.GONE);
                    profile_pager.setVisibility(View.GONE);
                    community_tabs.setVisibility(View.GONE);
                    community_pager.setVisibility(View.GONE);
                    // Remove the badge when you're done with it.
                    //wallet_tab.removeBadge();
                    break;
                case 2:
                    wallet_tabs.setVisibility(View.GONE);
                    wallet_pager.setVisibility(View.GONE);
                    market_tabs.setVisibility(View.VISIBLE);
                    market_pager.setVisibility(View.VISIBLE);
                    messages_tabs.setVisibility(View.GONE);
                    messages_pager.setVisibility(View.GONE);
                    profile_tabs.setVisibility(View.GONE);
                    profile_pager.setVisibility(View.GONE);
                    community_tabs.setVisibility(View.GONE);
                    community_pager.setVisibility(View.GONE);
                    break;
                case 3:
                    wallet_tabs.setVisibility(View.GONE);
                    wallet_pager.setVisibility(View.GONE);
                    market_tabs.setVisibility(View.GONE);
                    market_pager.setVisibility(View.GONE);
                    messages_tabs.setVisibility(View.VISIBLE);
                    messages_pager.setVisibility(View.VISIBLE);
                    profile_tabs.setVisibility(View.GONE);
                    profile_pager.setVisibility(View.GONE);
                    community_tabs.setVisibility(View.GONE);
                    community_pager.setVisibility(View.GONE);
                    break;
                case 4:
                    wallet_tabs.setVisibility(View.GONE);
                    wallet_pager.setVisibility(View.GONE);
                    market_tabs.setVisibility(View.GONE);
                    market_pager.setVisibility(View.GONE);
                    messages_tabs.setVisibility(View.GONE);
                    messages_pager.setVisibility(View.GONE);
                    profile_tabs.setVisibility(View.VISIBLE);
                    profile_pager.setVisibility(View.VISIBLE);
                    community_tabs.setVisibility(View.GONE);
                    community_pager.setVisibility(View.GONE);
                    // workaround for tab selected from start
                    if (!started) {
                        started = true;
                        selectProfileTab();
                    }
                    break;
            }
            return true;
        });
        bottomNavigation.setOnNavigationPositionListener(y -> {
            // Manage the new y position
        });

        requestSaveImage();
        switch (mPresenter.getEventModeType()) {
            case CommunityEvent.Type.FAIR:
                bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.tertiaryColor));
                iv_bg_fair.setVisibility(VISIBLE);
                //li_fair_decoration.setVisibility(VISIBLE);
                iv_bg_normal.setVisibility(INVISIBLE);
                break;
            case CommunityEvent.Type.MEETING:
                bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.tertiaryColor));
                iv_bg_fair.setVisibility(VISIBLE);
                //li_fair_decoration.setVisibility(VISIBLE);
                iv_bg_normal.setVisibility(INVISIBLE);
                break;
            case CommunityEvent.Type.STAFF:
                bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.tertiaryColor));
                iv_bg_fair.setVisibility(VISIBLE);
                //li_fair_decoration.setVisibility(VISIBLE);
                iv_bg_normal.setVisibility(INVISIBLE);
                break;
            case CommunityEvent.Type.PARTY:
                bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.tertiaryColor));
                iv_bg_fair.setVisibility(VISIBLE);
                //li_fair_decoration.setVisibility(VISIBLE);
                iv_bg_normal.setVisibility(INVISIBLE);
                break;
            default:
                bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.primaryColor));
                iv_bg_normal.setVisibility(VISIBLE);
                //li_fair_decoration.setVisibility(INVISIBLE);
                iv_bg_fair.setVisibility(INVISIBLE);
                break;
        }
        setData();
    }

    void setData() {
        if (mPresenter.ongoingEvents()) {
            int eventTypeToShow;
            if (mPresenter.getEventModeType() >= 0) {
                eventTypeToShow = mPresenter.getEventModeType();
                iv_event_status.setImageResource(R.drawable.ic_event_on);
            } else {
                eventTypeToShow = mPresenter.getOngoingEventType();
                iv_event_status.setImageResource(R.drawable.ic_event_off);
            }
            switch (eventTypeToShow) {
                case CommunityEvent.Type.FAIR:
                    tv_event_type.setText("feria");
                    break;
                case CommunityEvent.Type.MEETING:
                    tv_event_type.setText("meeting");
                    break;
                case CommunityEvent.Type.STAFF:
                    tv_event_type.setText("asamblea");
                    break;
                case CommunityEvent.Type.PARTY:
                    tv_event_type.setText("fiesta!");
                    break;
                default:
                    break;
            }
            cv_event.setVisibility(VISIBLE);
        } else {
            cv_event.setVisibility(GONE);
        }
    }

    public void selectProfileTab() {
        profile_pager.setCurrentItem(1, false);
        /*TabLayout.Tab currentTab = profile_tabs.getTabAt(1);
        if (currentTab != null) {
            View customView = currentTab.getCustomView();
            if (customView != null) {
                customView.setSelected(true);
            }
            currentTab.select();
        }*/
    }

    @Override
    public void setupActionBar(ActionBar actionBar) {
        actionBar.setCustomView(toolbar);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    public void showProgress(final boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        refreshButton.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
    }

    void showEventModeTransition(boolean show) {
        if (show) {
            Animation animFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
            Animation animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);

            animFadeOut.reset();
            iv_bg_fair.clearAnimation();
            iv_bg_fair.setVisibility(VISIBLE);
            iv_bg_fair.startAnimation(animFadeIn);
            /*li_fair_decoration.clearAnimation();
            li_fair_decoration.setVisibility(VISIBLE);
            li_fair_decoration.startAnimation(animFadeIn);*/

            animFadeIn.reset();
            iv_bg_normal.clearAnimation();
            iv_bg_normal.setVisibility(INVISIBLE);
            iv_bg_normal.startAnimation(animFadeOut);

            ObjectAnimator bottomAnimator = ObjectAnimator.ofInt(bottomNavigation, "backgroundColor", getResources().getColor(R.color.primaryColor), getResources().getColor(R.color.tertiaryColor)).setDuration(1500);
            bottomAnimator.setEvaluator(new ArgbEvaluator());
            bottomAnimator.start();
            bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.tertiaryColor));
        } else {
            Animation animFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
            Animation animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);

            animFadeOut.reset();
            iv_bg_normal.clearAnimation();
            iv_bg_normal.setVisibility(VISIBLE);
            iv_bg_normal.startAnimation(animFadeIn);

            animFadeIn.reset();
            iv_bg_fair.clearAnimation();
            iv_bg_fair.setVisibility(INVISIBLE);
            iv_bg_fair.startAnimation(animFadeOut);
            /*li_fair_decoration.clearAnimation();
            li_fair_decoration.setVisibility(INVISIBLE);
            li_fair_decoration.startAnimation(animFadeOut);*/

            ObjectAnimator bottomAnimator = ObjectAnimator.ofInt(bottomNavigation, "backgroundColor", getResources().getColor(R.color.tertiaryColor), getResources().getColor(R.color.primaryColor)).setDuration(1500);
            bottomAnimator.setEvaluator(new ArgbEvaluator());
            bottomAnimator.start();
            bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.primaryColor));
        }
    }

    @OnLongClick(R.id.cv_event)
    boolean communityEventButtonClickListener() {
        if (!mPresenter.isInEventMode()) {
            mPresenter.enterEventMode();
        } else {
            mPresenter.resetEventMode();
        }
        return true;
    }

    public AHBottomNavigation getBottomBar() {
        return bottomNavigation;
    }

    class CommunitySlidePagerAdapter extends FragmentStatePagerAdapter {

        CommunitySlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new AnnouncesFragment();
                }
                case 1: {
                    return new PostsFragment();
                }
                case 2: {
                    return new EventsFragment();
                }
                case 3: {
                    return new InfoFragment();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Anuncios";
                }
                case 1: {
                    return "Ágora";
                }
                case 2: {
                    return "Eventos";
                }
                case 3: {
                    return "Info";
                }
            }
            return null;
        }
    }

    class WalletSlidePagerAdapter extends FragmentStatePagerAdapter {

        WalletSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new BalanceFragment();
                }
                case 1: {
                    return new TransactionsFragment();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Balance";
                }
                case 1: {
                    return "Comunidad";
                }
            }
            return null;
        }
    }

    class MarketSlidePagerAdapter extends FragmentStatePagerAdapter {

        MarketSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new OffersFragment();
                }
                case 1: {
                    return new DemandsFragment();
                }
                case 2: {
                    return new OrdersFragment();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Ofertas";
                }
                case 1: {
                    return "Demandas";
                }
                case 2: {
                    return "Encargos";
                }
            }
            return null;
        }
    }

    class MessagesSlidePagerAdapter extends FragmentStatePagerAdapter {

        MessagesSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new ConversationsFragment();
                }
                case 1: {
                    return new NotificationsFragment();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Conversaciones";
                }
                case 1: {
                    return "Notificaciones";
                }
            }
            return null;
        }
    }

    class ProfileSlidePagerAdapter extends FragmentStatePagerAdapter {

        ProfileSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new RatingsFragment();
                }
                case 1: {
                    return new AccountFragment();
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Valoraciones";
                }
                case 1: {
                    return "Perfil";
                }
            }
            return null;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (intent.getStringExtra("tab") != null) {
            switch (intent.getStringExtra("tab")) {
                case "transfer": {
                    bottomNavigation.setCurrentItem(1, true);
                    wallet_pager.setCurrentItem(0, true);
                    BalanceFragment balanceFragment = (BalanceFragment) mContext.getSupportFragmentManager().findFragmentByTag(CommonUtils.getPagerFragmentTag(wallet_pager.getId(), 0));
                    if (balanceFragment != null && balanceFragment.isAdded()) {
                        ((RecyclerView) balanceFragment.getView().findViewById(R.id.rv_user_transactions)).smoothScrollToPosition(0);
                    }
                    break;
                }
                case "transaction": {
                    bottomNavigation.setCurrentItem(2, true);
                    market_pager.setCurrentItem(2, true);
                    //ordersFragment.getOrdersListView().setSelection(0);
                    //((RecyclerView) mContext.getSupportFragmentManager().findFragmentByTag(CommonUtils.getPagerFragmentTag(market_pager.getId(), 2)).getView().findViewById(R.id.rv_orders)).smoothScrollToPosition(0);
                    break;
                }
                case "message": {
                    int id_contact = intent.getIntExtra("id_target", 0);
                    User contact = mPresenter.getUser(id_contact);
                    Object[] parcelables = new Object[]{contact};
                    AppNav.navigateToActivity(AppConstants.USER_MESSAGES_ACTIVITY, mContext, parcelables);
                    break;
                }
            }
        }
    }

    //---- Requesting permissions. Fragments will call this functions -----//

    @Override
    public void requestReadContacts() {
        getPermissionPresenter().requestReadContactsPermission();
    }

    @Override
    public void requestSaveImage() {
        getPermissionPresenter().requestWriteExternalStoragePermission();
    }

    @Override
    public void requestSendSMS() {
        getPermissionPresenter().requestSendSMS();
    }


    @Override
    public void permissionAccepted(int action) {
        switch (action) {
            case Action.ACTION_CODE_READ_CONTACTS:
                break;
            case Action.ACTION_CODE_SAVE_IMAGE:
                Pushy.listen(App.getInstance());
                new RegisterForPushNotificationsAsync().execute();
                break;
            case Action.ACTION_CODE_SEND_SMS:
                break;
        }
    }

    @Override
    public void showRationale(int action) {
        switch (action) {
            case Action.ACTION_CODE_SAVE_IMAGE:
                createAndShowPermissionRationale(action, R.string.rationale_save_image_title, R.string.rationale_save_image_subtitle);
                break;
            case Action.ACTION_CODE_READ_CONTACTS:
                break;
            case Action.ACTION_CODE_SEND_SMS:
                break;
        }
    }

    private void initStatusBar() {
        Window window = ((MainActivity) getContext()).getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            SystemBarTintManager tintManager = new SystemBarTintManager((MainActivity) getContext());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getContext(), R.color.transparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}