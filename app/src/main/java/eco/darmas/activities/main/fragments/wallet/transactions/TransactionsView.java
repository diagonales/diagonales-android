package eco.darmas.activities.main.fragments.wallet.transactions;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class TransactionsView extends BaseFragmentView implements TransactionsContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private TransactionsPresenter mPresenter;
    private TransactionsAdapter flexibleTransactionsAdapter;
    private Unbinder unBinder;

    @BindView(R.id.tv_community_transactions_none)
    TextView tv_community_transactions_none;
    @BindView(R.id.rv_community_transactions)
    RecyclerView rv_community_transactions;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    TransactionsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.transactions_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (TransactionsPresenter) presenter;
        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshItems());
        swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor);
        setupAdapters();
    }

    private void setupAdapters() {
        rv_community_transactions.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_announces = new LinearLayoutManager(getContext());
        rv_community_transactions.setLayoutManager(lm_announces);
        rv_community_transactions.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.transaction, 15));

        // Initialize the Adapter
        flexibleTransactionsAdapter = new TransactionsAdapter(getFlexibleTransactionsItemList(mPresenter.getTransactionList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_community_transactions.setAdapter(flexibleTransactionsAdapter);
    }

    void dataSetChanged() {
        flexibleTransactionsAdapter.updateDataSet(getFlexibleTransactionsItemList(mPresenter.getTransactionList()));
    }

    void stopRefreshAnimation() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onTransactionItemClick(position);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_community_transactions.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_community_transactions_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    class TransactionsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        TransactionsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class TransactionItemAdapter extends AbstractFlexibleItem<TransactionItemAdapter.ViewHolder> {

        private _Transaction transaction;

        TransactionItemAdapter(_Transaction transaction) {
            this.transaction = transaction;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof TransactionItemAdapter) {
                _Transaction inItem = ((TransactionItemAdapter) inObject).transaction;
                //return this.transaction.equals(inItem) && mPresenter.getRatings(transaction)[0].equals(mPresenter.getRatings(inItem)[0]);
                return this.transaction.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return transaction.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.transaction;
        }

        @Override
        public TransactionItemAdapter.ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new TransactionItemAdapter.ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, TransactionItemAdapter.ViewHolder viewHolder, int position, List payloads) {
            Listing listing = mPresenter.getListing(transaction.getIdListing());
            CommunityEvent communityEvent = mPresenter.getCommunityEvent(transaction.getIdEvent());

            int transactionType = transaction.getType();

            if (transactionType == _Transaction.Type.TRANSFER) {
                ImageUtils.loadTransferImage(viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(null);
            } else if (transaction.getType() == _Transaction.Type.INSCRIPTION){
                ImageUtils.loadTransactionBackgroundImage(communityEvent, viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(view -> mPresenter.onConceptClicked(communityEvent));
            } else {
                ImageUtils.loadTransactionBackgroundImage(listing, viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(view -> mPresenter.onConceptClicked(listing));
            }

            int paymentSenderId;
            int paymentReceiverId;
            if (transaction.getType() != _Transaction.Type.DEMAND) {
                paymentSenderId = transaction.getIdSender();
                paymentReceiverId = transaction.getIdReceiver();
            } else {
                paymentSenderId = transaction.getIdReceiver();
                paymentReceiverId = transaction.getIdSender();
            }

            String amount = AppConstants.CURRENCY_FORMAT.format(transaction.getAmount());
            Spannable amountSpan;

            String contribution = "";
            Spannable contributionSpan;
            if (transaction.getContribution() > 0) {
                contribution = String.valueOf(transaction.getContribution());
                viewHolder.li_contribution.setVisibility(VISIBLE);
            } else {
                viewHolder.li_contribution.setVisibility(GONE);
            }

            String shipping = "";
            Spannable shippingSpan;
            if (transaction.getShippingPrice() > 0) {
                shipping = String.valueOf(transaction.getShippingPrice());
                viewHolder.li_delivery.setVisibility(VISIBLE);
            } else {
                viewHolder.li_delivery.setVisibility(GONE);
            }

            if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.textFair));
                contributionSpan = CommonUtils.getOrderConceptTextSpanNoListing(contribution, contribution, getResources().getColor(R.color.textFair));
                shippingSpan = CommonUtils.getOrderConceptTextSpanNoListing(shipping, shipping, getResources().getColor(R.color.textFair));
                viewHolder.iv_currency.setImageResource(R.drawable.ic_darmas_currency_fair);
                viewHolder.iv_currency_contribution.setImageResource(R.drawable.ic_darmas_currency_fair);
                viewHolder.iv_currency_delivery.setImageResource(R.drawable.ic_darmas_currency_fair);
            } else {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.secondaryAccentColor));
                contributionSpan = CommonUtils.getOrderConceptTextSpanNoListing(contribution, contribution, getResources().getColor(R.color.secondaryAccentColor));
                shippingSpan = CommonUtils.getOrderConceptTextSpanNoListing(shipping, shipping, getResources().getColor(R.color.secondaryAccentColor));
                viewHolder.iv_currency.setImageResource(R.drawable.ic_darmas_currency);
                viewHolder.iv_currency_contribution.setImageResource(R.drawable.ic_darmas_currency);
                viewHolder.iv_currency_delivery.setImageResource(R.drawable.ic_darmas_currency);
            }
            viewHolder.tv_amount.setText(amountSpan);
            viewHolder.tv_contribution_price.setText(contributionSpan);
            viewHolder.tv_delivery_price.setText(shippingSpan);

            String quantity = "";
            if (transaction.getQuantity() > 1) {
                quantity = "x" + String.valueOf(transaction.getQuantity()) + " ";
            }
            String concept = quantity + "\"" + transaction.getConcept() + "\"";
            Spannable conceptSpan;
            if ((listing != null || communityEvent != null) && transaction.getType() != _Transaction.Type.TRANSFER) {
                if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                }
            } else {
                if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                }
            }

            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDate()), AppConstants.ABBREVIATED_DATE_FORMAT));
            //viewHolder.tv_amount.setText(AppConstants.CURRENCY_FORMAT.format(transaction.getAmount()));
            viewHolder.tv_id_right.setText("ID: " + transaction.getIdTransaction());
            viewHolder.tv_left_name.setText(mPresenter.getUser(paymentSenderId).getName());
            viewHolder.tv_right_name.setText(mPresenter.getUser(paymentReceiverId).getName());
            viewHolder.tv_concept_left.setText(conceptSpan);

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(paymentSenderId), viewHolder.iv_left);
            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(paymentReceiverId), viewHolder.iv_right);

            viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_forward_white);

            /*if (AppData.INSTANCE.getLoggedUser().getIdUser() != transaction.getIdReceiver()) {
                viewHolder.iv_right.setOnClickListener(view1 -> {
                    User contact = mPresenter.getUser(transaction.getIdSender());
                    Object[] parcelables = new Object[]{contact};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> viewHolder.iv_right.performClick());
            } else {
                viewHolder.iv_right.setOnClickListener(view1 -> {
                    //
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> {
                    //
                });
            }*/

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != paymentSenderId) {
                viewHolder.li_contact_left.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(paymentSenderId)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
            } else {
                viewHolder.li_contact_left.setOnClickListener(null);
            }
            if (AppData.INSTANCE.getLoggedUser().getIdUser() != paymentReceiverId) {
                viewHolder.li_contact_right.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(paymentReceiverId)};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
            } else {
                viewHolder.li_contact_right.setOnClickListener(null);
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.tv_amount)
            TextView tv_amount;
            @BindView(R.id.iv_currency)
            ImageView iv_currency;
            @BindView(R.id.iv_arrow)
            ImageView iv_arrow;
            @BindView(R.id.tv_concept_left)
            TextView tv_concept_left;
            @BindView(R.id.iv_left)
            ImageView iv_left;
            @BindView(R.id.tv_left_name)
            TextView tv_left_name;
            @BindView(R.id.iv_right)
            ImageView iv_right;
            @BindView(R.id.tv_id_right)
            TextView tv_id_right;
            @BindView(R.id.tv_right_name)
            TextView tv_right_name;
            @BindView(R.id.iv_listing_front)
            ImageView iv_listing_front;
            @BindView(R.id.li_contact_left)
            LinearLayout li_contact_left;
            @BindView(R.id.li_contact_right)
            LinearLayout li_contact_right;
            @BindView(R.id.li_contribution)
            LinearLayout li_contribution;
            @BindView(R.id.iv_currency_contribution)
            ImageView iv_currency_contribution;
            @BindView(R.id.tv_contribution_price)
            TextView tv_contribution_price;
            @BindView(R.id.li_delivery)
            LinearLayout li_delivery;
            @BindView(R.id.iv_currency_delivery)
            ImageView iv_currency_delivery;
            @BindView(R.id.tv_delivery_price)
            TextView tv_delivery_price;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleTransactionsItemList(List<_Transaction> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (_Transaction item : list) {
            itemList.add(new TransactionItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
