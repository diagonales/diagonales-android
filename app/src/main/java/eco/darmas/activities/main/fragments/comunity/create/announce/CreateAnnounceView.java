package eco.darmas.activities.main.fragments.comunity.create.announce;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.User;
import eco.darmas.utils.CommonUtils;

class CreateAnnounceView extends BaseFragmentView implements CreateAnnounceContracts.View {

    private MainActivity mContext;
    private CreateAnnouncePresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.et_title)
    EditText et_title;
    @BindView(R.id.et_text)
    EditText et_text;
    @BindView(R.id.iv_front)
    ImageView iv_front;
    @BindView(R.id.rg_pinned)
    RadioGroup rg_pinned;
    @BindView(R.id.rb_normal)
    RadioButton rb_normal;
    @BindView(R.id.rb_pinned)
    RadioButton rb_pinned;
    @BindView(R.id.rb_header)
    RadioButton rb_header;

    @BindView(R.id.cv_event_info)
    CardView cv_event_info;
    @BindView(R.id.tv_event_info)
    TextView tv_event_info;

    CreateAnnounceView(@NonNull Context context) {
        super(context);
        this.mContext = (MainActivity) context;
    }

    @Override
    protected View inflate(@NonNull LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.create_post, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(@NonNull BaseFragmentPresenter presenter) {
        this.mPresenter = (CreateAnnouncePresenter) presenter;
        if (mPresenter.getEventModeId() >= 0) {
            cv_event_info.setVisibility(VISIBLE);
            tv_event_info.setText(mPresenter.getEventInfoText());
        } else {
            cv_event_info.setVisibility(GONE);
        }
        if (AppData.INSTANCE.getLoggedUser().getStatus() > User.Status.ACTIVE) {
            rg_pinned.setVisibility(VISIBLE);
        } else {
            rg_pinned.setVisibility(GONE);
        }
    }

    @OnClick(R.id.bt_publish)
    void onPublishClickListener() {
        String title = et_title.getText().toString();
        String text = et_text.getText().toString();
        String pinned = "0";
        if (rb_pinned.isChecked()) {
            pinned = "1";
        } else if (rb_header.isChecked()) {
            pinned = "2";
        }
        mPresenter.onPublishClicked(title, text, pinned);
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.CREATE_ANNOUNCE_FRAGMENT_TAG, false);
    }

    @OnClick(R.id.iv_front)
    void onAddImageClickListener() {
        if (!mContext.getPermissionPresenter().checkGrantedWriteExternalStoragePermission()) {
            mContext.getPermissionPresenter().requestWriteExternalStoragePermissionAfterRationale();
            return;
        }

        LayoutInflater factory = LayoutInflater.from(mContext);
        final View view = factory.inflate(R.layout.picture_source_dialog, null);

        CustomDialog picture_source_dialog = new CustomDialog(mContext);
        picture_source_dialog.setView(view);

        final ImageView iv_camera = view.findViewById(R.id.iv_camera);
        final ImageView iv_file = view.findViewById(R.id.iv_file);

        iv_camera.setOnClickListener(view1 -> {
            mPresenter.onAddCameraImageClicked();
            picture_source_dialog.dismiss();
        });
        iv_file.setOnClickListener(view12 -> {
            mPresenter.onAddFileImageClicked();
            picture_source_dialog.dismiss();
        });
        picture_source_dialog.show();
    }

    void loadPostImage(Bitmap bitmap) {
        iv_front.setPadding(0, 0, 0, 0);
        iv_front.setImageBitmap(bitmap);
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}