package eco.darmas.activities.main.fragments.comunity.announces;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Post;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class AnnouncesModel extends BaseFragmentModel implements AnnouncesContracts.Model {

    private List<Post> announceList = new ArrayList<>();
    private List<Post> headerList = new ArrayList<>();

    AnnouncesModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadData().subscribe();
    }

    Single loadData() {
        return Observable.create((ObservableOnSubscribe<Post>) e -> {
            for (int i = getDataManager().getPostMap().size() - 1; i > -1; i--) {
                Post post = getDataManager().getPostMap().valueAt(i);
                if (Integer.parseInt(post.getAnnounce()) != 1) {
                    continue;
                }
                if (post.getIdEvent() != getEventMode()) {
                    continue;
                }
                e.onNext(post);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toSortedList((post1, post2) -> new PinnedComparator().compare(post1, post2))
                .doOnSuccess(sortedAnnounces -> {
                    announceList = sortedAnnounces;
                    headerList.clear();
                    for (Post announce : sortedAnnounces) {
                        if (announce.getPinned().equals("2")) {
                            headerList.add(announce);
                        }
                    }
                });
    }

    // Should be this method inside Post class?
    class PinnedComparator implements Comparator<Post> {

        @Override
        public int compare(Post post1, Post post2) {
            return Integer.parseInt(post2.getPinned()) - Integer.parseInt(post1.getPinned());
        }
    }

    List<Post> getAnnounceList() {
        return announceList;
    }

    Post getAnnounce(int position) {
        return announceList.get(position);
    }

    List<Post> getHeaderList() {
        return headerList;
    }

    Post getHeader(int position) {
        return headerList.get(position);
    }

    void refreshAnnounces() {
        getDataManager().refreshPosts(true);
    }

    int getEventMode() {
        return App.getInstance().getPreferencesHelper().getSelectedEventId();
    }
}