package eco.darmas.activities.main.fragments.market.select.demands;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;

class SelectableDemandsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements SelectableDemandsContracts.Presenter {

    private final SelectableDemandsModel mModel;
    private final SelectableDemandsView mView;

    SelectableDemandsPresenter(SelectableDemandsModel mModel, SelectableDemandsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData() {
        getDisposables().add(mModel.loadSelectableDemandsData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::dataSetChanged)
                .doAfterTerminate(() -> CommonUtils.showProgress(mView.getContext(), false))
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.LISTINGS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    List<Listing> getSelectableDemandList() {
        return mModel.getSelectableDemandList();
    }

    boolean isRegisteredAtEvent(int listingId) {
        return mModel.getTransientUserSelectedListingsList().indexOfKey(listingId) >= 0;
    }

    void preAddListing(int listingId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id_listing", String.valueOf(listingId));
        } catch (JSONException e) {
            AppLogger.d(e, null);
        }
        mModel.getTransientUserSelectedListingsList().put(listingId, jsonObject);
    }

    void preRemoveListing(int listingId) {
        mModel.getTransientUserSelectedListingsList().remove(listingId);
    }

    void preAddStock(int listingId, int stock) {
        JSONObject listingJsonObject = mModel.getTransientUserSelectedListingsList().get(listingId, new JSONObject());
        try {
            listingJsonObject.put("stock", String.valueOf(stock));
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }
    }

    String getEventListingStockText(int listingId) {
        try {
            if (mModel.getTransientUserSelectedListingsList().get(listingId, new JSONObject()).has("stock")) {
                return (String) mModel.getTransientUserSelectedListingsList().get(listingId).get("stock");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "0";
    }
}