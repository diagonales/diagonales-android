package eco.darmas.activities.main.fragments.comunity.info;

import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;

class InfoModel extends BaseFragmentModel implements InfoContracts.Model {

    InfoModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    int getUserCount() {
        return getDataManager().getUserMap().size();
    }
}