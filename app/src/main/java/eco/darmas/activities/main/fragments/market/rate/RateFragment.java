package eco.darmas.activities.main.fragments.market.rate;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class RateFragment extends BaseFragment implements RateContracts.Fragment {

    @Override
    protected RateModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new RateModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected RatePresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new RatePresenter((RateModel) model, (RateView) view);
    }

    @Override
    protected RateView createView(Context context) {
        return new RateView(context);
    }
}