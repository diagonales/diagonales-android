package eco.darmas.activities.main.fragments.market.demands;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class DemandsFragment extends BaseFragment implements DemandsContracts.Fragment {

    @Override
    protected DemandsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new DemandsModel(dataManager, savedInstanceState);
    }

    @Override
    protected DemandsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new DemandsPresenter((DemandsModel) model, (DemandsView) view);
    }

    @Override
    protected DemandsView createView(Context context) {
        return new DemandsView(context);
    }
}
