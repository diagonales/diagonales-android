package eco.darmas.activities.main.fragments.market.offers;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class OffersModel extends BaseFragmentModel implements OffersContracts.Model {

    private List<Listing> offerList = new ArrayList<>();

    OffersModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadOffersData().subscribe();
    }

    Single loadOffersData() {
        return Observable.create((ObservableOnSubscribe<Listing>) e -> {
            for (int i = getDataManager().getListingsMap().size() - 1; i > -1; i--) {
                Listing listing = getDataManager().getListingsMap().valueAt(i);
                if (listing.getType() == Listing.Type.OFFER) {
                    int eventMode = getEventModeId();
                    int eventIndex = getDataManager().getEventListingIdList().indexOfKey(eventMode);
                    if ((eventMode >= 0 && eventIndex < 0) || (eventMode >= 0 && getDataManager().getEventListingIdList().get(eventMode).indexOfKey(listing.getIdListing()) < 0)) {
                        continue;
                    } else if (eventMode < 0 && listing.getIdEvent() >= 0) {
                        continue;
                    }
                    Node<Category> categoryNode = getDataManager().getCategoryNodeMap().get(listing.getIdCategory());
                    if (!getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getFullTree().contains(categoryNode)) {
                        continue;
                    }
                    if (!CommonUtils.containsText(listing.getTitle(), DataFilters.offersConstraint)) {
                        continue;
                    }
                    e.onNext(listing);
                }
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(offers -> offerList = offers);
    }

    Category getCategory(int categoryId) {
        return getDataManager().getCategoryNodeMap().get(categoryId).getData();
    }

    List<Listing> getOfferList() {
        return offerList;
    }

    SparseArray<SparseArray<Float>> getListingRatingsMap() {
        return getDataManager().getListingRatingsMap();
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorksRatingsMap() {
        return getDataManager().getWorkRatingsMap();
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    int getSelectedCategory() {
        return DataFilters.selectedCategoryId;
    }

    String getSelectedCategoryName() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getData().getName();
    }

    String getOfferListings() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getData().getOfferListings();
    }

    SparseArray<SparseArray<JSONObject>> getEventListingIdList() {
        return getDataManager().getEventListingIdList();
    }

    void addSearchConstraint(String search) {
        DataFilters.offersConstraint = search;
    }

    int getSelectedOrder() {
        return DataFilters.selectedOffersListOrder;
    }

    Listing getListing(int position) {
        return offerList.get(position);
    }

    Listing getListingFromId(int listingId) {
        return getDataManager().getListingsMap().get(listingId);
    }

    SparseArray<SparseArray<JSONObject>> getEventListingJsonObjectMap() {
        return getDataManager().getEventListingIdList();
    }

    SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return getDataManager().getEventInscriptionMap();
    }

    void refreshListings() {
        getDataManager().refreshListings(true);
        getDataManager().refreshEventListings(true);
        getDataManager().refreshRatings(true);
    }
}