package eco.darmas.activities.main.fragments.profile.account;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class AccountModel extends BaseFragmentModel implements AccountContracts.Model {

    private File userImageFile;
    private Uri tempImageUri;

    AccountModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    void logout(Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().logout(getLoggedUser().getIdUser()).enqueue(callback);
    }

    void writeLogout() {
        App.getInstance().logout();
    }

    File getUserImageFile() {
        Calendar cal = Calendar.getInstance();
        userImageFile = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/" + ("IMG_" + cal.getTimeInMillis() + ".jpg"));
        return userImageFile;
    }

    Uri getTempImageUri() {
        return tempImageUri;
    }

    void setTempImageUri(Uri tempImageUri) {
        this.tempImageUri = tempImageUri;
    }

    void commitImageUpload(String path, Callback<ResponseBody> callback) {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(App.getInstance().getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        userImageFile = new File(CommonUtils.getRealImagePath(photo));

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), userImageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file", userImageFile.getName(), reqFile);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getLoggedUser().getIdUser()));
        RequestBody actualImageReq = RequestBody.create(MediaType.parse("text/plain"), getLoggedUser().getImage());

        App.getInstance().getNetworkService().getCallableAPI().uploadProfileImage(userIdReq, actualImageReq, body).enqueue(callback);
    }

    void writeImageUpload() {
        getDataManager().getBoxStore().boxFor(User.class).put(getLoggedUser());
    }
}
