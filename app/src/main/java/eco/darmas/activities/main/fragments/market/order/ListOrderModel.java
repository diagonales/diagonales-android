package eco.darmas.activities.main.fragments.market.order;

import android.os.Bundle;

import eco.darmas.AppConstants;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;

class ListOrderModel extends BaseFragmentModel {

    private int caller;

    ListOrderModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.caller = arguments.getInt("caller", 0);
    }

    int getSelectedOrder() {
        if (caller == AppConstants.SELECT_OFFERS_LIST_ORDER_FRAGMENT) {
            return DataFilters.selectedOffersListOrder;
        } else if (caller == AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT) {
            return DataFilters.selectedDemandsListOrder;
        } else if (caller == AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT) {
            return DataFilters.selectedOrdersListOrder;
        } else if (caller == AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT) {
            return DataFilters.selectedPostCommentsListOrder;
        }
        return 0;
    }

    int getCaller() {
        return caller;
    }
}