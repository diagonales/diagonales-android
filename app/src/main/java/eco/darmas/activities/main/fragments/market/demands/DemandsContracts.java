package eco.darmas.activities.main.fragments.market.demands;

interface DemandsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}