package eco.darmas.activities.main.fragments.comunity.events;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class EventsFragment extends BaseFragment implements EventsContracts.Fragment {

    @Override
    protected EventsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new EventsModel(dataManager, savedInstanceState);
    }

    @Override
    protected EventsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new EventsPresenter((EventsModel) model, (EventsView) view);
    }

    @Override
    protected EventsView createView(Context context) {
        return new EventsView(context);
    }
}
