package eco.darmas.activities.main.fragments.profile.ratings;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class RatingsFragment extends BaseFragment implements RatingsContracts.Fragment {

    @Override
    protected RatingsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new RatingsModel(dataManager, savedInstanceState);
    }

    @Override
    protected RatingsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new RatingsPresenter((RatingsModel) model, (RatingsView) view);
    }

    @Override
    protected RatingsView createView(Context context) {
        return new RatingsView(context);
    }
}
