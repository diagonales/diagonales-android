package eco.darmas.activities.main.fragments.messages.conversations;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.Conversation;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ConversationsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements ConversationsContracts.Presenter{

    private final ConversationsModel mModel;
    private final ConversationsView mView;

    ConversationsPresenter(ConversationsModel mModel, ConversationsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData() {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(mView::dataSetChanged)
                //.doAfterTerminate(mView::stopRefreshAnimation)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.MESSAGES: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData();
                }
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }


    void onNewMessageClick(int selectedUserId) {
        Object[] parcelables = new Object[]{mModel.getUser(selectedUserId)};
        AppNav.navigateToActivity(AppConstants.USER_MESSAGES_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
    }

    void hideConversation(int position) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }
        mModel.hideConversation(position, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.get("result").equals("true")) {
                            mModel.writeHideConversation(position);
                        } else {
                            CommonUtils.showErrorToast("La conversación no pudo ser eliminada debido a un error");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                } else {
                    response.body().close();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    CommonUtils.showErrorToast("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                } else {
                    CommonUtils.showErrorToast("La conversación no pudo ser eliminada debido a un error");
                }
                AppLogger.e(t, null);
            }
        });

    }

    List<Conversation> getConversationList() {
        return mModel.getConversationList();
    }

    boolean onConversationItemClick(int position) {
        int contactId = mModel.getConversationList().get(position).getIdContact();
        Object[] parcelables = new Object[]{mModel.getUser(contactId)};
        AppNav.navigateToActivity(AppConstants.USER_MESSAGES_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
        return true;
    }

    List<CommonUtils.StringWithTag> getNameList() {
        return mModel.getNameList();
    }
}
