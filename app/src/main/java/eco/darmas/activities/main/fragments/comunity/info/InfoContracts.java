package eco.darmas.activities.main.fragments.comunity.info;

interface InfoContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}