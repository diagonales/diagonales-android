package eco.darmas.activities.main.fragments.market.rate;

import android.os.Bundle;

import com.thirtydegreesray.dataautoaccess.annotation.AutoAccess;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Rating;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class RateModel extends BaseFragmentModel implements RateContracts.Model {

    @AutoAccess
    Rating rating;
    @AutoAccess
    Rating rating2;

    RateModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        if (savedInstanceState == null) {
            rating = arguments.getParcelable("parcelable#0");
            rating2 = arguments.getParcelable("parcelable#1");
        }
    }

    void commitSendRating(Rating rating1, Rating rating2, Callback<ResponseBody> callback) {
        if (rating2 == null) {
            App.getInstance().getNetworkService().getCallableAPI().sendRating(rating1.getType(), rating1.getIdSender(), rating1.getIdTarget(), rating1.getIdReceiver(), rating1.getRating(), rating1.getComment()).enqueue(callback);
        } else {
            App.getInstance().getNetworkService().getCallableAPI().sendRating(rating1.getType(), rating1.getIdSender(), rating1.getIdTarget(), rating1.getIdReceiver(), rating1.getRating(), rating1.getComment(), rating2.getType(), rating2.getIdTarget(), rating2.getRating(), rating2.getComment()).enqueue(callback);
        }
    }

    void writeRating(Rating rating1, Rating rating2) {
        getDataManager().getBoxStore().runInTxAsync(() -> {
            getDataManager().getBoxStore().boxFor(Rating.class).put(rating1);
            if (rating2 != null) {
                getDataManager().getBoxStore().boxFor(Rating.class).put(rating2);
            }
        }, null);
    }

    int getReceiverId() {
        return rating.getIdReceiver();
    }

    int getTargetId() {
        return rating.getIdTarget();
    }

    int getRatingType() {
        return rating.getType();
    }

    float getRating() {
        return rating.getRating();
    }

    String getComment() {
        return rating.getComment();
    }

    float getRating2() {
        /*if (rating2 != null) {
            AppLogger.d(rating2.getRating() + "");
        } else {
            AppLogger.d("null");
        }*/
        return rating2 == null ? 0 : rating2.getRating();
    }

    String getComment2() {
        return rating2 == null ? "" : rating2.getComment();
    }

    int getCategoryId() {
        return getDataManager().getListingsMap().get(rating.getIdTarget()).getIdCategory();
    }
}