package eco.darmas.activities.main.fragments.comunity.posts;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class PostsFragment extends BaseFragment implements PostsContracts.Fragment {

    @Override
    protected PostsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new PostsModel(dataManager, savedInstanceState);
    }

    @Override
    protected PostsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new PostsPresenter((PostsModel) model, (PostsView) view);
    }

    @Override
    protected PostsView createView(Context context) {
        return new PostsView(context);
    }
}