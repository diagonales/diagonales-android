package eco.darmas.activities.main.fragments.market.rate;

interface RateContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}