package eco.darmas.activities.main.fragments.wallet.balance;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class BalanceFragment extends BaseFragment implements BalanceContracts.Fragment {

    @Override
    protected BalanceModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new BalanceModel(dataManager, savedInstanceState);
    }

    @Override
    protected BalancePresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new BalancePresenter((BalanceModel) model, (BalanceView) view);
    }

    @Override
    protected BalanceView createView(Context context) {
        return new BalanceView(context);
    }
}
