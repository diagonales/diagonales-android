package eco.darmas.activities.main.fragments.comunity.info;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class InfoFragment extends BaseFragment implements InfoContracts.Fragment {

    @Override
    protected InfoModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new InfoModel(dataManager, savedInstanceState);
    }

    @Override
    protected InfoPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new InfoPresenter((InfoModel) model, (InfoView) view);
    }

    @Override
    protected InfoView createView(Context context) {
        return new InfoView(context);
    }
}