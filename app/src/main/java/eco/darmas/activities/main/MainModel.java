package eco.darmas.activities.main;

import android.os.Bundle;

import java.util.List;

import eco.darmas.App;
import eco.darmas.AppData;
import eco.darmas.base.activity.BaseActivityModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;

class MainModel extends BaseActivityModel implements MainContracts.Model {

    MainModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        AppData.INSTANCE.loadInstanceState(savedInstanceState);
        getDataManager().startDataLoad();
    }

    public void toggleUpdates() {
        getDataManager().toggleUpdates();
    }

    @Override
    protected void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        AppData.INSTANCE.saveInstanceState(outState);
    }

    List<CommunityEvent> getCurrentCommunityEventsList() {
        return getDataManager().getCurrentCommunityEventsList();
    }

    CommunityEvent getCommunityEvent(int eventId) {
        return getDataManager().getCommunityEventMap().get(eventId);
    }

    void setEventModeId(int eventId) {
        App.getInstance().getPreferencesHelper().setSelectedEventId(eventId);
    }

    void resetSelectedEventModeId() {
        App.getInstance().getPreferencesHelper().setSelectedEventId(null);
    }
}