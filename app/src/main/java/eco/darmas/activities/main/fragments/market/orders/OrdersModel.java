package eco.darmas.activities.main.fragments.market.orders;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.pojos.Rating;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class OrdersModel extends BaseFragmentModel implements OrdersContracts.Model {

    private List<_Transaction> orderList = new ArrayList<>();
    private SparseArray<Rating> userToMembersRatingList;
    private SparseArray<Rating> userToListingsRatingList;
    private SparseArray<SparseArray<Rating>> userToWorksRatingList;

    OrdersModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        Single.merge(loadTransactionsData(), loadRatingsData()).subscribe();
    }

    Single<List<_Transaction>> loadTransactionsData() {
        return Observable.create((ObservableOnSubscribe<_Transaction>) e -> {
            for (int i = getDataManager().getLoggedUserOrdersList().size() - 1; i > -1; i--) {
                _Transaction transaction = getDataManager().getLoggedUserOrdersList().get(i);
                int eventMode = App.getInstance().getPreferencesHelper().getSelectedEventId();
                if (eventMode >= 0 && eventMode != transaction.getIdEvent()) {
                    continue;
                }
                if (!DataFilters.showArchived) {
                    if (getLoggedUser().getIdUser() == transaction.getIdSender() && transaction.getSenderArchived() == 1) {
                        continue;
                    } else if (getLoggedUser().getIdUser() == transaction.getIdReceiver() && transaction.getReceiverArchived() == 1) {
                        continue;
                    }
                }
                if (!DataFilters.showEnded && transaction.getStatus() == _Transaction.Status.RECEIVED) {
                    continue;
                }
                Node<Category> categoryNode = getDataManager().getCategoryNodeMap().get(transaction.getIdCategory());
                if (!getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getFullTree().contains(categoryNode)) {
                    continue;
                }
                if (!CommonUtils.containsText(transaction.getConcept(), DataFilters.ordersConstraint)) {
                    continue;
                }
                e.onNext(transaction);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                //.filter(transaction -> !(!DataFilters.showEnded && transaction.getStatus() == _Transaction.Status.RECEIVED))
                .toList()
                .doOnSuccess(orders -> orderList = orders);
    }

    Single<?> loadRatingsData() {
        return Single.create((SingleOnSubscribe<SparseArray[]>) e -> {
            SparseArray[] sparseArrays = new SparseArray[]{new SparseArray<Rating>(), new SparseArray<SparseArray<Rating>>(), new SparseArray<Rating>()};
            for (int i = getDataManager().getRatingsMap().size() - 1; i > -1; i--) {
                Rating rating = getDataManager().getRatingsMap().valueAt(i);

                int ratingType = rating.getType();
                int idReceiver = rating.getIdReceiver();
                int idTarget = rating.getIdTarget();

                if (rating.getIdSender() == getLoggedUser().getIdUser()) {
                    switch (ratingType) {
                        case Rating.Type.MEMBER: {
                            ((SparseArray<Rating>) sparseArrays[0]).put(idTarget, rating);
                            break;
                        }
                        case Rating.Type.WORK: {
                            SparseArray<Rating> sparseArray = ((SparseArray<SparseArray<Rating>>) sparseArrays[1]).get(idReceiver, new SparseArray<>());
                            sparseArray.put(idTarget, rating);
                            ((SparseArray<SparseArray<Rating>>) sparseArrays[1]).put(idReceiver, sparseArray);
                            break;
                        }
                        case Rating.Type.LISTING: {
                            ((SparseArray<Rating>) sparseArrays[2]).put(idTarget, rating);
                            break;
                        }
                    }
                }
            }
            e.onSuccess(sparseArrays);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(sparseArrays -> {
                    userToMembersRatingList = (SparseArray<Rating>) sparseArrays[0];
                    userToWorksRatingList = (SparseArray<SparseArray<Rating>>) sparseArrays[1];
                    userToListingsRatingList = (SparseArray<Rating>) sparseArrays[2];
                });
    }

    void commitTransactionCancel(_Transaction order, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().transactionCancel(order.getIdTransaction(), order.getIdSender(), order.getIdReceiver(), order.getConcept(), order.getAmount(), order.getType()).enqueue(callback);
    }

    void commitTransactionProcess(_Transaction order, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().processTransaction(order.getIdSender(), order.getIdReceiver(), order.getIdCategory(), order.getConcept(), order.getAmount(), order.getIdListing(), order.getType(), order.getIdTransaction(), order.getStatus(), order.getNote(), order.getQuantity(), order.getShippingMethod(), order.getShippingPrice(), order.getShippingInfo(), order.getIdEvent(), order.getContribution()).enqueue(callback);
    }

    void commitTransactionArchive(_Transaction order, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().archiveTransaction(order.getIdTransaction(), order.getIdSender(), order.getIdReceiver()).enqueue(callback);
    }

    Category getCategory(int categoryId) {
        return getDataManager().getCategoryNodeMap().get(categoryId).getData();
    }

    List<_Transaction> getOrderList() {
        return orderList;
        //return getDataManager().getLoggedUserOrdersList();
    }

    SparseArray<Rating> getUserToMembersRatingList() {
        return userToMembersRatingList;
    }

    SparseArray<Rating> getUserToListingsRatingList() {
        return userToListingsRatingList;
    }

    SparseArray<SparseArray<Rating>> getUserToWorksRatingList() {
        return userToWorksRatingList;
    }

    void writeTransactionCancel(_Transaction order) {
        getDataManager().getBoxStore().boxFor(_Transaction.class).remove(order);
    }

    void writePaymentRefund(int senderId, double payment) {
        User user = getUser(senderId);
        user.setFunds(user.getFunds() + payment);
        getDataManager().getBoxStore().boxFor(User.class).put(user);
    }

    void writeTransactionProcess(_Transaction order) {
        getDataManager().getBoxStore().boxFor(_Transaction.class).put(order);
    }

    void writeListingStock(_Transaction order) {
        if (order.getIdEvent() >= 0) {
            try {
                JSONObject listingJsonObject = getDataManager().getEventListingIdList().get(order.getIdEvent()).get(order.getIdListing());
                int stockAtEvent = Integer.valueOf((String) listingJsonObject.get("stock"));
                String stockAtEventText = String.valueOf(stockAtEvent - order.getQuantity());
                listingJsonObject.put("stock", stockAtEventText);

                Inscription inscription = getDataManager().getEventInscriptionMap().get(order.getIdEvent()).get(order.getIdReceiver());
                JSONArray listingsJsonArray = new JSONArray(inscription.getListings());
                listingsJsonArray.put(listingJsonObject);

                inscription.setListings(listingsJsonArray.toString());
                getDataManager().getBoxStore().boxFor(Inscription.class).put(inscription);
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        } else {
            Listing listing = getDataManager().getListingsMap().get(order.getIdListing());
            if (listing != null) {
                listing.setStock(listing.getStock() - order.getQuantity());
                getDataManager().getBoxStore().boxFor(Listing.class).put(listing);
            }
        }
    }

    void writePaymentReserve(double payment) {
        User user = getLoggedUser();
        user.setFunds(user.getFunds() - payment);
        getDataManager().getBoxStore().boxFor(User.class).put(user);
    }

    void writeReleasePayment(int receiverId, double payment) {
        User user = getUser(receiverId);
        user.setFunds(user.getFunds() + payment);
        getDataManager().getBoxStore().boxFor(User.class).put(user);
    }

    void setShowEnded(boolean showEnded) {
        DataFilters.showEnded = showEnded;
    }

    Listing getListing(int idListing) {
        return getDataManager().getListingsMap().get(idListing);
    }

    int getSelectedCategory() {
        return DataFilters.selectedCategoryId;
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorksRatingsMap() {
        return getDataManager().getWorkRatingsMap();
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    String getSelectedCategoryName() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId).getData().getName();
    }

    void addSearchConstraint(String search) {
        DataFilters.ordersConstraint = search;
    }

    int getSelectedOrder() {
        return DataFilters.selectedOrdersListOrder;
    }
}