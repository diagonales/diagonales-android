package eco.darmas.activities.main.fragments.market.demands;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Listing;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.utils.FlexibleUtils;
import eu.davidea.viewholders.FlexibleViewHolder;

class DemandsView extends BaseFragmentView implements DemandsContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private DemandsPresenter mPresenter;
    private DemandsAdapter flexibleDemandsAdapter;
    private Unbinder unBinder;
    private boolean scrollTop;

    // toolbar
    @BindView(R.id.li_order)
    LinearLayout li_order;
    @BindView(R.id.rb_order)
    SimpleRatingBar rb_order;
    @BindView(R.id.iv_order)
    ImageView iv_order;
    @BindView(R.id.iv_direction)
    ImageView iv_direction;
    @BindView(R.id.iv_search_demands)
    ImageView iv_search_demands;
    @BindView(R.id.li_categories)
    LinearLayout li_categories;
    @BindView(R.id.cv_category)
    CardView cv_category;
    @BindView(R.id.iv_category_demands)
    ImageView iv_category_demands;
    @BindView(R.id.tv_category_demands)
    TextView tv_category_demands;
    @BindView(R.id.tv_category_demands_count)
    TextView tv_category_demands_count;
    @BindView(R.id.li_add_demand)
    LinearLayout li_add_demand;

    // search box
    @BindView(R.id.li_search)
    LinearLayout li_search;
    @BindView(R.id.et_search)
    EditText et_search;

    // components
    @BindView(R.id.rv_demands)
    RecyclerView rv_demands;
    @BindView(R.id.tv_demands_none)
    TextView tv_demands_none;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    DemandsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.demands_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (DemandsPresenter) presenter;
        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshItems());
        swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor);
        setupAdapters();
        setData();
    }

    @OnClick(R.id.li_order)
    void onOrderClickListener() {
        mPresenter.onOrderClicked();
    }

    @OnTextChanged(R.id.et_search)
    void onSearchTextChangedListener(CharSequence s) {
        mPresenter.addSearchConstraint(s.toString());
        flexibleDemandsAdapter.setFilter(s.toString());
    }

    @OnClick(R.id.iv_clear)
    void onSearchClearClickListener() {
        et_search.setText("");
        mPresenter.addSearchConstraint("");
        flexibleDemandsAdapter.setFilter("");
    }

    @OnClick(R.id.li_categories)
    void onCategoryClickListener() {
        mPresenter.onCategoryClicked();
    }

    @OnClick(R.id.li_add_demand)
    void onAddDemandClickListener() {
        mPresenter.onAddDemandClicked();
    }

    @OnClick(R.id.iv_search_demands)
    void onSearchDemandsClickListener(View view) {
        if (li_search.getVisibility() == GONE) {
            et_search.setText("");
            li_search.setVisibility(VISIBLE);
            KeyboardUtils.showSoftInput(et_search, getContext());
        } else {
            KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), et_search);
            flexibleDemandsAdapter.setFilter("");
            mPresenter.addSearchConstraint("");
            li_search.setVisibility(GONE);
        }
    }

    void setData() {
        tv_category_demands.setText(mPresenter.getSelectedCategoryName());
        if (mPresenter.getSelectedCategoryId() == 0) {
            et_search.setHint("Buscar en todas las categorías");
            cv_category.setCardElevation(0);
            cv_category.setCardBackgroundColor(0);
            tv_category_demands_count.setVisibility(View.INVISIBLE);
        } else {
            Spannable hint = CommonUtils.getBoldItalicTextSpan("Buscar en " + mPresenter.getSelectedCategoryName(), mPresenter.getSelectedCategoryName());
            et_search.setHint(hint);
            tv_category_demands_count.setText(mPresenter.getDemandListingCountText());
            cv_category.setCardElevation(5);
            cv_category.setCardBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            tv_category_demands_count.setVisibility(View.VISIBLE);
        }
        LinearLayout.LayoutParams arrowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        switch (mPresenter.getSelectedOrder()) {
            case AppConstants.BY_MEMBER_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
        }
    }

    private void setupAdapters() {
        rv_demands.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_demands = new LinearLayoutManager(getContext());
        rv_demands.setLayoutManager(lm_demands);
        rv_demands.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.listing, 15));// use a linear layout manager

        // Initialize the Adapter
        flexibleDemandsAdapter = new DemandsAdapter(getFlexibleDemandsItemList(mPresenter.getDemandList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_demands.setAdapter(flexibleDemandsAdapter);
        //flexibleDemandsAdapter
                //.setNotifyMoveOfFilteredItems(true);
                //.setDisplayHeadersAtStartUp(true)
                //.setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    void dataSetChanged(boolean scrollTop) {
        setData();
        this.scrollTop = scrollTop;
        flexibleDemandsAdapter.updateDataSet(getFlexibleDemandsItemList(mPresenter.getDemandList()));
    }

    void stopRefreshAnimation() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onDemandItemClick(position);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_demands.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_demands_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_demands.scrollToPosition(0);
        }
    }

    class DemandsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        DemandsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class DemandItemAdapter extends AbstractFlexibleItem<DemandItemAdapter.ViewHolder> {

        private Listing listing;

        DemandItemAdapter(Listing listing) {
            this.listing = listing;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof DemandItemAdapter) {
                Listing inItem = ((DemandItemAdapter) inObject).listing;
                return this.listing.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return listing.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.listing;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int id_creator = listing.getIdCreator();

            if (!DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mPresenter.getUser(id_creator).getDate()))) {
                viewHolder.iv_badge_new.setVisibility(View.VISIBLE);
            }

            if (flexibleDemandsAdapter.hasFilter()) {
                FlexibleUtils.highlightText(viewHolder.tv_title, listing.getTitle(), flexibleDemandsAdapter.getFilter(String.class));
            } else {
                viewHolder.tv_title.setText(listing.getTitle());
            }
            if (mPresenter.isInEventMode()) {
                viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_stand);
                viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                viewHolder.li_secondary_shipping.setVisibility(GONE);
                viewHolder.tv_address.setTextColor(getResources().getColor(R.color.textFair));
            } else {
                viewHolder.tv_address.setTextColor(getResources().getColor(R.color.primaryTextColor));
                switch (listing.getShippingMethod()) {
                    case Listing.ShippingMethod.PICKUP:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_location);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.li_secondary_shipping.setVisibility(GONE);
                        break;
                    case Listing.ShippingMethod.DELIVERY:
                        viewHolder.li_primary_shipping.setVisibility(GONE);
                        viewHolder.tv_secondary_shipping.setText(mPresenter.getSecondaryShippingText(listing));
                        viewHolder.li_secondary_shipping.setVisibility(VISIBLE);
                        break;
                    case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_location);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.tv_secondary_shipping.setText(mPresenter.getSecondaryShippingText(listing));
                        viewHolder.li_secondary_shipping.setVisibility(VISIBLE);
                        break;
                    case Listing.ShippingMethod.DIGITAL:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_digital);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.li_secondary_shipping.setVisibility(GONE);
                        break;
                }
            }
            viewHolder.tv_address.setText(mPresenter.getAddressText(listing));

            viewHolder.tv_name.setText(mPresenter.getUser(id_creator).getName());
            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(listing.getDate()), AppConstants.ABBREVIATED_DATE_FORMAT));
            viewHolder.tv_category.setText(mPresenter.getCategoryName(listing.getIdCategory()));

            viewHolder.rb_rating_1.setFillColor(getResources().getColor(R.color.userRatingBar));
            viewHolder.rb_rating_1.setStarBackgroundColor(getResources().getColor(R.color.userBackgroundRatingBar));
            viewHolder.rb_rating_1.setBorderColor(getResources().getColor(R.color.transparent));

            viewHolder.rb_rating_1.setRating(mPresenter.getUserRating(listing));
            viewHolder.tv_rating_1.setText("(" + String.valueOf(mPresenter.getUserRatingsSize(listing)) + ")");

            viewHolder.li_rating_2.setVisibility(View.GONE);

            // Set price
            if (listing.getRanged().equals("1")) {
                viewHolder.tv_price_min.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMin()));
                viewHolder.tv_price_min.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_min.setVisibility(View.VISIBLE);

                viewHolder.tv_divider.setVisibility(View.VISIBLE);

                viewHolder.tv_price_max.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMax()));
                viewHolder.tv_price_max.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_max.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tv_price_max.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMax()));
                viewHolder.tv_price_max.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_max.setVisibility(View.VISIBLE);

                viewHolder.tv_divider.setVisibility(View.GONE);

                viewHolder.tv_price_min.setVisibility(View.GONE);
                viewHolder.iv_diag_min.setVisibility(View.GONE);
            }

            // Demands just can't issue event discounts or additions in price
            viewHolder.cv_percent_discount.setVisibility(GONE);

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(id_creator), viewHolder.iv_profile);
            ImageUtils.loadListingImage(listing, viewHolder.iv_front);

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != listing.getIdCreator()) {
                viewHolder.iv_profile.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(listing.getIdCreator())};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
            } else {
                viewHolder.iv_profile.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.iv_front)
            ImageView iv_front;
            @BindView(R.id.tv_title)
            TextView tv_title;
            @BindView(R.id.tv_name)
            TextView tv_name;
            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.tv_category)
            TextView tv_category;
            @BindView(R.id.iv_profile)
            ImageView iv_profile;
            @BindView(R.id.tv_price_min)
            TextView tv_price_min;
            @BindView(R.id.iv_diag_min)
            ImageView iv_diag_min;
            @BindView(R.id.tv_divider)
            TextView tv_divider;
            @BindView(R.id.tv_price_max)
            TextView tv_price_max;
            @BindView(R.id.iv_diag_max)
            ImageView iv_diag_max;
            @BindView(R.id.rb_rating_1)
            SimpleRatingBar rb_rating_1;
            @BindView(R.id.rb_rating_2)
            SimpleRatingBar rb_rating_2;
            @BindView(R.id.tv_rating_1)
            TextView tv_rating_1;
            @BindView(R.id.tv_rating_2)
            TextView tv_rating_2;
            @BindView(R.id.li_rating_2)
            LinearLayout li_rating_2;
            @BindView(R.id.iv_badge_verified)
            ImageView iv_badge_verified;
            @BindView(R.id.iv_badge_new)
            ImageView iv_badge_new;
            @BindView(R.id.cv_percent_discount)
            CardView cv_percent_discount;

            @BindView(R.id.li_primary_shipping)
            LinearLayout li_primary_shipping;
            @BindView(R.id.li_secondary_shipping)
            LinearLayout li_secondary_shipping;
            @BindView(R.id.tv_address)
            TextView tv_address;
            @BindView(R.id.tv_secondary_shipping)
            TextView tv_secondary_shipping;
            @BindView(R.id.iv_primary_shipping)
            ImageView iv_primary_shipping;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleDemandsItemList(List<Listing> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Listing item : list) {
            itemList.add(new DemandItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
