package eco.darmas.activities.main.fragments.profile.ratings;

import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

class RatingsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements RatingsContracts.Presenter{

    private final RatingsModel mModel;
    private final RatingsView mView;

    RatingsPresenter(RatingsModel mModel, RatingsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.LISTINGS: {
                if (event.isSuccess()) {
                    mView.setData();
                }
            }
            case DataLoadedEvent.RATINGS: {
                if (event.isSuccess()) {
                    mView.setData();
                }
            }
        }
    }

    Completable repaintUserRating() {
        final float[] rating = {0};
        final int[] size = {0};
        return Completable.create(e -> {
            float ratingPartial = 0;
            SparseArray<Float> ratings = mModel.getUserRatingsMap().get(mModel.getLoggedUser().getIdUser(), new SparseArray<>());
            size[0] = ratings.size();

            for (int i = 0; i < size[0]; i++) {
                ratingPartial = ratingPartial + ratings.valueAt(i);
            }
            if (ratingPartial > 0) {
                rating[0] = ratingPartial / size[0];
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> {
                    mView.setUserRating(rating[0]);
                    mView.setUserRatingCount("(" + String.valueOf(size[0]) + ")");
                });
    }

    Single<LinkedHashMap<Integer, Float>> repaintUserWorkRatings() {
        return Single.create((SingleOnSubscribe<LinkedHashMap<Integer, Float>>) e -> {
            final LinkedHashMap<Integer, Float> userWorkRatings = new LinkedHashMap<>();
            LinkedHashMap<Integer, Float> listUserWorkRatings = new LinkedHashMap<>();
            if (mModel.getWorkRatingsMap().indexOfKey(mModel.getLoggedUser().getIdUser()) > 0) {
                for (int i = 0; i < mModel.getWorkRatingsMap().get(mModel.getLoggedUser().getIdUser()).size(); i++) {
                    int categoryId = mModel.getWorkRatingsMap().get(mModel.getLoggedUser().getIdUser()).keyAt(i);
                    SparseArray<Float> ratings = mModel.getWorkRatingsMap().get(mModel.getLoggedUser().getIdUser()).valueAt(i);

                    float ratingPartial = 0;
                    float rating = 0;
                    int size = ratings.size();
                    for (int j = 0; j < size; j++) {
                        ratingPartial = ratingPartial + ratings.valueAt(j);
                    }
                    if (ratingPartial > 0) {
                        rating = ratingPartial / size;
                    }
                    listUserWorkRatings.put(categoryId, rating);
                }
            }
            Object[] a = listUserWorkRatings.entrySet().toArray();
            for (Object o : ratingsWorkAround(a)) {
                userWorkRatings.put(((Map.Entry<Integer, Float>) o).getKey(), ((Map.Entry<Integer, Float>) o).getValue());
            }
            e.onSuccess(userWorkRatings);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(integerFloatHashMap -> {
                            mView.workRatingsDataSetChanged(integerFloatHashMap);
                            mView.workRatingsNone(integerFloatHashMap.size() == 0);
                        }
                );
    }

    Single<LinkedHashMap<Integer, Float>> repaintUserListingRatings() {
        return Single.create((SingleOnSubscribe<LinkedHashMap<Integer, Float>>) e -> {
            final LinkedHashMap<Integer, Float> userListingRatings = new LinkedHashMap<>();
            LinkedHashMap<Integer, Float> listUserListingRatings = new LinkedHashMap<>();

            for (int i = 0; i < mModel.getListingRatingsMap().size(); i++) {
                int listingId = mModel.getListingRatingsMap().keyAt(i);
                SparseArray<Float> ratings = mModel.getListingRatingsMap().get(listingId);
                Listing listing = mModel.getListingsMap().get(listingId);

                if (listing == null) {
                    continue;
                }
                if (listing.getIdCreator() != mModel.getLoggedUser().getIdUser()) {
                    continue;
                }

                float ratingPartial = 0;
                float rating = 0;
                int size = ratings.size();
                for (int j = 0; j < size; j++) {
                    ratingPartial = ratingPartial + ratings.valueAt(j);
                }
                if (ratingPartial > 0) {
                    rating = ratingPartial / size;
                }
                listUserListingRatings.put(listingId, rating);
            }

            Object[] a = listUserListingRatings.entrySet().toArray();
            for (Object o : ratingsWorkAround(a)) {
                userListingRatings.put(((Map.Entry<Integer, Float>) o).getKey(), ((Map.Entry<Integer, Float>) o).getValue());
            }
            e.onSuccess(userListingRatings);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(integerFloatHashMap -> {
                            mView.listingRatingsDataSetChanged(integerFloatHashMap);
                            mView.listingsRatingsNone(integerFloatHashMap.size() == 0);
                        }
                );
    }

    private static synchronized Object[] ratingsWorkAround(Object[] a2) {
        Arrays.sort(a2, (o1, o2) -> ((Map.Entry<Integer, Float>) o2).getValue().compareTo(((Map.Entry<Integer, Float>) o1).getValue()));
        return a2;
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorkRatings() {
        return mModel.getWorkRatingsMap();
    }

    SparseArray<Node<Category>> getCategoryNodeMap() {
        return mModel.getCategoryNodeMap();
    }

    SparseArray<Listing> getListingMap() {
        return mModel.getListingsMap();
    }

    SparseArray<SparseArray<Float>> getListingRatings() {
        return mModel.getListingRatingsMap();
    }
}