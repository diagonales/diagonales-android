package eco.darmas.activities.main.fragments.comunity.events;

import android.animation.LayoutTransition;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class EventsView extends BaseFragmentView implements EventsContracts.View {

    private EventsPresenter mPresenter;
    private Unbinder unBinder;
    private PastCommunityEventsAdapter flexiblePastEventsAdapter;
    private CurrentCommunityEventsAdapter flexibleCurrentEventsAdapter;
    private ScheduledCommunityEventsAdapter flexibleScheduledEventsAdapter;
    private boolean showCurrentEvents;

    @BindView(R.id.rv_past_community_events)
    RecyclerView rv_past_community_events;
    @BindView(R.id.rv_current_community_events)
    RecyclerView rv_current_community_events;
    @BindView(R.id.rv_scheduled_community_events)
    RecyclerView rv_scheduled_community_events;
    @BindView(R.id.tv_past_community_events_none)
    TextView tv_past_community_events_none;
    @BindView(R.id.tv_current_community_events_none)
    TextView tv_current_community_events_none;
    @BindView(R.id.tv_scheduled_community_events_none)
    TextView tv_scheduled_community_events_none;
    @BindView(R.id.li_past_events)
    LinearLayout li_past_events;
    @BindView(R.id.li_current_events)
    LinearLayout li_current_events;
    @BindView(R.id.li_scheduled_events)
    LinearLayout li_scheduled_events;
    @BindView(R.id.li_add)
    LinearLayout li_add;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    EventsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.community_events_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (EventsPresenter) presenter;

        if (AppData.INSTANCE.getLoggedUser().getStatus() == User.Status.ADMIN) {
            li_add.setVisibility(View.VISIBLE);
        } else {
            li_add.setVisibility(View.GONE);
        }
        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshItems());
        swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor);
        setupAdapters();
        setData();
    }

    public void setData() {
    }

    void setupAdapters() {
        rv_past_community_events.setHasFixedSize(true);
        rv_current_community_events.setHasFixedSize(true);
        rv_scheduled_community_events.setHasFixedSize(true);

        LinearLayoutManager lm_past_events = new LinearLayoutManager(getContext());
        rv_past_community_events.setLayoutManager(lm_past_events);
        LinearLayoutManager lm_current_events = new LinearLayoutManager(getContext());
        rv_current_community_events.setLayoutManager(lm_current_events);
        LinearLayoutManager lm_scheduled_events = new LinearLayoutManager(getContext());
        rv_scheduled_community_events.setLayoutManager(lm_scheduled_events);

        rv_past_community_events.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.community_event, 15)
                .withBottomEdge(true));
        rv_current_community_events.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.community_event, 15)
                .withBottomEdge(true));
        rv_scheduled_community_events.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.community_event, 15)
                .withBottomEdge(true));

        flexiblePastEventsAdapter = new PastCommunityEventsAdapter(new ArrayList<>(), null, true);
        flexiblePastEventsAdapter.addListener((FlexibleAdapter.OnItemClickListener) (view, position) -> mPresenter.onPastCommunityEventItemClick(position));
        rv_past_community_events.setAdapter(flexiblePastEventsAdapter);
        flexibleCurrentEventsAdapter = new CurrentCommunityEventsAdapter(new ArrayList<>(), null, true);
        flexibleCurrentEventsAdapter.addListener((FlexibleAdapter.OnItemClickListener) (view, position) -> mPresenter.onCurrentCommunityEventItemClick(position));
        rv_current_community_events.setAdapter(flexibleCurrentEventsAdapter);
        flexibleScheduledEventsAdapter = new ScheduledCommunityEventsAdapter(new ArrayList<>(), null, true);
        flexibleScheduledEventsAdapter.addListener((FlexibleAdapter.OnItemClickListener) (view, position) -> mPresenter.onScheduledCommunityEventItemClick(position));
        rv_scheduled_community_events.setAdapter(flexibleScheduledEventsAdapter);
    }

    void communityEventsDataSetChanged() {
        flexiblePastEventsAdapter.updateDataSet(getFlexibleCommunityEventItemList(mPresenter.getPastCommunityEventList()));
        flexibleCurrentEventsAdapter.updateDataSet(getFlexibleCommunityEventItemList(mPresenter.getCurrentCommunityEventList()));
        flexibleScheduledEventsAdapter.updateDataSet(getFlexibleCommunityEventItemList(mPresenter.getScheduledCommunityEventList()));
    }

    void stopRefreshAnimation() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.li_add)
    void onAddClickListener() {
        mPresenter.onAddClicked();
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }

    public void pastEventsNone(boolean none) {
        tv_past_community_events_none.setVisibility(none ? View.VISIBLE : View.GONE);
        rv_past_community_events.setVisibility(none ? View.GONE : View.VISIBLE);
    }

    public void currentEventsNone(boolean none) {
        tv_current_community_events_none.setVisibility(none ? View.VISIBLE : View.GONE);
        rv_current_community_events.setVisibility(none ? View.GONE : View.VISIBLE);
        if (!showCurrentEvents && none) {
            li_current_events.setVisibility(View.GONE);
        } else {
            li_current_events.setVisibility(View.VISIBLE);
        }
    }

    public void scheduledEventsNone(boolean none) {
        tv_scheduled_community_events_none.setVisibility(none ? View.VISIBLE : View.GONE);
        rv_scheduled_community_events.setVisibility(none ? View.GONE : View.VISIBLE);
    }

    class PastCommunityEventsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        PastCommunityEventsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }

        @Override
        protected void onPostUpdate() {
            super.onPostUpdate();
            pastEventsNone(getItemCount() == 0);
        }
    }

    class CurrentCommunityEventsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        CurrentCommunityEventsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }

        @Override
        protected void onPostUpdate() {
            super.onPostUpdate();
            currentEventsNone(getItemCount() == 0);
        }
    }

    class ScheduledCommunityEventsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        ScheduledCommunityEventsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }

        @Override
        protected void onPostUpdate() {
            super.onPostUpdate();
            scheduledEventsNone(getItemCount() == 0);
        }
    }

    class CommunityEventItemAdapter extends AbstractFlexibleItem<CommunityEventItemAdapter.ViewHolder> {

        private CommunityEvent communityEvent;

        CommunityEventItemAdapter(CommunityEvent communityEvent) {
            this.communityEvent = communityEvent;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof CommunityEventItemAdapter) {
                CommunityEvent inItem = ((CommunityEventItemAdapter) inObject).communityEvent;
                return this.communityEvent.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return communityEvent.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.community_event;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            mPresenter.getDisposables().add(mPresenter.getCommunityEventTitleText(communityEvent)
                    .doOnSuccess(title -> viewHolder.tv_community_event_name.setText(title))
                    .subscribe());
            mPresenter.getDisposables().add(mPresenter.getCommunityEventDateText(communityEvent)
                    .doOnSuccess(startDate -> viewHolder.tv_community_event_date.setText(startDate))
                    .subscribe());
            mPresenter.getDisposables().add(mPresenter.getCommunityEventParticipantsText(communityEvent)
                    .doOnSuccess(participants -> viewHolder.tv_community_event_attendees.setText(participants))
                    .subscribe());

            if (mPresenter.isInscribedInEvent(communityEvent.getIdEvent())) {
                viewHolder.li_price_max.setVisibility(GONE);
                viewHolder.iv_currency_min.setVisibility(GONE);
                viewHolder.tv_price_min.setText("Inscripto!");
                viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.inscriptionComplete));
                viewHolder.cv_attendee.setVisibility(VISIBLE);
            } else if (mPresenter.noVacantsAvailable(communityEvent)) {
                viewHolder.li_price_max.setVisibility(GONE);
                viewHolder.iv_currency_min.setVisibility(GONE);
                viewHolder.tv_price_min.setText("Agotado");
                viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.inscriptionClosed));
            } else if (mPresenter.isInscriptionsAwaiting(communityEvent)) {
                viewHolder.li_price_max.setVisibility(GONE);
                viewHolder.iv_currency_min.setVisibility(GONE);
                viewHolder.tv_price_min.setText("En espera");
                viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.inscriptionClosed));
            } else if (mPresenter.isInscriptionsClosed(communityEvent)) {
                viewHolder.li_price_max.setVisibility(GONE);
                viewHolder.iv_currency_min.setVisibility(GONE);
                viewHolder.tv_price_min.setText("Cerrado");
                viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.inscriptionClosed));
            } else {
                viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
                viewHolder.iv_currency_min.setVisibility(VISIBLE);
                viewHolder.cv_attendee.setVisibility(GONE);

                mPresenter.getDisposables().add(mPresenter.getCommunityEventPriceMinText(communityEvent)
                        .doOnSuccess(priceMin -> viewHolder.tv_price_min.setText(priceMin))
                        .subscribe());
                if (communityEvent.getRanged() > 0) {
                    viewHolder.li_price_max.setVisibility(VISIBLE);
                    mPresenter.getDisposables().add(mPresenter.getCommunityEventPriceMaxText(communityEvent)
                            .doOnSuccess(priceMax -> viewHolder.tv_price_max.setText(priceMax))
                            .subscribe());
                } else {
                    viewHolder.li_price_max.setVisibility(GONE);
                }
            }

            viewHolder.cv_event.setVisibility(mPresenter.getEventModeId() == communityEvent.getIdEvent() ? VISIBLE : GONE);

            ImageUtils.loadEventImage(communityEvent, viewHolder.iv_event_front);

            viewHolder.iv_more.setOnClickListener(view -> {
                PopupMenu popup = new PopupMenu(getContext(), view);

                // Reflection workaround to show icons in menu
                try {
                    Field[] fields = popup.getClass().getDeclaredFields();
                    for (Field field : fields) {
                        if ("mPopup".equals(field.getName())) {
                            field.setAccessible(true);
                            Object menuPopupHelper = field.get(popup);
                            Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                            Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                            setForceIcons.invoke(menuPopupHelper, true);
                            break;
                        }
                    }
                } catch (Exception e) {
                    AppLogger.e(e, null);
                }

                popup.getMenuInflater().inflate(R.menu.event_popup, popup.getMenu());
                if (!mPresenter.isNow(communityEvent)) {
                    popup.getMenu().findItem(R.id.mode).setEnabled(false);
                    popup.getMenu().findItem(R.id.mode).setTitle("Modo no disponible");
                } else if (mPresenter.getEventModeId() == communityEvent.getIdEvent()) {
                    popup.getMenu().findItem(R.id.mode).setEnabled(false);
                    popup.getMenu().findItem(R.id.mode).setTitle("Modo activo");
                }
                if (!mPresenter.isInscribedInEvent(communityEvent.getIdEvent())) {
                    popup.getMenu().findItem(R.id.listings).setEnabled(false);
                    popup.getMenu().findItem(R.id.stand).setEnabled(false);
                } else {
                    popup.getMenu().findItem(R.id.inscription).setEnabled(false);
                }
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.mode:
                            mPresenter.enterEventMode(communityEvent.getIdEvent());
                            break;
                        case R.id.location:
                            CommonUtils.showDialog(getContext(), "Ubicación del evento", communityEvent.getAddress());
                            break;
                        case R.id.inscription:
                            viewHolder.cv_inscription.performClick();
                            break;
                        case R.id.listings:
                            mPresenter.onManageListingsClick(communityEvent.getIdEvent());
                            break;
                        case R.id.stand:
                            mPresenter.OnStandSelected(communityEvent.getIdEvent(), "assign");
                            /*LayoutInflater factory = LayoutInflater.from(getContext());
                            final View v = factory.inflate(R.layout.stand_selection, null);
                            Button bt_ok = v.findViewById(R.id.bt_ok);
                            Button bt_cancel = v.findViewById(R.id.bt_cancel);
                            Switch sw_no_stand = v.findViewById(R.id.sw_no_stand);

                            CustomDialog noStockDialog = new CustomDialog(getContext());
                            noStockDialog.setView(v);

                            sw_no_stand.setOnCheckedChangeListener((compoundButton, b) -> {
                                mPresenter.OnStandSelected(communityEvent.getIdEvent(), "reset");
                            });

                            bt_ok.setOnClickListener(view1 -> {
                                //TODO: Stand selection must be redone
                                mPresenter.OnStandSelected(communityEvent.getIdEvent(), "assign");
                                noStockDialog.dismiss();
                            });
                            bt_cancel.setOnClickListener(view1 -> noStockDialog.dismiss());
                            noStockDialog.show();
                            //mPresenter.onSelectStandClick(communityEvent.getIdEvent());
                            //CommonUtils.showNotImplementedToast();*/
                            break;
                    }
                    return true;
                });
                popup.show();
            });

            viewHolder.cv_inscription.setOnClickListener(v -> {
                if (mPresenter.isInscribedInEvent(communityEvent.getIdEvent())) {
                    CommonUtils.showDialog(getContext(), "Tu inscripción para este evento ya fue realizada", null);
                    return;
                }
                if (mPresenter.isInscriptionsClosed(communityEvent)) {
                    CommonUtils.showDialog(getContext(), "Inscripciones cerradas", "El plazo de inscripciones para este evento ha concluido");
                    return;
                }
                if (mPresenter.isInscriptionsAwaiting(communityEvent)) {
                    CommonUtils.showDialog(getContext(), "Inscripciones no abiertas", "El período de inscripciones para este evento aún no ha comenzado");
                    return;
                }
                if (mPresenter.noVacantsAvailable(communityEvent)) {
                    LayoutInflater factory = LayoutInflater.from(getContext());
                    final View view = factory.inflate(R.layout.inscription_no_vacants_dialog, null);
                    Button bt_ok = view.findViewById(R.id.bt_ok);
                    SwitchCompat sw_receive_notification = view.findViewById(R.id.sw_receive_notification);

                    CustomDialog noStockDialog = new CustomDialog(getContext());
                    noStockDialog.setView(view);

                    sw_receive_notification.setOnCheckedChangeListener((compoundButton, b) -> {
                        //TODO: implement
                        CommonUtils.showNotImplementedToast();
                    });
                    bt_ok.setOnClickListener(view1 -> noStockDialog.dismiss());
                    noStockDialog.show();
                    return;
                }

                LayoutInflater factory = LayoutInflater.from(getContext());
                final View view = factory.inflate(R.layout.inscription_confirm_dialog, null);
                Button bt_cancel = view.findViewById(R.id.bt_cancel);
                Button bt_ok = view.findViewById(R.id.bt_ok);
                EditText et_pay = view.findViewById(R.id.et_pay);
                SwitchCompat sw_include_note = view.findViewById(R.id.sw_include_note);
                EditText et_note = view.findViewById(R.id.et_note);
                TextView tv_total = view.findViewById(R.id.tv_total);
                CheckBox cb_donate = view.findViewById(R.id.cb_donate);

                tv_total.setText(AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMin()));

                et_pay.setText(AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMin()));
                if (communityEvent.getRanged() == 1) {
                    et_pay.setEnabled(true);
                } else {
                    et_pay.setEnabled(false);
                }

                if (communityEvent.getPriceMin() == 0) {
                    cb_donate.setEnabled(false);
                } else {
                    cb_donate.setEnabled(true);
                }

                et_pay.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        double newPrice;
                        if (s.toString().isEmpty()) {
                            tv_total.setText("-.");
                            cb_donate.setChecked(false);
                            cb_donate.setEnabled(false);
                            return;
                        } else {
                            newPrice = Double.parseDouble(s.toString());
                        }
                        if (newPrice == 0) {
                            cb_donate.setChecked(false);
                            cb_donate.setEnabled(false);
                        } else {
                            cb_donate.setEnabled(true);
                        }
                        double total = newPrice;
                        if (cb_donate.isChecked()) {
                            total = total * 1.01;
                        }
                        tv_total.setText(AppConstants.CURRENCY_FORMAT.format(total));
                    }
                });

                cb_donate.setOnCheckedChangeListener((compoundButton, b) -> {
                    double newPrice;
                    if (et_pay.getText().toString().isEmpty()) {
                        tv_total.setText("-.");
                        return;
                    } else {
                        newPrice = Double.parseDouble(et_pay.getText().toString());
                    }
                    double total = newPrice;
                    if (b) {
                        total = total * 1.01;
                    }
                    tv_total.setText(AppConstants.CURRENCY_FORMAT.format(total));
                });

                sw_include_note.setOnCheckedChangeListener((compoundButton, b) -> {
                    if (b) {
                        et_note.setVisibility(VISIBLE);
                        KeyboardUtils.showSoftInput(et_note, getContext());
                    } else {
                        et_note.setVisibility(GONE);
                        KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
                    }
                });

                CustomDialog orderDialog = new CustomDialog(getContext());
                orderDialog.setView(view);

                bt_cancel.setOnClickListener(view1 -> {
                    orderDialog.dismiss();
                    KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
                });
                bt_ok.setOnClickListener(v2 -> {
                    et_pay.setError(null);
                    View focusView = null;
                    String price = et_pay.getText().toString();
                    String note = et_note.getText().toString();
                    if (!mPresenter.confirmInscription(price, sw_include_note.isChecked(), note, communityEvent, cb_donate.isChecked())) {
                        switch (mPresenter.getErrorCode()) {
                            case AppConstants.INVALID_MIN_PRICE:
                                focusView = et_pay;
                                et_pay.setError(mPresenter.getError());
                                break;
                            case AppConstants.INVALID_MAX_PRICE:
                                focusView = et_pay;
                                et_pay.setError(mPresenter.getError());
                                break;
                            case AppConstants.EMPTY_TEXT_FIELD:
                                focusView = et_note;
                                et_note.setError(mPresenter.getError());
                                break;
                        }
                        if (focusView != null) {
                            focusView.requestFocus();
                        }
                    } else {
                        KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), view.findFocus());
                        orderDialog.dismiss();
                    }
                });
                ViewGroup layout = view.findViewById(R.id.li_inscription_confirm);
                LayoutTransition layoutTransition = layout.getLayoutTransition();
                layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
                orderDialog.show();
            });
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_community_event_name)
            TextView tv_community_event_name;
            @BindView(R.id.tv_community_event_date)
            TextView tv_community_event_date;
            @BindView(R.id.tv_community_event_attendees)
            TextView tv_community_event_attendees;
            @BindView(R.id.tv_price_min)
            TextView tv_price_min;
            @BindView(R.id.tv_price_max)
            TextView tv_price_max;
            @BindView(R.id.cv_attendee)
            CardView cv_attendee;
            @BindView(R.id.li_price_max)
            LinearLayout li_price_max;
            @BindView(R.id.iv_event_front)
            ImageView iv_event_front;
            @BindView(R.id.iv_currency_min)
            ImageView iv_currency_min;
            @BindView(R.id.cv_inscription)
            CardView cv_inscription;
            @BindView(R.id.iv_more)
            ImageView iv_more;
            @BindView(R.id.cv_event)
            CardView cv_event;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleCommunityEventItemList(List<CommunityEvent> communityEventList) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (CommunityEvent item : communityEventList) {
            itemList.add(new CommunityEventItemAdapter(item));
        }
        return itemList;
    }
}