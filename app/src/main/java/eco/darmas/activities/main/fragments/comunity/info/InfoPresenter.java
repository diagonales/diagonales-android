package eco.darmas.activities.main.fragments.comunity.info;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;

class InfoPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements InfoContracts.Presenter {

    private final InfoModel mModel;
    private final InfoView mView;

    InfoPresenter(InfoModel mModel, InfoView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoaded(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    mView.repaintUserCount();
                }
                break;
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    String getUserCount() {
        return String.valueOf(mModel.getUserCount() - 1);
    }
}