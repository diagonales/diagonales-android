package eco.darmas.activities.main.fragments.market.select.demands;

interface SelectableDemandsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}