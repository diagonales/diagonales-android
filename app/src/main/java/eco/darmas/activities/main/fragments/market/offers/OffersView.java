package eco.darmas.activities.main.fragments.market.offers;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Listing;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.utils.FlexibleUtils;
import eu.davidea.viewholders.FlexibleViewHolder;

class OffersView extends BaseFragmentView implements OffersContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private OffersPresenter mPresenter;
    private OffersAdapter flexibleOffersAdapter;
    private Unbinder unBinder;
    private boolean scrollTop;

    // toolbar
    @BindView(R.id.li_order)
    LinearLayout li_order;
    @BindView(R.id.rb_order)
    SimpleRatingBar rb_order;
    @BindView(R.id.iv_order)
    ImageView iv_order;
    @BindView(R.id.iv_direction)
    ImageView iv_direction;
    @BindView(R.id.iv_search_offers)
    ImageView iv_search_offers;
    @BindView(R.id.li_categories)
    LinearLayout li_categories;
    @BindView(R.id.cv_category)
    CardView cv_category;
    @BindView(R.id.tv_category_offers)
    TextView tv_category_offers;
    @BindView(R.id.tv_category_offers_count)
    TextView tv_category_offers_count;
    @BindView(R.id.li_add_offer)
    LinearLayout li_add_offer;

    // search box
    @BindView(R.id.li_search)
    LinearLayout li_search;
    @BindView(R.id.et_search)
    EditText et_search;

    // components
    @BindView(R.id.rv_offers)
    RecyclerView rv_offers;
    @BindView(R.id.tv_offers_none)
    TextView tv_offers_none;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    OffersView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.offers_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (OffersPresenter) presenter;
        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.refreshItems());
        swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor);
        setupAdapters();
        setData();
    }

    @OnClick(R.id.li_order)
    void onOrderClickListener() {
        mPresenter.onOrderClicked();
    }

    @OnTextChanged(R.id.et_search)
    void onSearchTextChangedListener(CharSequence s) {
        mPresenter.addSearchConstraint(s.toString());
        flexibleOffersAdapter.setFilter(s.toString());
    }

    @OnClick(R.id.iv_clear)
    void onSearchClearClickListener() {
        et_search.setText("");
        mPresenter.addSearchConstraint("");
        flexibleOffersAdapter.setFilter("");
    }

    @OnClick(R.id.li_categories)
    void onCategoryClickListener() {
        mPresenter.onCategoryClicked();
    }

    @OnClick(R.id.li_add_offer)
    void onAddOfferClickListener() {
        mPresenter.onAddOfferClicked();
    }

    @OnClick(R.id.iv_search_offers)
    void onSearchOffersClickListener() {
        if (li_search.getVisibility() == GONE) {
            et_search.setText("");
            li_search.setVisibility(VISIBLE);
            KeyboardUtils.showSoftInput(et_search, getContext());
        } else {
            KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), et_search);
            flexibleOffersAdapter.setFilter("");
            mPresenter.addSearchConstraint("");
            li_search.setVisibility(GONE);
        }
    }

    void setData() {
        tv_category_offers.setText(mPresenter.getSelectedCategoryName());
        if (mPresenter.getSelectedCategoryId() == 0) {
            et_search.setHint("Buscar en todas las categorías");
            cv_category.setCardElevation(0);
            cv_category.setCardBackgroundColor(0);
            tv_category_offers_count.setVisibility(View.INVISIBLE);
        } else {
            Spannable hint = CommonUtils.getBoldItalicTextSpan("Buscar en " + mPresenter.getSelectedCategoryName(), mPresenter.getSelectedCategoryName());
            et_search.setHint(hint);
            tv_category_offers_count.setText(mPresenter.getOfferListingCountText());
            cv_category.setCardElevation(5);
            cv_category.setCardBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            tv_category_offers_count.setVisibility(View.VISIBLE);
        }
        LinearLayout.LayoutParams arrowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        switch (mPresenter.getSelectedOrder()) {
            case AppConstants.BY_LISTING_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.listingRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.listingRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_LISTING_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.listingRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.listingRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_SKILL_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.categoryRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_SKILL_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.categoryRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.categoryRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_DESC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_MEMBER_RATING_ASC:
                iv_order.setVisibility(GONE);
                rb_order.setBorderColor(getResources().getColor(R.color.userRatingBar));
                rb_order.setFillColor(getResources().getColor(R.color.userRatingBar));
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                rb_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_PRICE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_darmas_currency);
                iv_order.setTranslationX(3);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_CREATION_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_access_time_white);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_DESC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_downward);
                arrowParams.gravity = Gravity.BOTTOM;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
            case AppConstants.BY_END_DATE_ASC:
                rb_order.setVisibility(GONE);
                iv_order.setImageResource(R.drawable.ic_date_end);
                iv_order.setTranslationX(-2);
                iv_direction.setImageResource(R.drawable.ic_arrow_upward);
                arrowParams.gravity = Gravity.TOP;
                iv_direction.setLayoutParams(arrowParams);
                iv_order.setVisibility(VISIBLE);
                break;
        }
    }

    private void setupAdapters() {
        rv_offers.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_offers = new LinearLayoutManager(getContext());
        rv_offers.setLayoutManager(lm_offers);
        rv_offers.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.listing, 15));// use a linear layout manager

        // Initialize the Adapter
        flexibleOffersAdapter = new OffersAdapter(getFlexibleOffersItemList(mPresenter.getOfferList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_offers.setAdapter(flexibleOffersAdapter);
        //flexibleOffersAdapter
                //.setNotifyMoveOfFilteredItems(true);
                //.setDisplayHeadersAtStartUp(true)
                //.setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    void dataSetChanged(boolean scrollTop) {
        setData();
        this.scrollTop = scrollTop;
        flexibleOffersAdapter.updateDataSet(getFlexibleOffersItemList(mPresenter.getOfferList()));
    }

    void stopRefreshAnimation() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onOfferItemClick(position);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_offers.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_offers_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
        if (scrollTop) {
            rv_offers.scrollToPosition(0);
        }
    }

    class OffersAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        OffersAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class OfferItemAdapter extends AbstractFlexibleItem<OfferItemAdapter.ViewHolder> {

        private Listing listing;

        OfferItemAdapter(Listing listing) {
            this.listing = listing;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof OfferItemAdapter) {
                Listing inItem = ((OfferItemAdapter) inObject).listing;
                return this.listing.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return listing.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.listing;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int id_creator = listing.getIdCreator();

            if (!DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mPresenter.getUser(id_creator).getDate()))) {
                viewHolder.iv_badge_new.setVisibility(View.VISIBLE);
            }

            if (flexibleOffersAdapter.hasFilter()) {
                FlexibleUtils.highlightText(viewHolder.tv_title, listing.getTitle(), flexibleOffersAdapter.getFilter(String.class));
            } else {
                viewHolder.tv_title.setText(listing.getTitle());
            }
            if (mPresenter.isInEventMode()) {
                viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_stand);
                viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                viewHolder.li_secondary_shipping.setVisibility(GONE);
                viewHolder.tv_address.setTextColor(getResources().getColor(R.color.textFair));
            } else {
                viewHolder.tv_address.setTextColor(getResources().getColor(R.color.primaryTextColor));
                switch (listing.getShippingMethod()) {
                    case Listing.ShippingMethod.PICKUP:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_location);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.li_secondary_shipping.setVisibility(GONE);
                        break;
                    case Listing.ShippingMethod.DELIVERY:
                        viewHolder.li_primary_shipping.setVisibility(GONE);
                        viewHolder.tv_secondary_shipping.setText(mPresenter.getSecondaryShippingText(listing));
                        viewHolder.li_secondary_shipping.setVisibility(VISIBLE);
                        break;
                    case Listing.ShippingMethod.PICKUP_AND_DELIVERY:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_location);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.tv_secondary_shipping.setText(mPresenter.getSecondaryShippingText(listing));
                        viewHolder.li_secondary_shipping.setVisibility(VISIBLE);
                        break;
                    case Listing.ShippingMethod.DIGITAL:
                        viewHolder.iv_primary_shipping.setImageResource(R.drawable.ic_digital);
                        viewHolder.li_primary_shipping.setVisibility(VISIBLE);
                        viewHolder.li_secondary_shipping.setVisibility(GONE);
                        break;
                }
            }
            viewHolder.tv_address.setText(mPresenter.getAddressText(listing));
            viewHolder.tv_name.setText(mPresenter.getUser(id_creator).getName());
            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(listing.getDate()), AppConstants.ABBREVIATED_DATE_FORMAT));
            viewHolder.tv_category.setText(mPresenter.getCategoryName(listing.getIdCategory()));

            viewHolder.rb_rating_1.setFillColor(getResources().getColor(R.color.listingRatingBar));
            viewHolder.rb_rating_1.setStarBackgroundColor(getResources().getColor(R.color.listingBackgroundRatingBar));
            viewHolder.rb_rating_1.setBorderColor(getResources().getColor(R.color.transparent));

            viewHolder.rb_rating_1.setRating(mPresenter.getListingRating(listing));
            viewHolder.tv_rating_1.setText("(" + String.valueOf(mPresenter.getListingRatingsSize(listing)) + ")");

            viewHolder.rb_rating_2.setFillColor(getResources().getColor(R.color.categoryRatingBar));
            viewHolder.rb_rating_2.setStarBackgroundColor(getResources().getColor(R.color.categoryBackgroundRatingBar));
            viewHolder.rb_rating_2.setBorderColor(getResources().getColor(R.color.transparent));

            viewHolder.rb_rating_2.setRating(mPresenter.getWorkRating(listing));
            viewHolder.tv_rating_2.setText("(" + String.valueOf(mPresenter.getWorkRatingsSize(listing)) + ")");

            viewHolder.li_rating_2.setVisibility(View.VISIBLE);

            // Set price
            if (listing.getRanged().equals("1")) {
                viewHolder.tv_price_min.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMin()));
                viewHolder.tv_price_min.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_min.setVisibility(View.VISIBLE);

                viewHolder.tv_divider.setVisibility(View.VISIBLE);

                viewHolder.tv_price_max.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMax()));
                viewHolder.tv_price_max.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_max.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tv_price_min.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMin()));
                viewHolder.tv_price_min.setVisibility(View.VISIBLE);
                viewHolder.iv_diag_min.setVisibility(View.VISIBLE);

                viewHolder.tv_divider.setVisibility(View.GONE);

                viewHolder.tv_price_max.setVisibility(View.GONE);
                viewHolder.iv_diag_max.setVisibility(View.GONE);
            }

            // Apply discount if any
            double discount = mPresenter.getEventDiscount(listing);
            if (discount > 0) {
                viewHolder.tv_price_min.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMin() - discount));
                viewHolder.tv_price_max.setText(AppConstants.CURRENCY_FORMAT.format(listing.getPriceMax() - discount));
                viewHolder.tv_discount_percent.setText(mPresenter.getPercentageDiscountText(listing, discount));
                viewHolder.cv_percent_discount.setVisibility(VISIBLE);
                //viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.textFair));
                //viewHolder.tv_divider.setTextColor(getResources().getColor(R.color.textFair));
                //viewHolder.tv_price_max.setTextColor(getResources().getColor(R.color.textFair));
            } else {
                viewHolder.cv_percent_discount.setVisibility(GONE);
                //viewHolder.tv_price_min.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
                //viewHolder.tv_divider.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
                //viewHolder.tv_price_max.setTextColor(getResources().getColor(R.color.secondaryAccentColor));
            }

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(id_creator), viewHolder.iv_profile);
            ImageUtils.loadListingImage(listing, viewHolder.iv_front);

            if (AppData.INSTANCE.getLoggedUser().getIdUser() != listing.getIdCreator()) {
                viewHolder.iv_profile.setOnClickListener(view1 -> {
                    Object[] parcelables = new Object[]{mPresenter.getUser(listing.getIdCreator())};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
            } else {
                viewHolder.iv_profile.setOnClickListener(view1 -> {
                    //
                });
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.iv_front)
            ImageView iv_front;
            @BindView(R.id.tv_title)
            TextView tv_title;
            @BindView(R.id.tv_name)
            TextView tv_name;
            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.tv_category)
            TextView tv_category;
            @BindView(R.id.iv_profile)
            ImageView iv_profile;
            @BindView(R.id.tv_price_min)
            TextView tv_price_min;
            @BindView(R.id.iv_diag_min)
            ImageView iv_diag_min;
            @BindView(R.id.tv_divider)
            TextView tv_divider;
            @BindView(R.id.tv_price_max)
            TextView tv_price_max;
            @BindView(R.id.iv_diag_max)
            ImageView iv_diag_max;
            @BindView(R.id.rb_rating_1)
            SimpleRatingBar rb_rating_1;
            @BindView(R.id.rb_rating_2)
            SimpleRatingBar rb_rating_2;
            @BindView(R.id.tv_rating_1)
            TextView tv_rating_1;
            @BindView(R.id.tv_rating_2)
            TextView tv_rating_2;
            @BindView(R.id.li_rating_2)
            LinearLayout li_rating_2;
            @BindView(R.id.iv_badge_verified)
            ImageView iv_badge_verified;
            @BindView(R.id.iv_badge_new)
            ImageView iv_badge_new;
            @BindView(R.id.tv_discount_percent)
            TextView tv_discount_percent;
            @BindView(R.id.cv_percent_discount)
            CardView cv_percent_discount;

            @BindView(R.id.li_primary_shipping)
            LinearLayout li_primary_shipping;
            @BindView(R.id.li_secondary_shipping)
            LinearLayout li_secondary_shipping;
            @BindView(R.id.tv_address)
            TextView tv_address;
            @BindView(R.id.tv_secondary_shipping)
            TextView tv_secondary_shipping;
            @BindView(R.id.iv_primary_shipping)
            ImageView iv_primary_shipping;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleOffersItemList(List<Listing> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Listing item : list) {
            itemList.add(new OfferItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}