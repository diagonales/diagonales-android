package eco.darmas.activities.main.fragments.messages.conversations;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class ConversationsFragment extends BaseFragment implements ConversationsContracts.Fragment {

    @Override
    protected ConversationsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new ConversationsModel(dataManager, savedInstanceState);
    }

    @Override
    protected ConversationsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new ConversationsPresenter((ConversationsModel) model, (ConversationsView) view);
    }

    @Override
    protected ConversationsView createView(Context context) {
        return new ConversationsView(context);
    }
}
