package eco.darmas.activities.main.fragments.market.select.offers;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Listing;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class SelectableOffersModel extends BaseFragmentModel implements SelectableOffersContracts.Model {

    private int eventId;
    private List<Listing> selectableOfferList = new ArrayList<>();

    SelectableOffersModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.eventId = arguments.getInt("eventId");
        loadSelectableOffersData().subscribe();
    }

    Single loadSelectableOffersData() {
        return Observable.create((ObservableOnSubscribe<Listing>) e -> {
            for (int i = getDataManager().getListingsMap().size() - 1; i > -1; i--) {
                Listing listing = getDataManager().getListingsMap().valueAt(i);
                if (listing.getIdEvent() >= 0 && listing.getIdEvent() != eventId) {
                    continue;
                }
                if (listing.getType() == Listing.Type.OFFER && listing.getIdCreator() == getLoggedUser().getIdUser()) {
                    e.onNext(listing);
                }
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(selectableOffers -> selectableOfferList = selectableOffers);
    }

    List<Listing> getSelectableOfferList() {
        return selectableOfferList;
    }

    SparseArray<JSONObject> getTransientUserSelectedListingsList() {
        return getDataManager().getTransientUserEventListings();
    }

    Listing getListing(int position) {
        return selectableOfferList.get(position);
    }

    int getEventId() {
        return eventId;
    }
}