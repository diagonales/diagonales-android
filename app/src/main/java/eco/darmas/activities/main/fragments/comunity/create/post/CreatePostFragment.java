package eco.darmas.activities.main.fragments.comunity.create.post;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class CreatePostFragment extends BaseFragment implements CreatePostContracts.Fragment {

    @Override
    protected CreatePostModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new CreatePostModel(dataManager, savedInstanceState);
    }

    @Override
    protected CreatePostPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new CreatePostPresenter((CreatePostModel) model, (CreatePostView) view);
    }

    @Override
    protected CreatePostView createView(Context context) {
        return new CreatePostView(context);
    }
}