package eco.darmas.activities.main.fragments.market.offers;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class OffersFragment extends BaseFragment implements OffersContracts.Fragment {

    @Override
    protected OffersModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new OffersModel(dataManager, savedInstanceState);
    }

    @Override
    protected OffersPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new OffersPresenter((OffersModel) model, (OffersView) view);
    }

    @Override
    protected OffersView createView(Context context) {
        return new OffersView(context);
    }
}
