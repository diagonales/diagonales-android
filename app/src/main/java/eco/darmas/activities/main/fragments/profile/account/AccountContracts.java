package eco.darmas.activities.main.fragments.profile.account;

interface AccountContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}