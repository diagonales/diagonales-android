package eco.darmas.activities.main.fragments.market.orders;

interface OrdersContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}