package eco.darmas.activities.main.fragments.wallet.transactions;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos._Transaction;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class TransactionsModel extends BaseFragmentModel implements TransactionsContracts.Model {

    private List<_Transaction> transactionList = new ArrayList<>();

    TransactionsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadData().subscribe();
    }

    Single loadData() {
        return Observable.create((ObservableOnSubscribe<_Transaction>) e -> {
            for (int i = getDataManager().getTransactionMap().size() - 1; i > -1; i--) {
                _Transaction transaction = getDataManager().getTransactionMap().valueAt(i);
                int eventMode = App.getInstance().getPreferencesHelper().getSelectedEventId();
                if (eventMode >= 0 && (eventMode != transaction.getIdEvent() || transaction.getType() == _Transaction.Type.INSCRIPTION)) {
                    continue;
                }
                if (transaction.getStatus() != _Transaction.Status.RECEIVED) {
                    continue;
                }
                e.onNext(transaction);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(transactions -> transactionList = transactions);
    }

    List<_Transaction> getTransactionList() {
        return transactionList;
    }

    Listing getListing(int listingId) {
        return getDataManager().getListingsMap().get(listingId);
    }

    CommunityEvent getCommunityEvent(int eventId) {
        return getDataManager().getCommunityEventMap().get(eventId);
    }
}
