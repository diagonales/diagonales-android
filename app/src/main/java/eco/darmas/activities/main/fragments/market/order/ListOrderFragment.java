package eco.darmas.activities.main.fragments.market.order;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class ListOrderFragment extends BaseFragment {

    @Override
    protected BaseFragmentModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new ListOrderModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected BaseFragmentPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new ListOrderPresenter(((ListOrderModel) model), ((ListOrderView) view));
    }

    @Override
    protected BaseFragmentView createView(Context context) {
        return new ListOrderView(context);
    }
}