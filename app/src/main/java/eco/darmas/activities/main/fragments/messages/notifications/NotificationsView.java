package eco.darmas.activities.main.fragments.messages.notifications;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;

class NotificationsView extends BaseFragmentView implements NotificationsContracts.View {

    private NotificationsPresenter mPresenter;
    private Unbinder unBinder;

    NotificationsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.notifications_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (NotificationsPresenter) presenter;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
