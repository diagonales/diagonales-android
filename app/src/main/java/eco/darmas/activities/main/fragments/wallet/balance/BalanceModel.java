package eco.darmas.activities.main.fragments.wallet.balance;

import android.os.Bundle;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class BalanceModel extends BaseFragmentModel implements BalanceContracts.Model {

    private List<_Transaction> transactionList = new ArrayList<>();

    BalanceModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
        loadData().subscribe();
    }

    Single loadData() {
        return Observable.create((ObservableOnSubscribe<_Transaction>) e -> {
            for (int i = getDataManager().getTransactionMap().size() - 1; i > -1; i--) {
                _Transaction transaction = getDataManager().getTransactionMap().valueAt(i);
                int eventMode = App.getInstance().getPreferencesHelper().getSelectedEventId();
                if (eventMode >= 0 && (eventMode != transaction.getIdEvent() || transaction.getType() == _Transaction.Type.INSCRIPTION)) {
                    continue;
                }
                if (transaction.getIdSender() != getLoggedUser().getIdUser() && transaction.getIdReceiver() != getLoggedUser().getIdUser()) {
                    continue;
                }
                if (transaction.getStatus() != _Transaction.Status.RECEIVED) {
                    continue;
                }
                e.onNext(transaction);
            }
            e.onComplete();
        })
                .subscribeOn(Schedulers.computation())
                .toList()
                .doOnSuccess(transactions -> transactionList = transactions);
    }

    void commitTransfer(_Transaction transaction, Callback<ResponseBody> callback) {
        App.getInstance().getNetworkService().getCallableAPI().executeTransaction(transaction.getIdSender(), transaction.getIdReceiver(), transaction.getConcept(), transaction.getAmount(), transaction.getIdListing(), transaction.getStatus(), transaction.getType(), transaction.getContribution(), transaction.getNote(), transaction.getIdEvent()).enqueue(callback);
    }

    void writeTransaction(_Transaction transaction) {
        User sender = getDataManager().getBoxStore().boxFor(User.class).get(transaction.getIdSender());
        User receiver = getDataManager().getBoxStore().boxFor(User.class).get(transaction.getIdReceiver());
        sender.setFunds(sender.getFunds() - transaction.getAmount());
        receiver.setFunds(receiver.getFunds() + transaction.getAmount());
        getDataManager().getBoxStore().runInTxAsync(() -> {
            getDataManager().getBoxStore().boxFor(_Transaction.class).put(transaction);
            getDataManager().getBoxStore().boxFor(User.class).put(sender);
            getDataManager().getBoxStore().boxFor(User.class).put(receiver);
        }, null);
    }

    List<_Transaction> getTransactionList() {
        return transactionList;
    }

    List<_Transaction> getLoggedUserOrdersList() {
        return getDataManager().getLoggedUserOrdersList();
    }

    double getCounterPartMaxAvailableIncome(int idCreator) {
        AppLogger.d(getDataManager().getUserMaxAvailableIncome(idCreator) + "");
        return getDataManager().getUserMaxAvailableIncome(idCreator);
    }

    Listing getListing(int listingId) {
        return getDataManager().getListingsMap().get(listingId);
    }

    CommunityEvent getCommunityEvent(int eventId) {
        return getDataManager().getCommunityEventMap().get(eventId);
    }

    SparseArray<Inscription> getEventInscriptions(int eventId) {
        return getDataManager().getEventInscriptionMap().get(eventId);
    }
}
