package eco.darmas.activities.main.fragments.profile.ratings;

interface RatingsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}