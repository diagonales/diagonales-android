package eco.darmas.activities.main.fragments.market.demands;

import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;

class DemandsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements DemandsContracts.Presenter {

    private final DemandsModel mModel;
    private final DemandsView mView;

    DemandsPresenter(DemandsModel mModel, DemandsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateData(boolean scrollTop) {
        getDisposables().add(mModel.loadDemandsData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> mView.dataSetChanged(scrollTop))
                .doAfterTerminate(() -> CommonUtils.showProgress(mView.getContext(), false))
                .doAfterTerminate(mView::stopRefreshAnimation)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.LISTINGS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.RATINGS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.CATEGORIES: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
            case DataLoadedEvent.EVENT_INSCRIPTIONS: {
                if (event.isSuccess()) {
                    updateData(false);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.LISTINGS_FILTERED: {
                updateData(true);
            }
            case FilterEvent.EVENT_MODE_ENTERED: {
                updateData(true);
            }
            case FilterEvent.EVENT_MODE_EXIT: {
                updateData(true);
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    String getCategoryName(int categoryId) {
        return mModel.getCategory(categoryId).getName();
    }

    List<Listing> getDemandList() {
        List<Listing> demandList = mModel.getDemandList();
        Collections.sort(demandList, getComparator());
        return demandList;
    }

    Comparator<Listing> getComparator() {
        return (t1, t2) -> {
            switch (mModel.getSelectedOrder()) {
                case AppConstants.BY_MEMBER_RATING_DESC:
                    return listingMemberRatingCompare(t1, t2, true);
                case AppConstants.BY_MEMBER_RATING_ASC:
                    return listingMemberRatingCompare(t1, t2, false);
                case AppConstants.BY_PRICE_DESC:
                    return listingPriceCompare(t1, t2, true);
                case AppConstants.BY_PRICE_ASC:
                    return listingPriceCompare(t1, t2, false);
                case AppConstants.BY_CREATION_DATE_DESC:
                    return listingCreationDateCompare(t1, t2, true);
                case AppConstants.BY_CREATION_DATE_ASC:
                    return listingCreationDateCompare(t1, t2, false);
                case AppConstants.BY_END_DATE_DESC:
                    return listingEndDateCompare(t1, t2, true);
                case AppConstants.BY_END_DATE_ASC:
                    return listingEndDateCompare(t1, t2, false);
            }
            return 0;
        };
    }

    private int listingCreationDateCompare(Listing t1, Listing t2, boolean recent) {
        Date descDate1 = DateUtils.parseFromBackendLocale(t1.getDate());
        Date descDate2 = DateUtils.parseFromBackendLocale(t2.getDate());
        if (recent) {
            if (descDate1.after(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (descDate1.before(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    private int listingPriceCompare(Listing t1, Listing t2, boolean lower) {
        double ascPrice1 = t1.getPriceMax();
        double ascPrice2 = t2.getPriceMax();
        if (t1.getShippingMethod() == Listing.ShippingMethod.DELIVERY) {
            ascPrice1 = ascPrice1 + t1.getShippingPrice();
        }
        if (t2.getShippingMethod() == Listing.ShippingMethod.DELIVERY) {
            ascPrice2 = ascPrice2 + t2.getShippingPrice();
        }
        if (lower) {
            if (ascPrice1 < ascPrice2) {
                return -1;
            } else if (ascPrice1 == ascPrice2) {
                listingMemberRatingCompare(t1, t2, true);
            } else if (ascPrice1 > ascPrice2) {
                return 1;
            }
        } else {
            if (ascPrice1 > ascPrice2) {
                return -1;
            } else if (ascPrice1 == ascPrice2) {
                listingMemberRatingCompare(t1, t2, true);
            } else if (ascPrice1 < ascPrice2) {
                return 1;
            }
        }
        return 0;
    }

    private int listingMemberRatingCompare(Listing t1, Listing t2, boolean higher) {
        float ascMemberRating1 = getUserRating(t1);
        float ascMemberRating2 = getUserRating(t2);
        if (higher) {
            if (ascMemberRating1 > ascMemberRating2) {
                return -1;
            } else if (ascMemberRating1 == ascMemberRating2) {
                return listingCreationDateCompare(t1, t2, true);
            } else if (ascMemberRating1 < ascMemberRating2) {
                return 1;
            }
        } else {
            if (ascMemberRating1 < ascMemberRating2) {
                return -1;
            } else if (ascMemberRating1 == ascMemberRating2) {
                return listingCreationDateCompare(t1, t2, true);
            } else if (ascMemberRating1 > ascMemberRating2) {
                return 1;
            }
        }
        return 0;
    }

    private int listingEndDateCompare(Listing t1, Listing t2, boolean recent) {
        //TODO design a better expiration system and methods for each user
        Date descDate1 = DateUtils.parseFromBackendLocale(t1.getDate());
        Date descDate2 = DateUtils.parseFromBackendLocale(t2.getDate());
        if (recent) {
            if (descDate1.after(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (descDate1.before(descDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    boolean onDemandItemClick(int position) {
        Object[] parcelables = new Object[]{mModel.getListing(position)};
        AppNav.navigateToActivity(AppConstants.LISTING_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
        return true;
    }

    float getUserRating(Listing listing) {
        float ratingPartial = 0;
        float ratingTotal = 0;
        SparseArray<Float> ratings = mModel.getUserRatingsMap().get(listing.getIdCreator(), new SparseArray<>());
        int size = ratings.size();

        for (int i = 0; i < size; i++) {
            ratingPartial = ratingPartial + ratings.valueAt(i);
        }
        if (ratingPartial > 0) {
            ratingTotal = ratingPartial / size;
        }
        return ratingTotal;
    }

    int getUserRatingsSize(Listing listing) {
        return mModel.getUserRatingsMap().get(listing.getIdCreator(), new SparseArray<>()).size();
    }

    void onCategoryClicked() {
        Object[] arguments = new Object[]{_Transaction.Type.DEMAND};
        AppNav.navigateToFragment(AppConstants.CATEGORY_FRAGMENT, (AppCompatActivity) mView.getContext(), arguments);
    }

    void onAddDemandClicked() {
        if (mModel.getLoggedUser().getActive().equals("0")) {
            CommonUtils.showInfoToast("No puedes publicar en este momento");
        }
        if (isInEventMode() && !isInscribedInEventMode()) {
            CommonUtils.showErrorToast("No puedes publicar sin haberte inscripto en este evento");
            return;
        }
        Object[] arguments = new Object[]{_Transaction.Type.DEMAND, mModel.getSelectedCategory()};
        AppNav.navigateToFragment(AppConstants.CREATE_LISTING_FRAGMENT, (AppCompatActivity) mView.getContext(), arguments);
    }

    void onOrderClicked() {
        AppNav.navigateToFragment(AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT, (AppCompatActivity) mView.getContext());
    }

    String getSelectedCategoryName() {
        return mModel.getSelectedCategoryName();
    }

    int getSelectedCategoryId() {
        return mModel.getSelectedCategory();
    }

    String getDemandListingCountText() {
        int demandListingsCount = 0;
        int eventMode = mModel.getEventModeId();
        try {
            JSONArray demandListings = new JSONArray();
            if (!mModel.getDemandListings().isEmpty()) {
                demandListings = new JSONArray(mModel.getDemandListings());
            }
            for (int j = 0; j < demandListings.length(); j++) {
                int listingId = demandListings.getInt(j);
                int eventIndex = mModel.getEventListingIdList().indexOfKey(eventMode);
                if ((eventMode >= 0 && eventIndex < 0) || (eventMode >= 0 && mModel.getEventListingIdList().get(eventMode).indexOfKey(listingId) < 0)) {
                    continue;
                }
                demandListingsCount++;
            }
        } catch (JSONException e) {
            AppLogger.e(e, null);
        }

        // Count event exclusive listings as well
        if (eventMode >= 0 && mModel.getEventListingJsonObjectMap().indexOfKey(eventMode) >= 0) {
            try {
                for (int i = 0; i < mModel.getEventListingJsonObjectMap().get(eventMode).size(); i++) {
                    JSONObject jsonObject = mModel.getEventListingJsonObjectMap().get(eventMode).valueAt(i);
                    if (mModel.getListingFromId(jsonObject.getInt("id_listing")) != null && mModel.getListingFromId(jsonObject.getInt("id_listing")).getIdEvent() == eventMode) {
                        demandListingsCount++;
                    }
                }
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
        }
        return "(" + demandListingsCount + ")";
    }

    void addSearchConstraint(String search) {
        mModel.addSearchConstraint(search);
        updateData(false);
    }

    int getSelectedOrder() {
        return mModel.getSelectedOrder();
    }

    String getAddressText(Listing listing) {
        if (mModel.getEventModeId() >= 0) {
            int eventIndex = mModel.getEventInscriptionMap().indexOfKey(mModel.getEventModeId());
            if (eventIndex >= 0) {
                int stand = mModel.getEventInscriptionMap().valueAt(eventIndex).get(listing.getIdCreator(), new Inscription()).getStand();
                if (stand > 0) {
                    return "Stand #" + stand;
                }
            }
            return "(Sin stand)";
        } else if (listing.getShippingMethod() == Listing.ShippingMethod.DIGITAL) {
            return "Envío digital";
        }
        return listing.getAddress();
    }

    String getSecondaryShippingText(Listing listing) {
        String secondaryShippingText = "Envío ";
        if (listing.getShippingMethod() == Listing.ShippingMethod.PICKUP_AND_DELIVERY) {
            secondaryShippingText = secondaryShippingText.concat("opcional, ");
        }
        if (listing.getShippingPrice() == 0) {
            secondaryShippingText = secondaryShippingText.concat("sin cargo");
        } else {
            secondaryShippingText = secondaryShippingText.concat("con cargo");
        }
        return secondaryShippingText;
    }

    void refreshItems() {
        mModel.refreshListings();
    }
}