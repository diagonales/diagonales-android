package eco.darmas.activities.main.fragments.messages.notifications;

interface NotificationsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}