package eco.darmas.activities.main.fragments.comunity.create.post;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.SparseArray;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Post;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class CreatePostModel extends BaseFragmentModel implements CreatePostContracts.Model {


    private File postImageFile;
    private Uri tempImageUri;

    CreatePostModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    void commitCreatePost(final String title, final String text, String pinned, Callback<ResponseBody> callback) {
        MultipartBody.Part body = null;
        if (postImageFile != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), postImageFile);
            body = MultipartBody.Part.createFormData("uploaded_file", postImageFile.getName(), reqFile);
        }

        RequestBody idCreatorBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getLoggedUser().getIdUser()));
        RequestBody idEventBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getEventModeId()));
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody postBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(0));
        RequestBody pinnedBody = RequestBody.create(MediaType.parse("text/plain"), pinned);

        App.getInstance().getNetworkService().getCallableAPI().createPost(idCreatorBody, idEventBody, titleBody, textBody, postBody, pinnedBody, body).enqueue(callback);

        /*@Override
        public void onProgress(long bytesWritten, long totalSize) {
            double mbWritten = (double) bytesWritten / 1048576;
            double mbTotalSize = (double) totalSize / 1048576;
            progress.setTitle("Cargando " + dfLoadImage.format(mbWritten) + "m de " + dfLoadImage.format(mbTotalSize) + "m");
            super.onProgress(bytesWritten, totalSize);
        }*/
    }

    void writePost(Post post) {
        getDataManager().getBoxStore().boxFor(Post.class).put(post);
    }

    File getPostImageFile() {
        Calendar cal = Calendar.getInstance();
        postImageFile = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/" + ("IMG_" + cal.getTimeInMillis() + ".jpg"));
        return postImageFile;
    }

    void setPostImageFile(String path) {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(App.getInstance().getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        postImageFile = new File(CommonUtils.getRealImagePath(photo));
    }

    Uri getTempImageUri() {
        return tempImageUri;
    }

    void setTempImageUri(Uri tempImageUri) {
        this.tempImageUri = tempImageUri;
    }

    SparseArray<CommunityEvent> getCommunityEventMap() {
        return getDataManager().getCommunityEventMap();
    }
}