package eco.darmas.activities.main.fragments.profile.ratings;

import android.os.Bundle;
import android.util.SparseArray;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;

class RatingsModel extends BaseFragmentModel implements RatingsContracts.Model {

    RatingsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    SparseArray<SparseArray<Float>> getUserRatingsMap() {
        return getDataManager().getUserRatingsMap();
    }

    SparseArray<SparseArray<SparseArray<Float>>> getWorkRatingsMap() {
        return getDataManager().getWorkRatingsMap();
    }

    SparseArray<SparseArray<Float>> getListingRatingsMap() {
        return getDataManager().getListingRatingsMap();
    }

    SparseArray<Listing> getListingsMap() {
        return getDataManager().getListingsMap();
    }

    SparseArray<Node<Category>> getCategoryNodeMap() {
        return getDataManager().getCategoryNodeMap();
    }
}
