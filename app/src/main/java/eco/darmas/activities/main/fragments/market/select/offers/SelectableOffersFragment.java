package eco.darmas.activities.main.fragments.market.select.offers;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class SelectableOffersFragment extends BaseFragment implements SelectableOffersContracts.Fragment {

    @Override
    protected SelectableOffersModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new SelectableOffersModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected SelectableOffersPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new SelectableOffersPresenter((SelectableOffersModel) model, (SelectableOffersView) view);
    }

    @Override
    protected SelectableOffersView createView(Context context) {
        return new SelectableOffersView(context);
    }
}
