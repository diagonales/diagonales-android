package eco.darmas.activities.main.fragments.wallet.balance;

interface BalanceContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}