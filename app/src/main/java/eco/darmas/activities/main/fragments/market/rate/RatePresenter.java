package eco.darmas.activities.main.fragments.market.rate;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import eco.darmas.AppConstants;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.pojos.Rating;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class RatePresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements RateContracts.Presenter {

    private final RateModel mModel;
    private final RateView mView;

    RatePresenter(RateModel mModel, RateView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    void sendRating(final float ratingFloat, final String comment, final float ratingFloat2, final String comment2) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
        }

        Rating rating1;
        Rating rating2;
        rating1 = new Rating()
                .withIdSender(mModel.getLoggedUser().getIdUser())
                .withRating(ratingFloat)
                .withComment(comment)
                .withDate(DateUtils.getTimeStamp())
                .withIdReceiver(mModel.getReceiverId())
                .withIdTarget(mModel.getTargetId())
                .withType(mModel.getRatingType());
        if (ratingFloat2 > 0) {
            rating2 = new Rating()
                    .withIdSender(mModel.getLoggedUser().getIdUser())
                    .withRating(ratingFloat2)
                    .withComment(comment2)
                    .withDate(DateUtils.getTimeStamp())
                    .withIdReceiver(mModel.getReceiverId())
                    .withIdTarget(mModel.getCategoryId())
                    .withType(Rating.Type.WORK);
        } else {
            rating2 = null;
        }
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.RATING_MODIFIED);
        mModel.commitSendRating(rating1, rating2, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            rating1.setIdRating((int) responseJSON.get("id1"));
                            if (rating2 != null) {
                                rating2.setIdRating((int) responseJSON.get("id2"));
                            }
                            mModel.writeRating(rating1, rating2);
                            event.setSuccess(true);
                            if (rating2 == null) {
                                event.setDialogTitle("Valoración enviada!");
                                event.setDialogMessage("Gracias por ayudar a mejorar nuestra comunidad :)");
                            } else {
                                event.setDialogTitle("Valoraciónes enviadas!");
                                event.setDialogMessage("Gracias por ayudar a mejorar nuestra comunidad :)");
                            }

                        } else {
                            if (rating2 == null) {
                                event.setDialogTitle("La valoración no pudo ser enviada");
                            } else {
                                event.setDialogTitle("Las valoraciónes no pudieron ser enviadas");
                            }
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                        if (rating2 == null) {
                            event.setDialogTitle("La valoración no pudo ser enviada");
                            event.setDialogMessage("Por favor contacta un administrador");
                        } else {
                            event.setDialogTitle("Las valoraciónes no pudieron ser enviadas");
                            event.setDialogMessage("Por favor contacta un administrador");
                        }
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.e(t, null);
                if (rating2 == null) {
                    event.setDialogTitle("La valoración no pudo ser enviada");
                    event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                } else {
                    event.setDialogTitle("Las valoraciónes no pudieron ser enviadas");
                    event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                }
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, por favor vuelve a intentarlo mas tarde");

                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.dismissFragment(mView, AppConstants.RATE_FRAGMENT_TAG, true);
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    float getRating() {
        return mModel.getRating();
    }

    int getRatingType() {
        return mModel.getRatingType();
    }

    String getComment() {
        return mModel.getComment();
    }

    float getRating2() {
        return mModel.getRating2();
    }

    String getComment2() {
        return mModel.getComment2();
    }
}