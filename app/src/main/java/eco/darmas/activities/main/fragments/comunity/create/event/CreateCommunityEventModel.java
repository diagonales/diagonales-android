package eco.darmas.activities.main.fragments.comunity.create.event;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class CreateCommunityEventModel extends BaseFragmentModel {

    private File communityEventImageFile;
    private Uri tempImageUri;

    CreateCommunityEventModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }

    void commitCreateCommunityEvent(final int type, final String date_created, String title, final String text, final String date_start, final String date_end, final String address, final double price_min, final double price_max, final int ranged, final int vacants, final int limited, final String date_inscription_open, final String date_inscription_close, Callback<ResponseBody> callback) {
        MultipartBody.Part body = null;
        if (communityEventImageFile != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), communityEventImageFile);
            body = MultipartBody.Part.createFormData("uploaded_file", communityEventImageFile.getName(), reqFile);
        }

        RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(type));
        RequestBody idCreatorBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getLoggedUser().getIdUser()));
        RequestBody dateCreatedBody = RequestBody.create(MediaType.parse("text/plain"), date_created);
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody dateStartBody = RequestBody.create(MediaType.parse("text/plain"), date_start);
        RequestBody dateEndBody = RequestBody.create(MediaType.parse("text/plain"), date_end);
        RequestBody addressBody = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody priceMinBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(price_min));
        RequestBody priceMaxBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(price_max));
        RequestBody rangedBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ranged));
        RequestBody vacantsBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(vacants));
        RequestBody limitedBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(limited));
        RequestBody dateInscriptionOpenBody = null;
        RequestBody dateInscriptionCloseBody = null;
        if (date_inscription_open != null && date_inscription_close != null) {
            dateInscriptionOpenBody = RequestBody.create(MediaType.parse("text/plain"), date_inscription_open);
            dateInscriptionCloseBody = RequestBody.create(MediaType.parse("text/plain"), date_inscription_close);
        }

        App.getInstance().getNetworkService().getCallableAPI().createCommunityEvent(typeBody, idCreatorBody, dateCreatedBody, titleBody, textBody, dateStartBody, dateEndBody, addressBody, priceMinBody, priceMaxBody, rangedBody, vacantsBody, limitedBody, dateInscriptionOpenBody, dateInscriptionCloseBody, body).enqueue(callback);

        /*@Override
        public void onProgress(long bytesWritten, long totalSize) {
            double mbWritten = (double) bytesWritten / 1048576;
            double mbTotalSize = (double) totalSize / 1048576;
            progress.setTitle("Cargando " + dfLoadImage.format(mbWritten) + "m de " + dfLoadImage.format(mbTotalSize) + "m");
            super.onProgress(bytesWritten, totalSize);
        }*/
    }

    void writeCommunityEvent(CommunityEvent communityEvent) {
        getDataManager().getBoxStore().boxFor(CommunityEvent.class).put(communityEvent);
    }

    File getCommunityEventImageFile() {
        Calendar cal = Calendar.getInstance();
        communityEventImageFile = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/" + ("IMG_" + cal.getTimeInMillis() + ".jpg"));
        return communityEventImageFile;
    }

    void setCommunityEventImageFile(String path) {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(App.getInstance().getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        communityEventImageFile = new File(CommonUtils.getRealImagePath(photo));
    }

    Uri getTempImageUri() {
        return tempImageUri;
    }

    void setTempImageUri(Uri tempImageUri) {
        this.tempImageUri = tempImageUri;
    }
}