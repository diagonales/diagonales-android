package eco.darmas.activities.main.fragments.wallet.balance;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Spannable;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.AppNav;
import eco.darmas.R;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.User;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.KeyboardUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class BalanceView extends BaseFragmentView implements BalanceContracts.View, FlexibleAdapter.OnUpdateListener, FlexibleAdapter.OnItemClickListener {

    private BalancePresenter mPresenter;
    private TransactionsAdapter flexibleTransactionsAdapter;
    private Unbinder unBinder;

    @BindView(R.id.tv_available_funds)
    TextView tv_available_funds;
    @BindView(R.id.tv_user_transactions_none)
    TextView tv_user_transactions_none;
    @BindView(R.id.li_reserve_funds)
    LinearLayout li_reserve_funds;
    @BindView(R.id.tv_reserve_funds)
    TextView tv_reserve_funds;
    @BindView(R.id.li_incoming_funds)
    LinearLayout li_incoming_funds;
    @BindView(R.id.tv_incoming_funds)
    TextView tv_incoming_funds;
    @BindView(R.id.bt_transfer)
    Button bt_transfer;
    @BindView(R.id.rv_user_transactions)
    RecyclerView rv_user_transactions;
    @BindView(R.id.sb_balance)
    SeekBar sb_balance;
    @BindView(R.id.tv_vol)
    TextView tv_vol;

    BalanceView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.balance_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (BalancePresenter) presenter;
        setData();
        setupAdapters();
    }

    void setData() {
        if (mPresenter.hasReserveFunds()) {
            li_reserve_funds.setVisibility(VISIBLE);
            tv_reserve_funds.setText(mPresenter.getReserveFunds());
        } else {
            li_reserve_funds.setVisibility(INVISIBLE);
        }
        if (mPresenter.hasIncomingFunds()) {
            li_incoming_funds.setVisibility(VISIBLE);
            tv_incoming_funds.setText(mPresenter.getIncomingFunds());
        } else {
            li_incoming_funds.setVisibility(INVISIBLE);
        }
        if (mPresenter.isInEventMode()) {
            switch (mPresenter.getEventType(mPresenter.getEventModeId())) {
                case CommunityEvent.Type.FAIR:
                    bt_transfer.setBackgroundResource(R.drawable.transfer_button_fair);
                    break;
                case CommunityEvent.Type.MEETING:
                    bt_transfer.setBackgroundResource(R.drawable.transfer_button_fair);
                    break;
                case CommunityEvent.Type.PARTY:
                    bt_transfer.setBackgroundResource(R.drawable.transfer_button_fair);
                    break;
                case CommunityEvent.Type.STAFF:
                    bt_transfer.setBackgroundResource(R.drawable.transfer_button_fair);
                    break;
            }
        } else {
            bt_transfer.setBackgroundResource(R.drawable.transfer_button);
        }
        tv_available_funds.setText(mPresenter.getFundsText());
        tv_vol.setText(mPresenter.getVol());

        sb_balance.setOnTouchListener((v2, event) -> true);
        /*sb_balance.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.accentColor), PorterDuff.Mode.MULTIPLY));
        sb_balance.getIndeterminateDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.accentColor), PorterDuff.Mode.MULTIPLY));*/
        sb_balance.setMax(AppData.INSTANCE.getLoggedUser().getPaymentMargin() * 2);
        ValueAnimator anim = ValueAnimator.ofInt(sb_balance.getProgress(), (int) AppData.INSTANCE.getLoggedUser().getFunds() + AppData.INSTANCE.getLoggedUser().getPaymentMargin());
        anim.setDuration(1000);
        anim.addUpdateListener(animation -> {
            int animProgress = (Integer) animation.getAnimatedValue();
            if (sb_balance != null) {
                sb_balance.setProgress(animProgress);
            }
        });
        anim.start();
    }

    private void setupAdapters() {
        rv_user_transactions.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_announces = new LinearLayoutManager(getContext());
        rv_user_transactions.setLayoutManager(lm_announces);
        rv_user_transactions.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.user_transaction, 15));

        // Initialize the Adapter
        flexibleTransactionsAdapter = new TransactionsAdapter(getFlexibleTransactionsItemList(mPresenter.getTransactionList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_user_transactions.setAdapter(flexibleTransactionsAdapter);
    }

    @OnClick(R.id.ib_qr)
    void onQrClickListener() {
        mPresenter.onQrClicked();
    }

    @OnClick(R.id.bt_transfer)
    void onPayClickListener(View v) {
        if (mPresenter.isInEventMode() && !mPresenter.isInscribedInEventMode()) {
            CommonUtils.showErrorToast("No podes enviar ni recibir transferencias desde este evento sin estar inscripto");
            return;
        }
        LayoutInflater li = LayoutInflater.from(getContext());
        final View pView = li.inflate(R.layout.send_transfer, (ViewGroup) v.getRootView(), false);

        CustomDialog payDialog = new CustomDialog(getContext());
        payDialog.setView(pView);

        final EditText et_concept = pView.findViewById(R.id.et_concept);
        final EditText et_amount = pView.findViewById(R.id.et_amount);
        final TextView tv_receiver = pView.findViewById(R.id.tv_receiver);
        final ImageView iv_account_receiver = pView.findViewById(R.id.iv_account_receiver);
        final Spinner sp_users = pView.findViewById(R.id.sp_users);
        final ImageView btnAdd1 = pView.findViewById(R.id.iv_send);
        final ImageView btnAdd2 = pView.findViewById(R.id.iv_cancel);
        final CheckBox cb_donate = pView.findViewById(R.id.cb_donate);
        final ConstraintLayout cl_transfer = pView.findViewById(R.id.cl_transfer);
        final SwitchCompat sw_include_note = pView.findViewById(R.id.sw_include_note);
        final EditText et_note = pView.findViewById(R.id.et_note);

        iv_account_receiver.setImageResource(R.drawable.ic_account_image);
        tv_receiver.setText("Selecciona destinatario...");
        tv_receiver.setOnClickListener(view -> sp_users.performClick());

        sw_include_note.setOnCheckedChangeListener((compoundButton, b) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition(cl_transfer);
            }
            if (b) {
                et_note.setVisibility(VISIBLE);
                KeyboardUtils.showSoftInput(et_note, getContext());
            } else {
                et_note.setVisibility(GONE);
                KeyboardUtils.hideSoftInput((AppCompatActivity) getContext(), pView.findFocus());
            }
        });
        btnAdd1.setOnClickListener(v1 -> {
            if (sp_users.getSelectedItemPosition() == 0) {
                CommonUtils.showInfoToast("Debes seleccionar un destinatario");
                return;
            }
            CommonUtils.StringWithTag s = (CommonUtils.StringWithTag) sp_users.getSelectedItem();
            int id_receiver = (int) s.tag;
            String concept = "Transferencia sin concepto";
            if (et_amount.getText().toString().trim().length() != 0) {
                double amount = Double.parseDouble(et_amount.getText().toString());
                double contribution = 0;
                if (cb_donate.isChecked()) {
                    contribution = amount * 0.01;
                    amount = amount + contribution;
                }
                if (amount > 0) {
                    String note = et_note.getText().toString().trim();
                    if (sw_include_note.isChecked() && note.isEmpty()) {
                        CommonUtils.showErrorToast("La nota adicional no esta escrita");
                        return;
                    }
                    if ((AppData.INSTANCE.getLoggedUser().getFunds() - amount) >= (0 - AppData.INSTANCE.getLoggedUser().getPaymentMargin())) {
                        if (amount <= mPresenter.getCounterPartMaxAvailableIncome(id_receiver)) {

                            if (et_concept.getText().toString().trim().length() != 0) {
                                concept = et_concept.getText().toString();
                            }
                            if (mPresenter.isInEventMode() && mPresenter.getEventInscriptions(mPresenter.getEventModeId()).indexOfKey(id_receiver) < 0) {
                                CommonUtils.showErrorToast("El usuario seleccionado no esta inscripto en este evento");
                                return;
                            }
                            mPresenter.executeTransaction(id_receiver, concept, amount, contribution, note, mPresenter.getEventModeId());
                            KeyboardUtils.hideSoftInput((MainActivity) getContext(), pView.findFocus());
                            payDialog.dismiss();
                        } else {
                            CommonUtils.showErrorToast("El usuario seleccionado no cuenta con margen suficiente para recibir esta transferencia");
                        }
                    } else {
                        CommonUtils.showErrorToast("Esta transferencia excedería tu margen de saldo actual: (-" + AppData.INSTANCE.getLoggedUser().getPaymentMargin() + " +" + AppData.INSTANCE.getLoggedUser().getPaymentMargin() + ")");
                    }
                } else {
                    CommonUtils.showWarningToast("Ingresa un valor mayor a 0");
                }
            } else {
                CommonUtils.showWarningToast("Ingresa una cantidad");
            }
        });

        btnAdd2.setOnClickListener(view -> {
            KeyboardUtils.hideSoftInput((MainActivity) getContext(), pView.findFocus());
            payDialog.dismiss();
        });

        sp_users.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, mPresenter.getNameList()));
        sp_users.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                KeyboardUtils.hideSoftInput((MainActivity) getContext(), pView.findFocus());
                CommonUtils.StringWithTag s = (CommonUtils.StringWithTag) parent.getItemAtPosition(pos);

                int selected_user_id = (int) s.tag;
                tv_receiver.setText(s.toString());
                ImageUtils.loadRoundedProfileImage(mPresenter.getUser(selected_user_id), iv_account_receiver);
            }

            @Override
            public void onNothingSelected(AdapterView parent) {
                KeyboardUtils.hideSoftInput((MainActivity) getContext(), pView.findFocus());
            }
        });
        iv_account_receiver.setOnClickListener(v2 -> sp_users.performClick());
        payDialog.show();
    }

    void dataSetChanged() {
        setData();
        flexibleTransactionsAdapter.updateDataSet(getFlexibleTransactionsItemList(mPresenter.getTransactionList()));
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onTransactionItemClick(position);
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_user_transactions.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_user_transactions_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    class TransactionsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        TransactionsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class TransactionItemAdapter extends AbstractFlexibleItem<TransactionItemAdapter.ViewHolder> {

        private _Transaction transaction;

        TransactionItemAdapter(_Transaction transaction) {
            this.transaction = transaction;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof TransactionItemAdapter) {
                _Transaction inItem = ((TransactionItemAdapter) inObject).transaction;
                return this.transaction.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return transaction.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.user_transaction;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            Listing listing = mPresenter.getListing(transaction.getIdListing());
            CommunityEvent communityEvent = mPresenter.getCommunityEvent(transaction.getIdEvent());

            if (transaction.getType() == _Transaction.Type.TRANSFER) {
                ImageUtils.loadTransferImage(viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(null);
            } else if (transaction.getType() == _Transaction.Type.INSCRIPTION){
                ImageUtils.loadTransactionBackgroundImage(communityEvent, viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(view -> mPresenter.onConceptClicked(communityEvent));
            } else {
                ImageUtils.loadTransactionBackgroundImage(listing, viewHolder.iv_listing_front);
                viewHolder.tv_concept_left.setOnClickListener(view -> mPresenter.onConceptClicked(listing));
            }

            if (!transaction.getNote().isEmpty()) {
                viewHolder.bt_notes.setVisibility(VISIBLE);
                viewHolder.bt_notes.setOnClickListener(view -> CommonUtils.showDialog(getContext(), "Notas adicionales:", transaction.getNote()));
            } else {
                viewHolder.bt_notes.setVisibility(GONE);
            }

            viewHolder.tv_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(transaction.getDate()), AppConstants.ABBREVIATED_DATE_FORMAT));
            viewHolder.tv_id_right.setText("ID: " + transaction.getIdTransaction());

            int paymentSenderId;
            int paymentReceiverId;
            if (transaction.getType() != _Transaction.Type.DEMAND) {
                paymentSenderId = transaction.getIdSender();
                paymentReceiverId = transaction.getIdReceiver();
            } else {
                paymentSenderId = transaction.getIdReceiver();
                paymentReceiverId = transaction.getIdSender();
            }

            String amount;
            Spannable amountSpan;
            if (paymentSenderId == AppData.INSTANCE.getLoggedUser().getIdUser()) {
                amount = "-".concat(AppConstants.CURRENCY_FORMAT.format(transaction.getAmount()));
            } else {
                amount = "+".concat(AppConstants.CURRENCY_FORMAT.format(transaction.getAmount()));

            }

            String contribution = "";
            Spannable contributionSpan;
            if (transaction.getContribution() > 0) {
                contribution = String.valueOf(transaction.getContribution());
                viewHolder.li_contribution.setVisibility(VISIBLE);
            } else {
                viewHolder.li_contribution.setVisibility(GONE);
            }

            String shipping = "";
            Spannable shippingSpan;
            if (transaction.getShippingPrice() > 0) {
                shipping = String.valueOf(transaction.getShippingPrice());
                viewHolder.li_delivery.setVisibility(VISIBLE);
            } else {
                viewHolder.li_delivery.setVisibility(GONE);
            }

            if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.textFair));
                contributionSpan = CommonUtils.getOrderConceptTextSpanNoListing(contribution, contribution, getResources().getColor(R.color.textFair));
                shippingSpan = CommonUtils.getOrderConceptTextSpanNoListing(shipping, shipping, getResources().getColor(R.color.textFair));
                viewHolder.iv_currency.setImageResource(R.drawable.ic_darmas_currency_fair);
                viewHolder.iv_currency_contribution.setImageResource(R.drawable.ic_darmas_currency_fair);
                viewHolder.iv_currency_delivery.setImageResource(R.drawable.ic_darmas_currency_fair);
            } else {
                amountSpan = CommonUtils.getOrderConceptTextSpanNoListing(amount, amount, getResources().getColor(R.color.secondaryAccentColor));
                contributionSpan = CommonUtils.getOrderConceptTextSpanNoListing(contribution, contribution, getResources().getColor(R.color.secondaryAccentColor));
                shippingSpan = CommonUtils.getOrderConceptTextSpanNoListing(shipping, shipping, getResources().getColor(R.color.secondaryAccentColor));
                viewHolder.iv_currency.setImageResource(R.drawable.ic_darmas_currency);
                viewHolder.iv_currency_contribution.setImageResource(R.drawable.ic_darmas_currency);
                viewHolder.iv_currency_delivery.setImageResource(R.drawable.ic_darmas_currency);
            }
            viewHolder.tv_amount.setText(amountSpan);
            viewHolder.tv_contribution_price.setText(contributionSpan);
            viewHolder.tv_delivery_price.setText(shippingSpan);

            String quantity = "";
            if (transaction.getQuantity() > 1) {
                quantity = "x" + String.valueOf(transaction.getQuantity()) + " ";
            }
            String concept = quantity + "\"" + transaction.getConcept() + "\"";
            Spannable conceptSpan;
            if ((listing != null || communityEvent != null) && transaction.getType() != _Transaction.Type.TRANSFER) {
                if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpan(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                }
            } else {
                if (transaction.getType() != _Transaction.Type.INSCRIPTION && transaction.getIdEvent() >= 0) {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.textFair));
                } else {
                    conceptSpan = CommonUtils.getOrderConceptTextSpanNoListing(concept, "\"" + transaction.getConcept() + "\"", getResources().getColor(R.color.secondaryAccentColor));
                }
            }

            if (paymentSenderId == AppData.INSTANCE.getLoggedUser().getIdUser()) {
                viewHolder.guide_sketch_start.setGuidelinePercent(0.25f);
                viewHolder.guide_sketch_mid.setGuidelinePercent(0.55f);
                viewHolder.guide_sketch_end.setGuidelinePercent(0.95f);
                viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_forward_white);
                viewHolder.tv_right_name.setText(mPresenter.getUser(paymentReceiverId).getName());
                viewHolder.tv_concept_left.setText(conceptSpan);
                viewHolder.tv_concept_left.setVisibility(View.VISIBLE);

                ImageUtils.loadRoundedProfileImage(mPresenter.getUser(paymentReceiverId), viewHolder.iv_right);

                viewHolder.iv_right.setOnClickListener(view1 -> {
                    User contact = mPresenter.getUser(paymentReceiverId);
                    Object[] parcelables = new Object[]{contact};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> viewHolder.iv_right.performClick());
            } else {
                viewHolder.guide_sketch_start.setGuidelinePercent(0.05f);
                viewHolder.guide_sketch_mid.setGuidelinePercent(0.40f);
                viewHolder.guide_sketch_end.setGuidelinePercent(0.80f);
                viewHolder.tv_right_name.setText(mPresenter.getUser(paymentSenderId).getName());
                viewHolder.iv_arrow.setImageResource(R.drawable.ic_arrow_back_white);
                viewHolder.tv_concept_left.setText(conceptSpan);
                viewHolder.tv_concept_left.setVisibility(View.VISIBLE);

                ImageUtils.loadRoundedProfileImage(mPresenter.getUser(paymentSenderId), viewHolder.iv_right);

                viewHolder.iv_right.setOnClickListener(view1 -> {
                    User contact = mPresenter.getUser(paymentSenderId);
                    Object[] parcelables = new Object[]{contact};
                    AppNav.navigateToActivity(AppConstants.USER_SUMMARY_ACTIVITY, (MainActivity) getContext(), parcelables);
                });
                viewHolder.tv_right_name.setOnClickListener(view1 -> viewHolder.iv_right.performClick());
            }
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_date)
            TextView tv_date;
            @BindView(R.id.tv_amount)
            TextView tv_amount;
            @BindView(R.id.iv_currency)
            ImageView iv_currency;
            @BindView(R.id.iv_arrow)
            ImageView iv_arrow;
            @BindView(R.id.tv_concept_left)
            TextView tv_concept_left;
            @BindView(R.id.iv_right)
            ImageView iv_right;
            @BindView(R.id.tv_id_right)
            TextView tv_id_right;
            @BindView(R.id.tv_right_name)
            TextView tv_right_name;
            @BindView(R.id.iv_listing_front)
            ImageView iv_listing_front;
            @BindView(R.id.li_contribution)
            LinearLayout li_contribution;
            @BindView(R.id.iv_currency_contribution)
            ImageView iv_currency_contribution;
            @BindView(R.id.tv_contribution_price)
            TextView tv_contribution_price;
            @BindView(R.id.li_delivery)
            LinearLayout li_delivery;
            @BindView(R.id.iv_currency_delivery)
            ImageView iv_currency_delivery;
            @BindView(R.id.tv_delivery_price)
            TextView tv_delivery_price;
            @BindView(R.id.guide_sketch_start)
            Guideline guide_sketch_start;
            @BindView(R.id.guide_sketch_mid)
            Guideline guide_sketch_mid;
            @BindView(R.id.guide_sketch_end)
            Guideline guide_sketch_end;
            @BindView(R.id.bt_notes)
            FloatingActionButton bt_notes;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleTransactionsItemList(List<_Transaction> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (_Transaction item : list) {
            itemList.add(new TransactionItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
