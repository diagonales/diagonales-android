package eco.darmas.activities.main.fragments.market.select;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.activities.main.fragments.market.select.demands.SelectableDemandsFragment;
import eco.darmas.activities.main.fragments.market.select.offers.SelectableOffersFragment;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.KeyboardUtils;

@SuppressLint("ViewConstructor")
class SelectableListingsView extends BaseFragmentView implements SelectableListingsContracts.View {

    private SelectableListingsPresenter mPresenter;
    private Unbinder unBinder;
    private FragmentManager fm;

    @BindView(R.id.vp_selectable_listings)
    ViewPager vp_selectable_listings;
    PagerAdapter selectableListingsPagerAdapter;
    @BindView(R.id.tl_selectable_listings)
    TabLayout tl_selectable_listings;

    SelectableListingsView(Context context, FragmentManager fm) {
        super(context);
        this.fm = fm;
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.selectable_listings_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (SelectableListingsPresenter) presenter;

        tl_selectable_listings.setupWithViewPager(vp_selectable_listings);
        selectableListingsPagerAdapter = new SelectableListingsPagerAdapter(fm);
        vp_selectable_listings.setAdapter(selectableListingsPagerAdapter);
        vp_selectable_listings.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    Fragment fragment = ((AppCompatActivity) getContext()).getSupportFragmentManager().findFragmentByTag(AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT_TAG);
                    KeyboardUtils.hideSoftInputFromFragment(getContext(), fragment);
                }
            }
        });

        setData();
    }

    @OnClick(R.id.bt_confirm)
    void onConfirmClickListener() {
        mPresenter.onConfirmClicked();
    }

    @OnClick(R.id.bt_cancel)
    void onCancelClickListener() {
        CommonUtils.dismissFragment(this, AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT_TAG, false);
    }

    void setData() {
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }

    class SelectableListingsPagerAdapter extends FragmentStatePagerAdapter {

        SelectableListingsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    SelectableOffersFragment selectableOffersFragment = new SelectableOffersFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("eventId", mPresenter.getEventId());
                    selectableOffersFragment.setArguments(bundle);
                    return selectableOffersFragment;
                }
                case 1: {
                    SelectableDemandsFragment selectableDemandsFragment = new SelectableDemandsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("eventId", mPresenter.getEventId());
                    selectableDemandsFragment.setArguments(bundle);
                    return selectableDemandsFragment;
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: {
                    return "Tus ofertas";
                }
                case 1: {
                    return "Tus demandas";
                }
            }
            return null;
        }
    }
}
