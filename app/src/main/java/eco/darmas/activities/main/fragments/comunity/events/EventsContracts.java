package eco.darmas.activities.main.fragments.comunity.events;

interface EventsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}