package eco.darmas.activities.main.fragments.profile.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.utils.ImageUtils;

class AccountView extends BaseFragmentView implements AccountContracts.View {

    private AccountPresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.iv_settings)
    ImageView iv_settings;
    @BindView(R.id.tv_settings)
    TextView tv_settings;
    @BindView(R.id.iv_logout)
    ImageView iv_logout;
    @BindView(R.id.tv_logout)
    TextView tv_logout;
    @BindView(R.id.iv_profile)
    ImageView iv_profile;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_join_date)
    TextView tv_join_date;
    @BindView(R.id.iv_badge_admin)
    ImageView iv_badge_admin;
    @BindView(R.id.iv_badge_verified)
    ImageView iv_badge_verified;
    @BindView(R.id.iv_badge_new_member)
    ImageView iv_badge_new_member;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_margins)
    TextView tv_margins;

    AccountView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.acount_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (AccountPresenter) presenter;
    }

    void updateView() {
        ImageUtils.loadRoundedProfileImage(AppData.INSTANCE.getLoggedUser(), iv_profile);
        mPresenter.getDisposables().add(mPresenter.getNameText()
                .doOnSuccess(name -> tv_name.setText(name))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getEmailText()
                .doOnSuccess(email -> tv_email.setText(email))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getAddressText()
                .doOnSuccess(addressText -> tv_address.setText(addressText))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getPhoneText()
                .doOnSuccess(phoneText -> tv_phone.setText(phoneText))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getDescriptionText()
                .doOnSuccess(descriptionText -> tv_description.setText(descriptionText))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getJoinDateText()
                .doOnSuccess(joinDateText -> tv_join_date.setText(joinDateText))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.getMarginsText()
                .doOnSuccess(marginsText -> tv_margins.setText(marginsText))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.isNewMember()
                .doOnSuccess(isNewMember -> iv_badge_new_member.setVisibility(isNewMember ? VISIBLE : GONE))
                .subscribe());
        mPresenter.getDisposables().add(mPresenter.isAdmin()
                .doOnSuccess(isAdmin -> iv_badge_admin.setVisibility(isAdmin ? VISIBLE : GONE))
                .subscribe());
    }

    @OnClick(R.id.iv_badge_admin)
    void onAdminPanelClickListener() {
        mPresenter.onAdminPanelClicked();
    }

    @OnClick({R.id.iv_settings, R.id.tv_settings})
    void onPreferencesClickListener() {
        mPresenter.onPreferencesClicked();
    }

    @OnClick(R.id.iv_profile)
    void onProfilePictureClickListener() {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.picture_source_dialog, null);

        CustomDialog profilePictureDialog = new CustomDialog(getContext());
        profilePictureDialog.setView(view);

        final ImageView iv_camera = view.findViewById(R.id.iv_camera);
        final ImageView iv_file = view.findViewById(R.id.iv_file);

        iv_camera.setOnClickListener(view1 -> {
            mPresenter.onAddCameraImageClicked();
            profilePictureDialog.dismiss();
        });
        iv_file.setOnClickListener(view12 -> {
            mPresenter.onAddFileImageClicked();
            profilePictureDialog.dismiss();
        });
        profilePictureDialog.show();
    }

    @OnClick({R.id.iv_logout, R.id.tv_logout})
    void onLogoutClickListener() {
        CustomDialog logoutDialog = new CustomDialog(getContext());
        logoutDialog.setTitle("Deseas cerrar la sesión?");
        logoutDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialogInterface, i) -> logoutDialog.dismiss());
        logoutDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialogInterface, i) -> mPresenter.logout());
        logoutDialog.show();
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
