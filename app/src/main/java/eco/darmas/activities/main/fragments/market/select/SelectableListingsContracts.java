package eco.darmas.activities.main.fragments.market.select;

interface SelectableListingsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}