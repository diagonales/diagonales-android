package eco.darmas.activities.main.fragments.comunity.create.event;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Date;

import eco.darmas.AppConstants;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.NetworkUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CreateCommunityEventPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> {

    private CreateCommunityEventModel mModel;
    private CreateCommunityEventView mView;

    CreateCommunityEventPresenter(CreateCommunityEventModel mModel, CreateCommunityEventView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        mView.initialize(this);
        EventBus.getDefault().register(this);
    }

    void onPublishClicked(int type, String title, String text, String date_start, String date_end, String address, String priceMinText, String priceMaxText, int ranged, String vacantsText, int limited, String date_inscription_open, String date_inscription_close) {
        if (!title.equals("")) {
            if (!text.equals("")) {
                if (!date_start.equals("")) {
                    if (!date_end.equals("")) {
                        if (!address.equals("")) {
                            if (date_inscription_open != null && date_inscription_open.isEmpty()) {
                                CommonUtils.showInfoToast("Selecciona la fecha de apertura de inscripciones");
                                return;
                            } else if (date_inscription_open != null) {
                                Date dateInscriptionOpen = DateUtils.parseFromFrontendLocale(date_inscription_open);
                                date_inscription_open = DateUtils.formatToBackendLocale(dateInscriptionOpen, AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT);
                            }
                            if (date_inscription_close != null && date_inscription_close.isEmpty()) {
                                CommonUtils.showInfoToast("Selecciona la fecha de cierre de inscripciones");
                                return;
                            } else if (date_inscription_close != null) {
                                Date dateInscriptionClose = DateUtils.parseFromFrontendLocale(date_inscription_close);
                                date_inscription_close = DateUtils.formatToBackendLocale(dateInscriptionClose, AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT);
                            }

                            double priceMin = 0;
                            double priceMax = 0;
                            if (!priceMinText.equals("")) {
                                priceMin = Double.parseDouble(priceMinText);
                            } else {
                                CommonUtils.showInfoToast("Debes ingresar un precio");
                                return;
                            }
                            if (ranged > 0) {
                                if (!priceMaxText.equals("")) {
                                    priceMax = Double.parseDouble(priceMaxText);
                                } else {
                                    CommonUtils.showInfoToast("Debes ingresar un precio maximo");
                                    return;
                                }
                                if (priceMax <= priceMin) {
                                    CommonUtils.showInfoToast("El precio maximo debe ser mayor que el minimo");
                                    return;
                                }
                            }

                            if (!vacantsText.equals("")) {
                                int vacants = Integer.valueOf(vacantsText);
                                if (NetworkUtils.isNetworkConnected(mView.getContext())) {
                                    Date dateStart = DateUtils.parseFromFrontendLocale(date_start);
                                    date_start = DateUtils.formatToBackendLocale(dateStart, AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT);
                                    Date dateEnd = DateUtils.parseFromFrontendLocale(date_end);
                                    date_end = DateUtils.formatToBackendLocale(dateEnd, AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT);
                                    createCommunityEvent(type, title, text, date_start, date_end, address, priceMin, priceMax, ranged, vacants, limited, date_inscription_open, date_inscription_close);
                                } else {
                                    CommonUtils.showErrorToast("Comprueba tu conección");
                                }
                            } else {
                                CommonUtils.showInfoToast("Ingresa el número de cupos disponibles o cero para ilimitado");
                            }
                        } else {
                            CommonUtils.showInfoToast("Ingresa la dirección del evento");
                        }
                    } else {
                        CommonUtils.showInfoToast("Selecciona la fecha de cierre");
                    }
                } else {
                    CommonUtils.showInfoToast("Selecciona la fecha de apertura");
                }
            } else {
                CommonUtils.showInfoToast("Ingresa una descripción del evento");
            }
        } else {
            CommonUtils.showInfoToast("Debes escribir un titulo para el evento");
        }
    }

    private void createCommunityEvent(int type, String title, String text, String dateStart, String dateEnd, String address, double price_min, double price_max, int ranged, int vacants, int limited, String date_inscription_open, String date_inscription_close) {
        String date_created = DateUtils.getTimeStamp();
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.COMMUNITY_EVENT_CREATED);
        mModel.commitCreateCommunityEvent(type, date_created, title, text, dateStart, dateEnd, address, price_min, price_max, ranged, vacants, limited, date_inscription_open, date_inscription_close, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            int id = (int) responseJSON.get("id");
                            String image = (String) responseJSON.get("image");
                            CommunityEvent communityEvent = new CommunityEvent()
                                    .withIdEvent(id)
                                    .withType(type)
                                    .withIdCreator(mModel.getLoggedUser().getIdUser())
                                    .withDateCreated(date_created)
                                    .withTitle(title)
                                    .withText(text)
                                    .withImage(image)
                                    .withDateStart(dateStart)
                                    .withDateEnd(dateEnd)
                                    .withAddress(address)
                                    .withPriceMin(price_min)
                                    .withPriceMax(price_max)
                                    .withRanged(ranged)
                                    .withVacants(vacants)
                                    .withStrictAssistance(limited)
                                    .withDateInscriptionOpen(date_inscription_open)
                                    .withDateInscriptionClose(date_inscription_close)
                                    .withStatus(CommunityEvent.Status.PLANNED);

                            mModel.writeCommunityEvent(communityEvent);
                            event.setSuccess(true);
                            event.setDialogTitle("El evento ha sido programado!");
                        } else {
                            event.setDialogTitle("El evento no pudo ser programado debido a un error");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                        event.setDialogTitle("El evento no pudo ser programado debido a un error");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().postSticky(event);
                        //TODO: check this
                        //capturedImageFile = null;
                        //capturedImageUri = null;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                event.setDialogTitle("Error al crear el evento");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo, vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().postSticky(event);
                AppLogger.e(t, null);
            }
        });
        CommonUtils.dismissFragment(mView, AppConstants.CREATE_COMMUNITY_EVENT_FRAGMENT_TAG, true);
    }

    void onAddCameraImageClicked() {
        mModel.setTempImageUri(ImageUtils.takePictureFromCamera((MainActivity) mView.getContext(), AppConstants.COMMUNITY_EVENT_CAMERA_REQUEST, mModel.getCommunityEventImageFile()));
    }

    void onAddFileImageClicked() {
        ImageUtils.chooseImageFromFile((MainActivity) mView.getContext(), AppConstants.COMMUNITY_EVENT_CHOOSE_FILE);
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActivityResult(ActivityResultEvent event) {
        if (event.resultCode != AppConstants.RESULT_OK) {
            return;
        }
        switch (event.eventCode) {
            case AppConstants.COMMUNITY_EVENT_CAMERA_REQUEST: {
                Picasso.get().load(mModel.getTempImageUri()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_image.getWidth(), mView.iv_image.getHeight()).centerCrop().into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al procesar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setCommunityEventImageFile(mModel.getTempImageUri().getPath());
                break;
            }
            case AppConstants.COMMUNITY_EVENT_CHOOSE_FILE: {
                String path = event.data.getDataString();
                Picasso.get().load(Uri.parse(path)).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(mView.iv_image.getWidth(), mView.iv_image.getHeight()).centerCrop().into(new Target() {

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mView.loadPostImage(bitmap);
                        CommonUtils.showProgress(mView.getContext(), false);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        CommonUtils.showProgress(mView.getContext(), false);
                        CommonUtils.showErrorToast("Error al procesar la imagen");
                        AppLogger.e(e, "");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        CommonUtils.showProgress(mView.getContext(), true);
                    }
                });
                mModel.setCommunityEventImageFile(path);
            }
        }
    }

    CommonUtils.StringWithTag[] getCommunityEventTypes() {
        return new CommonUtils.StringWithTag[]{
                new CommonUtils.StringWithTag("Feria", CommunityEvent.Type.FAIR),
                new CommonUtils.StringWithTag("Meeting", CommunityEvent.Type.MEETING),
                new CommonUtils.StringWithTag("Staff", CommunityEvent.Type.STAFF),
                new CommonUtils.StringWithTag("Fiesta!", CommunityEvent.Type.PARTY)
        };
    }
}