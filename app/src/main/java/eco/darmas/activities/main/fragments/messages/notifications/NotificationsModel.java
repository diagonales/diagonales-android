package eco.darmas.activities.main.fragments.messages.notifications;

import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;

class NotificationsModel extends BaseFragmentModel implements NotificationsContracts.Model {

    NotificationsModel(DataManager dataManager, Bundle savedInstanceState) {
        super(dataManager, savedInstanceState);
    }
}
