package eco.darmas.activities.main.fragments.messages.notifications;

import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;

class NotificationsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements NotificationsContracts.Presenter{

    private final NotificationsModel mModel;
    private final NotificationsView mView;

    NotificationsPresenter(NotificationsModel mModel, NotificationsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }
}
