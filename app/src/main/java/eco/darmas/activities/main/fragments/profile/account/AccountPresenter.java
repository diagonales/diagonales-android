package eco.darmas.activities.main.fragments.profile.account;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ActivityResultEvent;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.pojos.User;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class AccountPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements AccountContracts.Presenter{

    private final AccountModel mModel;
    private final AccountView mView;

    AccountPresenter(AccountModel mModel, AccountView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        mView.initialize(this);
        mView.updateView();
        EventBus.getDefault().register(this);
    }

    void onAdminPanelClicked() {
        AppNav.navigateToActivity(AppConstants.ADMIN_PANEL_ACTIVITY, (MainActivity) mView.getContext());
    }

    void onPreferencesClicked() {
        AppNav.navigateToActivity(AppConstants.PREFERENCES_ACTIVITY, (MainActivity) mView.getContext());
    }

    void logout() {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }

        CommonUtils.showProgress(mView.getContext(), true);
        mModel.logout(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            mModel.writeLogout();
                            AppNav.navigateToActivity(AppConstants.LOGIN_ACTIVITY, (MainActivity) mView.getContext());
                            ((MainActivity) mView.getContext()).finish();
                        } else {
                            CommonUtils.showErrorToast("No se pudo cerrar la sesión, funciona Internet?");
                        }
                    } catch (Exception e) {
                        AppLogger.e(e, null);
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                AppLogger.e(t, null);
                CommonUtils.showErrorToast("No se pudo cerrar la sesión debido a un error");
            }
        });
    }

    void onAddCameraImageClicked() {
        mModel.setTempImageUri(ImageUtils.takePictureFromCamera((MainActivity) mView.getContext(), AppConstants.PROFILE_CAMERA_REQUEST, mModel.getUserImageFile()));
    }

    void onAddFileImageClicked() {
        ImageUtils.chooseImageFromFile((MainActivity) mView.getContext(), AppConstants.PROFILE_CHOOSE_FILE);
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    mView.updateView();
                }
            }
        }
    }

    Single<String> getNameText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(mModel.getLoggedUser().getName()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getEmailText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(mModel.getLoggedUser().getEmail()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getAddressText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(mModel.getLoggedUser().getAddress()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getPhoneText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(mModel.getLoggedUser().getPhone()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getDescriptionText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(mModel.getLoggedUser().getComment()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getJoinDateText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(mModel.getLoggedUser().getDate()), AppConstants.PARTIAL_DATE_FORMAT)))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getMarginsText() {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess("+" + mModel.getLoggedUser().getPaymentMargin() + " / -" + mModel.getLoggedUser().getPaymentMargin()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<Boolean> isNewMember() {
        return Single.create((SingleOnSubscribe<Boolean>) e -> e.onSuccess(!DateUtils.isOlderThan3Months(DateUtils.parseFromBackendLocale(mModel.getLoggedUser().getDate()))))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<Boolean> isAdmin() {
        return Single.create((SingleOnSubscribe<Boolean>) e -> e.onSuccess(mModel.getLoggedUser().getStatus() > User.Status.ACTIVE))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActivityResult(ActivityResultEvent event) {
        if (event.resultCode != AppConstants.RESULT_OK) {
            return;
        }
        ContentModifiedEvent event2 = new ContentModifiedEvent(ContentModifiedEvent.PROFILE_IMAGE_UPDATED);
        switch (event.eventCode) {
            case AppConstants.PROFILE_CAMERA_REQUEST: {
                String path = mModel.getTempImageUri().getPath();
                mModel.commitImageUpload(path, new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                                if (responseJSON.get("result").equals("true")) {
                                    String image = (String) responseJSON.get("image");
                                    mModel.getLoggedUser().setImage(image);
                                    mModel.writeImageUpload();
                                    event2.setSuccess(true);
                                } else {
                                    CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado");
                                }
                            } catch (Exception e) {
                                AppLogger.e(e, null);
                                CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado debido a un error");
                            } finally {
                                EventBus.getDefault().postSticky(event2);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        event2.setSuccess(false);
                        if (t instanceof SocketTimeoutException) {
                            CommonUtils.showErrorToast("El servidor no parece estar respondiendo, intentalo mas tarde");
                        } else {
                            CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado debido a un error");
                        }
                        EventBus.getDefault().post(event2);
                        AppLogger.e(t, null);
                    }
                });
                break;
            }
            case AppConstants.PROFILE_CHOOSE_FILE: {
                String path = event.data.getDataString();
                mModel.commitImageUpload(path, new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                                if (responseJSON.get("result").equals("true")) {
                                    String image = (String) responseJSON.get("image");
                                    mModel.getLoggedUser().setImage(image);
                                    mModel.writeImageUpload();
                                    event2.setSuccess(true);
                                } else {
                                    CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado");
                                }
                            } catch (Exception e) {
                                AppLogger.e(e, null);
                                CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado debido a un error");
                            } finally {
                                EventBus.getDefault().post(event2);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        event2.setSuccess(false);
                        if (t instanceof SocketTimeoutException) {
                            CommonUtils.showErrorToast("El servidor no parece estar respondiendo, intentalo mas tarde");
                        } else {
                            CommonUtils.showErrorToast("Tu perfil no pudo ser actualizado debido a un error");
                        }
                        EventBus.getDefault().post(event2);
                        AppLogger.e(t, null);
                    }
                });
                CommonUtils.showProgress(mView.getContext(), true);
            }
        }
    }
}
