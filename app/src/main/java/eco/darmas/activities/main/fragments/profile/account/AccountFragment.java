package eco.darmas.activities.main.fragments.profile.account;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class AccountFragment extends BaseFragment implements AccountContracts.Fragment {

    @Override
    protected AccountModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new AccountModel(dataManager, savedInstanceState);
    }

    @Override
    protected AccountPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new AccountPresenter((AccountModel) model, (AccountView) view);
    }

    @Override
    protected AccountView createView(Context context) {
        return new AccountView(context);
    }
}
