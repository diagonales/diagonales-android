package eco.darmas.activities.main.fragments.comunity.events;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.ContentModifiedEvent;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos._Transaction;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.NetworkUtils;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class EventsPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements EventsContracts.Presenter{

    private final EventsModel mModel;
    private final EventsView mView;
    private int errorCode;
    private String error;

    EventsPresenter(EventsModel mModel, EventsView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        updateLists();
        EventBus.getDefault().register(this);
    }

    private void updateLists() {
        mView.communityEventsDataSetChanged();
        mView.stopRefreshAnimation();
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.COMMUNITY_EVENT: {
                if (event.isSuccess()) {
                    updateLists();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.EVENT_MODE_ENTERED:
                updateLists();
                break;
            case FilterEvent.EVENT_MODE_EXIT:
                updateLists();
                break;
        }
    }

    Single<String> getCommunityEventTitleText(CommunityEvent communityEvent) {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(communityEvent.getTitle()))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getCommunityEventDateText(CommunityEvent communityEvent) {
        return Single.create((SingleOnSubscribe<String>) e -> {
            Date startDate = DateUtils.parseFromBackendLocale(communityEvent.getDateStart());
            Date endDate = DateUtils.parseFromBackendLocale(communityEvent.getDateEnd());
            String startDateText = DateUtils.formatToFrontEndLocale(startDate, AppConstants.ABBREVIATED_PARTIAL_DATE_FORMAT);
            String endDateText = DateUtils.formatToFrontEndLocale(endDate, AppConstants.ABBREVIATED_PARTIAL_DATE_FORMAT);

            String date;
            if (startDateText.equals(endDateText)) {
                date = startDateText + ", " + DateUtils.formatToFrontEndLocale(startDate, AppConstants.HOUR_FORMAT) + " - " + DateUtils.formatToFrontEndLocale(endDate, AppConstants.HOUR_FORMAT);
            } else {
                date = startDateText + ", " + DateUtils.formatToFrontEndLocale(startDate, AppConstants.HOUR_FORMAT) + " - " + endDateText + ", " + DateUtils.formatToFrontEndLocale(endDate, AppConstants.HOUR_FORMAT);
            }
            e.onSuccess(date);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getCommunityEventParticipantsText(CommunityEvent communityEvent) {
        return Single.create((SingleOnSubscribe<String>) e -> {
            String participants = "Inscripciones: " + communityEvent.getInscriptions();
            if (communityEvent.getStrictAssistance() == 1) {
                participants = participants.concat("/" + communityEvent.getVacants());
            }
            if (isInscriptionsAwaiting(communityEvent)) {
                Date inscriptionOpenDate = DateUtils.parseFromBackendLocale(communityEvent.getDateInscriptionOpen());
                participants = participants.concat("\nApertura: " + DateUtils.formatToFrontEndLocale(inscriptionOpenDate, AppConstants.ABBREVIATED_DATE_FORMAT));
            } else if (communityEvent.getDateInscriptionClose() != null && !isInscriptionsClosed(communityEvent)) {
                Date inscriptionCloseDate = DateUtils.parseFromBackendLocale(communityEvent.getDateInscriptionClose());
                participants = participants.concat("\nCierre: " + DateUtils.formatToFrontEndLocale(inscriptionCloseDate, AppConstants.ABBREVIATED_DATE_FORMAT));
            }
            e.onSuccess(participants);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getCommunityEventPriceMinText(CommunityEvent communityEvent) {
        return Single.create((SingleOnSubscribe<String>) e -> {
            String priceMin;
            if (communityEvent.getRanged() == 0 && communityEvent.getPriceMin() == 0) {
                priceMin = "LIBRE";
            } else {
                priceMin = AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMin());
            }
            e.onSuccess(priceMin);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<String> getCommunityEventPriceMaxText(CommunityEvent communityEvent) {
        return Single.create((SingleOnSubscribe<String>) e -> e.onSuccess(AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMax())))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    void onAddClicked() {
        AppNav.navigateToFragment(AppConstants.CREATE_COMMUNITY_EVENT_FRAGMENT, (MainActivity) mView.getContext());
    }

    List<CommunityEvent> getPastCommunityEventList() {
        return mModel.getPastCommunityEventsList();
    }

    List<CommunityEvent> getCurrentCommunityEventList() {
        return mModel.getCurrentCommunityEventList();
    }

    List<CommunityEvent> getScheduledCommunityEventList() {
        return mModel.getScheduledCommunityEventList();
    }

    boolean onPastCommunityEventItemClick(int position) {
        AppLogger.d(mModel.getPastCommunityEventsList().get(position).toString());
        /*Object[] parcelables = new Object[]{mModel.getAnnounce(position)};
        AppNav.navigateToActivity(AppConstants.POST_ACTIVITY, (MainActivity) mView.getContext(), parcelables);*/
        return true;
    }

    boolean onCurrentCommunityEventItemClick(int position) {
        AppLogger.d(mModel.getCurrentCommunityEventList().get(position).toString());
        /*Object[] parcelables = new Object[]{mModel.getAnnounce(position)};
        AppNav.navigateToActivity(AppConstants.POST_ACTIVITY, (MainActivity) mView.getContext(), parcelables);*/
        return true;
    }

    boolean onScheduledCommunityEventItemClick(int position) {
        AppLogger.d(mModel.getScheduledCommunityEventList().get(position).toString());
        /*Object[] parcelables = new Object[]{mModel.getAnnounce(position)};
        AppNav.navigateToActivity(AppConstants.POST_ACTIVITY, (MainActivity) mView.getContext(), parcelables);*/
        return true;
    }

    public boolean noVacantsAvailable(CommunityEvent communityEvent) {
        return communityEvent.getStrictAssistance() == 1 && communityEvent.getVacants() == communityEvent.getInscriptions();
    }

    boolean confirmInscription(String priceText, boolean includeNote, String note, CommunityEvent communityEvent, boolean contribution) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showErrorToast("Comprueba tu conección");
            setErrorResponse(AppConstants.OFFLINE_MODE, null);
            return false;
        }
        if (priceText.isEmpty()) {
            setErrorResponse(AppConstants.INVALID_MIN_PRICE, "Debes ingresar un precio");
            return false;
        }
        double price = Double.valueOf(priceText);
        if (isInscribedInEvent(communityEvent.getIdEvent())) {
            setErrorResponse(AppConstants.ALREADY_INSCRIBED, "Tu inscripción para este evento ya fue realizada!");
            return false;
        }
        if (isInscriptionsClosed(communityEvent)) {
            setErrorResponse(AppConstants.TERM_CONCLUDED, "El plazo de inscripciones para este evento ha concluido");
            return false;
        }
        if (noVacantsAvailable(communityEvent)) {
            setErrorResponse(AppConstants.SOLD_OUT, "No quedan cupos disponibles");
            return false;
        }
        double totalPrice = price;
        double contributionPrice = 0;
        if (contribution) {
            contributionPrice = totalPrice * 0.01;
            totalPrice = totalPrice + contributionPrice;
        }
        if (communityEvent.getRanged() == 1 && price < communityEvent.getPriceMin()) {
            setErrorResponse(AppConstants.INVALID_MIN_PRICE, "El precio no puede ser inferior a " + AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMin()));
            return false;
        } else if (communityEvent.getRanged() == 1 && price > communityEvent.getPriceMax()) {
            setErrorResponse(AppConstants.INVALID_MAX_PRICE, "El precio no puede ser superior a " + AppConstants.CURRENCY_FORMAT.format(communityEvent.getPriceMax()));
            return false;
        } else if ((mModel.getLoggedUser().getFunds() - totalPrice) < (0 - mModel.getLoggedUser().getPaymentMargin())) {
            CommonUtils.showErrorToast("Este pago excedería tu margen de saldo actual: (-" + mModel.getLoggedUser().getPaymentMargin() + " +" + mModel.getLoggedUser().getPaymentMargin() + ")");
            return false;
        } else if (includeNote && note.isEmpty()) {
            setErrorResponse(AppConstants.EMPTY_TEXT_FIELD, "La nota adicional no esta escrita");
            return false;
        } else if (!includeNote) {
            note = "";
        }
        resetErrors();
        inscribe(communityEvent.getTitle(), note, totalPrice, communityEvent.getIdEvent(), contributionPrice);
        return true;
    }

    private void setErrorResponse(int errorCode, String error) {
        this.errorCode = errorCode;
        this.error = error;
    }

    private void resetErrors() {
        errorCode = AppConstants.CLEARED;
        error = "";
    }

    int getErrorCode() {
        return errorCode;
    }

    String getError() {
        return error;
    }

    private void inscribe(final String concept, final String note, final double amount, final int eventId, final double contribution) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }

        String date = DateUtils.getTimeStamp();
        int userId = mModel.getLoggedUser().getIdUser();
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.COMMUNITY_EVENT_MODIFIED);
        ContentModifiedEvent event2 = new ContentModifiedEvent(ContentModifiedEvent.TRANSFER_SENT);
        mModel.commitInscription(userId, concept, date, amount, note, eventId, contribution, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            int transactionId = 0;
                            if (amount != 0) {
                                transactionId = (int) responseJSON.get("id_transaction");
                                _Transaction transaction = new _Transaction()
                                        .withIdTransaction(transactionId)
                                        .withIdSender(userId)
                                        .withIdReceiver(1)
                                        .withConcept(concept)
                                        .withAmount(amount)
                                        .withDate(date)
                                        .withStatus(_Transaction.Status.RECEIVED)
                                        .withType(_Transaction.Type.INSCRIPTION)
                                        .withNote(note)
                                        .withIdEvent(eventId)
                                        .withContribution(contribution);

                                mModel.writeTransaction(transaction);
                                mModel.writePayment(amount);
                                event2.setSuccess(true);
                                EventBus.getDefault().post(event2);
                            }

                            int inscriptionId = (int) responseJSON.get("id_inscription");
                            Inscription inscription = new Inscription()
                                    .withIdInscription(inscriptionId)
                                    .withInscription(amount)
                                    .withDateInscription(date)
                                    .withIdEvent(eventId)
                                    .withIdUser(userId)
                                    .withIdTransaction(transactionId)
                                    .withListings("");
                            mModel.writeInscription(inscription);
                            mModel.writeCommunityEvent(amount - contribution, eventId);
                            event.setSuccess(true);
                            event.setDialogTitle("Inscripción realizada!");
                        } else {
                            event.setDialogTitle("La inscripción no pudo completarse");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        event.setDialogTitle("La inscripción no pudo completarse");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.e(t, null);
                event.setDialogTitle("La inscripción no pudo completarse");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo. Por favor vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    boolean isInscriptionsClosed(CommunityEvent communityEvent) {
        if (communityEvent.getDateInscriptionClose() != null) {
            Date inscriptionCloseDate = DateUtils.parseFromBackendLocale(communityEvent.getDateInscriptionClose());
            return DateUtils.now().after(inscriptionCloseDate);
        }
        return false;
    }

    boolean isInscriptionsAwaiting(CommunityEvent communityEvent) {
        if (communityEvent.getDateInscriptionOpen() != null) {
            Date inscriptionOpenDate = DateUtils.parseFromBackendLocale(communityEvent.getDateInscriptionOpen());
            return DateUtils.now().before(inscriptionOpenDate);
        }
        return false;
    }

    public boolean isNow(CommunityEvent communityEvent) {
        Date startDate = DateUtils.parseFromBackendLocale(communityEvent.getDateStart());
        Date endDate = DateUtils.parseFromBackendLocale(communityEvent.getDateEnd());
        return DateUtils.now().after(startDate) && DateUtils.now().before(endDate);
    }

    void enterEventMode(int eventId) {
        mModel.setEventModeId(eventId);
        FilterEvent event = new FilterEvent(FilterEvent.EVENT_MODE_ENTERED);
        EventBus.getDefault().post(event);
    }

    void onManageListingsClick(int eventId) {
        Object[] arguments = new Object[]{eventId};
        AppNav.navigateToFragment(AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT, (MainActivity) mView.getContext(), arguments);
    }

    void OnStandSelected(int eventId, String standOption) {
        if (!NetworkUtils.isNetworkConnected(mView.getContext())) {
            CommonUtils.showWarningToast("Comprueba tu conección");
            return;
        }

        int userId = mModel.getLoggedUser().getIdUser();
        Inscription inscription = mModel.getEventInscriptionMap().get(eventId).get(userId);
        ContentModifiedEvent event = new ContentModifiedEvent(ContentModifiedEvent.COMMUNITY_EVENT_MODIFIED);
        mModel.commitInscriptionEdit(inscription, standOption, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject responseJSON = new JSONObject(new String(response.body().bytes()));
                        if (responseJSON.get("result").equals("true")) {
                            int standId = (int) responseJSON.get("standId");
                            inscription.setStand(standId);
                            mModel.writeInscription(inscription);
                            event.setSuccess(true);
                            if (standId == 0) {
                                event.setDialogTitle("Stand liberado");
                            } else {
                                event.setDialogTitle("Stand asignado: #" + standId);
                            }
                        } else {
                            event.setDialogTitle("La selección de stand no pudo completarse");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        event.setDialogTitle("La selección de stand no pudo completarse");
                        event.setDialogMessage("Por favor contacta un administrador");
                    } finally {
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLogger.e(t, null);
                event.setDialogTitle("La selección de stand no pudo completarse");
                event.setDialogMessage("Hubo un error en el servidor, por favor contacta un administrador");
                if (t instanceof SocketTimeoutException) {
                    event.setDialogMessage("El servidor no parece estar respondiendo. Por favor vuelve a intentarlo mas tarde");
                }
                EventBus.getDefault().post(event);
            }
        });
        CommonUtils.showProgress(mView.getContext(), true);
    }

    void refreshItems() {
        mModel.refreshEvents();
    }

    /*public void resetEventMode() {
        AppData.INSTANCE.setEventModeId(-1);
        FilterEvent event = new FilterEvent(FilterEvent.EVENT_MODE_SELECTED);
        EventBus.getDefault().post(event);
    }*/
}