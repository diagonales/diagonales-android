package eco.darmas.activities.main.fragments.messages.conversations;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Conversation;
import eco.darmas.utils.CommonUtils;
import eco.darmas.utils.DateUtils;
import eco.darmas.utils.ImageUtils;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class ConversationsView extends BaseFragmentView implements ConversationsContracts.View, FlexibleAdapter.OnItemClickListener {

    private ConversationsPresenter mPresenter;
    private ConversationsAdapter flexibleConversationsAdapter;
    private Unbinder unBinder;

    @BindView(R.id.iv_search_messages)
    ImageView iv_search_messages;
    @BindView(R.id.tv_conversations_none)
    TextView tv_conversations_none;
    @BindView(R.id.rv_conversations)
    RecyclerView rv_conversations;
    @BindView(R.id.sp_users)
    Spinner sp_users;

    ConversationsView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.conversations_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (ConversationsPresenter) presenter;
        setupAdapters();
    }

    private void setupAdapters() {
        rv_conversations.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_announces = new LinearLayoutManager(getContext());
        rv_conversations.setLayoutManager(lm_announces);
        rv_conversations.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.conversation, 15)
                .withBottomEdge(true));

        // Initialize the Adapter
        flexibleConversationsAdapter = new ConversationsAdapter(getFlexibleConversationsItemList(mPresenter.getConversationList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_conversations.setAdapter(flexibleConversationsAdapter);

        tv_conversations_none.setVisibility(flexibleConversationsAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @OnClick({R.id.iv_new_message, R.id.tv_new_message})
    void onNewMessageClickListener() {
        //final Spinner sp_users = this.sp_users;
        sp_users.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, mPresenter.getNameList()));
        sp_users.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (sp_users.getSelectedItemPosition() == 0) {
                    return;
                }
                CommonUtils.StringWithTag s = (CommonUtils.StringWithTag) parent.getItemAtPosition(pos);
                int selectedUserId = (int) s.tag;
                mPresenter.onNewMessageClick(selectedUserId);
            }

            @Override
            public void onNothingSelected(AdapterView parent) {
            }
        });
        sp_users.performClick();
    }

    void dataSetChanged() {
        flexibleConversationsAdapter.updateDataSet(getFlexibleConversationsItemList(mPresenter.getConversationList()), false);
        tv_conversations_none.setVisibility(flexibleConversationsAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onItemClick(View view, int position) {
        return mPresenter.onConversationItemClick(position);
    }

    class ConversationsAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        ConversationsAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class ConversationItemAdapter extends AbstractFlexibleItem<ConversationItemAdapter.ViewHolder> {

        private Conversation conversation;

        ConversationItemAdapter(Conversation conversation) {
            this.conversation = conversation;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof ConversationItemAdapter) {
                Conversation inItem = ((ConversationItemAdapter) inObject).conversation;
                return this.conversation.getIdContact() == inItem.getIdContact();
            }
            return false;
        }

        @Override
        public int hashCode() {
            return conversation.getIdContact();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.conversation;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            int contactId;

            if (conversation.getLastMessage().getIdSender() == AppData.INSTANCE.getLoggedUser().getIdUser()) {
                contactId = conversation.getLastMessage().getIdReceiver();
            } else {
                contactId = conversation.getLastMessage().getIdSender();
            }

            ImageUtils.loadRoundedProfileBackgroundImage(getContext(), mPresenter.getUser(contactId), viewHolder.iv_receiver_background);

            viewHolder.tv_receiver_name.setText(mPresenter.getUser(contactId).getName());
            viewHolder.tv_message_date.setText(DateUtils.formatToFrontEndLocale(DateUtils.parseFromBackendLocale(conversation.getLastMessage().getDate()), AppConstants.COMPLETE_DATE_FORMAT));
            viewHolder.tv_message.setText(conversation.getLastMessage().getMessage());
            viewHolder.iv_delete_conversation.setOnClickListener(v -> {
                final CustomDialog confirmDialog = new CustomDialog(getContext());
                confirmDialog.setTitle("Deseas eliminar esta conversación?");
                confirmDialog.setButton(CustomDialog.BUTTON_POSITIVE, "Si", (dialog, which) -> {
                    mPresenter.hideConversation(position);
                    dialog.dismiss();
                });
                confirmDialog.setButton(CustomDialog.BUTTON_NEGATIVE, "No", (dialog, which) -> dialog.dismiss());
                confirmDialog.show();
            });

            ImageUtils.loadRoundedProfileImage(mPresenter.getUser(contactId), viewHolder.iv_receiver_image);
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.tv_receiver_name)
            TextView tv_receiver_name;
            @BindView(R.id.tv_message_date)
            TextView tv_message_date;
            @BindView(R.id.tv_message)
            TextView tv_message;
            @BindView(R.id.iv_receiver_image)
            ImageView iv_receiver_image;
            @BindView(R.id.iv_delete_conversation)
            ImageView iv_delete_conversation;
            @BindView(R.id.iv_receiver_background)
            ImageView iv_receiver_background;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleConversationsItemList(List<Conversation> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Conversation item : list) {
            itemList.add(new ConversationItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
