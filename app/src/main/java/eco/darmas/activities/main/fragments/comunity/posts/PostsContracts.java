package eco.darmas.activities.main.fragments.comunity.posts;

interface PostsContracts {

    interface Fragment {
    }

    interface View {
    }

    interface Presenter {
    }

    interface Model {
    }
}