package eco.darmas.activities.main.fragments.market.select;

import android.os.Bundle;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONObject;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class SelectableListingsModel extends BaseFragmentModel implements SelectableListingsContracts.Model {

    private int eventId;

    SelectableListingsModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        this.eventId = arguments.getInt("integer#0");
        loadSelectedListingsData().subscribe();
    }

    private Single loadSelectedListingsData() {
        return Single.create((SingleOnSubscribe<SparseArray<JSONObject>>) e -> {
            SparseArray<JSONObject> jsonObjectSparseArray = new SparseArray<>();
            if (getDataManager().getEventListingIdList().indexOfKey(eventId) >= 0) {

                Inscription inscription = getDataManager().getUserEventInscriptionMap().get(eventId);
                JSONArray listingsJsonArray = new JSONArray();
                if (!inscription.getListings().isEmpty()) {
                    listingsJsonArray = new JSONArray(inscription.getListings());
                }
                for (int i = 0; i < listingsJsonArray.length(); i++) {
                    JSONObject listingJsonObject = listingsJsonArray.getJSONObject(i);
                    int listingId = listingJsonObject.getInt("id_listing");
                    jsonObjectSparseArray.put(listingId, listingJsonObject);
                }

                /*for (int i = getDataManager().getEventListingIdList().get(eventId).size() - 1; i > -1; i--) {
                    JSONObject listingJsonObject = getDataManager().getEventListingIdList().get(eventId).valueAt(i);
                    int listingId = listingJsonObject.getInt("id_listing");
                    jsonObjectSparseArray.put(listingId, listingJsonObject);
                }*/
            }
            e.onSuccess(jsonObjectSparseArray);
        })
                .subscribeOn(Schedulers.computation())
                .doOnSuccess(selectedListings -> getDataManager().setTransientUserEventListings(selectedListings));
    }

    void commitEventListingsUpdate(Callback<ResponseBody> callback) {
        Inscription inscription = getDataManager().getEventInscriptionMap().get(eventId).get(getLoggedUser().getIdUser());
        JSONArray eventListingsJsonArray = new JSONArray();
        for (int i = 0; i < getDataManager().getTransientUserEventListings().size(); i++) {
            JSONObject eventListingJsonObject = getDataManager().getTransientUserEventListings().valueAt(i);
            eventListingsJsonArray.put(eventListingJsonObject);
        }
        App.getInstance().getNetworkService().getCallableAPI().eventInscriptionUpdate(inscription.getIdInscription(), inscription.getIdEvent(), null, eventListingsJsonArray.toString()).enqueue(callback);
    }

    void writeEventListing() {
        Inscription inscription = getDataManager().getEventInscriptionMap().get(eventId).get(getLoggedUser().getIdUser());
        JSONArray eventListingsJsonArray = new JSONArray();
        for (int i = 0; i < getDataManager().getTransientUserEventListings().size(); i++) {
            JSONObject eventListingJsonObject = getDataManager().getTransientUserEventListings().valueAt(i);
            eventListingsJsonArray.put(eventListingJsonObject);
        }

        inscription.setListings(eventListingsJsonArray.toString());
        getDataManager().getBoxStore().boxFor(Inscription.class).put(inscription);
    }

    int getEventId() {
        return eventId;
    }

    public SparseArray<CommunityEvent> getCommunityEventsMap() {
        return getDataManager().getCommunityEventMap();
    }
}