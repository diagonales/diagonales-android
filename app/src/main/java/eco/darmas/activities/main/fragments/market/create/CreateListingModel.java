package eco.darmas.activities.main.fragments.market.create;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import eco.darmas.App;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.data.DataFilters;
import eco.darmas.data.DataManager;
import eco.darmas.pojos.Category;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Inscription;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Node;
import eco.darmas.utils.AppLogger;
import eco.darmas.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;

class CreateListingModel extends BaseFragmentModel {

    private int type;
    private File listingImageFile;
    private Uri tempImageUri;

    CreateListingModel(DataManager dataManager, Bundle savedInstanceState, Bundle arguments) {
        super(dataManager, savedInstanceState);
        type = arguments.getInt("integer#0");
    }

    void commitCreateListing(final String title, final String text, final double price_min, final double price_max, final int ranged, int stock, final int shipping_method, final String address, final double shipping_price, int perBuyMaxStock, Callback<ResponseBody> callback) {
        MultipartBody.Part body = null;
        if (listingImageFile != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), listingImageFile);
            body = MultipartBody.Part.createFormData("uploaded_file", listingImageFile.getName(), reqFile);
        }

        RequestBody idCreatorBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getLoggedUser().getIdUser()));
        RequestBody idEventBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getEventModeId()));
        RequestBody idCategoryBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getSelectedCategoryNode().getId()));
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(type));
        RequestBody priceMinBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(price_min));
        RequestBody priceMaxBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(price_max));
        RequestBody rangedBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ranged));
        RequestBody stockBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(stock));
        RequestBody shippingMethodBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(shipping_method));
        RequestBody addressBody = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody shippingPriceBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(shipping_price));
        RequestBody perBuyMaxStockBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(perBuyMaxStock));

        App.getInstance().getNetworkService().getCallableAPI().createListing(idCreatorBody, idEventBody, idCategoryBody, titleBody, textBody, typeBody, priceMinBody, priceMaxBody, rangedBody, stockBody, shippingMethodBody, addressBody, shippingPriceBody, perBuyMaxStockBody, body).enqueue(callback);

        /*@Override
        public void onProgress(long bytesWritten, long totalSize) {
            double mbWritten = (double) bytesWritten / 1048576;
            double mbTotalSize = (double) totalSize / 1048576;
            progress.setTitle("Cargando " + dfLoadImage.format(mbWritten) + "m de " + dfLoadImage.format(mbTotalSize) + "m");
            super.onProgress(bytesWritten, totalSize);
        }*/
    }

    void writeListing(Listing listing) {
        getDataManager().getBoxStore().boxFor(Listing.class).put(listing);
        if (listing.getIdEvent() >= 0) {
            Inscription inscription = getDataManager().getEventInscriptionMap().get(listing.getIdEvent()).get(listing.getIdCreator());
            try {
                JSONArray eventListingsJsonArray = new JSONArray();
                if (!inscription.getListings().isEmpty()) {
                    eventListingsJsonArray = new JSONArray(inscription.getListings());
                }
                JSONObject eventListingJsonObject = new JSONObject();
                eventListingJsonObject.put("id_listing", String.valueOf(listing.getIdListing()));
                eventListingJsonObject.put("stock", String.valueOf(listing.getStock()));
                eventListingsJsonArray.put(eventListingJsonObject);
                inscription.setListings(eventListingsJsonArray.toString());
            } catch (JSONException e) {
                AppLogger.e(e, null);
            }
            getDataManager().getBoxStore().boxFor(Inscription.class).put(inscription);
        } else {
            Category category = getDataManager().getBoxStore().boxFor(Category.class).get(listing.getIdCategory());
            if (type == Listing.Type.OFFER) {
                try {
                    JSONArray offerListings = new JSONArray();
                    if (!category.getOfferListings().isEmpty()) {
                        offerListings = new JSONArray(category.getOfferListings());
                    }
                    offerListings.put(listing.getIdListing());
                    category.setOfferListings(offerListings.toString());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            } else {
                try {
                    JSONArray demandListings = new JSONArray();
                    if (!category.getDemandListings().isEmpty()) {
                        demandListings = new JSONArray(category.getDemandListings());
                    }
                    demandListings.put(listing.getIdListing());
                    category.setDemandListings(demandListings.toString());
                } catch (JSONException e) {
                    AppLogger.e(e, null);
                }
            }
            getDataManager().getBoxStore().boxFor(Category.class).put(category);
        }
    }

    int getType() {
        return type;
    }

    Node<Category> getSelectedCategoryNode() {
        return getDataManager().getCategoryNodeMap().get(DataFilters.selectedCategoryId);
    }

    File getListingImageFile() {
        Calendar cal = Calendar.getInstance();
        listingImageFile = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/" + ("IMG_" + cal.getTimeInMillis() + ".jpg"));
        return listingImageFile;
    }

    void setListingImageFile(String path) {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(App.getInstance().getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        listingImageFile = new File(CommonUtils.getRealImagePath(photo));
    }

    Uri getTempImageUri() {
        return tempImageUri;
    }

    void setTempImageUri(Uri tempImageUri) {
        this.tempImageUri = tempImageUri;
    }

    SparseArray<CommunityEvent> getCommunityEventMap() {
        return getDataManager().getCommunityEventMap();
    }

    SparseArray<SparseArray<Inscription>> getEventInscriptionMap() {
        return getDataManager().getEventInscriptionMap();
    }
}