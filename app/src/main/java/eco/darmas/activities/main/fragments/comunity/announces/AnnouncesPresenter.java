package eco.darmas.activities.main.fragments.comunity.announces;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import eco.darmas.AppConstants;
import eco.darmas.AppNav;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.events.DataLoadedEvent;
import eco.darmas.events.FilterEvent;
import eco.darmas.pojos.Post;
import eco.darmas.utils.CommonUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;

class AnnouncesPresenter extends BaseFragmentPresenter<BaseFragmentModel, BaseFragmentView> implements AnnouncesContracts.Presenter {

    private final AnnouncesModel mModel;
    private final AnnouncesView mView;

    AnnouncesPresenter(AnnouncesModel mModel, AnnouncesView mView) {
        super(mModel, mView);
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        EventBus.getDefault().register(this);
    }

    private void updateLists(boolean scrollTop) {
        getDisposables().add(mModel.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> mView.dataSetChanged(scrollTop))
                .doAfterTerminate(mView::stopRefreshAnimation)
                .subscribe());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadedEvent(DataLoadedEvent event) {
        switch (event.getEventCode()) {
            case DataLoadedEvent.POSTS: {
                if (event.isSuccess()) {
                    updateLists(false);
                }
            }
            case DataLoadedEvent.USERS: {
                if (event.isSuccess()) {
                    updateLists(false);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataFilteredEvent(FilterEvent event) {
        switch (event.getEventCode()) {
            case FilterEvent.POSTS_FILTERED: {
                updateLists(true);
            }
            case FilterEvent.EVENT_MODE_ENTERED: {
                updateLists(true);
            }
            case FilterEvent.EVENT_MODE_EXIT: {
                updateLists(true);
            }
        }
    }

    @Override
    protected void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    void onAddAnnounceClicked() {
        if (!mModel.getLoggedUser().getActive().equals("1")) {
            CommonUtils.showWarningToast("No puedes publicar en este momento");
            return;
        }
        AppNav.navigateToFragment(AppConstants.CREATE_ANNOUNCE_FRAGMENT, (MainActivity) mView.getContext());
    }

    boolean onAnnounceItemClick(int position) {
        Object[] parcelables = new Object[]{mModel.getAnnounce(position)};
        AppNav.navigateToActivity(AppConstants.POST_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
        return true;
    }

    boolean onHeaderItemClick(int position) {
        Object[] parcelables = new Object[]{mModel.getHeader(position)};
        AppNav.navigateToActivity(AppConstants.POST_ACTIVITY, (MainActivity) mView.getContext(), parcelables);
        return true;
    }

    List<Post> getAnnounceList() {
        return mModel.getAnnounceList();
    }

    List<Post> getHeaderList() {
        return mModel.getHeaderList();
    }

    void refreshItems() {
        mModel.refreshAnnounces();
    }
}