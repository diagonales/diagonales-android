package eco.darmas.activities.main.fragments.messages.notifications;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class NotificationsFragment extends BaseFragment implements NotificationsContracts.Fragment {

    @Override
    protected NotificationsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new NotificationsModel(dataManager, savedInstanceState);
    }

    @Override
    protected NotificationsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new NotificationsPresenter((NotificationsModel) model, (NotificationsView) view);
    }

    @Override
    protected NotificationsView createView(Context context) {
        return new NotificationsView(context);
    }
}
