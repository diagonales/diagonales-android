package eco.darmas.activities.main.fragments.market.select.offers;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.pojos.Listing;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

class SelectableOffersView extends BaseFragmentView implements SelectableOffersContracts.View, FlexibleAdapter.OnUpdateListener {

    private SelectableOffersPresenter mPresenter;
    private SelectableOffersAdapter flexibleSelectableOffersAdapter;
    private Unbinder unBinder;

    // search box
    /*@BindView(R.id.li_search)
    LinearLayout li_search;
    @BindView(R.id.et_search)
    EditText et_search;*/

    // components
    @BindView(R.id.rv_selectable_offers)
    RecyclerView rv_selectable_offers;
    @BindView(R.id.tv_selectable_offers_none)
    TextView tv_selectable_offers_none;

    SelectableOffersView(Context context) {
        super(context);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.selectable_offers_fragment, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initialize(BaseFragmentPresenter presenter) {
        this.mPresenter = (SelectableOffersPresenter) presenter;
        setupAdapters();
        setData();
    }

    private void setupAdapters() {
        rv_selectable_offers.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager lm_demands = new LinearLayoutManager(getContext());
        rv_selectable_offers.setLayoutManager(lm_demands);
        rv_selectable_offers.addItemDecoration(new FlexibleItemDecoration(getContext())
                .addItemViewType(R.layout.selectable_listing, 15));// use a linear layout manager

        // Initialize the Adapter
        flexibleSelectableOffersAdapter = new SelectableOffersAdapter(getFlexibleSelectableOffersItemList(mPresenter.getSelectableOfferList()), this, true);

        // Initialize the RecyclerView and attach the Adapter to it as usual
        rv_selectable_offers.setAdapter(flexibleSelectableOffersAdapter);
        //flexibleDemandsAdapter
        //.setNotifyMoveOfFilteredItems(true);
        //.setDisplayHeadersAtStartUp(true)
        //.setStickyHeaders(true); //Make headers sticky (headers need to be shown)!
    }

    void setData() {
    }

    void dataSetChanged() {
        setData();
        flexibleSelectableOffersAdapter.updateDataSet(getFlexibleSelectableOffersItemList(mPresenter.getSelectableOfferList()));
    }

    @Override
    public void onUpdateEmptyView(int size) {
        rv_selectable_offers.setVisibility(size == 0 ? View.GONE : View.VISIBLE );
        tv_selectable_offers_none.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    class SelectableOffersAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

        SelectableOffersAdapter(List<AbstractFlexibleItem> items, Object listeners, boolean stableIds) {
            super(items, listeners, stableIds);
        }
    }

    class DemandItemAdapter extends AbstractFlexibleItem<DemandItemAdapter.ViewHolder> {

        private Listing listing;

        DemandItemAdapter(Listing listing) {
            this.listing = listing;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof DemandItemAdapter) {
                Listing inItem = ((DemandItemAdapter) inObject).listing;
                return this.listing.equals(inItem);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return listing.hashCode();
        }

        @Override
        public int getLayoutRes() {
            return R.layout.selectable_listing;
        }

        @Override
        public ViewHolder createViewHolder(View view, FlexibleAdapter adapter) {
            return new ViewHolder(view, adapter);
        }

        @Override
        public void bindViewHolder(FlexibleAdapter adapter, ViewHolder viewHolder, int position, List payloads) {
            if (mPresenter.isRegisteredAtEvent(listing.getIdListing())) {
                viewHolder.cb_include.setChecked(true);
                if (listing.getIdEvent() >= 0) {
                    viewHolder.cb_include.setEnabled(false);
                }
                viewHolder.et_stock.setText(mPresenter.getEventListingStockText(listing.getIdListing()));
                viewHolder.et_stock.setEnabled(true);
                viewHolder.et_discount.setText(mPresenter.getEventListingDiscountText(listing.getIdListing()));
                viewHolder.et_discount.setEnabled(true);
            } else {
                viewHolder.cb_include.setChecked(false);
                viewHolder.et_stock.setText("");
                viewHolder.et_stock.setEnabled(false);
                viewHolder.et_discount.setText("");
                viewHolder.et_discount.setEnabled(false);
            }

            viewHolder.cb_include.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    mPresenter.preAddListing(listing.getIdListing());
                    viewHolder.et_discount.setEnabled(true);
                    viewHolder.et_stock.setEnabled(true);
                } else {
                    mPresenter.preRemoveListing(listing.getIdListing());
                    viewHolder.et_discount.setText("");
                    viewHolder.et_stock.setText("");
                    viewHolder.et_discount.setEnabled(false);
                    viewHolder.et_stock.setEnabled(false);
                }
            });
            viewHolder.et_discount.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().isEmpty()) {
                        mPresenter.preAddDiscount(listing.getIdListing(), 0);
                    } else {
                        mPresenter.preAddDiscount(listing.getIdListing(), Double.parseDouble(s.toString()));
                    }
                }
            });
            viewHolder.et_stock.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().isEmpty()) {
                        mPresenter.preAddStock(listing.getIdListing(), 0);
                    } else {
                        mPresenter.preAddStock(listing.getIdListing(), Integer.parseInt(s.toString()));
                    }
                }
            });
            viewHolder.tv_title.setText(listing.getTitle() + " (Disp: " + listing.getStock() + ")");
        }

        class ViewHolder extends FlexibleViewHolder {

            @BindView(R.id.cb_include)
            CheckBox cb_include;
            @BindView(R.id.tv_title)
            TextView tv_title;
            @BindView(R.id.et_discount)
            EditText et_discount;
            @BindView(R.id.et_stock)
            EditText et_stock;

            ViewHolder(View view, FlexibleAdapter adapter) {
                super(view, adapter);
                ButterKnife.bind(this, view);
            }
        }
    }

    private List<AbstractFlexibleItem> getFlexibleSelectableOffersItemList(List<Listing> list) {
        List<AbstractFlexibleItem> itemList = new ArrayList<>();
        for (Listing item : list) {
            itemList.add(new DemandItemAdapter(item));
        }
        return itemList;
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}
