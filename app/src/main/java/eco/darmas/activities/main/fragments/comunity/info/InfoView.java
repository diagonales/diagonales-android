package eco.darmas.activities.main.fragments.comunity.info;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import eco.darmas.R;
import eco.darmas.base.fragment.BaseFragmentPresenter;
import eco.darmas.base.fragment.BaseFragmentView;

class InfoView extends BaseFragmentView implements InfoContracts.View {

    private InfoPresenter mPresenter;
    private Unbinder unBinder;

    @BindView(R.id.tv_members)
    TextView tv_members;

    InfoView(Context context) {
        super(context);
    }

    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.info_screen_slide_page, container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    protected void initialize(BaseFragmentPresenter presenter) {
        mPresenter = (InfoPresenter) presenter;
        tv_members.setText("Miembros (" + mPresenter.getUserCount() + ")");
    }

    public void repaintUserCount() {
        tv_members.setText("Miembros (" + mPresenter.getUserCount() + ")");
    }

    @Override
    protected Unbinder getUnBinder() {
        return unBinder;
    }
}