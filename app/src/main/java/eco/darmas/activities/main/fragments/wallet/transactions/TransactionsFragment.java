package eco.darmas.activities.main.fragments.wallet.transactions;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class TransactionsFragment extends BaseFragment implements TransactionsContracts.Fragment {

    @Override
    protected TransactionsModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new TransactionsModel(dataManager, savedInstanceState);
    }

    @Override
    protected TransactionsPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new TransactionsPresenter((TransactionsModel) model, (TransactionsView) view);
    }

    @Override
    protected TransactionsView createView(Context context) {
        return new TransactionsView(context);
    }
}
