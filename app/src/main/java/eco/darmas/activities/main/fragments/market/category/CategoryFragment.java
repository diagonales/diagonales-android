package eco.darmas.activities.main.fragments.market.category;

import android.content.Context;
import android.os.Bundle;

import eco.darmas.base.fragment.BaseFragment;
import eco.darmas.base.fragment.BaseFragmentModel;
import eco.darmas.base.fragment.BaseFragmentView;
import eco.darmas.data.DataManager;

public class CategoryFragment extends BaseFragment {

    @Override
    protected CategoryModel createModel(DataManager dataManager, Bundle savedInstanceState) {
        return new CategoryModel(dataManager, savedInstanceState, getArguments());
    }

    @Override
    protected CategoryPresenter createPresenter(BaseFragmentModel model, BaseFragmentView view) {
        return new CategoryPresenter((CategoryModel) model, (CategoryView) view);
    }

    @Override
    protected CategoryView createView(Context context) {
        return new CategoryView(context);
    }
}
