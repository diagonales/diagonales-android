package eco.darmas;

import java.text.DecimalFormat;

public final class AppConstants {

    // Request codes
    public static final int PROFILE_CAMERA_REQUEST = 1;
    public static final int PROFILE_CHOOSE_FILE = 2;
    public static final int ANNOUNCE_CAMERA_REQUEST = 3;
    public static final int ANNOUNCE_CHOOSE_FILE = 4;
    public static final int POST_CAMERA_REQUEST = 5;
    public static final int POST_CHOOSE_FILE = 6;
    public static final int LISTING_CAMERA_REQUEST = 7;
    public static final int LISTING_CHOOSE_FILE = 8;
    public static final int COMMUNITY_EVENT_CAMERA_REQUEST = 9;
    public static final int COMMUNITY_EVENT_CHOOSE_FILE = 10;

    // Result codes
    public static final int RESULT_PROGRESS = 1;
    public static final int RESULT_NEUTRAL = 0;
    public static final int RESULT_OK = -1;

    // Navigation codes
    public static final int LOGIN_ACTIVITY = 9;
    public static final int MAIN_ACTIVITY = 10;
    public static final int CREATE_POST_FRAGMENT = 11;
    public static final int CREATE_ANNOUNCE_FRAGMENT = 12;
    public static final int POST_ACTIVITY = 13;
    public static final int CREATE_LISTING_FRAGMENT = 14;
    public static final int LISTING_ACTIVITY = 15;
    public static final int RATE_FRAGMENT = 16;
    public static final int CATEGORY_FRAGMENT = 17;
    public static final int USER_SUMMARY_ACTIVITY = 18;
    public static final int USER_MESSAGES_ACTIVITY = 19;
    public static final int ADMIN_PANEL_ACTIVITY = 20;
    public static final int PREFERENCES_ACTIVITY = 21;
    public static final int SELECT_OFFERS_LIST_ORDER_FRAGMENT = 22;
    public static final int SELECT_DEMANDS_LIST_ORDER_FRAGMENT = 23;
    public static final int SELECT_ORDERS_LIST_ORDER_FRAGMENT = 24;
    public static final int SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT = 25;
    public static final int CREATE_COMMUNITY_EVENT_FRAGMENT = 26;
    public static final int MANAGE_EVENT_LISTINGS_FRAGMENT = 27;


    // Tags
    public static final String CREATE_ANNOUNCE_FRAGMENT_TAG = "CREATE_ANNOUNCE_FRAGMENT_TAG";
    public static final String CREATE_POST_FRAGMENT_TAG = "CREATE_POST_FRAGMENT_TAG";
    public static final String CREATE_LISTING_FRAGMENT_TAG = "CREATE_LISTING_FRAGMENT_TAG";
    public static final String CATEGORY_FRAGMENT_TAG = "CATEGORY_FRAGMENT_TAG";
    public static final String RATE_FRAGMENT_TAG = "RATE_FRAGMENT_TAG";
    public static final String SELECT_LIST_ORDER_FRAGMENT_TAG = "SELECT_LIST_ORDER_FRAGMENT_TAG";
    public static final String CREATE_COMMUNITY_EVENT_FRAGMENT_TAG = "CREATE_COMMUNITY_EVENT_FRAGMENT_TAG";
    public static final String MANAGE_EVENT_LISTINGS_FRAGMENT_TAG = "MANAGE_EVENT_LISTINGS_FRAGMENT_TAG";

    // Formats
    public static final DecimalFormat CURRENCY_FORMAT = new DecimalFormat("####.##");
    public static final DecimalFormat PERCENTAGE_FORMAT = new DecimalFormat("##'%'");
    public static final DecimalFormat LOAD_FILE_FORMAT = new DecimalFormat("####.#");
    public static final String TIMESTAMP_DATABASE_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String TIMESTAMP_DATABASE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIMESTAMP_DATABASE_TIME_FORMAT = "HH:mm:ss";
    public static final String COMPLETE_DATE_FORMAT = "HH:mm, d 'de' MMMM, yyyy";
    public static final String PARTIAL_DATE_FORMAT = "d 'de' MMMM, yyyy";
    public static final String ABBREVIATED_PARTIAL_DATE_FORMAT = "d 'de' MMMM";
    public static final String ABBREVIATED_DATE_FORMAT = "HH:mm, dd/MM/yy";
    public static final String HOUR_FORMAT = "HH:mm";
    public static final String DATE_FORMAT = "dd/MM/yy";

    // List orders
    public static final int BY_LISTING_RATING_DESC = 1;
    public static final int BY_LISTING_RATING_ASC = 2;
    public static final int BY_SKILL_RATING_DESC = 3;
    public static final int BY_SKILL_RATING_ASC = 4;
    public static final int BY_MEMBER_RATING_DESC = 5;
    public static final int BY_MEMBER_RATING_ASC = 6;
    public static final int BY_PRICE_DESC = 7;
    public static final int BY_PRICE_ASC = 8;
    public static final int BY_END_DATE_DESC = 9;
    public static final int BY_END_DATE_ASC = 10;
    public static final int BY_LIKES_DESC = 11;
    public static final int BY_CREATION_DATE_DESC = 12;
    public static final int BY_CREATION_DATE_ASC = 13;

    // Error response codes
    public static final int CLEARED = 0;
    public static final int INVALID_MIN_PRICE = 1;
    public static final int INVALID_MAX_PRICE = 2;
    public static final int OFFLINE_MODE = 3;
    public static final int EMPTY_TEXT_FIELD = 4;
    public static final int OVER_LIMIT_AMOUNT = 5;
    public static final int COUNTERPART_OVER_LIMIT_AMOUNT = 6;
    public static final int ALREADY_INSCRIBED = 7;
    public static final int TERM_CONCLUDED = 8;
    public static final int SOLD_OUT = 9;
    public static final int EMPTY_SHIPPING_INFO = 10;
}