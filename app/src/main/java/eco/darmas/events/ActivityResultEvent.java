package eco.darmas.events;


import android.content.Intent;

public class ActivityResultEvent {

    public static final int MESSAGE_SENT = 1;
    public static final int RATING_MODIFIED = 2;
    public static final int TRANSACTION_UPDATED = 3;
    public static final int TRANSFER_SENT = 4;
    public static final int LIKE_SENT = 5;
    public static final int COMMENT_SENT = 6;
    public static final int POST_CREATED = 7;
    public static final int LISTING_CREATED = 8;
    public static final int PROFILE_IMAGE_UPDATED = 9;
    public final int eventCode;
    public final int resultCode;
    public final Intent data;
    public int id;
    public boolean success;

    public ActivityResultEvent(int eventCode, int resultCode, Intent data) {
        this.eventCode = eventCode;
        this.resultCode = resultCode;
        this.data = data;
    }

    public void setId(int id) {
        this.id = id;
    }
}
