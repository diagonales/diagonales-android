package eco.darmas.events;

public class FilterEvent {

    public static final int POSTS_FILTERED = 1;
    public static final int CONVERSATIONS_FILTERED = 2;
    public static final int TRANSACTIONS_FILTERED = 3;
    public static final int LISTINGS_FILTERED = 4;
    public static final int POST_COMMENTS_FILTERED = 5;
    public static final int EVENT_MODE_ENTERED = 6;
    public static final int EVENT_MODE_EXIT = 7;
    private final int eventCode;

    public FilterEvent(int eventCode) {
        this.eventCode = eventCode;
    }

    public int getEventCode() {
        return eventCode;
    }
}
