package eco.darmas.events;

public class ContentModifiedEvent {

    public static final int MESSAGE_SENT = 1;
    public static final int RATING_MODIFIED = 2;
    public static final int TRANSACTION_UPDATED = 3;
    public static final int TRANSFER_SENT = 4;
    public static final int LIKE_SENT = 5;
    public static final int COMMENT_SENT = 6;
    public static final int COMMENT_DELETED = 7;
    public static final int POST_CREATED = 8;
    public static final int POST_MODIFIED = 9;
    public static final int LISTING_CREATED = 10;
    public static final int LISTING_MODIFIED = 11;
    public static final int PROFILE_IMAGE_UPDATED = 12;
    public static final int COMMUNITY_EVENT_CREATED = 13;
    public static final int COMMUNITY_EVENT_MODIFIED = 14;
    //public static final int COMMUNITY_EVENT_STARTED = 15;
    private final int eventCode;
    private int id;
    private boolean success;
    private String dialogTitle;
    private String dialogMessage;

    public ContentModifiedEvent(int eventCode) {
        this.eventCode = eventCode;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public void setDialogMessage(String dialogMessage) {
        this.dialogMessage = dialogMessage;
    }

    public String getDialogMessage() {
        return dialogMessage;
    }

    public String getDialogTitle() {
        return dialogTitle;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getId() {
        return id;
    }

    public int getEventCode() {
        return eventCode;
    }
}
