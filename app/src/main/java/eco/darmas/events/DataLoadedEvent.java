package eco.darmas.events;

public class DataLoadedEvent {

    public static final int USERS = 1;
    public static final int POSTS = 2;
    public static final int TRANSACTIONS = 3;
    public static final int RATINGS = 4;
    public static final int LISTINGS = 5;
    public static final int EVENT_INSCRIPTIONS = 6;
    public static final int CATEGORIES = 7;
    public static final int MESSAGES = 8;
    public static final int COMMUNITY_EVENT = 9;
    private final int eventCode;
    private int id;
    private boolean success;

    public DataLoadedEvent(int eventCode) {
        this.eventCode = eventCode;
    }

    public DataLoadedEvent(int eventCode, boolean success) {
        this.eventCode = eventCode;
        this.success = success;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getEventCode() {
        return eventCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getId() {
        return id;
    }
}
