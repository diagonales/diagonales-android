package eco.darmas.events;

public class NotificationReceivedEvent {

    public static final int MESSAGE_RECEIVED = 1;
    public static final int RATING_RECEIVED = 2;
    public static final int TRANSACTION_UPDATED = 3;
    public static final int TRANSFER_RECEIVED = 4;
    public static final int LIKE_RECEIVED = 5;
    public static final int COMMENT_RECEIVED = 6;
    private final int eventCode;

    public NotificationReceivedEvent(int eventCode) {
        this.eventCode = eventCode;
    }

    public int getEventCode() {
        return eventCode;
    }
}
