package eco.darmas;

import android.os.Build;
import android.os.Bundle;

import com.thirtydegreesray.dataautoaccess.DataAutoAccess;
import com.thirtydegreesray.dataautoaccess.annotation.AutoAccess;

import java.util.Locale;

import eco.darmas.pojos.User;

public enum AppData {

    INSTANCE;

    @AutoAccess(dataName = "appData_loggedUser")
    User loggedUser;
    @AutoAccess(dataName = "appData_systemDefaultLocale")
    Locale systemDefaultLocale;
    @AutoAccess(dataName = "appData_refreshInterval")
    int refreshInterval;

    public Locale getSystemDefaultLocale() {
        if (systemDefaultLocale == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                systemDefaultLocale = App.getInstance().getResources().getConfiguration().getLocales().get(0);
            } else{
                //no deprecation inspection
                systemDefaultLocale = App.getInstance().getResources().getConfiguration().locale;
            }
        }
        return systemDefaultLocale;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public void loadInstanceState(Bundle savedInstanceState) {
        DataAutoAccess.getData(this, savedInstanceState);
    }

    public void saveInstanceState(Bundle outState) {
        DataAutoAccess.saveData(this, outState);
    }
}