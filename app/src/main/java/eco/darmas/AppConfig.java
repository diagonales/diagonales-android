package eco.darmas;

public final class AppConfig {

    // App configuration
    public final static double VERSION = Double.parseDouble(BuildConfig.VERSION_NAME.substring(0, 4));
    public final static String BASE_URL = "http://darmas.tk/";
    public final static String TEST_BASE_URL = "https://darmas-dev.000webhostapp.com/";
    public final static String API_BASE_URL = "api/";
    public final static String USERS_URL = "users/";
    public final static String USER_IMAGES_URL = "images/user/";
    public final static String POST_IMAGES_URL = "images/post/";
    public final static String LISTING_IMAGES_URL = "images/listing/";
    public final static String EVENT_IMAGES_URL = "images/event/";
    public final static int NULL_INDEX = -1;
    public final static int HTTP_TIME_OUT = 32 * 1000;
    public final static int HTTP_MAX_CACHE_SIZE = 32 * 1024 * 1024;
    public final static int IMAGE_MAX_CACHE_SIZE = 32 * 1024 * 1024;
    public final static int CACHE_MAX_AGE = 4 * 7 * 24 * 60 * 60;

    public static String getBaseUrl() {
        boolean test = BuildConfig.DEBUG || App.getInstance().getPreferencesHelper().isTestMode();
        return test ? TEST_BASE_URL : BASE_URL;
    }

    public static String getUsersUrl() {
        return getBaseUrl() + USERS_URL;
    }

    public static String getUserImagesUrl() {
        return getBaseUrl() + USER_IMAGES_URL;
    }

    public static String getPostImagesUrl() {
        return getBaseUrl() + POST_IMAGES_URL;
    }

    public static String getListingImagesUrl() {
        return getBaseUrl() + LISTING_IMAGES_URL;
    }

    public static String getEventImagesUrl() {
        return getBaseUrl() + EVENT_IMAGES_URL;
    }
}