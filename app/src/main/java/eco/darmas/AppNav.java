package eco.darmas;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import eco.darmas.activities.admin.AdminActivity;
import eco.darmas.activities.listing.ListingActivity;
import eco.darmas.activities.login.LoginActivity;
import eco.darmas.activities.main.MainActivity;
import eco.darmas.activities.main.fragments.comunity.create.announce.CreateAnnounceFragment;
import eco.darmas.activities.main.fragments.comunity.create.event.CreateCommunityEventFragment;
import eco.darmas.activities.main.fragments.comunity.create.post.CreatePostFragment;
import eco.darmas.activities.main.fragments.market.category.CategoryFragment;
import eco.darmas.activities.main.fragments.market.create.CreateListingFragment;
import eco.darmas.activities.main.fragments.market.order.ListOrderFragment;
import eco.darmas.activities.main.fragments.market.rate.RateFragment;
import eco.darmas.activities.main.fragments.market.select.SelectableListingsFragment;
import eco.darmas.activities.post.PostActivity;
import eco.darmas.activities.preferences.PreferencesActivity;
import eco.darmas.activities.user.UserActivity;

import static eco.darmas.AppConstants.USER_MESSAGES_ACTIVITY;
import static eco.darmas.AppConstants.USER_SUMMARY_ACTIVITY;

public final class AppNav {

    public static void navigateToActivity(int target, AppCompatActivity context, Object[] parcelables) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        switch (target) {
            case AppConstants.LOGIN_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, LoginActivity.class);
                break;
            }
            case AppConstants.MAIN_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setClass(context, MainActivity.class);
                break;
            }
            case AppConstants.POST_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, PostActivity.class);
                break;
            }
            case AppConstants.ADMIN_PANEL_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, AdminActivity.class);
                break;
            }
            case AppConstants.LISTING_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, ListingActivity.class);
                break;
            }
            case AppConstants.PREFERENCES_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, PreferencesActivity.class);
                break;
            }
            /*case RATE_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, RatingFullScreenActivity.class);
                if (parcelables != null) {
                    for (int i = 0; i < parcelables.length; i++) {
                        bundle.putParcelable("parcelable#" + i, (Parcelable) parcelables[i]);
                    }
                }
                break;
            }*/
            /*case CATEGORY_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, CategorySelectionActivity.class);
                if (parcelables != null) {
                    for (int i = 0; i < parcelables.length; i++) {
                        bundle.putParcelable("parcelable#" + i, (Parcelable) parcelables[i]);
                    }
                }
                break;
            }*/
            case USER_SUMMARY_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, UserActivity.class);
                bundle.putInt("tab", 0);
                break;
            }
            case USER_MESSAGES_ACTIVITY: {
                intent.setAction(Intent.ACTION_VIEW);
                intent.setClass(context, UserActivity.class);
                bundle.putInt("tab", 2);
                break;
            }
        }

        if (parcelables != null) {
            for (int i = 0; i < parcelables.length; i++) {
                // pending intent loading
                try {
                    bundle.putParcelable("parcelable#" + i, (Parcelable) parcelables[i]);
                } catch (ClassCastException e) {
                    bundle.putInt("id_target", (int) parcelables[i]);
                }
            }
        }
        intent.putExtras(bundle);
        context.startActivityForResult(intent, target);
    }

    public static void navigateToActivity(int target, AppCompatActivity appCompatActivity) {
        navigateToActivity(target, appCompatActivity, null);
    }

    public static void navigateToFragment(int target, AppCompatActivity context, Object[] arguments) {
        DialogFragment fragment = new DialogFragment();
        FragmentManager fm = context.getSupportFragmentManager();
        String tag = "";
        Bundle bundle = new Bundle();
        int parcelableCount = 0;
        int intCount = 0;
        int stringCount = 0;
        if (arguments != null) {
            for (Object argument : arguments) {
                if (argument instanceof Parcelable) {
                    bundle.putParcelable("parcelable#" + parcelableCount, (Parcelable) argument);
                    parcelableCount++;
                } else if (argument instanceof Integer) {
                    bundle.putInt("integer#" + intCount, (int) argument);
                    intCount++;
                } else if (argument instanceof String) {
                    bundle.putString("string#" + stringCount, (String) argument);
                    stringCount++;
                }
            }
        }
        switch (target) {
            case AppConstants.CREATE_POST_FRAGMENT: {
                fragment = new CreatePostFragment();
                tag = AppConstants.CREATE_POST_FRAGMENT_TAG;
                break;
            }
            case AppConstants.CREATE_ANNOUNCE_FRAGMENT: {
                fragment = new CreateAnnounceFragment();
                tag = AppConstants.CREATE_ANNOUNCE_FRAGMENT_TAG;
                break;
            }
            case AppConstants.CREATE_LISTING_FRAGMENT: {
                fragment = new CreateListingFragment();
                tag = AppConstants.CREATE_LISTING_FRAGMENT_TAG;
                break;
            }
            case AppConstants.RATE_FRAGMENT: {
                fragment = new RateFragment();
                tag = AppConstants.RATE_FRAGMENT_TAG;
                break;
            }
            case AppConstants.CATEGORY_FRAGMENT: {
                fragment = new CategoryFragment();
                tag = AppConstants.CATEGORY_FRAGMENT_TAG;
                break;
            }
            case AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT: {
                fragment = new ListOrderFragment();
                bundle.putInt("caller", AppConstants.SELECT_POST_COMMENTS_LIST_ORDER_FRAGMENT);
                tag = AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG;
                break;
            }
            case AppConstants.SELECT_OFFERS_LIST_ORDER_FRAGMENT: {
                fragment = new ListOrderFragment();
                bundle.putInt("caller", AppConstants.SELECT_OFFERS_LIST_ORDER_FRAGMENT);
                tag = AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG;
                break;
            }
            case AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT: {
                fragment = new ListOrderFragment();
                bundle.putInt("caller", AppConstants.SELECT_DEMANDS_LIST_ORDER_FRAGMENT);
                tag = AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG;
                break;
            }
            case AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT: {
                fragment = new ListOrderFragment();
                bundle.putInt("caller", AppConstants.SELECT_ORDERS_LIST_ORDER_FRAGMENT);
                tag = AppConstants.SELECT_LIST_ORDER_FRAGMENT_TAG;
                break;
            }
            case AppConstants.CREATE_COMMUNITY_EVENT_FRAGMENT: {
                fragment = new CreateCommunityEventFragment();
                tag = AppConstants.CREATE_COMMUNITY_EVENT_FRAGMENT_TAG;
                break;
            }
            case AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT: {
                fragment = new SelectableListingsFragment();
                tag = AppConstants.MANAGE_EVENT_LISTINGS_FRAGMENT_TAG;
                break;
            }
        }
        fragment.setArguments(bundle);
        fragment.show(fm, tag);
    }

    public static void navigateToFragment(int target, AppCompatActivity context) {
        navigateToFragment(target, context, null);
    }
}
