package eco.darmas.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import eco.darmas.AppConstants;
import eco.darmas.AppData;

public final class DateUtils {

    private DateUtils() {
        // This utility class is not publicly instantiable
    }

    public static Date parseFromBackendLocale(String s) {
        DateFormat inputFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT, Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        Date date = new Date();
        try {
            date = inputFormat.parse(s);
        } catch (ParseException e) {
            AppLogger.e(e, null);
        }
        return date;
    }

    public static Date parseFromFrontendLocale(String s) {
        DateFormat inputFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT, AppData.INSTANCE.getSystemDefaultLocale());
        Date date = new Date();
        try {
            date = inputFormat.parse(s);
        } catch (ParseException e) {
            AppLogger.e(e, null);
        }
        return date;
    }

    public static String getTimeStamp() {
        DateFormat inputFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT, Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        return inputFormat.format(new Date());
    }

    public static Date parsedSelectedDate(String s) {
        DateFormat inputFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_DATE_FORMAT, AppData.INSTANCE.getSystemDefaultLocale());
        Date date = new Date();
        try {
            date = inputFormat.parse(s);
        } catch (ParseException e) {
            AppLogger.e(e, null);
        }
        return date;
    }

    public static Date parsedSelectedTime(String s) {
        DateFormat inputFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_TIME_FORMAT, AppData.INSTANCE.getSystemDefaultLocale());
        Date date = new Date();
        try {
            date = inputFormat.parse(s);
        } catch (ParseException e) {
            AppLogger.e(e, null);
        }
        return date;
    }

    public static String formatToFrontEndLocale(final Date date, String type) {
        DateFormat outputFormat = new SimpleDateFormat(type, AppData.INSTANCE.getSystemDefaultLocale());
        return outputFormat.format(date);
    }

    public static String formatToBackendLocale(final Date date, String type) {
        DateFormat outputFormat = new SimpleDateFormat(type, Locale.US);
        return outputFormat.format(date);
    }

    public static boolean isOlderThan3Months(Date givenDate) {
        Calendar calendar  = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        Date d1 = calendar.getTime();
        return givenDate.before(d1);
    }

    public static int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static int getCurrentMonth() {
        return Calendar.getInstance().get(Calendar.MONTH);
    }

    public static int getCurrentDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public static int getCurrentHour() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    public static int getCurrentMinute() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }

    public static Date now() {
        return Calendar.getInstance().getTime();
    }
}