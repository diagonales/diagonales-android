package eco.darmas.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eco.darmas.App;
import eco.darmas.AppConstants;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.base.activity.BaseActivity;
import eco.darmas.base.fragment.BaseFragment;
import es.dmoral.toasty.Toasty;

public final class CommonUtils {

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    //TODO: delete this use the below method
    public static boolean containsId(JSONArray jsonArray, int id) {
        return jsonArray.toString().contains("\"" + id + "\"");
    }

    public static boolean containsUser(JSONArray jsonArray, int userId) {
        return jsonArray.toString().contains("\"" + "id_user" + "\"" + ":" + "\"" + userId + "\"");
    }

    public static int getRandom(int minValue, int maxValue) {
        Random random = new Random();
        return random.nextInt(maxValue - minValue + 1) + minValue;
    }

    public static void dismissFragment(View rootView, String fragmentTag, boolean showProgress) {
        if (showProgress) {
            showProgress(rootView.getContext(), true);
        }
        Fragment fragment = ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager().findFragmentByTag(fragmentTag);
        KeyboardUtils.hideSoftInputFromFragment(rootView.getContext(), fragment);
        ((BaseFragment) ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager().findFragmentByTag(fragmentTag)).dismiss();
    }

    public static void dismissActivity(View rootView, boolean showProgress) {
        if (showProgress) {
            ((BaseActivity) rootView.getContext()).setResult(AppConstants.RESULT_PROGRESS);
        }
        KeyboardUtils.hideSoftInput((AppCompatActivity) rootView.getContext(), rootView.findFocus());
        ((BaseActivity) rootView.getContext()).finish();
    }

    public static void showProgress(Context context, boolean show) {
        ((BaseActivity) context).showProgress(show);
    }

    public static void showDialog(Context context, String dialogTitle, String dialogMessage) {
        if (dialogTitle != null || dialogMessage != null) {
            /*//final CustomDialog dialog = new CustomDialog(context);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setView(LayoutInflater.from(builder.getContext()).inflate(R.layout.custom_dialog,null));
            //final AlertDialog dialog = builder.create();
            //final CustomDialog dialog = builder.create();

            if (dialogTitle != null) {
                builder.setTitle(dialogTitle);
            }
            builder.setMessage(dialogMessage);
            builder.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();*/

            /*dialog.setButton(CustomDialog.BUTTON_POSITIVE, "Ok", (dialogInterface, i) -> dialog.dismiss());
            dialog.show();*/

            CustomDialog dialog = new CustomDialog(context);
            /*View customView = LayoutInflater.from(context).inflate(R.layout.custom_dialog,null);
            dialog.setContentView(customView);*/
            dialog.setTitle(dialogTitle);
            dialog.setMessage(dialogMessage);
            dialog.setButton(CustomDialog.BUTTON_POSITIVE, "Ok", (dialogInterface, i) -> dialogInterface.dismiss());
            dialog.show();
        }
    }

    public static String getRealImagePath(Bitmap photo) {
        return getRealPathFromURI(getImageUri(photo));
    }

    public static String getRealPathFromURI(Uri uri) {
        Cursor cursor = App.getInstance().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static Uri getImageUri(Bitmap photo) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(App.getInstance().getContentResolver(), photo, "Title", null);
        return Uri.parse(path);
    }

    public static void showNotImplementedToast() {
        showToast("Característica aún no implementada");
    }

    public static class StringWithTag {

        public String string;

        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }

    }

    public static void copyToClipboard(@NonNull Context context, @NonNull String uri) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getString(R.string.app_name), uri);
        clipboard.setPrimaryClip(clip);
        Toasty.success(App.getInstance(), "Copiado al portapapeles").show();
    }

    public static void launchEmail(@NonNull Context context, @NonNull String email) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(Intent.createChooser(intent, "Enviar email").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (ActivityNotFoundException e) {
            Toasty.warning(context, "No se encontró ninguna aplicación para redactar emails").show();
        }
    }

    public static void openInBrowser(@NonNull Context context, @NonNull String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri).addCategory(Intent.CATEGORY_BROWSABLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent = createActivityChooserIntent(context, intent, uri, null);
        if (intent != null) {
            context.startActivity(intent);
        } else {
            Toasty.warning(context, "No se encontró ningún buscador instalado", Toast.LENGTH_LONG).show();
        }
    }

    public static void shareText(@NonNull Context context, @NonNull String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(Intent.createChooser(shareIntent, "Compartir").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (ActivityNotFoundException e) {
            Toasty.warning(context, "No se encontró ninguna aplicación para compartir este contenido").show();
        }
    }

    private static Intent createActivityChooserIntent(Context context, Intent intent, Uri uri, List<String> ignorePackageList) {
        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> activities = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        final List<Intent> chooserIntents = new ArrayList<>();
        final String ourPackageName = context.getPackageName();

        Collections.sort(activities, new ResolveInfo.DisplayNameComparator(pm));

        for (ResolveInfo resInfo : activities) {
            ActivityInfo info = resInfo.activityInfo;
            if (!info.enabled || !info.exported) {
                continue;
            }
            if (info.packageName.equals(ourPackageName)) {
                continue;
            }
            if (ignorePackageList != null && ignorePackageList.contains(info.packageName)) {
                continue;
            }

            Intent targetIntent = new Intent(intent);
            targetIntent.setPackage(info.packageName);
            targetIntent.setDataAndType(uri, intent.getType());
            chooserIntents.add(targetIntent);
        }

        if (chooserIntents.isEmpty()) {
            return null;
        }

        final Intent lastIntent = chooserIntents.remove(chooserIntents.size() - 1);
        if (chooserIntents.isEmpty()) {
            // there was only one, no need to showImage the chooser
            return lastIntent;
        }

        Intent chooserIntent = Intent.createChooser(lastIntent, null);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, chooserIntents.toArray(new Intent[chooserIntents.size()]));
        return chooserIntent;
    }

    public static String getPagerFragmentTag(int viewPagerId, int index) {
        return "android:switcher:" + viewPagerId + ":" + index;
    }

    public static long getFirstInstallTime() {
        long time = 0;
        try {
            PackageInfo packageInfo = App.getInstance().getPackageManager().getPackageInfo(App.getInstance().getPackageName(), 0);
            time = packageInfo.firstInstallTime;
        } catch (PackageManager.NameNotFoundException e) {
            AppLogger.e(e, null);
        }
        return time;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static Boolean fileExist(Activity activity, String filePath) {
        String finalPath = activity.getFilesDir().toString() + File.separator + filePath;
        File file = new File(finalPath);
        return file.exists();
    }

    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isMarshmallowOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    final public static void showToast(String message) {
        Toasty.normal(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    final public static void showInfoToast(String message) {
        Toasty.info(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    final public static void showSuccessToast(String message) {
        Toasty.success(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    final public static void showErrorToast(String message) {
        Toasty.error(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    final public static void showWarningToast(String message) {
        Toasty.warning(App.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static boolean containsText(String t1, String t2) {
        t1 = StringUtils.stripAccents(t1.toLowerCase());
        t2 = StringUtils.stripAccents(t2.toLowerCase());
        return t1.contains(t2);
    }

    public static Spannable getColorTextSpan(String fulltext, String subtext, int color) {
        Spannable textToSpan = new SpannableString(fulltext);
        int i = fulltext.indexOf(subtext);
        textToSpan.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return textToSpan;
    }

    public static String toLowerCase(String text) {
        if (text == null) text = "";
        return text.toLowerCase(Locale.getDefault());
    }

    public static Spannable getOrderConceptTextSpan(String fulltext, String subtext, int color) {
        Spannable textToSpan = new SpannableString(fulltext);
        int i = fulltext.indexOf(subtext);
        textToSpan.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textToSpan.setSpan(new UnderlineSpan(), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return textToSpan;
    }

    public static Spannable getOrderConceptTextSpanNoListing(String fulltext, String subtext, int color) {
        Spannable textToSpan = new SpannableString(fulltext);
        int i = fulltext.indexOf(subtext);
        textToSpan.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return textToSpan;
    }

    public static Spannable getBoldItalicTextSpan(String fulltext, String subtext) {
        Spannable textToSpan = new SpannableString(fulltext);
        int i = fulltext.indexOf(subtext);
        textToSpan.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return textToSpan;
    }

    public static void adaptGridLayoutColumnCount(int columnCount, GridLayout gridLayout) {
        if (gridLayout.getColumnCount() != columnCount) {
            final int viewsCount = gridLayout.getChildCount();
            for (int i = 0; i < viewsCount; i++) {
                View view = gridLayout.getChildAt(i);
                //new GridLayout.LayoutParams created with Spec.UNSPECIFIED
                //which are package visible
                view.setLayoutParams(new GridLayout.LayoutParams());
            }
            gridLayout.setColumnCount(columnCount);
        }
    }
}