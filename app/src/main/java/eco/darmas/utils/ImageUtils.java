package eco.darmas.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import eco.darmas.App;
import eco.darmas.AppConfig;
import eco.darmas.AppConstants;
import eco.darmas.AppData;
import eco.darmas.R;
import eco.darmas.auxiliary.CustomDialog;
import eco.darmas.pojos.CommunityEvent;
import eco.darmas.pojos.Listing;
import eco.darmas.pojos.Post;
import eco.darmas.pojos.User;

public final class ImageUtils {

    private ImageUtils() {
        // This utility class is not publicly instantiable
    }

    public static Uri takePictureFromCamera(Activity activity, int requestCode, File imageFile) {
        File dir = new File(Environment.getExternalStorageDirectory(), "Pictures/Darmas/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            imageFile.createNewFile();
        } catch (IOException e) {
            AppLogger.e(e, null);
        }
        Uri tempImageUri = Uri.fromFile(imageFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempImageUri);
        activity.startActivityForResult(intent, requestCode);
        return tempImageUri;
    }

    public static void chooseImageFromFile(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", "image/*");
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (App.getInstance().getPackageManager().resolveActivity(sIntent, 0) != null) {
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            activity.startActivityForResult(chooserIntent, requestCode);
        } catch (ActivityNotFoundException ex) {
            CustomDialog errorDialog = new CustomDialog(App.getInstance());
            errorDialog.setTitle("Error!");
            errorDialog.setMessage("No se pudo encontrar un administrador de archivos adecuado, por favor contacta un administrador.");
            errorDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", (dialogInterface, i) -> errorDialog.dismiss());
            errorDialog.show();
        }
    }

    public static void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            CustomDialog errorDialog = new CustomDialog(App.getInstance());
            errorDialog.setMessage("Es necesario agregar permisos de escritura para utilizar esta caracteristica");
            errorDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", (dialogInterface, i) -> errorDialog.dismiss());
            errorDialog.show();
            AppLogger.w("Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            AppLogger.w("File not found: " + e.getMessage());
        } catch (IOException e) {
            AppLogger.w("Error accessing file: " + e.getMessage());
        }
        //uploadProfileImage(pictureFile);
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Android/data/" + App.getInstance().getPackageName() + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat(AppConstants.TIMESTAMP_DATABASE_DATETIME_FORMAT, AppData.INSTANCE.getSystemDefaultLocale()).format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private static void loadRoundedImage(int resource, ImageView imageView) {
        /*RequestOptions options = new RequestOptions();
        options.fitCenter();
        options.centerCrop();
        options.optionalCircleCrop();

        Glide.with(App.getInstance())
                .load(resource)
                .apply(options)
                .into(imageView);*/

        Transformation transformation = new RoundedTransformationBuilder()
                //.borderColor(Color.WHITE)
                //.borderWidthDp(3)
                .cornerRadiusDp(30)
                .oval(false)
                .build();

        Picasso.get()
                .load(resource)
                .fit()
                .centerCrop()
                .transform(transformation)
                .into(imageView);
    }

    private static void loadRoundedImage(String path, ImageView imageView, int errorResource) {
        /*RequestOptions options = new RequestOptions();
        options.fitCenter();
        options.centerCrop();
        options.optionalCircleCrop();
        options.error(errorResource);

        Glide.with(App.getInstance())
                .load(path)
                .apply(options)
                .into(imageView);*/

        Transformation transformation = new RoundedTransformationBuilder()
                //.borderColor(Color.WHITE)
                //.borderWidthDp(3)
                .cornerRadiusDp(30)
                .oval(false)
                .build();

        Picasso.get()
                .load(path)
                .error(errorResource)
                .fit()
                .centerCrop()
                .transform(transformation)
                .into(imageView);
    }

    private static void loadImage(int resource, ImageView imageView) {
        /*RequestOptions options = new RequestOptions();
        options.centerCrop();

        Glide.with(App.getInstance())
                .load(resource)
                .apply(options)
                .into(imageView);*/
        Picasso.get()
                .load(resource)
                //.resize(50, 50)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    private static void loadImage(String path, ImageView imageView, int errorResource) {
        /*RequestOptions options = new RequestOptions();
        options.fitCenter();
        options.centerCrop();
        options.error(errorResource);

        Glide.with(App.getInstance())
                .load(path)
                .apply(options)
                .into(imageView);*/
        Picasso.get()
                .load(path)
                .error(errorResource)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    public static void loadRoundedProfileImage(User user, ImageView imageView) {
        if (user != null && !user.getImage().isEmpty()) {
            loadRoundedImage(AppConfig.getUserImagesUrl() + user.getImage(), imageView, R.drawable.ic_account_image);
        } else {
            loadRoundedImage(R.drawable.ic_account_image, imageView);
        }
    }

    public static void loadRoundedProfileBackgroundImage(Context context, User user, ImageView imageView) {
        if (!user.getImage().isEmpty()) {
            imageView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            loadRoundedImage(AppConfig.getUserImagesUrl() + user.getImage(), imageView, R.drawable.ic_account_image);
            imageView.setAlpha(0.4f);
        } else {
            imageView.setBackgroundColor(context.getResources().getColor(R.color.secondaryLightColor));
            loadRoundedImage(R.color.secondaryLightColor, imageView);
            imageView.setAlpha(1f);
        }
    }

    public static void loadPostImage(Post post, ImageView imageView) {
        if (post != null && !post.getImage().isEmpty()) {
            //Picasso.get().load(AppConfig.getPostImagesUrl() + post.getIdPost() + ".jpg").fit().centerCrop().into(imageView);
            loadImage(AppConfig.getPostImagesUrl() + post.getImage(), imageView, R.drawable.ic_photo_camera_white);
        } else {
            loadImage(R.drawable.ic_photo_camera_white, imageView);
        }
    }

    public static void loadListingImage(Listing listing, ImageView imageView) {
        if (listing != null && !listing.getImage().isEmpty()) {
            loadImage(AppConfig.getListingImagesUrl() + listing.getImage(), imageView, R.drawable.ic_photo_camera_white);
        } else {
            loadImage(R.drawable.ic_photo_camera_white, imageView);
        }
    }

    public static void loadEventImage(CommunityEvent communityEvent, ImageView imageView) {
        if (communityEvent != null && !communityEvent.getImage().isEmpty()) {
            loadImage(AppConfig.getEventImagesUrl() + communityEvent.getImage(), imageView, R.drawable.ic_photo_camera_white);
        } else {
            loadImage(R.color.secondaryLightColor, imageView);
        }
    }

    public static void loadTransactionBackgroundImage(Object content, ImageView imageView) {
        if (content != null && content instanceof Listing) {
            Listing listing = (Listing) content;
            if (!listing.getImage().isEmpty()) {
                loadImage(AppConfig.getListingImagesUrl() + listing.getImage(), imageView, R.drawable.ic_photo_camera_white);
            } else {
                loadRoundedImage(R.color.secondaryLightColor, imageView);
            }
        } else if (content != null && content instanceof CommunityEvent) {
            CommunityEvent communityEvent = (CommunityEvent) content;
            if (!communityEvent.getImage().isEmpty()) {
                loadImage(AppConfig.getEventImagesUrl() + communityEvent.getImage(), imageView, R.drawable.ic_photo_camera_white);
            } else {
                loadRoundedImage(R.color.secondaryLightColor, imageView);
            }
        } else {
            loadRoundedImage(R.color.secondaryLightColor, imageView);
        }
    }

    public static void loadTransferImage(ImageView imageView) {
        loadImage(R.drawable.ic_logo, imageView);
    }

    public static void loadProfileBackgroundImage(Context context, User contact, ImageView imageView) {
        if (!contact.getImage().isEmpty()) {
            imageView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            loadImage(AppConfig.getUserImagesUrl() + contact.getImage(), imageView, R.drawable.ic_account_image);
            imageView.setAlpha(0.4f);
        } else {
            imageView.setBackgroundColor(context.getResources().getColor(R.color.secondaryDarkColor));
            loadImage(R.color.secondaryDarkColor, imageView);
            imageView.setAlpha(1f);
        }
    }
}